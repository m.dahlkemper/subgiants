void get_resol(float *lam, int nspec, int **nppo, int *nord, float *stepvel); 

void get_lines(float *lam, int nspec, int *nppo, int nord, double *wave, int *tobeused, 
               int nlines, int **index, int **pix, int **used, int *ntot); 

void cont_pol(float *lam, float *specv, float *specn, int *nppo, int nord, int pol); 

void select_lines(float *lam, int nspec, double *wave, int *index, int *pixc, 
                  int *used, int ntot, int *nppo, int nord, float liml, float limu, 
                  int *fpix, int *lpix, float *vel, int nvel, int *first, int *last); 

void get_limits(float *vel, int nvel, float *lam, float *spec, int nspec,
                double *wave, float *prof, int *index, int *fpix, int *lpix,
                int *used, int *first, int *last, int ntot, float *liml, float *limu,
                float threshold); 

void get_weights(float *I, float *err, int nspec, double *wave, float *code, float *prof, 
                 float *lande, int *used, float *wwi, float *wwv, float *sn, 
                 int *index, int *fpix, int *lpix, int ntot, int choice, float equ, float eqi);  

void extract_spec(float *lam, float *spec, float *specv, float *specn, float *err, int nspec,
                  int *used, int *fpix, int *lpix, int nsel, int pol, float **ll, float **ii, 
                  float **vv, float **nn, float **ss, int *ntot); 

void ls_dcv(float *vel, int nvel, float *ll, float *ii, float *vv, float *nn,
            float *ss, int ntot, int *fpix, int *lpix, int *used, 
            float *wwi, float *wwv, double *wave, int *index, int *first, int *last,
            int nsel, int pol, float *mean, float *meanv, float *meann, float *mean_errI, 
            float *mean_errV, float *mean_errN); 

void filter(float *meanv, float *meann, float *meanF, int *first, int *last, int *used, int nsel,
            int nvel, float filt); 

void statistics(float *vel, float *mean, float *meanv, float *meann, float *errv, float *errn, 
                int nvel, int pol, float *wwv, float *wwi, float *sn, 
                double *wave, float *lande, float *prof, float *exc, int *used, 
                int *index, int *first, int *last, int nlines); 

void show(float *vel, float *mean, float *meanv, float *meann, float *meanF, int nvel, int *first,
          int *last, int *used, int nsel, int pol); 

void set_trace(int ok_to_plot, int verbose, int test); 

void conv(float *lam, int nspec, int *used, int *index, int *fpix, int *lpix, int nsel,
          double *wave, int pol, float *Is, float *Vs, float *Ns, float *vel,
          float *meanI, float *meanV, float *meanN, float *wi, float *wv, int nvel); 

