/*
  This package of subroutines handles all interfacing to files.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include "structures.h"
#define  TO_RAD(x)  ((x)*1.7453292252e-2)
#define  TO_DEG(x)  ((x)*57.2295779551)


static void get_spec_dims(spec_info_t spec_info[], int nobs, int *npns, int datsw[]);
static void print_array(FILE *fp, float data[], int n);
static int read_field(char field[], char string[], char header[], int nline);
static int write_field(char field[], char string[], char header[], int nline); 
static int get_line(char field[], char header[], int nline);
static void add_line(char field[], char string[], char **header, fits_info_t *info); 
static void del_line(int j, char **header, fits_info_t *info); 
static void swap(char *buffer, int size, int ntot); 
static float *vector(int nl, int nh);
static void *svector(int length, int size);
static double *dvector(int nl, int nh);
static int *ivector(int nl, int nh);
static float **matrix(int nrl, int nrh, int ncl, int nch); 
static void free_vector(float *v, int nl, int nh);
static void free_svector(void *v);
static void free_dvector(double *v, int nl, int nh);
static void free_ivector(int *v, int nl, int nh);



int read_fits(float **image, int *naxis, int **npt, float **first, float **step, 
              fits_info_t *info, char **header, int getname, char filename[], int bswap)
{
    FILE   *fp; 
    char   buffer[2880], string[21]; 
    int    j, ntot, size; 
    float  *tmp; 
    double bscale, bzero; 
int k; 

    if (getname) {
        printf("Name of file to read FITS data from : ");
        scanf("%s", filename);
    }
    
    fp = fopen(filename,"r");
    if (fp == NULL) return 1;

    /* Reads header */ 
    info->nline = 0; 
    info->moon = 0; 
    do { 
        fread(buffer, 1, 2880, fp); 
        info->nline += 36; 
    } while ((j = get_line("END     ", buffer, 36)) < 0); 
    info->nlused = info->nline-36+j+1; 
    close(fp); 

    *header = (char *) svector(info->nline * 80 + 1, (int) sizeof(char)); 
    fp = fopen(filename,"r");
    fread(*header, 1, info->nline * 80, fp); 
    (*header)[info->nline * 80] = '\0'; 

    /* Reads fields */ 
    if (!read_field("BITPIX  ", string, *header, info->nline)) 
        sscanf(string, "%d", &(info->bitpix)); 
    else {
        printf("No BITPIX field in header\n"); 
        free_svector((void *) *header); exit(1); 
    }
    /*    if (!read_field("NAXIS   ", string, *header, info->nline)) 
      sscanf(string, "%d", naxis) 
    else {
        printf("No NAXIS field in header\n"); 
        free_svector((void *) *header); exit(1); 
	}*/
//    *naxis = 2;
    *npt    = ivector(0, *naxis-1); 
    *first  =  vector(0, *naxis-1); 
    *step   =  vector(0, *naxis-1); 
    for (j = 0, ntot = 1; j < *naxis; j++) { 
        if      (j <   9) sprintf(string, "NAXIS%1d  ", j+1); 
        else if (j <  99) sprintf(string, "NAXIS%2d ",  j+1); 
        else if (j < 999) sprintf(string, "NAXIS%3d",   j+1); 
        if (!read_field(string, string, *header, info->nline))
            sscanf(string, "%d", (*npt)+j); 
        else {
            printf("No NAXIS%d field in header\n", j); 
            free_svector((void *) *header); 
            free_ivector(*npt, 0, *naxis-1); 
            exit(1); 
        }

        if      (j <   9) sprintf(string, "CRVAL%1d  ", j+1); 
        else if (j <  99) sprintf(string, "CRVAL%2d ",  j+1); 
        else if (j < 999) sprintf(string, "CRVAL%3d",   j+1); 
        if (!read_field(string, string, *header, info->nline))
            sscanf(string, "%f", (*first)+j); 
        else 
            (*first)[j] = 0.0; 

        if      (j <   9) sprintf(string, "CDELT%1d  ", j+1); 
        else if (j <  99) sprintf(string, "CDELT%2d ",  j+1); 
        else if (j < 999) sprintf(string, "CDELT%3d",   j+1); 
        if (!read_field(string, string, *header, info->nline))
            sscanf(string, "%f", (*step)+j); 
        else 
            (*step)[j] = 1.0; 

        ntot *= (*npt)[j]; 
    }
    if (!read_field("BSCALE  ", string, *header, info->nline)) 
        sscanf(string, "%20lf", &bscale); 
    else
        bscale = 1.0; 
    if (!read_field("BZERO   ", string, *header, info->nline)) 
        sscanf(string, "%20lf", &bzero); 
    else
        bzero = 0.0; 
    read_field("COMMENT2", info->object, *header, info->nline);  
 
    /* Reads image */ 
    *image = vector(0, ntot-1); tmp = vector(0, ntot-1); 
    size = info->bitpix/8; if (size < 0) size *= -1; 
    fread(tmp, size, ntot, fp); 
    if (bswap) swap((char *) tmp, size, ntot); 
    if      (info->bitpix ==  16) 
        for (j = 0; j < ntot; j++) { 
/*
if (*naxis == 3)
            (*image)[j] = (double) (*((short *) tmp+j)+(j%2==0)*32767.0) * bscale + bzero;
else 
*/
            (*image)[j] = (double) *((short *) tmp+j) * bscale + bzero; 
            if ((*image)[j] < 0.0) (*image)[j] += 65536.0; 
        } 
    else if (info->bitpix ==  32) 
        for (j = 0; j < ntot; j++) 
            (*image)[j] = (double) *((int   *) tmp+j) * bscale + bzero; 
    else if (info->bitpix == -32) 
        for (j = 0; j < ntot; j++) 
            (*image)[j] = (double)             tmp[j] * bscale + bzero ; 

/*
k=0; 
do {
for (j = 0; j < (*npt)[0]; j++) { 
printf("%8.1f", (*image)[j+k]); 
if (j%10==9) printf("\n"); 
}
k+=(*npt)[0]; 
printf("\n"); 
} while (k < ntot/100); 
*/

    free_vector(tmp, 0, ntot-1); 
    fclose(fp); 
    return 0; 
}


void save_fits(float *image, int naxis, int *npt, float *first, float *step, 
               fits_info_t info, char *header, int getname, char filename[], int bswap)
{
    FILE  *fp; 
    char  string[21], field[21]; 
    int   j, k, ntot, size; 
    float *tmp; 
    double bscale, bzero, MAX, max, min; 

    if (getname) {
        printf("Name of file to save FITS data to : ");
        scanf("%s", filename);
    }
    
    fp = fopen(filename,"w");

    /* Rescales data according to bitpix */ 
    for (j = 0, ntot = 1; j < naxis; j++) ntot *= npt[j]; 
    tmp = vector(0, ntot-1); 
    if (info.bitpix > 0) {
        max = -1.0e30; min = 1.0e30; 
        for (j = 0; j < ntot; j++) {
            if (image[j] > max) max = image[j]; 
            if (image[j] < min) min = image[j]; 
        }
        if (info.bitpix == 16) { 
            size = 2; MAX = 65534.0; 
            if (max-min > MAX) {
                bscale = (max-min) / MAX; 
                bzero = (max+min) * 0.5; 
            } else {
                bscale = 1.0; 
                if ((max > 0.5*MAX) || (min < -0.5*MAX)) 
                    bzero = (max+min) * 0.5; 
                else 
                    bzero = 0.0; 
            }
            for (j = 0; j < ntot; j++) 
                *((short *) tmp+j) = (float) (image[j]-bzero)/bscale; 
       } else if (info.bitpix == 32) { 
            size = 4; MAX = 4294967294.0; 
            if (max-min > MAX) {
                bscale = (max-min) / MAX; 
                bzero = (max+min) * 0.5; 
            } else {
                bscale = 1.0; 
                if ((max > MAX) || (min < -MAX)) bzero = (max+min) * 0.5; 
                else                             bzero = 0.0; 
            }
            for (j = 0; j < ntot; j++) 
                *((int *)   tmp+j) = (float) (image[j]-bzero)/bscale; 
        }
    } else { 
        size = 4; 
        bscale = 1.0; 
        bzero  = 0.0; 
        for (j = 0; j < ntot; j++) tmp[j] = image[j]; 
    }

    /* Corrects header */ 
    sprintf(string, "%20d", info.bitpix); 
    if (write_field("BITPIX  ", string, header, info.nline)) 
        add_line("BITPIX  ", string, &header, &info); 
    sprintf(string, "%20d", naxis); 
    if (write_field("NAXIS   ", string, header, info.nline)) 
        add_line("NAXIS   ", string, &header, &info); 
    for (j = 0; j < 999; j++) { 
        sprintf(string, "%20d", npt[j]); 
        if      (j <   9) sprintf(field, "NAXIS%1d  ", j+1); 
        else if (j <  99) sprintf(field, "NAXIS%2d ",  j+1); 
        else if (j < 999) sprintf(field, "NAXIS%3d",   j+1); 
        if (j < naxis) {
            if (write_field(field, string, header, info.nline)) 
                add_line(field, string, &header, &info); 
        } else {
            if ((k = get_line(field, header, info.nline)) >= 0) 
                del_line(k, &header, &info); 
            else 
                break; 
        }

        sprintf(string, "%20e", first[j]); 
        if      (j <   9) sprintf(field, "CRVAL%1d  ", j+1); 
        else if (j <  99) sprintf(field, "CRVAL%2d ",  j+1); 
        else if (j < 999) sprintf(field, "CRVAL%3d",   j+1); 
        if (j < naxis) {
            if (write_field(field, string, header, info.nline)) 
                add_line(field, string, &header, &info); 
        } else {
            if ((k = get_line(field, header, info.nline)) >= 0) 
                del_line(k, &header, &info); 
            else 
                break; 
        }

        sprintf(string, "%20e", step[j]); 
        if      (j <   9) sprintf(field, "CDELT%1d  ", j+1); 
        else if (j <  99) sprintf(field, "CDELT%2d ",  j+1); 
        else if (j < 999) sprintf(field, "CDELT%3d",   j+1); 
        if (j < naxis) {
            if (write_field(field, string, header, info.nline)) 
                add_line(field, string, &header, &info); 
        } else {
            if ((k = get_line(field, header, info.nline)) >= 0) 
                del_line(k, &header, &info); 
            else 
                break; 
        }

    }
    sprintf(string, "%20.10e", bscale); 
    if (write_field("BSCALE  ", string, header, info.nline))  
        add_line("BSCALE  ", string, &header, &info); 
    sprintf(string, "%20.10e", bzero); 
    if (write_field("BZERO   ", string, header, info.nline)) 
        add_line("BZERO   ", string, &header, &info); 
    sprintf(string, "%20s", info.object); string[20] = '\0'; 
    write_field("OBJECT  ", string, header, info.nline); 

    /* Saves header and data */ 
    printf("dans file.c %d %s \n", info.nlused, filename);
    fwrite(header, 1, 80 * info.nline, fp); 
    printf("dans file.c\n");
    if (bswap) swap((char *) tmp, size, ntot); 
    fwrite(tmp, size, ntot, fp); 
        
    free_vector(tmp, 0, ntot-1); 
    fclose(fp); 
}


int read_ascfile(int *nspec, int *nfile, float **lam, float **spec, int getname, 
                 char filename[], char comment[])
{
    FILE *fp;
    int   j, k;
    float lam0; 
    char *line; 

    if (getname == 1) {
        printf("Name of file to read Stokes I profiles from : ");
        scanf("%s", filename);
    }
    
    fp = fopen(filename,"r");
    if (fp == NULL)
        return(1);

    j = 0;
    while ((comment[j++] = getc(fp)) != '\n');
    comment[--j] = '\0';

    if ((comment[0] == '*') && (comment[1] == '*') && (comment[2] == '*')) { 
        line = (char *) svector(j-2, (int) sizeof(char)); 
        sprintf(line, "%s", comment+3); sprintf(comment, "%s", line); 
        free_svector((void *) line); 
        fscanf(fp,"%d %d",nspec,nfile);
        if (*nfile < 1) { 
            printf("Only one column available\n"); 
            exit(1); 
        } else { 
            *lam  = vector(0,*nspec-1);
            *spec = vector(0,(*nspec)*(*nfile)-1);
            for (j = 0; j < *nspec; j++) { 
                fscanf(fp,"%f", (*lam)+j);
                for (k = 0; k < *nfile; k++) fscanf(fp,"%f", (*spec)+j+k*(*nspec));
            } 
        } 
    } else { 
        *nfile = 1; 
        fscanf(fp,"%d %f",nspec,&lam0);
        *lam  = vector(0,*nspec-1);
        *spec = vector(0,*nspec-1);
        for (j = 0; j < *nspec; j++) { 
            fscanf(fp,"%f %f", &((*lam)[j]), &((*spec)[j]));
            while (getc(fp) != '\n');
        } 
    } 
    fclose(fp);
    return(0);
}


void save_ascfile(int nspec, int nfile, float lam[], float spec[], int getname, 
                  char filename[], char comment[])
{
    FILE *fp;
    int  j, k;

    if (getname == 1) {
        printf("Name of file to save Stokes profiles to : ");
        scanf("%s", filename);
        if ((filename[0] == '.') && (filename[1] == '\0')) return; 
    }
    
    fp = fopen(filename,"w");

    fprintf(fp,"***%s\n",comment);
    fprintf(fp," %d %d\n",nspec,nfile);

    for (j = 0; j < nspec; j++) {
        if (lam[j] < 10000.0) fprintf(fp," %9.4f", lam[j]);
        else                  fprintf(fp," %9.2f", lam[j]);
        for (k = 0; k < nfile; k++) fprintf(fp," %11.4e", spec[j+k*nspec]); 
        fprintf(fp, "\n"); 
    } 

    fclose(fp);
}


int read_dfgdata(int *nspec, float **I, int getname, char filename[], 
                 char comment[])
{
    FILE *fp;
    int  j;

    if (getname == 1) {
        printf("Name of file to read dfg data from : ");
        scanf("%s", filename);
    }
    
    fp = fopen(filename,"r");
    if (fp == NULL)
        return(1);

    j = 0;
    while ((comment[j++] = getc(fp)) != '\n');
    comment[--j] = '\0';

    for (*nspec = 0; feof(fp) == 0; (*nspec)++) fscanf(fp,"%*f"); 
    fclose(fp); (*nspec)--; 

    *I   = vector(0,*nspec-1);

    fp = fopen(filename,"r");

    while (getc(fp) != '\n');
    for (j = 0; j < *nspec; j++) 
        fscanf(fp,"%f", &((*I)[j]));

    fclose(fp);
    return(0);
}


int read_dfgspec(int *nspec, float *lam0, float **lam, float **I, int getname, 
                 char filename[], char comment[])
{
    FILE *fp;
    int  j;

    if (getname == 1) {
        printf("Name of file to read Stokes I profiles from : ");
        scanf("%s", filename);
    }
    
    fp = fopen(filename,"r");
    if (fp == NULL)
        return(1);

    j = 0;
    while ((comment[j++] = getc(fp)) != '\n');
    comment[--j] = '\0';

    fscanf(fp,"%d %f",nspec,lam0);
    *lam = vector(0,*nspec-1);
    *I   = vector(0,*nspec-1);

    for (j = 0; j < *nspec; j++) { 
        fscanf(fp,"%f %f", &((*lam)[j]), &((*I)[j]));
        while (getc(fp) != '\n');
    } 

    fclose(fp);
    return(0);
}


void save_dfgspec(int nspec, float lam0, float lam[], float I[], int getname, 
                  char filename[], char comment[])
{
    FILE *fp;
    int  j;

    if (getname == 1) {
        printf("Name of file to save Stokes profiles to : ");
        scanf("%s", filename);
        if ((filename[0] == '.') && (filename[1] == '\0')) return; 
    }
    
    fp = fopen(filename,"w");

    fprintf(fp,"%s\n",comment);
    fprintf(fp," %d %.3f\n",nspec,lam0);

    for (j = 0; j < nspec; j++) 
        if (lam[j] < 10000.0) 
            fprintf(fp," %9.4f %9f\n", lam[j], I[j]);
        else 
            fprintf(fp," %9.2f %9f\n", lam[j], I[j]);

    fclose(fp);
}


int read_dfg2spec(int *nspec, float *lam0, float **lam, float **I1, float **I2, int getname, 
                  char filename[], char comment[])
{
    FILE *fp;
    int  j;

    if (getname == 1) {
        printf("Name of file to read Stokes I profiles from : ");
        scanf("%s", filename);
    }
    
    fp = fopen(filename,"r");
    if (fp == NULL)
        return(1);

    j = 0;
    while ((comment[j++] = getc(fp)) != '\n');
    comment[j] = '\0';

    fscanf(fp,"%d %f",nspec,lam0);
    *lam = vector(0,*nspec-1);
    *I1  = vector(0,*nspec-1);
    *I2  = vector(0,*nspec-1);

    for (j = 0; j < *nspec; j++) 
        fscanf(fp,"%f %f %f", &((*lam)[j]), &((*I1)[j]), &((*I2)[j]));

    fclose(fp);
    return(0);
}


void save_dfg2spec(int nspec, float lam0, float lam[], float I1[], float I2[], int getname, 
                   char filename[], char comment[])
{
    FILE *fp;
    int  j;

    if (getname == 1) {
        printf("Name of file to save Stokes profiles to : ");
        scanf("%s", filename);
        if (filename[0] == '.') return; 
    }
    
    fp = fopen(filename,"w");

    fprintf(fp,"%s\n",comment);
    fprintf(fp," %d %.3f\n",nspec,lam0);

    for (j = 0; j < nspec; j++) 
        if (lam[j] < 10000.0) 
            fprintf(fp," %9.4f %9.6f %9.6f\n", lam[j], I1[j], I2[j]);
        else 
            fprintf(fp," %9.2f %9.6f %9.6f\n", lam[j], I1[j], I2[j]);

    fclose(fp);
}


void save_bright_old(int ngrid, float Cq[], float Cm[], float Br[], float Bp[], float Bt[],
                     char filename[], int getname, char comment[],
                     float inc, float ve, int magnetic)
{
    int  j;
    FILE *fp;

    if (getname == 1) {
        printf("Name of file to save brightness data to : ");
        scanf("%s", filename);
    }
    
    fp = fopen(filename,"w");

    fprintf(fp,"%s\n", comment);
    fprintf(fp,"%d %f %g %1d\n", ngrid, inc, ve, magnetic);
    for (j = 0; j < ngrid; j++) {
        fprintf(fp,"%14.5e  ", Cq[j]);
        if (j%8 == 7) fprintf(fp,"\n");
    }
    if (j%8 != 0) fprintf(fp,"\n");
    if(magnetic == 1) {
        fprintf(fp,"\n");
        for (j = 0; j < ngrid; j++) {
            fprintf(fp,"%14.5e  ", Cm[j]);
            if (j%8 == 7) fprintf(fp,"\n");
        }
        if (j%8 != 0) fprintf(fp,"\n");
        fprintf(fp,"\n");
        for (j = 0; j < ngrid; j++) {
            fprintf(fp,"%14.5e  ", Br[j]);
            if (j%8 == 7) fprintf(fp,"\n");
        }
        if (j%8 != 0) fprintf(fp,"\n");
        fprintf(fp,"\n");
        for (j = 0; j < ngrid; j++) {
            fprintf(fp,"%14.5e  ", Bp[j]);
            if (j%8 == 7) fprintf(fp,"\n");
        }
        if (j%8 != 0) fprintf(fp,"\n");
        fprintf(fp,"\n");
        for (j = 0; j < ngrid; j++) {
            fprintf(fp,"%14.5e  ", Bt[j]);
            if (j%8 == 7) fprintf(fp,"\n");
        }
        if (j%8 != 0) fprintf(fp,"\n");
    }
    fclose(fp);
}


void save_spec_old(int nphase, float phases[], int datsw[], int photometric,
                   float I[], float Q[], float U[], float V[],
                   int nspec[], float laml[], float lamu[], float SN[], float FWHM[],
                   float lamref, char filename[], int getname, char comment[])
{
    int  j,k,n;
    FILE *fp;

    if (getname == 1) {
        printf("Name of file to save spectral data to : ");
        scanf("%s",filename);
    }
    
    fp = fopen(filename,"w");

    fprintf(fp,"%s\n",comment);
    fprintf(fp,"%d  %1d %1d %1d %1d  %1d  %e\n\n",nphase,datsw[0],datsw[1],
                                         datsw[2],datsw[3],photometric,lamref);

    n = 0;
    for (j = 0; j < nphase; j++) {
        fprintf(fp,"%f  %d  %e  %e  %e  %e\n\n",phases[j],nspec[j],
                                           laml[j],lamu[j],SN[j],FWHM[j]);
        if (datsw[0] == 1) {
            for (k = 0; k < nspec[j]; k++) {
                fprintf(fp,"%14.5e  ", I[n+k]);
                if (k%8 == 7) fprintf(fp,"\n");
            }
            if (k%8 != 0) fprintf(fp,"\n");
            fprintf(fp,"\n");
        }
        if (datsw[1] == 1) {
            for (k = 0; k < nspec[j]; k++) {
                fprintf(fp,"%14.5e  ", Q[n+k]);
                if (k%8 == 7) fprintf(fp,"\n");
            }
            if (k%8 != 0) fprintf(fp,"\n");
            fprintf(fp,"\n");
        }
        if (datsw[2] == 1) {
            for (k = 0; k < nspec[j]; k++) {
                fprintf(fp,"%14.5e  ", U[n+k]);
                if (k%8 == 7) fprintf(fp,"\n");
            }
            if (k%8 != 0) fprintf(fp,"\n");
            fprintf(fp,"\n");
        }
        if (datsw[3] == 1) {
            for (k = 0; k < nspec[j]; k++) {
                fprintf(fp,"%14.5e  ", V[n+k]);
                if (k%8 == 7) fprintf(fp,"\n");
            }
            if (k%8 != 0) fprintf(fp,"\n");
            fprintf(fp,"\n");
        }
        n += nspec[j];
    }
    fclose(fp);
}


int read_bright_old(int *ngrid, float **Cq, float **Cm, float **Br,
                    float **Bp, float **Bt, char filename[], int getname,
                    char comment[], float *inc, float *ve, int *magnetic)
{
    int  j;
    FILE *fp;

    if (getname == 1) {
        printf("Name of file to read brightness data from : ");
        scanf("%s", filename);
    }
    
    fp = fopen(filename,"r");
    if (fp == NULL)
        return(1);

    j = 0;
    while ((comment[j++] = getc(fp)) != '\n');
    comment[--j] = '\0';

    fscanf(fp,"%d %f %f %d", ngrid, inc, ve, magnetic);
    *Cq = vector(0,*ngrid-1);
    for (j = 0; j < *ngrid; j++) {
        fscanf(fp,"%f", &((*Cq)[j]));
    }
    if(*magnetic == 1) {
        *Cm = vector(0,*ngrid-1);
        *Br = vector(0,*ngrid-1);
        *Bp = vector(0,*ngrid-1);
        *Bt = vector(0,*ngrid-1);
        for (j = 0; j < *ngrid; j++) {
            fscanf(fp,"%f", &((*Cm)[j]));
        }
        for (j = 0; j < *ngrid; j++) {
            fscanf(fp,"%f", &((*Br)[j]));
        }
        for (j = 0; j < *ngrid; j++) {
            fscanf(fp,"%f", &((*Bp)[j]));
        }
        for (j = 0; j < *ngrid; j++) {
            fscanf(fp,"%f", &((*Bt)[j]));
        }
    }
    fclose(fp); 
    return(0);
}


int read_spec_old(int *nphase, float **phases, int datsw[], int *photometric,
                  float **I, float **Q, float **U, float **V,
                  int **nspec, float **laml, float **lamu, float **SN, float **FWHM,
                  float *lamref, char filename[], int getname, char comment[])
{
    FILE *fp;
    int  j,k,n,npns;

    if (getname == 1) {
        printf("Name of file to read spectral data from : ");
        scanf("%s", filename);
    }
    
    fp = fopen(filename,"r");
    if (fp == NULL)
        return(1);

    j = 0;
    while ((comment[j++] = getc(fp)) != '\n');
    comment[--j] = '\0';

    fscanf(fp,"%d %d %d %d %d %d %f",nphase,&datsw[0],&datsw[1],
                                      &datsw[2],&datsw[3],photometric,lamref);
    *phases = vector(0,*nphase-1);
    *nspec  = ivector(0,*nphase-1);
    *laml   = vector(0,*nphase-1);
    *lamu   = vector(0,*nphase-1);
    *SN     = vector(0,*nphase-1);
    *FWHM   = vector(0,*nphase-1);

    npns = 0;
    for (j = 0; j < *nphase; j++) {
        fscanf(fp,"%f %d %f %f %f %f",&((*phases)[j]),&((*nspec)[j]),
                  &((*laml)[j]),&((*lamu)[j]),&((*SN)[j]),&((*FWHM)[j]));
        if (datsw[0] == 1)
            for (k = 0; k < (*nspec)[j]; k++)
                fscanf(fp,"%*f");
        if (datsw[1] == 1)
            for (k = 0; k < (*nspec)[j]; k++)
                fscanf(fp,"%*f");
        if (datsw[2] == 1)
            for (k = 0; k < (*nspec)[j]; k++)
                fscanf(fp,"%*f");
        if (datsw[3] == 1)
            for (k = 0; k < (*nspec)[j]; k++)
                fscanf(fp,"%*f");
        npns += (*nspec)[j];
    }
    fclose(fp);
    if (datsw[0] == 1)  *I = vector(0,npns-1);
    if (datsw[1] == 1)  *Q = vector(0,npns-1);
    if (datsw[2] == 1)  *U = vector(0,npns-1);
    if (datsw[3] == 1)  *V = vector(0,npns-1);

/*  Now do second scan of the file and read the data  */
    fp = fopen(filename,"r");
    while (getc(fp) != '\n');
    fscanf(fp,"%*d %*d %*d %*d %*d %*d %*f");
    n = 0;
    for (j = 0; j < *nphase; j++) {
        fscanf(fp,"%*f %*d %*f %*f %*f %*f");
        if (datsw[0] == 1)
            for (k = 0; k < (*nspec)[j]; k++)
                fscanf(fp,"%f",&((*I)[n+k]));
        if (datsw[1] == 1)
            for (k = 0; k < (*nspec)[j]; k++)
                fscanf(fp,"%f",&((*Q)[n+k]));
        if (datsw[2] == 1)
            for (k = 0; k < (*nspec)[j]; k++)
                fscanf(fp,"%f",&((*U)[n+k]));
        if (datsw[3] == 1)
            for (k = 0; k < (*nspec)[j]; k++)
                fscanf(fp,"%f",&((*V)[n+k]));
        n += (*nspec)[j];
    }
    fclose(fp);
    return(0);
}


int read_photometry(float **phases, double **jd, float **uu, float **bb, 
                    float **vv, 
                    float **rr, float **ii, int photsw[], float errs[], 
                    int *nphot, int getname, char filename[], char comment[]) 
{
    FILE *fp;
    int  j,k,n,npns;

    if (getname == 1) {
        printf("Name of file to read photometry from : ");
        scanf("%s", filename);
    }
    
    fp = fopen(filename,"r");
    if (fp == NULL)
        return(1);

    j = 0;
    while ((comment[j++] = getc(fp)) != '\n');
    comment[--j] = '\0';

    fscanf(fp,"%d   %d %d %d %d %d   %f %f %f %f %f",nphot,&photsw[0],
           &photsw[1],&photsw[2],&photsw[3],&photsw[4],&errs[0],&errs[1],
           &errs[2],&errs[3],&errs[4]); 
    *phases = vector(0,*nphot-1);
    *jd     = dvector(0,*nphot-1);
    *uu     = vector(0,*nphot-1);
    *bb     = vector(0,*nphot-1);
    *vv     = vector(0,*nphot-1);
    *rr     = vector(0,*nphot-1);
    *ii     = vector(0,*nphot-1);

    for (j = 0; j < *nphot; j++) {
        fscanf(fp,"%f %lf",&((*phases)[j]),&((*jd)[j])); 
        if (photsw[0] == 1) fscanf(fp,"%f", &((*uu)[j])); 
        if (photsw[1] == 1) fscanf(fp,"%f", &((*bb)[j])); 
        if (photsw[2] == 1) fscanf(fp,"%f", &((*vv)[j])); 
        if (photsw[3] == 1) fscanf(fp,"%f", &((*rr)[j])); 
        if (photsw[4] == 1) fscanf(fp,"%f", &((*ii)[j])); 
    } 
    fclose(fp);
    return(0);
}


void save_photometry(float *phases, double *jd, float *uu, float *bb, 
                     float *vv, 
                     float *rr, float *ii, int photsw[], float errs[], 
                     int nphot, int getname, char filename[], char comment[]) 
{
    FILE *fp;
    int  j,k,n,npns;

    if (getname == 1) {
        printf("Name of file to save photometry to : ");
        scanf("%s", filename);
    }
    
    fp = fopen(filename,"w");

    fprintf(fp,"%s\n", comment);
    fprintf(fp,"%d   %d %d %d %d %d   %.3f %.3f %.3f %.3f %.3f\n",nphot,
            photsw[0],photsw[1],photsw[2],photsw[3],photsw[4],
            errs[0],errs[1],errs[2],errs[3],errs[4]); 

    for (j = 0; j < nphot; j++) {
        fprintf(fp,"%5.3f  %11.3lf ",phases[j],jd[j]); 
        if (photsw[0] == 1) fprintf(fp,"%7.3f ", uu[j]); 
        if (photsw[1] == 1) fprintf(fp,"%7.3f ", bb[j]); 
        if (photsw[2] == 1) fprintf(fp,"%7.3f ", vv[j]); 
        if (photsw[3] == 1) fprintf(fp,"%7.3f ", rr[j]); 
        if (photsw[4] == 1) fprintf(fp,"%7.3f ", ii[j]); 
        fprintf(fp,"\n"); 
    } 
    fclose(fp);
}

/*

This package of routines handles all interfacing between the
data arrays and disk files.

The image files are written out in an ASCII format so they can be easily
transferred between different machines and architectures. The only information
written out about the grid itself is the number of grid points, |ngrid|, the
inclination of the stellar rotation axis, |inc|, and the equatorial rotation
speed, |ve|. This information is sufficient to generate a new grid to use
with data read back from the file.
*/


void save_bright(ngrid,Cq,Cm,Br,Bt,Bp,filename,getname,comment,inc,ve,magnetic)
int ngrid;
float Cq[],Cm[],Br[],Bt[],Bp[];
char filename[], comment[];
int getname;
float inc, ve;
int magnetic;
{
    FILE *fp;

    if (getname) {
        printf("Name of file to save brightness data to : ");
        scanf("%s", filename);
    }
    
    fp = fopen(filename,"w");

    fprintf(fp,"%s\n", comment);
    fprintf(fp,"%d %e %e %1d\n", ngrid, inc, ve, magnetic);
    print_array(fp,Cq,ngrid);
    if (magnetic) {
        fprintf(fp,"\n");
        print_array(fp,Cm,ngrid);
        fprintf(fp,"\n");
        print_array(fp,Br,ngrid);
        fprintf(fp,"\n");
        print_array(fp,Bt,ngrid);
        fprintf(fp,"\n");
        print_array(fp,Bp,ngrid);
    }
    fclose(fp);
}


/*
The format of the image file has already been described in the comments for
the |save_bright()| routine. This routine follows essentially the same format
as that routine. The only significant difference is that this routine must
allocate storage space for the data arrays before they are read from the file.
*/

int read_bright(ngrid,Cq,Cm,Br,Bt,Bp,filename,getname,comment,inc,ve,magnetic)
int *ngrid;
float **Cq,**Cm,**Br,**Bt,**Bp;
char  filename[],comment[];
int   getname,*magnetic;
float *inc,*ve;
{
    int  j;
    FILE *fp;

    if (getname) {
        printf("Name of file to read brightness data from : ");
        scanf("%s", filename);
    }
    
    fp = fopen(filename,"r");
    if (fp == NULL)
        return 1;

    j = 0;
    while ((comment[j++] = getc(fp)) != '\n');
    comment[j] = '\0';

    fscanf(fp,"%d %f %f %d", ngrid, inc, ve, magnetic);
    *Cq = vector(0,*ngrid-1);
    for (j = 0; j < *ngrid; j++) {
        fscanf(fp,"%f", &((*Cq)[j]));
    }
    if(*magnetic) {
        *Cm = vector(0,*ngrid-1);
        *Br = vector(0,*ngrid-1);
        *Bt = vector(0,*ngrid-1);
        *Bp = vector(0,*ngrid-1);
        for (j = 0; j < *ngrid; j++) {
            fscanf(fp,"%f", &((*Cm)[j]));
        }
        for (j = 0; j < *ngrid; j++) {
            fscanf(fp,"%f", &((*Br)[j]));
        }
        for (j = 0; j < *ngrid; j++) {
            fscanf(fp,"%f", &((*Bt)[j]));
        }
        for (j = 0; j < *ngrid; j++) {
            fscanf(fp,"%f", &((*Bp)[j]));
        }
    }
    fclose(fp); 
    return 0;
}


void save_spec(I,Q,U,V,nobs,spec_info,filename,getname,comment,datsel)
float       I[],Q[],U[],V[];
int         nobs,getname,datsel; 
spec_info_t spec_info[];
char        filename[],comment[];
{
    int  j,k,n,ntot;
    FILE *fp;

    if (datsel)  
	for (j = ntot = 0; j < nobs; j++) 
            if (spec_info[j].selected) { 
                ntot++; 
	        for (k = 0; k < 4; k++) spec_info[j].datsw[k] = spec_info[j].datsel[k];
            } 

    if (getname) {
        printf("Name of file to save spectral data to : ");
        scanf("%s",filename);
    }
    
    fp = fopen(filename,"w");

//printf("%d\n", ntot);

    fprintf(fp,"%s\n",comment);
    //fprintf(fp,"%d\n\n",ntot);
	fprintf(fp,"%d\n\n",nobs);

    n = 0;
    for (j = 0; j < nobs; j++) {
        if (spec_info[j].selected) {
            fprintf(fp,"%s  %17.9e  %1d   %1d %1d %1d %1d\n",
			 spec_info[j].line_name,
			 spec_info[j].line_centre,spec_info[j].photometric,
			 spec_info[j].datsw[0],spec_info[j].datsw[1],
                         spec_info[j].datsw[2],spec_info[j].datsw[3]);
            fprintf(fp,"%d  %f  %f  %17.9e  %17.9e  %e  %e  %e  %f\n\n",
			 spec_info[j].nspec,
			 spec_info[j].phasec,spec_info[j].phasef,
                         spec_info[j].laml,spec_info[j].lamu,
			 spec_info[j].SN,spec_info[j].continuum,
			 spec_info[j].FWHM,TO_DEG(spec_info[j].polariser));
            if (spec_info[j].datsw[0]) {
	        print_array(fp,&(I[n]),spec_info[j].nspec);
                fprintf(fp,"\n");
            }
            if (spec_info[j].datsw[1]) {
	        print_array(fp,&(Q[n]),spec_info[j].nspec);
                fprintf(fp,"\n");
            }
            if (spec_info[j].datsw[2]) {
	        print_array(fp,&(U[n]),spec_info[j].nspec);
                fprintf(fp,"\n");
            }
            if (spec_info[j].datsw[3]) {
	        print_array(fp,&(V[n]),spec_info[j].nspec);
                fprintf(fp,"\n");
            }
        }
        n += spec_info[j].nspec;
    } 
    fclose(fp);
}


int read_spec(I,Q,U,V,nobs,spec_info,filename,getname,comment)
float       **I,**Q,**U,**V;
int         *nobs;
spec_info_t **spec_info;
char        filename[],comment[];
int         getname;
{
    int  j,k,n,npns,datsw[4];
    FILE *fp;

    if (getname) {
        printf("Name of file to read spectral data from : ");
        scanf("%s", filename);
    }
    
    fp = fopen(filename,"r");
    if (fp == NULL)
        return 1;

    j = 0;
    while ((comment[j++] = getc(fp)) != '\n');
    comment[--j] = '\0';

    fscanf(fp,"%d",nobs);
    *spec_info = (spec_info_t *) svector(*nobs,(int) sizeof(spec_info_t));

    for (j = 0; j < *nobs; j++) {
        fscanf(fp,"%s %lf %d %d %d %d %d",
                    (*spec_info)[j].line_name,&((*spec_info)[j].line_centre),
		    &((*spec_info)[j].photometric),
                    &((*spec_info)[j].datsw[0]),&((*spec_info)[j].datsw[1]),
                    &((*spec_info)[j].datsw[2]),&((*spec_info)[j].datsw[3]));
        fscanf(fp,"%d %f %f %lf %lf %f %f %f %f",
                    &((*spec_info)[j].nspec),
                    &((*spec_info)[j].phasec),&((*spec_info)[j].phasef),
                    &((*spec_info)[j].laml),&((*spec_info)[j].lamu),
                    &((*spec_info)[j].SN),&((*spec_info)[j].continuum),
                    &((*spec_info)[j].FWHM),&((*spec_info)[j].polariser));
        (*spec_info)[j].polariser = TO_RAD((*spec_info)[j].polariser);
        if ((*spec_info)[j].datsw[0])
            for (k = 0; k < (*spec_info)[j].nspec; k++)
                fscanf(fp,"%*f");
        if ((*spec_info)[j].datsw[1])
            for (k = 0; k < (*spec_info)[j].nspec; k++)
                fscanf(fp,"%*f");
        if ((*spec_info)[j].datsw[2])
            for (k = 0; k < (*spec_info)[j].nspec; k++)
                fscanf(fp,"%*f");
        if ((*spec_info)[j].datsw[3])
            for (k = 0; k < (*spec_info)[j].nspec; k++)
                fscanf(fp,"%*f");
    }
    fclose(fp);

    get_spec_dims(*spec_info,*nobs,&npns,datsw);

    if (datsw[0])  *I = vector(0,npns-1);
    if (datsw[1])  *Q = vector(0,npns-1);
    if (datsw[2])  *U = vector(0,npns-1);
    if (datsw[3])  *V = vector(0,npns-1);

/*  Now do second scan of the file and read the data  */
    fp = fopen(filename,"r");
    while (getc(fp) != '\n');
    fscanf(fp,"%*d");
    n = 0;
    for (j = 0; j < *nobs; j++) {
        fscanf(fp,"%*s %*f %*d %*d %*d %*d %*d");
        fscanf(fp,"%*d %*f %*f %*f %*f %*f %*f %*f %*f");
        if ((*spec_info)[j].datsw[0])
            for (k = 0; k < (*spec_info)[j].nspec; k++)
                fscanf(fp,"%f",&((*I)[n+k]));
        if ((*spec_info)[j].datsw[1])
            for (k = 0; k < (*spec_info)[j].nspec; k++)
                fscanf(fp,"%f",&((*Q)[n+k]));
        if ((*spec_info)[j].datsw[2])
            for (k = 0; k < (*spec_info)[j].nspec; k++)
                fscanf(fp,"%f",&((*U)[n+k]));
        if ((*spec_info)[j].datsw[3])
            for (k = 0; k < (*spec_info)[j].nspec; k++)
                fscanf(fp,"%f",&((*V)[n+k]));
        n += (*spec_info)[j].nspec;
    }
    fclose(fp);
    return 0;
}


static void get_spec_dims(spec_info,nobs,npns,datsw)
spec_info_t spec_info[];
int nobs,*npns,datsw[];
{
    int j,k;

    for (k = 0; k < 4; k++)
        datsw[k] = 0;

    *npns = 0;

    for (j = 0; j < nobs; j++) {
        *npns += spec_info[j].nspec;
        for (k = 0; k < 4; k++)
            if (spec_info[j].datsw[k]) datsw[k] = 1;
    }
}


/*
This routine prints out the contents of an array, putting eight values
on each line.
*/

static void print_array(fp,data,n)
FILE *fp;
float data[];
int n;
{
    int i;

    for (i = 0; i < n; i++) {
        fprintf(fp,"%14.5e  ", data[i]);
        if (i%8 == 7) fprintf(fp,"\n");
    }
    if (i%8 != 0) fprintf(fp,"\n");
}


static int read_field(char field[], char string[], char header[], int nline) 
{
    int j, k; 

    j = get_line(field, header, nline); 
    if (j < 0) return 1; 
    for (k = 0; k < 20; k++) string[k] = header[80*j+k+10]; 
    string[20] = '\0'; 
    return 0; 
}


static int write_field(char field[], char string[], char header[], int nline) 
{
    int j, k; 

    j = get_line(field, header, nline); 
    if (j < 0) return 1; 
    if (strlen(string) < 20) 
        for (k = strlen(string); k < 20; k++) string[k] = ' '; 
    for (k = 0; k < 20; k++)  header[80*j+k+10] = string[k]; 
    return 0; 
}


static int get_line(char field[], char header[], int nline) 
{
    int j; 

    for (j = 0; j < nline; j++) 
        if (!strncmp(field, header+80*j, 8)) return j; 
    return -1; 
}


static void add_line(char field[], char string[], char **header, fits_info_t *info)
{
    int j; 
    char line[81], *tmps; 

    sprintf(line, "%s= %s", field, string); 
    for (j = strlen(line); j < 80; j++) line[j] = ' '; line[80] = '\0'; 

    if (info->nlused < info->nline) {
        for (j = 0; j < 80; j++) 
            (*header)[j+info->nlused*80] = (*header)[j+(info->nlused-1)*80]; 
        for (j = 0; j < 80; j++) 
            (*header)[j+(info->nlused-1)*80] = line[j]; 
        info->nlused++; 
    } else {
        tmps = (char *) svector(info->nline*80, (int) sizeof(char)); 
        for (j = 0; j <= 80*info->nline; j++) tmps[j] = (*header)[j]; 
        free_svector((void *) *header); 
        *header = (char *) svector((info->nline+36)*80 + 1, (int) sizeof(char)); 
        for (j = 0; j <= 80*info->nline; j++) (*header)[j] = tmps[j]; 
        free_svector((void *) tmps); 
        info->nline += 36; 
        add_line(field, string, header, info); 
        (*header)[info->nline*80] = '\0'; 
    }
}


static void del_line(int j, char **header, fits_info_t *info)
{
    int k; 
    char *tmps; 

    for (k = j*80; k < 80*(info->nline-1); k++)            
        (*header)[k] = (*header)[k+80]; 
    for (k = 80*(info->nline-1); k < 80*info->nline; k++) 
        (*header)[k] = ' '; 
    info->nlused--; 
    if (info->nline - info->nlused >= 36) { 
        info->nline -= 36; 
        tmps = (char *) svector(info->nline*80, (int) sizeof(char)); 
        for (k = 0; k < 80*info->nline; k++) tmps[k] = (*header)[k]; 
        free_svector((void *) *header); 
        *header = (char *) svector(info->nline*80 + 1, (int) sizeof(char)); 
        for (k = 0; k < 80*info->nline; k++) (*header)[k] = tmps[k]; 
        (*header)[info->nline*80] = '\0'; 
        free_svector((void *) tmps); 
    }
}


static void swap(char *buffer, int size, int ntot)
{
    int  j; 
    char tmp; 

    if (size == 2) 
        for (j = 0; j < ntot; j++) {
            tmp           = buffer[2*j]; 
            buffer[2*j]   = buffer[2*j+1]; 
            buffer[2*j+1] = tmp; 
        }
    else if (size == 4) 
        for (j = 0; j < ntot; j++) {
            tmp           = buffer[4*j]; 
            buffer[4*j]   = buffer[4*j+3]; 
            buffer[4*j+3] = tmp; 
            tmp           = buffer[4*j+1]; 
            buffer[4*j+1] = buffer[4*j+2]; 
            buffer[4*j+2] = tmp; 
        }
}


static float *vector(int nl, int nh)
{
    float *v;

    v = (float *) malloc((unsigned) (nh-nl+1)*sizeof(float));
    if (!v) {
        fprintf(stderr,"Unable to allocate sufficient space for vector\n");
        exit(1);
    }

    return v-nl;
}


static void *svector(int length, int size)
{
    float *v;

    v = (void *) malloc((unsigned) (length*size));
    if (!v) {
        fprintf(stderr,"Unable to allocate sufficient space for svector\n");
        exit(1);
    }

    return v;
}


static double *dvector(int nl, int nh)
{
    double *v;

    v = (double *) malloc((unsigned) (nh-nl+1)*sizeof(double));
    if (!v) {
        fprintf(stderr,"Unable to allocate sufficient space for dvector\n");
        fprintf(stderr,"nl = %d, nh = %d\n",nl,nh);
        exit(1);
    }

    return v-nl;
}


static int *ivector(int nl, int nh)
{
    int *v;

    v = (int *) malloc((unsigned) (nh-nl+1)*sizeof(int));
    if (!v) {
        fprintf(stderr,"Unable to allocate sufficient space for ivector\n");
        exit(1);
    }

    return v-nl;
}


static float **matrix(int nrl, int nrh, int ncl, int nch)
{
    int   i;
    float **m;

    m = (float **) malloc((unsigned) (nrh-nrl+1)*sizeof(float *));
    if (!m) {
        fprintf(stderr,"Unable to allocate sufficient space for matrix\n");
        exit(1);
    }
    m -= nrl;

    for (i = nrl; i <= nrh; i++) {
        m[i] = (float *) malloc((unsigned) (nch-ncl+1)*sizeof(float));
        if (!m[i]) {
            fprintf(stderr,"Unable to allocate sufficient space for matrix\n");
            exit(1);
        }
        m[i] -= ncl;
    }

    return m;
}



static void free_vector(float *v, int nl, int nh)
{
    free((void *) (v+nl));
}


static void free_svector(void *v)
{
    free((void *) v);
}


static void free_dvector(double *v, int nl, int nh)
{
    free((void *) (v+nl));
}


static void free_ivector(int *v, int nl, int nh)
{
    free((void *) (v+nl));
}


void read_header(char *header, fits_info_t *info) 
{
    int    i, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6; 
    float  tmp; 
    char   string[21]; 


    /* UT date */ 
    if (!read_field("UTDATE  ", string, header, info->nline)) {   
        string[0] = string[5] = string[8] = ' '; 
        sscanf(string, "%d %d %d", &tmp1, &tmp2, &tmp3); 
        info->date = (double) tmp1 + 0.01*tmp2 + 0.0001*tmp3; 
    } else 
        info->date = 10000.0; 
        
    /* UT time */ 
    if (!read_field("UTSTART ", string, header, info->nline)) { 
        string[0] = string[3] = string[6] = ' '; 
        sscanf(string, "%d %d %d", &tmp1, &tmp2, &tmp3); 
        read_field("UTEND   ", string, header, info->nline);  
        string[0] = string[3] = string[6] = ' '; 
        sscanf(string, "%d %d %d", &tmp4, &tmp5, &tmp6); 
        if (tmp4 < tmp1) tmp4 += 24; 
/*
        info->exptime = (double) (tmp4-tmp1)*3600.0 + (tmp5-tmp2)*60.0 + tmp6-tmp3; 
*/
        tmp3 += tmp6 + (tmp5+tmp2)*60 + (tmp4+tmp1)*3600; tmp3 /= 2; 
        tmp1 = tmp3/3600; tmp3 -= tmp1*3600; 
        tmp2 = tmp3/60;   tmp3 -= tmp2*60; 
        info->ut = (double) tmp1 + 0.01*tmp2 + 0.0001*tmp3; 

        read_field("TOTALEXP", string, header, info->nline);  
        sscanf(string, "%f", &tmp); 
        info->exptime = (float) tmp; 
    } else { 
        info->ut = 10000.0; 
        info->exptime = 0.0; 
    } 

    /* Coordinates */ 
    if (!read_field("MEANRA  ", string, header, info->nline))  
        sscanf(string, "%20f", &(info->ra)); 
    else 
        info->ra = 10000.0; 
    if (!read_field("MEANDEC ", string, header, info->nline))  
        sscanf(string, "%20f", &(info->dec)); 
    else 
        info->dec = 10000.0; 

    /* Hour angle */ 
/*
    if (!read_field("HASTART ", string, header, info->nline)) { 
        sscanf(string, "%20f", &(info->ha)); info->ha *= 0.5; 
        read_field("HAEND   ", string, header, info->nline); 
        sscanf(string, "%20f", &tmp); info->ha += 0.5*tmp; 
    } else 
        info->ha = 10000.0; 
*/

    if (!read_field("STSTART ", string, header, info->nline)) { 
        string[0] = string[3] = string[6] = ' '; 
        sscanf(string, "%d %d %d", &tmp1, &tmp2, &tmp3); 
        read_field("STEND   ", string, header, info->nline); 
        string[0] = string[3] = string[6] = ' '; 
        sscanf(string, "%d %d %d", &tmp4, &tmp5, &tmp6); 
        if (tmp4 < tmp1) tmp4 += 24; 
        tmp3 += tmp6 + (tmp5+tmp2)*60 + (tmp4+tmp1)*3600; tmp3 /= 2; 
        tmp1 = tmp3/3600; tmp3 -= tmp1*3600; 
        tmp2 = tmp3/60;   tmp3 -= tmp2*60; 
        tmp =  (double) tmp1 + 1.0/60.0*tmp2 + 1.0/3600.0*tmp3; 
        info->ha = 15.0*tmp - info->ra; 
    } else 
        info->ha = 10000.0; 

    /* Latitude of observations */ 
    if (!read_field("LAT_OBS ", string, header, info->nline)) 
        sscanf(string, "%20f", &(info->lat)); 
    else 
        info->lat = 10000.0; 

    info->moon = 0; 
    printf("jusqu'ici tout va bien\n");
    /*  for (i = 0; i < strlen(info->object)-4; i++)  
      if (((info->object[i]   == 'm') || (info->object[i]   == 'M')) && 
	  ((info->object[i+1] == 'o') || (info->object[i+1] == 'O')) &&
	  ((info->object[i+2] == 'o') || (info->object[i+2] == 'O')) &&
	  ((info->object[i+3] == 'n') || (info->object[i+3] == 'N'))) {
	info->moon = 1; 
	break; 
      } 
    for (i = 0; i < strlen(info->object)-3; i++)  
      if (((info->object[i]   == 's') || (info->object[i]   == 'S')) && 
	  ((info->object[i+1] == 'u') || (info->object[i+1] == 'U')) &&
	  ((info->object[i+2] == 'n') || (info->object[i+2] == 'N'))) { 
	info->moon = 1; 
	break; 
	}*/ 
    
}


void save_map(int nx, int ny, float *x, float *y, float **map, int getname,
             char filename[], char labx[], char laby[], char title[])
{
    FILE *fp;
    int  j, k;

    if (getname == 1) {
        printf("Name of file to save map to : ");
        scanf("%s", filename);
        if ((filename[0] == '.') && (filename[1] == '\0')) return;
    }
    fp = fopen(filename,"w");
    fprintf(fp,"%s\n",labx); fprintf(fp,"%s\n",laby); fprintf(fp,"%s\n",title);
    fprintf(fp,"%d %d\n",ny,nx);
    for (j = 0; j < ny; j++)
        if (j%8 == 7) fprintf(fp, "%9.4f\n", y[j]);
        else          fprintf(fp, "%9.4f ",  y[j]);
    if (ny%8) fprintf(fp,"\n");
    for (j = 0; j < nx; j++)
        if (j%8 == 7) fprintf(fp, "%9.4f\n", x[j]);
        else          fprintf(fp, "%9.4f ",  x[j]);
    if (nx%8) fprintf(fp,"\n");
    for (j = 0; j < nx; j++) {
        for (k = 0; k < ny; k++)
            if (k%8 == 7) fprintf(fp, "%11.4e\n", map[j][k]);
            else          fprintf(fp, "%11.4e ",  map[j][k]);
        if (ny%8) fprintf(fp,"\n");
    }
    fclose(fp);
}


int read_map(int *nx, int *ny, float **x, float **y, float ***map, int getname, char filename[], 
             char labx[], char laby[], char title[])
{
    FILE *fp;
    int  j, k;

    if (getname == 1) {
        printf("Name of file to read Doppler map from : ");
        scanf("%s", filename);
    }
    fp = fopen(filename,"r");
    if (fp == NULL) return 1;
    j = 0; while ((labx[j++] = getc(fp)) != '\n'); labx[--j] = '\0';
    j = 0; while ((laby[j++] = getc(fp)) != '\n'); laby[--j] = '\0';
    j = 0; while ((title[j++] = getc(fp)) != '\n'); title[--j] = '\0';
    fscanf(fp,"%d",ny); *y = vector(0, *ny-1);
    fscanf(fp,"%d",nx); *x = vector(0, *nx-1);
    *map = matrix(0, *nx-1, 0, *ny-1);
    for (j = 0; j < *ny; j++) fscanf(fp, "%f", (*y)+j);
    for (j = 0; j < *nx; j++) fscanf(fp, "%f", (*x)+j);
    for (j = 0; j < *nx; j++) for (k = 0; k < *ny; k++) fscanf(fp, "%f", (*map)[j]+k);
    fclose(fp);
    return 0;
}


