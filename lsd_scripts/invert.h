int gaussj(float a[], int n, int np, float b[], int m, int mp); 
void dgaussj(double a[], int n, int np, double b[], int m, int mp); 
void cholsl(float **a, int n, float p[], float b[], float x[]); 
void choldc(float **a, int n, float p[]); 
void dcholsl(double **a, int n, double p[], double b[], double x[]); 
void dcholdc(double **a, int n, double p[]); 
void tridag(float a[], float b[], float c[], float r[], float u[], int n); 
void toeplz(float r[], float x[], float y[], int n);
