#include <stdio.h> 
#include <string.h> 
#include <math.h> 
#include "structures.h"
#include "mem_alloc.h"
#include "file.h" 

#define twopi 6.283185307

void get_spec_dims(spec_info_t spec_info[], int nobs, int *npns, int datsw[]); 
static void select_file(int npt, int nfile, float **x); 
void remove_sec(float *lam, float *spec, int nspec, float phase, float shift, char name[]); 
static void gaussian(float *lam, float *sec, int nspec, float vs, float fwhm, float ew); 
static void fit(float *lam, float *spec, int *good, int nspec, int sub); 


int main(void) 
{
    int    j, k, p, n, *nspec, nphase, getname, open_ok, ntot, nfile, bin, 
           nphot, nobsp, datsel, nline, datsw[4], *type; 
    float  **lam, **spec, xmin, xmax, *Ip, *Qp, *Up, *Vp, *I, vel, rat, phishift, 
           *shift, *Q, *U, *V, *lam00, *deltal, *deltar, centr, cc, *cont, velshift; 
    char   answer[2], name[80], com[120], **ID; 
    spec_info_t *info, *infop; 
    int nn, *good, side; 

    cc = 299792.5; 
    getname = 1; 
    printf("Do you want to include photometry : "); 
    scanf("%1s", answer); 
    if ((answer[0] == 'y') || (answer[0] == 'Y')) { 
        printf("yes\n"); 
        do { 
            open_ok = read_spec(&Ip, &Qp, &Up, &Vp, &nobsp, &infop, name, getname, com); 
            if (open_ok == 1) printf("File not found. "); 
            else printf("%s\n", name); 
        } while (open_ok == 1); 
        for (j = nphot = 0; j < nobsp; j++) 
            if (infop[j].photometric) 
                nphot += infop[j].nspec; 
    } else {  
        printf("no\n"); 
        nphot = 0; 
    } 

    printf("Is it a binary star : "); 
    scanf("%1s", answer);
    if ((answer[0] == 'y') || (answer[0] == 'Y')) {
        printf("yes\n"); bin = 1; 
        printf("Velocity span, orbital phase shift, mean V mag difference between components : "); 
        scanf("%f %f %f", &vel, &phishift, &rat); 
        printf("%f %f %f\n", vel, phishift, rat); 
        rat = pow(10.0, 0.4*rat); rat = 1.0 + 1.0/rat; 
    } else { 
        vel = phishift = 0.0; 
        rat = 1.0; bin = 0; 
    } 

    /* Reads in the spectra */ 
    printf("How many phases to read : "); 
    scanf("%d", &nphase);  
    printf("%d\n", nphase); 
    if (nphase > 0) { 
        printf("Number of lines to extract : "); 
        scanf("%d", &nline); 
        printf("%d\n", nline); 
        lam00 = vector(0, nline-1); 
        deltal = vector(0, nline-1); 
        deltar = vector(0, nline-1); 
        shift = vector(0, nline-1); 
        cont = vector(0, nline-1); 
        ID = (char **) svector(nline, (int) sizeof(char *)); 
        for (j = 0; j < nline; j++) {
            ID[j] = (char *) svector(32, (int) sizeof(char)); 
            printf("ID, lam (nm), left and right span (nm), shift (nm) and cont level for line #%d : ", j);
            scanf("%s %f %f %f %f %f", ID[j], lam00+j, deltal+j, deltar+j, shift+j, cont+j); 
shift[j] *= -1.0; 
            printf("%s %f %f %f %f %f\n", ID[j], lam00[j], deltal[j], deltar[j], shift[j], cont[j]); 
deltal[j] -= shift[j]; 
deltar[j] -= shift[j]; 
        }
    } else 
        nline = 0; 

    info   = (spec_info_t *) svector(nphase*nline+nphot, (int) sizeof(spec_info_t)); 

    if (nphase > 0) { 
        lam    = (float **)      svector(nphase, (int) sizeof(float *)); 
        spec   = (float **)      svector(nphase, (int) sizeof(float *)); 
        nspec  = ivector(0, nphase-1); 
        type   = ivector(0, nphase-1); 
    } 

    for (j = 0; j < nphase; j++) { 
        do { 
            open_ok = read_ascfile(nspec+j, &nfile, lam+j, spec+j, getname, name, com); 
            if (open_ok == 1) printf("File not found. "); 
            printf("%s\n", name); 
        } while (open_ok == 1); 
        if (nfile > 1) select_file(nspec[j],nfile,spec+j);

        type[j] = -1; 
        do { 
            printf("Type of observation (IQUV) : "); 
            scanf("%1s", answer); 
            if ((answer[0] == 'i') || (answer[0] == 'I'))      type[j] = 0; 
            else if ((answer[0] == 'h') || (answer[0] == 'H')) type[j] = 4;
	    else if ((answer[0] == 'q') || (answer[0] == 'Q')) type[j] = 1;
            else if ((answer[0] == 'u') || (answer[0] == 'U')) type[j] = 2; 
            else if ((answer[0] == 'v') || (answer[0] == 'V')) type[j] = 3; 
            if (type[j] < 0) printf("\nType should be either IQUV.  Try again\n"); 
        } while (type[j] < 0); 
        if (type[j] == 0)      printf("I\n"); 
        else if (type[j] == 1) printf("Q\n"); 
        else if (type[j] == 2) printf("U\n"); 
        else if (type[j] == 3) printf("V\n"); 


        printf("Phase, SN and resolution for spectrum #%d : ", j); 
        scanf("%f %f %f", &(info[j].phasec), &(info[j].SN), &(info[j].FWHM));  
        printf("%f %f %f\n", info[j].phasec, info[j].SN, info[j].FWHM);  
    printf("@@@@@@@@@@ %f\n", info[j].SN);
       info[j].SN /= rat; 
    printf("@@@@@@@@@@ %f\n", info[j].SN);
        if (nline > 1) 
            for (k = 1; k < nline; k++) {
                info[j+k*nphase].phasec = info[j].phasec; 
                info[j+k*nphase].SN     = info[j].SN; 
                info[j+k*nphase].FWHM   = info[j].FWHM; 
            }

	if ((type[j] == 0) && (bin == 1)) remove_sec(lam[j],spec[j],nspec[j],info[j].phasec,phishift,name); 
/*
remove_sec(lam[j],spec[j],nspec[j],info[j].phasec,phishift,name); 
*/
    }
    for (p = ntot = 0; p < nline; p++) {
        xmin = lam00[p]+deltal[p]; 
        xmax = lam00[p]+deltar[p]; 
        for (j = 0; j < nphase; j++) { 
            velshift = vel*sin(twopi*(info[j+p*nphase].phasec-phishift)); 
            if (lam00[p] > 0.1) velshift *= lam00[p]/cc; 
            n = -1; 
            for (k = info[j+p*nphase].nspec = 0; k < nspec[j]; k++) 
                if ((lam[j][k] >= xmin+velshift) && (lam[j][k] <= xmax+velshift)) { 
                    if (n < 0) n = k; 
                    (info[j+p*nphase].nspec)++; 
                }

if (type[j] == 0) {
  //side = 1; //HR 1099
  side = 6; 
good = ivector(0, nspec[j]-1); 
nn = n+info[j+p*nphase].nspec; 
for (k = 0; k < nspec[j]; k++) {
if (((k > n-side) && (k <= n)) || ((k >= nn) && (k < nn+side))) good[k] = 1;
else good[k] = 0; 
//if (((k >= n-side) && (k < n-1)) || ((k > nn+1) && (k <= nn+side))) good[k] = 1;
//else good[k] = 0; 
}
fit(lam[j],spec[j],good,nspec[j],0); 
free_ivector(good, 0, nspec[j]-1); 
}
    
            sprintf(info[j+p*nphase].line_name, ID[p]); 
            info[j+p*nphase].laml = lam[j][n]+shift[p]-velshift; 
            info[j+p*nphase].lamu = lam[j][n+info[j+p*nphase].nspec-1]+shift[p]-velshift; 
            info[j+p*nphase].line_centre = lam00[p]; 
            if (lam00[p] < 0.1) { 
                info[j+p*nphase].line_centre = (double) 550.0*(1.0+info[j+p*nphase].line_centre/cc); 
                info[j+p*nphase].laml = 550.0*(1.0+info[j+p*nphase].laml/cc); 
                info[j+p*nphase].lamu = 550.0*(1.0+info[j+p*nphase].lamu/cc); 
            } 
            info[j+p*nphase].phasef = info[j+p*nphase].phasec; 
            info[j+p*nphase].FWHM = info[j+p*nphase].line_centre/info[j+p*nphase].FWHM; 
            info[j+p*nphase].continuum = cont[p]; 
            ntot += info[j+p*nphase].nspec; 
        }
    }


/* Saves the spectrum */ 
    I = vector(0, ntot+nphot); 
    Q = vector(0, ntot+nphot); 
    U = vector(0, ntot+nphot); 
    V = vector(0, ntot+nphot); 

    for (p = n = 0; p < nline; p++) 
        for (j = 0; j < nphase; j++) { 
            velshift = vel*sin(twopi*(info[j+p*nphase].phasec-phishift)); 
            if (lam00[p] > 300.0) velshift *= lam00[p]/cc; 
            for (k = 0; k < nspec[j]; k++) 
                if ((lam[j][k] >= lam00[p]+deltal[p]+velshift) && 
                    (lam[j][k] <= lam00[p]+deltar[p]+velshift)) 
                    if (type[j] == 0)      I[n++] = cont[p] + rat*(spec[j][k]-cont[p]); 
                    else if (type[j] == 1) Q[n++] = -spec[j][k]*rat; 
                    else if (type[j] == 2) U[n++] = spec[j][k]*rat; 
                    else if (type[j] == 3) V[n++] = spec[j][k]*rat; 
        }

    for (j = p = 0; j < nphase*nline; j++) { 
        info[j].photometric = 0; 
        info[j].selected = 1; 
        info[j].polariser = 0.0; 
        if (type[j%nphase] == 0) { 
            info[j].datsel[0] = 1; 
            info[j].datsel[1] = info[j].datsel[2] = info[j].datsel[3] = 0; 
        } else if (type[j%nphase] == 1) { 
            info[j].datsel[1] = 1; 
            info[j].datsel[0] = info[j].datsel[2] = info[j].datsel[3] = 0; 
        } else if (type[j%nphase] == 2) { 
            info[j].datsel[2] = 1; 
            info[j].datsel[0] = info[j].datsel[1] = info[j].datsel[3] = 0; 
        } else if (type[j%nphase] == 3) { 
            info[j].datsel[3] = 1; 
            info[j].datsel[0] = info[j].datsel[1] = info[j].datsel[2] = 0; 
        } 
    }

    p = ntot; 
    if (nphot > 0) {
        for (k = n = 0; k < nobsp; n += infop[k].nspec, k++) 
            if (infop[k].photometric == 1) { 
                I[p] = Ip[n]; 
                sprintf(info[j].line_name, infop[k].line_name); 
                info[j].line_centre = infop[k].line_centre;
                info[j].laml = infop[k].laml; 
                info[j].lamu = infop[k].lamu; 
                info[j].phasec = infop[k].phasec; 
                info[j].phasef = infop[k].phasef; 
                info[j].SN = infop[k].SN; 
                info[j].FWHM = infop[k].FWHM; 
                info[j].nspec = infop[k].nspec; 
                info[j].photometric = infop[k].photometric; 
                info[j].continuum = infop[k].continuum; 
                info[j].polariser = infop[k].polariser; 
                info[j].datsel[0] = 1; 
                info[j].datsel[1] = info[j].datsel[2] = info[j].datsel[3] = 0; 
                info[j].selected = 1; 
                j++; p++;  
            }
    }
    datsel = 1; 
    save_spec(I, Q, U, V, nline*nphase+nphot, info, name, getname, com, datsel); 
    printf("%s\n", name); 

    for (j = 0; j < nphase; j++) { 
        free_vector(lam[j],  0, nspec[j]-1); 
        free_vector(spec[j], 0, nspec[j]-1); 
    }
    free_svector((void *) info); 
    free_vector(I, 0, ntot+nphot); 
    free_vector(Q, 0, ntot+nphot); 
    free_vector(U, 0, ntot+nphot); 
    free_vector(V, 0, ntot+nphot); 
    if (nphase > 0) { 
        for (j = 0 ;j < nline; j++) 
            free_svector((void *) ID[j]); 
        free_svector((void *) ID); 
        free_svector((void *) lam);              
        free_svector((void *) spec); 
        free_ivector(nspec,0, nphase-1); 
        free_ivector(type,0, nphase-1); 
        free_vector(lam00, 0, nline-1);  
        free_vector(deltal, 0, nline-1); 
        free_vector(deltar, 0, nline-1); 
        free_vector(shift, 0, nline-1); 
        free_vector(cont, 0, nline-1); 
    }
    if (nphot > 0) { 
        get_spec_dims(infop, nobsp, &p, datsw);
        if (datsw[0] == 1) free_vector(Ip, 0, p-1);
        if (datsw[1] == 1) free_vector(Qp, 0, p-1);
        if (datsw[2] == 1) free_vector(Up, 0, p-1);
        if (datsw[3] == 1) free_vector(Vp, 0, p-1);
        free_svector((void *) infop);
    } 
}


void get_spec_dims(spec_info_t spec_info[], int nobs, int *npns, int datsw[])
{
    int j,k;

    for (k = 0; k < 4; k++)
        datsw[k] = 0;

    *npns = 0;

    for (j = 0; j < nobs; j++) {
        *npns += spec_info[j].nspec;
        for (k = 0; k < 4; k++)
            if (spec_info[j].datsw[k]) datsw[k] = 1;
    }
}


static void select_file(int npt, int nfile, float **x)
{
    int   j, k;
    float *tmp;

    do {
        printf("File to be selected (1-%d) : ", nfile);
        scanf("%d", &k);
    } while ((k > nfile) || (k < 1));
    k--;

    tmp = vector(0, npt-1);
    for (j = 0; j < npt; j++) tmp[j] = (*x)[j+k*npt];
    free_vector(*x, 0, npt*nfile-1);
    *x = tmp;
}


void remove_sec(float *lam, float *spec, int nspec, float phase, float shift, char name[])
{
    float vs, *sec, *sec2, *sec3, Ks, fwhm, gamma, ew; 
    int   j; 

printf("Here\n"); 
printf("%s\n", name); 

if (strstr(name,"hr1099") != 0L) {
    if (strstr(name,"aug91") != 0L) {
        Ks = 63.28; gamma = -14.88; fwhm = 18.3; ew = 1.05; 
    } else if (strstr(name,"dec91") != 0L) {
        Ks = 63.28; gamma = -14.88; fwhm = 16.6; ew = 0.90; 
    } else if (strstr(name,"dec92") != 0L) {
        Ks = 63.24; gamma = -15.00; fwhm = 17.9; ew = 1.04; 
    } else if (strstr(name,"dec93") != 0L) {
        Ks = 63.14; gamma = -14.95; fwhm = 17.7; ew = 1.01; 
    } else if (strstr(name,"dec95") != 0L) {
        Ks = 63.10; gamma = -14.81; fwhm = 18.6; ew = 1.13; 
    } else if (strstr(name,"dec96") != 0L) {
        Ks = 63.02; gamma = -14.63; fwhm = 18.1; ew = 0.95; 
    } else if (strstr(name,"nov96") != 0L) {
        Ks = 63.07; gamma = -14.33; fwhm = 18.9; ew = 1.09; 
    } else if (strstr(name,"jan98") != 0L) {
        Ks = 62.80; gamma = -14.66; fwhm = 18.4; ew = 1.00; 

	/*    } else if (strstr(name,"07dec98") != 0L) {
        Ks = 63.02; gamma = -13.53; fwhm = 21.48; ew = 1.02; 
    } else if (strstr(name,"19jan99") != 0L) {
        Ks = 63.02; gamma = -13.53; fwhm = 20.5; ew = 1.1; 
    } else if (strstr(name,"23jan99") != 0L) {
        Ks = 63.02; gamma = -13.53; fwhm = 20.; ew = .98; 
	*/
    } else if (strstr(name,"dec00") != 0L) {
        Ks = 62.95; gamma = -14.89; fwhm = 18.2; ew = 1.005; 
	    } else if ((strstr(name,"jan99") != 0L)) {
	      Ks = 62.66; gamma = -14.40; fwhm = 19.7; ew = 1.006; 
    } else if (strstr(name,"dec98") != 0L) {
	      Ks = 62.25; gamma = -13.92; fwhm = 20.1; ew = .988; 
    } else if (strstr(name,"dec99") != 0L) {
        Ks = 62.86; gamma = -14.85; fwhm = 18.23; ew = 0.99; 
/* 
else if (strstr(name,"02jan99") != 0L) {
        Ks = 63.02; gamma = -13.53; fwhm = 18.; ew = .92; 
    }
 else if (strstr(name,"29dec98") != 0L) {
        Ks = 63.02; gamma = -13.53; fwhm = 17.7; ew = .96; 
    } 
*/

    } else if ((strstr(name,"feb98") != 0L) || (strstr(name,"mar98") != 0L)) {
        Ks = 62.8; gamma = -13.6; fwhm = 20.; ew = .99; 
    }/* else if (strstr(name,"08mar00") != 0L) {
        Ks = 62.68; gamma = -13.08; fwhm = 19.7; ew = 1.; 
	} else if (strstr(name,"03feb00") != 0L) {
    Ks = 62.68; gamma = -13.78; fwhm = 19.7; ew = 0.84; 
    }*/ else if ((strstr(name,"feb00") != 0L) || (strstr(name,"mar00") != 0L)) {
        Ks = 62.85; gamma = -14.23; fwhm = 19.8; ew = .88; 
	} else if ((strstr(name,"jan02") != 0L) || (strstr(name,"dec01") != 0L)) {
        Ks = 62.82; gamma = -14.6; fwhm = 20.2; ew = 1.0; 
    }

    else if ((strstr(name,"jan04") != 0L) || (strstr(name,"feb04") != 0L)) {
        Ks = 62.9; gamma = -14.3; fwhm = 20.4; ew = 1.0; 
    }

}

if (strstr(name,"iipeg") != 0L) {
    Ks = 0.01; gamma = 3000.0; fwhm = 12.0; ew = 0.15; 
}

if (strstr(name,"sigmagem") != 0L) {
    Ks = 0.01; gamma = 3000.0; fwhm = 12.0; ew = 0.15; 
}

if (strstr(name,"cceri") != 0L) {
    Ks = 0.01; gamma = 3000.0; fwhm = 12.0; ew = 0.15; 
}


if (strstr(name,"uxari") != 0L) {
     if (strstr(name,"feb98") != 0L) {
    Ks = -12.1; gamma = 30.0; fwhm = 12.0; ew = 0.15; 
    } else if (strstr(name,"dec01") || strstr(name,"jan02") != 0L) {
    Ks = 6.2; gamma = 30.0; fwhm = 12.1; ew = .17; 
    }
         	     
	       

           vs = gamma + Ks; 
	printf("%f %f %f %f %f\n", Ks,gamma,fwhm,ew,vs); 
   sec = vector(0, nspec-1); 
    gaussian(lam,sec,nspec,vs,fwhm,ew); 
   for (j = 0; j < nspec; j++) spec[j] += sec[j]; 
   //   for (j = 0; j < nspec; j++) spec[j] += 0.; 
    free_vector(sec, 0, nspec-1); 

         if (strstr(name,"mar98") != 0L || strstr(name,"feb98") != 0L) {
        Ks = 66.8; gamma = 30.0; fwhm = 17.5; ew = 1.00; 
	} else if (strstr(name,"dec01") || strstr(name,"jan02") != 0L) { 
	  Ks = 67.4; gamma = 23.3; fwhm = 17.2; ew = 1.21; 
    } 
}


    vs = gamma - Ks*sin(twopi*(phase-shift)); 
    printf("%f %f %f %f %f\n", Ks,gamma,fwhm,ew,vs); 
    sec = vector(0, nspec-1); 
    gaussian(lam,sec,nspec,vs,fwhm,ew); 
    for (j = 0; j < nspec; j++) spec[j] += sec[j]; 
    //for (j = 0; j < nspec; j++) spec[j] += 0.; 
    free_vector(sec, 0, nspec-1); 
/*
    vs = gamma - Ks*sin(twopi*(phase-shift)); 
    sec = vector(0, nspec-1); 
    sec2 = vector(0, nspec-1); 
    sec3 = vector(0, nspec-1); 
    gaussian(lam,sec, nspec,vs- 2.6,18.9, 1.85); 
    gaussian(lam,sec2,nspec,vs-10.8,12.0,-1.60); 
    gaussian(lam,sec3,nspec,vs+18.2,10.8,-0.36); 
    for (j = 0; j < nspec; j++) spec[j] += 0.005*(sec3[j]+sec2[j]+sec[j])/3.5; 
    free_vector(sec, 0, nspec-1); 
    free_vector(sec2, 0, nspec-1); 
    free_vector(sec3, 0, nspec-1); 
*/
} 


static void gaussian(float *lam, float *sec, int nspec, float vs, float fwhm, float ew)
{
    int   j;
    float arg, amp, sig;

    sig = 0.5*fwhm/sqrt(log(2.0));
    amp = ew/sig/sqrt(twopi*0.5);
    for (j = 0; j < nspec; j++) {
        arg = (lam[j]-vs)/sig;
        sec[j] = amp*exp(-arg*arg);
    }
}


static void fit(float *lam, float *spec, int *good, int nspec, int sub)
{
    float sx, sx2, sy, sxy, a, b, d, mean, ww, tmp;
    int   j, n, k;

    tmp = sx = sy = sx2 = sxy = 0.0;
    for (j = n = 0; j < nspec; j++)
        if (good[j]) {
            ww  = 1.0;
            sx  += ww*lam[j];
            sy  += ww*spec[j];
            sx2 += ww*lam[j]*lam[j];
            sxy += ww*lam[j]*spec[j];
            tmp += ww;
        }
    d = tmp*sx2-sx*sx;
    a = (tmp*sxy-sx*sy)/d; b = (sx2*sy-sx*sxy)/d;

    if (sub) for (j = 0; j < nspec; j++) spec[j] -= a*lam[j]+b;
    else     for (j = 0; j < nspec; j++) spec[j] /= a*lam[j]+b;
printf("%f %f %f\n", a, b, d);
}

