#include <stdio.h>
#include <stdlib.h>
#include "mem_alloc.h"
#include "invert.h"

static float pch = 1.0;

static void mrqcof(float x[], float y[], float sig[], int ndata, float a[], 
                   int ma, int lista[], int mfit, float alpha[], float beta[], 
                   int nalp, float *chisq, void (*funcs)(float x[], float a[], 
                   float y[], float **dyda, int na, int nfit, int ndata));
static void covsrt(float covar[], int ncvm, int ma, int lista[], int mfit); 
static float *atry, *beta, ochisq, *da;

/* mrqmin returns 2 for an improper permutation in LISTA, 
                  1 for a singular matrix problem in gaussj, 
                  0 otherwise */ 

int mrqmin(float x[], float y[], float sig[], int ndata, float a[], int ma, 
            int lista[], int mfit, float covar[], float alpha[], int nca, 
            float *chisq, void (*funcs)(float x[], float a[], float y[], 
            float **dyda, int na, int nfit, int ndata), float *alamda)
{
    int   k,kk,j,ihit; 

    if (*alamda < 0.0) {
        atry = vector(0,ma-1); 
        beta = vector(0,mfit-1); 
        da   = vector(0,mfit-1); 
        kk = mfit; 
        for (j = 0; j < ma; j++) {
            ihit = 0; 
            for (k = 0; k < mfit; k++) 
                if (lista[k] == j) ihit++; 
            if (ihit == 0) 
                lista[++kk] = j; 
            else if (ihit > 1) return 2; 
        }
        if (kk != ma) return 2; 

        *alamda = 0.001; 
        mrqcof(x,y,sig,ndata,a,ma,lista,mfit,alpha,beta,nca,chisq,funcs); 
        ochisq = *chisq; 
        for (j = 0; j < ma; j++) 
           atry[j] = a[j]; 
    }
    for (j = 0; j < mfit; j++) {
        for (k = 0; k < mfit; k++) 
            covar[j+nca*k] = alpha[j+nca*k];
        covar[j*(nca+1)] = alpha[j*(nca+1)] * (1.0 + *alamda); 
        da[j] = beta[j];
    }
    if (gaussj(covar,mfit,nca,da,1,1)) return 1;
    if (*alamda == 0.0) { 
        covsrt(covar,nca,ma,lista,mfit);
        free_vector(atry,0,ma-1); 
        free_vector(beta,0,mfit-1); 
        free_vector(da  ,0,mfit-1); 
        return 0;
    } 
    for (j = 0; j < mfit; j++) {
        atry[lista[j]] = a[lista[j]] + da[j]; 
        if (atry[lista[j]] < a[lista[j]]-pch) atry[lista[j]] = a[lista[j]]-pch; 
        if (atry[lista[j]] > a[lista[j]]+pch) atry[lista[j]] = a[lista[j]]+pch; 
    }
    mrqcof(x,y,sig,ndata,atry,ma,lista,mfit,covar,da,nca,chisq,funcs); 
    if (*chisq < ochisq) {
        *alamda *= 0.1; 
        ochisq = *chisq; 
        for (j = 0; j < mfit; j++) {
            for (k = 0; k < mfit; k++) 
                alpha[j+nca*k] = covar[j+nca*k];
            beta[j] = da[j]; 
            a[lista[j]] = atry[lista[j]]; 
        }
    } else { 
        *alamda *= 10.0; 
        *chisq = ochisq; 
    }
    return 0;
}


static void mrqcof(float x[], float y[], float sig[], int ndata, float a[], 
                   int ma, int lista[], int mfit, float alpha[], float beta[], 
                   int nalp, float *chisq, void (*funcs)(float x[], float a[], 
                   float y[], float **dyda, int na, int nfit, int ndata))
{
    int   i,j,k;
    float **dyda,*ymod,sig2i,wt,dy;

    dyda = matrix(0,ma-1,0,ndata-1);
    ymod = vector(0,ndata-1);
    for (j = 0; j < mfit; j++) {
        for (k = 0; k <= j; k++) 
            alpha[j+nalp*k] = 0.0; 
        beta[j] = 0.0;
    }
    *chisq = 0.0; 
    (*funcs)(x,a,ymod,dyda,ma,mfit,ndata);
    for (i = 0; i < ndata; i++) { 
        sig2i = 1.0/(sig[i]*sig[i]); 
        dy = y[i]-ymod[i]; 
/*
printf("%d %e %e %e %e >> %e \n", i, y[i],ymod[i],dy,sig[i],*chisq); 
*/
        for (j = 0; j < mfit; j++) {
            wt = *(*(dyda+lista[j])+i)*sig2i; 
            for (k = 0; k <= j; k++) 
                alpha[j+nalp*k] += *(*(dyda+lista[k])+i)*wt; 
            beta[j] += dy*wt; 
        }
        *chisq += dy*dy*sig2i; 
    }
    for (j = 1; j < mfit; j++) 
        for (k = 0; k <= j-1; k++) 
            alpha[k+nalp*j] = alpha[j+nalp*k]; 
    free_matrix(dyda,0,mfit-1,0,ndata-1);
    free_vector(ymod,0,ndata-1);
}


static void covsrt(float covar[], int ncvm, int ma, int lista[], int mfit)
{
    int   i,j;
    float swap; 

    for (j = 0; j < ma-1; j++) 
        for (i = j+1; i < ma; i++) 
            covar[i+ncvm*j] = 0.0; 
    for (i = 0; i < mfit-1; i++) 
        for (j = i+1; j < mfit; j++) {
            if (lista[j] > lista[i]) 
                covar[lista[j]+ncvm*lista[i]] = covar[i+ncvm*j];
            else 
                covar[lista[i]+ncvm*lista[j]] = covar[i+ncvm*j];
        }
    swap = covar[0]; 
    for (j = 0; j < ma; j++) {
        covar[j*ncvm] = covar[j*(ncvm+1)]; 
        covar[j*(ncvm+1)] = 0.0; 
    }
    covar[lista[0]*(ncvm+1)] = swap; 
    for (j = 1; j < mfit; j++) 
        covar[lista[j]*(ncvm+1)] = covar[j*ncvm]; 
    for (j = 1; j < ma; j++) 
        for (i = 0; i <= j-1; i++) 
            covar[i+ncvm*j] = covar[j+ncvm*i];
}

