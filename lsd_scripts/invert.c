#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <math.h>
#include "mem_alloc.h"


/* Inversion de matrice par methode du pivot (simple precision) */ 

int gaussj(float a[], int n, int np, float b[], int m, int mp) 
{
    int   ll,l,k,j,i,irow,icol,*ipiv,*indxr,*indxc; 
    float big,pivinv,dum; 

    ipiv  = ivector(0,n-1);
    indxr = ivector(0,n-1);
    indxc = ivector(0,n-1);
    for (j = 0; j < n; j++) ipiv[j] = 0; 
    for (i = 0; i < n; i++) { 
        big = 0.0; 
        for (j = 0; j < n; j++) 
            if (ipiv[j] != 1) 
                for (k = 0; k < n; k++) {
                    if (ipiv[k] == 0) {
                        if (fabs((double) a[j+np*k]) >= big) {
                            big = fabs((double) a[j+np*k]); 
                            irow = j; 
                            icol = k;
                        }
                    } else if (ipiv[k] > 1) return 1; 
                }
        ipiv[icol] += 1; 
        if (irow != icol) {
            for (l = 0; l < n; l++) {
                dum = a[irow+np*l];
                a[irow+np*l] = a[icol+np*l]; 
                a[icol+np*l] = dum; 
            }
            for (l = 0; l < m; l++) {
                dum = b[irow+np*l];
                b[irow+np*l] = b[icol+np*l]; 
                b[icol+np*l] = dum; 
            }
        }
        indxr[i] = irow;
        indxc[i] = icol;
        if (a[icol*(np+1)] == 0.0) return 1; 

        pivinv = 1.0/a[icol*(np+1)]; 
        a[icol*(np+1)] = 1.0; 
        for (l = 0; l < n; l++) a[icol+np*l] *= pivinv; 
        for (l = 0; l < m; l++) b[icol+np*l] *= pivinv; 
        for (ll = 0; ll < n; ll++) 
            if (ll != icol) {
                dum = a[ll+np*icol]; 
                a[ll+np*icol] = 0.0; 
                for (l = 0; l < n; l++) 
                    a[ll+np*l] -= dum*a[icol+np*l]; 
                for (l = 0; l < m; l++) 
                    b[ll+np*l] -= dum*b[icol+np*l]; 
            }
    }
    for (l = n-1; l >= 0; l--) 
        if (indxr[l] != indxc[l]) 
            for (k = 0; k < n; k++) {
                dum = a[k+np*indxr[l]]; 
                a[k+np*indxr[l]] = a[k+np*indxc[l]]; 
                a[k+np*indxc[l]] = dum; 
            }
    free_ivector(ipiv ,0,n-1);
    free_ivector(indxr,0,n-1);
    free_ivector(indxc,0,n-1);
    return 0; 
}


/* Inversion de matrice par methode du pivot (double precision) */ 

void dgaussj(double a[], int n, int np, double b[], int m, int mp) 
{
    int   ll,l,k,j,i,irow,icol,*ipiv,*indxr,*indxc; 
    double big,pivinv,dum; 

    ipiv  = ivector(0,n-1);
    indxr = ivector(0,n-1);
    indxc = ivector(0,n-1);
    for (j = 0; j < n; j++) 
        ipiv[j] = 0; 
    for (i = 0; i < n; i++) { 
        big = 0.0; 
        for (j = 0; j < n; j++) 
            if (ipiv[j] != 1) 
                for (k = 0; k < n; k++) {
                    if (ipiv[k] == 0) {
                        if (fabs((double) a[j+np*k]) >= big) {
                            big = fabs((double) a[j+np*k]); 
                            irow = j; 
                            icol = k;
                        }
                    } else if (ipiv[k] > 1) {
                        printf("Singular matrix\n"); 
                        exit(0); 
                    }
                }
        ipiv[icol] += 1; 
        if (irow != icol) {
            for (l = 0; l < n; l++) {
                dum = a[irow+np*l];
                a[irow+np*l] = a[icol+np*l]; 
                a[icol+np*l] = dum; 
            }
            for (l = 0; l < m; l++) {
                dum = b[irow+np*l];
                b[irow+np*l] = b[icol+np*l]; 
                b[icol+np*l] = dum; 
            }
        }
        indxr[i] = irow;
        indxc[i] = icol;
        if (a[icol*(np+1)] == 0.0) {
            printf("Singular matrix\n"); 
            exit(0); 
        }
        pivinv = (double) 1.0/a[icol*(np+1)]; 
        a[icol*(np+1)] = 1.0; 
        for (l = 0; l < n; l++) 
            a[icol+np*l] *= pivinv; 
        for (l = 0; l < m; l++) 
            b[icol+np*l] *= pivinv; 
        for (ll = 0; ll < n; ll++) 
            if (ll != icol) {
                dum = a[ll+np*icol]; 
                a[ll+np*icol] = 0.0; 
                for (l = 0; l < n; l++) 
                    a[ll+np*l] -= a[icol+np*l]*dum; 
                for (l = 0; l < m; l++) 
                    b[ll+np*l] -= b[icol+np*l]*dum; 
            }
    }
    for (l = n-1; l >= 0; l--) 
        if (indxr[l] != indxc[l]) 
            for (k = 0; k < n; k++) {
                dum = a[k+np*indxr[l]]; 
                a[k+np*indxr[l]] = a[k+np*indxc[l]]; 
                a[k+np*indxc[l]] = dum; 
            }
    free_ivector(ipiv ,0,n-1);
    free_ivector(indxr,0,n-1);
    free_ivector(indxc,0,n-1);
}


/* Inversion d'une matrice symetrique par decomposition LU */ 

void cholsl(float **a, int n, float p[], float b[], float x[])
{
    int   i, k;
    float sum;

    for (i = 0; i < n; i++) {
        for (sum = b[i], k = i-1; k >= 0; k--) sum -= a[i][k]*x[k];
        x[i] = sum/p[i];
    }
    for (i = n-1; i >= 0; i--) {
        for (sum = x[i], k = i+1; k < n; k++) sum -= a[k][i]*x[k];
        x[i] = sum/p[i];
    }
}


void dcholsl(double **a, int n, double p[], double b[], double x[])
{
    int   i, k;
    double sum;

    for (i = 0; i < n; i++) {
        for (sum = b[i], k = i-1; k >= 0; k--) sum -= a[i][k]*x[k];
        x[i] = sum/p[i];
    }
    for (i = n-1; i >= 0; i--) {
        for (sum = x[i], k = i+1; k < n; k++) sum -= a[k][i]*x[k];
        x[i] = sum/p[i];
    }
}


/* Decomposition de Cholevsky (LU) d'une matrice symetrique */ 

void choldc(float **a, int n, float p[])
{
    int   i, j, k;
    float sum;

    for (i = 0; i < n; i++) {
        for (j = i; j < n; j++) {
            for (sum = a[i][j], k = i-1; k >= 0; k--) sum -= a[i][k]*a[j][k];
            if (i == j) {
                if (sum <= 0.0) {
                    printf("Choldc failed sum=%f %d\n",sum,i);
                    exit(1);
                }
                p[i] = sqrt(sum);
            } else
                a[j][i] = sum/p[i];
        }
    }
}


void dcholdc(double **a, int n, double *p)
{
    int   i, j, k;
    double sum;

    for (i = 0; i < n; i++) {
        for (j = i; j < n; j++) {
            for (sum = a[i][j], k = i-1; k >= 0; k--) sum -= a[i][k]*a[j][k];
            if (i == j) {
                if (sum <= 0.0) {
                    printf("Choldc failed sum=%f\n",sum);
                    exit(1);
                }
                p[i] = sqrt(sum);
            } else a[j][i] = sum/p[i];
        }
    }
}


void tridag(float a[], float b[], float c[], float r[], float u[], int n)
{
    int j;
    float bet,*gam;

    gam=vector(0,n-1);
    if (b[0] == 0.0) {
        printf("Error 1 in tridag\n");
        exit(1);
    }
    u[0]=r[0]/(bet=b[0]);
    for (j=1;j<n;j++) {
        gam[j]=c[j-1]/bet;
        bet=b[j]-a[j]*gam[j];
        if (bet == 0.0) {
            printf("Error 2 in tridag\n");
            exit(1);
        }
        u[j]=(r[j]-a[j]*u[j-1])/bet;
    }
    for (j=n-2;j>=0;j--)
        u[j] -= gam[j+1]*u[j+1];
    free_vector(gam,0,n-1);
}


void toeplz(float r[], float x[], float y[], int n)
{
    int j,k,m,m1,m2;
    float pp,pt1,pt2,qq,qt1,qt2,sd,sgd,sgn,shn,sxn;
    float *g,*h;
    
    if (r[n] == 0.0) { 
        printf("toeplz-1 singular principal minor\n");
        exit(1); 
    } 
    g=vector(1,n);
    h=vector(1,n);
    x[1]=y[1]/r[n];
    if (n == 1) { 
        free_vector(h,1,n); 
        free_vector(g,1,n); 
        return;  
    } 
    g[1]=r[n-1]/r[n];
    h[1]=r[n+1]/r[n];
    for (m=1;m<=n;m++) {
        m1=m+1;
        sxn = -y[m1];
        sd = -r[n];
        for (j=1;j<=m;j++) {
            sxn += r[n+m1-j]*x[j];
            sd += r[n+m1-j]*g[m-j+1];
        }
        if (sd == 0.0) { 
            printf("toeplz-2 singular principal minor\n");
            exit(1); 
        } 
        x[m1]=sxn/sd;
        for (j=1;j<=m;j++) x[j] -= x[m1]*g[m-j+1];
        if (m1 == n) { 
            free_vector(h,1,n); 
            free_vector(g,1,n); 
            return; 
        } 
        sgn = -r[n-m1];
        shn = -r[n+m1];
        sgd = -r[n];
        for (j=1;j<=m;j++) {
            sgn += r[n+j-m1]*g[j];
            shn += r[n+m1-j]*h[j];
            sgd += r[n+j-m1]*h[m-j+1];
        }
        if (sd == 0.0 || sgd == 0.0) { 
            printf("toeplz-3 singular principal minor\n");
            exit(1); 
        } 
        g[m1]=sgn/sgd;
        h[m1]=shn/sd;
        k=m;
        m2=(m+1) >> 1;
        pp=g[m1];
        qq=h[m1];
        for (j=1;j<=m2;j++) {
            pt1=g[j];
            pt2=g[k];
            qt1=h[j];
            qt2=h[k];
            g[j]=pt1-pp*qt2;
            g[k]=pt2-pp*qt1;
            h[j]=qt1-qq*pt2;
            h[k--]=qt2-qq*pt1;
        }
    }
    printf("toeplz - should not arrive here!");
    exit(1); 
}
