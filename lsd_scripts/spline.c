#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mem_alloc.h"

#define big 1.0e30


void spline(float x[], float y[], int n, float yp1, float ypn, float y2[])
{
    double *u, qn, un, sig, p, dx1, dx2, dx3, dy1, dy2;
    int    i, k;

    u = dvector(0, n-1);
    if (yp1 > 0.99*big) {
        y2[0] = 0.0;
        u[0]  = 0.0;
    } else {
        y2[0] = -0.5;
        u[0]  = 3.0/(x[1]-x[0])*((y[1]-y[0])/(x[1]-x[0]) - yp1);
    }
    for (i = 1; i < n-1; i++) {
        dx1 = x[i] - x[i-1]; dx2 = x[i+1] - x[i]; dx3 = dx2 + dx1;
        dy1 = y[i] - y[i-1]; dy2 = y[i+1] - y[i]; 
        sig = dx1/dx3; 
        p = sig*y2[i-1] + 2.0;
        y2[i] = (sig-1.0)/p;
        u[i]  = (6.0*(dy2/dx2 - dy1/dx1)/dx3 - sig*u[i-1])/p;
    }
    if (ypn > 0.99*big) {
        qn = 0.0;
        un = 0.0;
    } else {
        qn = 0.5;
        un = 3.0/(x[n-1]-x[n-2])*(ypn - (y[n-1]-y[n-2])/(x[n-1]-x[n-2]));
    }
    y2[n-1] = (un-qn*u[n-2])/(qn*y2[n-2]+1.0);
    for (k = n-2; k >= 0; k--) 
        y2[k] = y2[k]*y2[k+1] + u[k];
    free_dvector(u, 0, n-1); 
}


void splint(float xa[], float ya[], float y2a[], float x, float *y)
{
    double h, a, b;

    h = xa[1] - xa[0];
    if (h != 0.0) {
        a = (xa[1]-x)/h;
        b = (x-xa[0])/h;
        *y = a*ya[0] + b*ya[1] + 
             ((a*a*a-a)*y2a[0] + (b*b*b-b)*y2a[1])*h*h/6.0;
    } else {
        printf("Bad XA input\n");
        exit(1);
    }
}

#undef big

