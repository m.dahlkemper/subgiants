/* Additional routines for various slave tasks */ 

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include "util.h"
#include "mem_alloc.h"
#include "invert.h"
#include "lsfit.h"
#include "smear.h"
#include "spline.h"

#define BIG 1.0e30


/* Look for continuum points */ 

void continuum(float *x, float *lam, float *spec, int nspec, int step, float *pix, float *cont, 
               int *npix) 
{
    int   j, k, m, p, *good, nout; 
    float max, maxdev, xx0, tmp, sdv, mean; 

    maxdev = 5.0; 
    good = ivector(0, nspec-1); 
    for (j = 0; j < nspec; j++) good[j] = 1; 

    while (reject(spec,good,nspec,&mean,&sdv,maxdev,nspec/100+1));

/*
    do { 
        sdv = mean = 0.0;  
        for (j = p = 0; j < nspec; j++) 
            if (good[j]) {
                tmp = spec[j]; 
                sdv += tmp*tmp; 
                mean += tmp; 
                p++; 
            } 
        mean /= p; sdv = sqrt(sdv/p-mean*mean);  
printf(">> mean sdv=%f %f\n", mean, sdv); 
        for (j = 0; j < nspec; j++) if (spec[j] > mean+2.0*sdv) { good[j] = 0; nout = 1;} 

        max = 0.0; 
        nout = -1;  
        for (j = 0; j < nspec; j++)  
            if ((good[j]) && (spec[j] > mean+sdv) && (spec[j] < mean + 2.0*sdv)) { 
                tmp = 0.0; 
                for (p = -1; p+j >= 0; p--) if (good[p+j]) break; 
                if (p+j >= 0) tmp += spec[j]-spec[p+j]; 

printf("%d %f %d %f ",j, spec[j],j+p,spec[p+j]);  

                for (p = 1; p+j < nspec; p++) if (good[p+j]) break; 
                if (p+j < nspec) tmp += spec[j]-spec[p+j]; 
                if ((tmp > max) && (tmp > maxdev*sdv)) { 
                    max = tmp; 
                    nout = j; 
                } 

printf("%d %f %f %f %d\n",j+p, spec[p+j],tmp, max, nout);  

            } 
        if (nout > 0) { 
            good[nout] = 0; 

printf("Pixel %d (%f %f) discarded\n", nout, max, sdv); 

        } 
    } while (nout > 0); 
*/

    for (j = *npix = 0; j < nspec; j += step) {
        max = -BIG; 
        for (k = 0; (k < 2*step) && (k+j < nspec); k++) {
/* remove Halpha, Hbeta, He1D3, Hgamma, OIII, AIII, HeII, FeII */ 
if ((fabs(lam[j+k]-656.28) > 1.5) && (fabs(lam[j+k]-486.13) > 4.0) && (fabs(lam[j+k]-587.56) > 0.5) && (fabs(lam[j+k]-589.30) > 1.0) && (fabs(lam[j+k]-434.05) > 2.0) && (fabs(lam[j+k]-495.89) > 0.2) && (fabs(lam[j+k]-500.68) > 0.2) && (fabs(lam[j+k]-713.58) > 0.2) && (fabs(lam[j+k]-468.57) > 0.5) && (fabs(lam[j+k]-492.39) > 0.5) && (fabs(lam[j+k]-501.84) > 0.5) && (fabs(lam[j+k]-516.90) > 0.5)) { 
            tmp = 0.0; p = 0; 
            for (m = -2; m <= 2; m++) 
                if ((j+k+m >= 0) && (j+k+m < nspec) && (good[j+k+m])) { 
                    tmp += spec[j+k+m]; 
                    p++; 
                }
            if (p) 
                if (tmp/p > max) { 
                    max = tmp/p; 
                    xx0 = x[j+k]; 
                }
} 
        }
        if (max > -0.9*BIG) { 
            pix[*npix]  = xx0; 
            cont[*npix] = max; 
            (*npix)++; 
        } 
    }

    free_ivector(good, 0, nspec-1); 
}


/* Cross correlation routine */ 
/* if fill = 0, spec padded with 0; 
           = 1, spec padded with last element in array; 
           = 2, spec min-subtracted, normalised, padded with 0; 
           = 3, spec max-subtracted, normalised, padded with 0    */

float ccf(float *spec1, float *spec2, int nspec, float maxshift, int fill)
{
    int    dim, j, k, pix, shift, npix, sub; 
    float  centr, ew, fwhm, *ccf, max, *xx, min1, min2, max1, max2, csq; 
    double *vect1, *vect2, *ans; 
    float  *xxx, *yyy; 

    dim = 1; while (dim < nspec) dim *= 2; 
    xx    =  vector(0, dim-1); ccf   =  vector(0, dim-1); 
    vect1 = dvector(0, dim-1); vect2 = dvector(0, dim-1); 
    ans   = dvector(0, 2*dim+1); 

    min1 = min2 = BIG; 
    max1 = max2 = -BIG; 
    for (j = 0; j < nspec; j++) {
        vect1[j] = spec1[j]; vect2[j] = spec2[j];
        if (spec1[j] < min1) min1 = spec1[j]; 
        if (spec2[j] < min2) min2 = spec2[j]; 
        if (spec1[j] > max1) max1 = spec1[j]; 
        if (spec2[j] > max2) max2 = spec2[j]; 
        xx[j]    = (float) (j-0.5*dim); 
    }
    if (fill == 2)  
        for (j = 0; j < nspec; j++) {
            vect1[j] -= min1; vect1[j] /= max1-min1+1.0;
            vect2[j] -= min2; vect2[j] /= max2-min2+1.0;
        } 
    else if (fill == 3)  
        for (j = 0; j < nspec; j++) {
            vect1[j] -= max1; vect1[j] /= max1-min1+1.0;
            vect2[j] -= max2; vect2[j] /= max2-min2+1.0;
        } 
    for (j = nspec; j < dim; j++) {
        if (fill != 1) vect1[j] = vect2[j] = 0.0; 
        else { 
            vect1[j] = spec1[nspec-1]; 
            vect2[j] = spec2[nspec-1]; 
        } 
        xx[j]    = xx[j-1]+1.0; 
    }
    correl(vect2, vect1, dim, ans);
    for (j = 0; j < dim/2; j++) {
        ccf[j]       = ans[dim/2+j];
        ccf[dim/2+j] = ans[j];
    }
    free_dvector(vect1, 0, dim-1); free_dvector(vect2, 0, dim-1);
    free_dvector(ans,   0, 2*dim+1);

    max = -BIG; pix = 0; 
    npix = maxshift; if (dim/2 < npix) npix = dim/2;  
    if (npix < 3) npix = 3; 
if (npix < 4) npix = 4; 
if (npix < 5) npix = 5; 
    for (j = dim/2-npix; j < dim/2+npix; j++) 
        if (ccf[j] > max) max = ccf[pix=j]; 

    for (j = 0; j < dim; j++) ccf[j] /= max; 
    centr = xx[pix];
    fwhm  = npix; 
    ew    = fwhm; 
    shift = pix-npix; 
    if (shift < 0) shift = 0; 
    if (pix+npix >= dim) shift = dim-2-2*npix; 
    csq = gaussian_lsfit(xx+shift,ccf+shift,2*npix+1,&centr,&fwhm,&ew,BIG,0);
/*
if (fabs(centr) > npix) { 
*/
if (csq < 0.0) { 
printf(">>> %f %f %d\n", centr, xx[pix], pix); 
for (j = 0; j <= 2*npix; j++) printf("%d %f %f\n", j, xx[shift+j], ccf[shift+j]); 
}

    if (csq < 0.0) { 
        printf("Error in gaussian fit\n"); 
        exit(1); 
    } 
    free_vector(xx, 0, dim-1); free_vector(ccf, 0, dim-1);

    return centr; 
}


/* Spline interpolation of data onto x */ 

void spl(float *x, float *y, int npt, float *lam, float *data, int nspec)
{
    int   k, xin;
    float *y2;
int l, j; 
float step, a, b; 

    y2 = vector(0, nspec-1);
    spline(lam,data,nspec,0.0,0.0,y2);
    for (k = 0; k < npt; k++) { 
        xin = 0; l = nspec-1; 
        while (l-xin > 1) {
            j = (l + xin)/2;
            if (lam[j] > x[k]) l   = j;
            else               xin = j; 
        } 
        step = lam[xin+1]-lam[xin];
        a = (lam[xin+1]-x[k])/step;
        b = (x[k]-lam[xin])/step;
        y[k] = a*data[xin] + b*data[xin+1] +
             ((a*a*a-a)*y2[xin] + (b*b*b-b)*y2[xin+1])*step*step/6.0;
/*
        xin = locate(x[k], lam, nspec);
        splint(lam+xin,data+xin,y2+xin,x[k],y+k);
*/
    } 
    free_vector(y2, 0, nspec-1);
}


/* Linear data interpolation of data onto x */ 

void linear(float *x, float *y, int npt, float *lam, float *data, int nspec)
{
    int   k, xin;
    float slope;                            

    for (k = 0; k < npt; k++) {
        xin = locate(x[k], lam, nspec);
        slope = (x[k]-lam[xin]) / (lam[xin+1]-lam[xin]); 
        y[k] = (data[xin+1]-data[xin])*slope + data[xin];
    }
}


/* Locates value in array */ 

int locate(float x, float array[], int n)
{
    int klo, khi, k, dir;

    klo = 0; 
    khi = n-1; 
    if (array[0] < array[n-1]) 
        dir =  1; 
    else 
        dir = -1; 
    while (khi-klo > 1) {
    k = (khi + klo)/2;
    if (((array[k] > x) && (dir == 1)) || ((array[k] < x) && (dir == -1))) 
        khi = k;
    else 
        klo = k;
    }
/*
    if (fabsf(x-array[klo]) > fabsf(array[khi]-x)) 
        return khi;
    else 
        return klo;
*/
    return klo;
}


/* Gaussian line fit in spectrum */ 

void get_loc(float loc, float *lam, float *spec, int nspec, int npixx, 
             float *centre, float *pix, float *width)
{
    int   pix1, pix0, npix, j, *good, nout;
    float max2, max, tmp, centr, fwhm, ew, moy, dev, step, csq;

    pix1 = locate(loc, lam, nspec);
    if (loc > 0.5*(lam[pix1+1]+lam[pix1])) pix1++;

/* Estimates mean and standard dev in region */
    npix = 100; if (npix > nspec) npix = nspec; 
    pix0 = pix1-npix/2; 
    if (pix0 < 0)             pix0 = 0;  
    if (pix0+npix+1 > nspec ) pix0 = nspec-npix-1;  
    good = ivector(0, npix); 
    for (j = 0; j <= npix; j++) good[j] = 1; 
    do nout = reject(spec+pix0, good, npix+1, &moy, &dev, 2.5, 1);
    while (nout);
    free_ivector(good, 0, npix); 

/* Looks for local maximum */
    npix = npixx;
    pix0 = pix1-npix/2; 
    if (pix0 < 0)             pix0 = 0;  
    if (pix0+npix+1 > nspec ) pix0 = nspec-npix-1;  
    max = -BIG;
    for (j = 0; j <= npix; j++) {
        if (max < spec[j+pix0]) {
            max = spec[j+pix0];
            pix1 = j+pix0;
        }
    }

    if ((max < 5.0*dev+moy) || (pix1 == pix0) || (pix1 == pix0+npix)) { 
        *centre = *pix = *width = -1.0;
        return; 
    }

/* Derives centroid from gaussian fitting */
    npix = npixx;
    pix0 = pix1-npix/2; 
    if ((pix0 < 0) || (pix0+npix+1 > nspec)) {
        *centre = *pix = *width = -1.0;
        return; 
    }
    step = (lam[pix0+npix]-lam[pix0])/npix; 
    loc = lam[pix1];
    for (j = 0; j <= npix; j++) { 
        lam[j+pix0]  -= loc;
        spec[j+pix0] /= max;
    } 
    max2 = -BIG;
    for (j = 0; j <= npix; j++) 
        if (max2 < spec[j+pix0]) max2 = spec[j+pix0];
/*
for (j = 0; j <= npix; j++) printf("%d %f %f\n", j, lam[j+pix0], spec[j+pix0]); 
*/
    centr = 0.0; fwhm  = 2.0*step; ew = fwhm;

    if (max2 <= 1.0001) 
        csq = gaussian_lsfit(lam+pix0,spec+pix0,npix+1,&centr,&fwhm,&ew,BIG,0);

    for (j = 0; j <= npix; j++) { 
        lam[j+pix0]  += loc; 
        spec[j+pix0] *= max;
    }
    centr += loc;

/* Filters bad events */
    if ((max2 > 1.0001) || (ew < 0.0) || (fwhm > 8.0*step) || (fwhm < 0.5*step) || (csq < 0.0))
        *centre = *pix = *width = -1.0;
    else {
        *centre = centr;
        *pix    = (float) pix1 + (centr-loc)/step; 
        *width  = fwhm; 
    }
}


/* Decode arguments of main  */

void parse_args(int argc, char *argv[], int *batch, int *ok_to_plot, int *verbose, 
                int *swap, int *test, int *invpol, int *contpol)
{
    int i;

    *batch = *ok_to_plot = *verbose = *swap = 1; *test = *invpol = *contpol = 0; 
    for (i = 1; i < argc; i++)
        if ((argv[i][0] == '-') && (argv[i][1] == 'b'))
            *batch = 0;
        else if ((argv[i][0] == '-') && (argv[i][1] == 'p'))
            *ok_to_plot = 0;
        else if ((argv[i][0] == '-') && (argv[i][1] == 'v'))
            *verbose = 0;
        else if ((argv[i][0] == '-') && (argv[i][1] == 's'))
            *swap = 0;
        else if ((argv[i][0] == '-') && (argv[i][1] == 't'))
            *test = 1;
        else if ((argv[i][0] == '-') && (argv[i][1] == 'l'))
            *ok_to_plot = 2;
        else if ((argv[i][0] == '-') && (argv[i][1] == 'i'))
            *invpol = 1; 
        else if ((argv[i][0] == '-') && (argv[i][1] == 'c'))
            *contpol = 1; 
        else
            fprintf(stderr,"Warning: unknown command line option %s\n",argv[i]);

#ifdef ASK_IF_BATCH      /* An alternative for systems without command line options */
    if (!*batch) {
        printf("Are you running in batch (y/n) : ");
        scanf("%1s", answer);
        if ((answer[0] == 'Y') || (answer[0] == 'y'))
            *batch = 1;
        else {
            *batch = 0;
            printf("no\n");
        }
    }
#endif

#ifdef ASK_IF_PLOT      /* An alternative for systems without command line options */
    if (*ok_to_plot) {
        printf("Do you want to cancel plots (y/n) : ");
        scanf("%1s", answer);
        if ((answer[0] == 'Y') || (answer[0] == 'y')) {
            *ok_to_plot = 1;
            if (*batch) printf("yes\n");
        } else {
            *ok_to_plot = 0;
            if (*batch) printf("no\n");
        }
    }
#endif

#ifdef ASK_IF_VERBOSE      /* An alternative for systems without command line options */
    if (*verbose) {
        printf("Do you want to run quietly (y/n) : ");
        scanf("%1s", answer);
        if ((answer[0] == 'Y') || (answer[0] == 'y')) {
            *verbose = 1;
            if (*batch) printf("yes\n");
        } else {
            *verbose = 0;
            if (*batch) printf("no\n");
        }
    }
#endif

#ifdef ASK_IF_SWAP      /* An alternative for systems without command line options */
    if (*swap) {
        printf("Do you want to cancel byte swap in FITS files (y/n) : ");
        scanf("%1s", answer);
        if ((answer[0] == 'Y') || (answer[0] == 'y')) {
            *swap = 1;
            if (*batch) printf("yes\n");
        } else {
            *swap = 0;
            if (*batch) printf("no\n");
        }
    }
#endif

#ifdef ASK_IF_TEST      /* An alternative for systems without command line options */
    if (!*test) {
        printf("Do you want test displays (y/n) : ");
        scanf("%1s", answer);
        if ((answer[0] == 'Y') || (answer[0] == 'y')) {
            *test = 1;
            if (*batch) printf("yes\n");
        } else {
            *test = 0;
            if (*batch) printf("no\n");
        }
    }
#endif

#ifdef ASK_IF_INVERT      /* An alternative for systems without command line options */
    if (!*invpol) {
        printf("Do you want to change polarisation sign (y/n) : ");
        scanf("%1s", answer);
        if ((answer[0] == 'Y') || (answer[0] == 'y')) {
            *invpol = 1;
            if (*batch) printf("yes\n");
        } else {
            *invpol = 0;
            if (*batch) printf("no\n");
        }
    }
#endif
}


void indexx(int n, float *arrin, int *indx)
{
    int   j, l, ir, indxt, i;
    float q;

    for (j = 0; j < n; j++) indx[j] = j;
    l = n/2; ir = n-1;
    for (;;) {
        if (l > 0) {
            l--;
            indxt = indx[l];
            q = arrin[indxt];
        } else {
            indxt = indx[ir];
            q = arrin[indxt];
            indx[ir] = indx[0];
            ir--;
            if (ir == 0) {
                indx[0] = indxt;
                break;
            }
        }
        i = l; j = 2*l+1;
        while (j <= ir) {
            if (j < ir) if (arrin[indx[j]] < arrin[indx[j+1]]) j++;
            if (q < arrin[indx[j]]) {
                indx[i] = indx[j];
                i  = j;
                j += j+1;
            } else
                j = ir+1;
        }
        indx[i] = indxt;
    }
    return;
}



