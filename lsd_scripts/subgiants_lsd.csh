#! /bin/csh -x
#run using source epseri.csh m a p g
#
# Initialises parameters for given star 
# 
# get a list of stars from Pascal
# get the data (maybe from Polarbase?) -> put in 1 directory "subgiants" file: starname_date_obs or ?
# what is the format of the data?
# questions: do I need to redo the LSD weights in sum_esp for each star ?
# e.g. geq = 1.208; eeq = 0.562; leq = 570.000; -> look through notes / email about how to do this!
# just write a script that uses a file as input for the foreach command: file should contain star/with/full/path! temperature and LSD template to use -> look at Valenti paper for parameters
# at least if it is necessary to do everything by hand (e.g. geq, eeq and leq parameters) put all the parameters in an input file and then running it again will be automatic.
# 
# 
#
#set jd0 = 2454101.5 
#set period = 11.68
#set true_period = 11.68
#set star = epsiloneri # script searches for this name -- also check epseri
#set ST   =  mask_5000_g4.5md0.1
#set vsin = 2.2
set WORKINGDIR = /home/mertennikolay/Uni/Tutorenjobs/Sandra/polar_database/files_for_Subgiants
set amp = 0.
set pha = 0.
set meanvel = 0.
set cont = 1.

\rm -rf $WORKINGDIR/subgiants/stellar_para_sub.dat
touch $WORKINGDIR/subgiants/stellar_para_sub.dat

\rm -rf $WORKINGDIR/subgiants/subgiant_detections.dat
touch $WORKINGDIR/subgiants/subgiant_detections.dat

\rm -rf $WORKINGDIR/subgiants/Corrine_stellar_para_sub.dat
touch $WORKINGDIR/subgiants/Corrine_stellar_para_sub.dat

set addcode = $WORKINGDIR/add_subgiants

# Looks for option flags
#     $mean  = 1   when working on lsd data (0 otherwise)
#     $add   = 1   when computing lsd profiles in the loop (0 otherwise)
#     $mode  = mod when working on polarisation data (int otherwise)
#     $group = 1   when grouping all data from one run together (0 otherwise)

set i = 1
set mean = 0
set add  = 0
set mode = int
set group = 0

while ($i <= $#argv)
    if ($argv[$i] == m) then
        set mean = 1
        echo "Using mean profiles"
    else if ($argv[$i] == a) then
        set add = 1
        echo "Calculating mean profiles"
    else if ($argv[$i] == p) then
        set mode = pol
        echo "Using polarisation profiles"
    else if ($argv[$i] == g) then
        set group = 1
        echo "Grouping all data together"
    endif
    @ i++
end

if ($mean == 0) then
    set i = $#argv 
    set hi     = $argv[$i]
    @ i--
    set lo     = $argv[$i]
    @ i--
    set lambda = $argv[$i]
    set meanvel = `echo $lambda $meanvel | awk '{print $1+$1*$2/299792.5}'`
endif

if (-e list) \rm list

set run = subgiants

foreach run (subgiants)

cd $run

#set date = 07oct13

ls * >! ls_out.dat

foreach date (`ls`)

echo "I'm now here..." $date
pwd

#        set save = $WORKINGDIR/${star}_lsd/$date/pol
        set save = $WORKINGDIR/subgiants_lsd/

        if !(-e $save:h) mkdir $save:h
        if !(-e $save) mkdir $save

        set dir = $WORKINGDIR/$date
#	set title = $star

	cd $date
	set foo = `ls *.out`
#	echo $foo
	set count = 1  

	foreach j (${foo})
	echo $j $count

	set junk = `echo $j | sed 's/_v_/ /g'`  	
	set star = $junk[1]

	set junk1 = `echo $star | sed 's/hd/HD /g'`
        set val_fischer = `grep "$junk1" $WORKINGDIR/table8_valenti_fischer.dat`
	set temp = $val_fischer[4]
	set logg = $val_fischer[5]
        set vsini = $val_fischer[12]
        
        set val_fischer_t9 = `grep "$junk1" $WORKINGDIR/table9_valenti_fischer.dat`

	set lumin = $val_fischer_t9[13]
	set luminerr = $val_fischer_t9[14]

	set mass1 = $val_fischer_t9[17]
        set mass1err = $val_fischer_t9[18]
	set mass2 = $val_fischer_t9[19]        

	set age = $val_fischer_t9[23]

	set junk2 = `echo $temp | awk '{printf "%4.0f\n", $1/250}' `
        set mask_temp = `echo $junk2 | awk '{print $1*250}'`
  	set junk3 = `echo $logg | awk '{printf "%2.0f\n", $1/0.5}' `
        set junk4 = `echo $junk3 | awk '{print $1*0.5}'` 
	set mask_logg = `echo $junk4 | awk '{printf "%1.1f\n", $1}' `
        set metal = 0.00

        set ST = t$mask_temp:r_g$mask_logg"_m"$metal
        set mask = /home/mertennikolay/Uni/Tutorenjobs/Sandra/polar_database/VALD/VALD_output/$ST

	echo $junk3 $mask_logg $mask_temp $ST $mask

#	set mask_logg = `printf "%.0f\n" "$logg"`

	echo "MASK:" $mask_temp $mask_logg $temp $logg

	cd $WORKINGDIR
	set junk5 = `sed -n "/$mask_temp.*$mask_logg.*hd.*/p" lsd_weights`
	sed "s/yyyy/$junk5[7]/g" sum_subgiants_test.c >! junk
	sed "s/xxxx/$junk5[8]/g" junk >! sum_subgiants.c
	make sum_subgiants

        echo $date $star $temp $logg $mask_temp $mask_logg $junk5[8] >> $WORKINGDIR/subgiants/stellar_para_sub.dat 

	echo "Star = " $star

	@ count++

#        foreach i (`ls $dir/*{$star}*.out`)
#	    echo "***" $i 
            if ($add == 1) then 
		if !(-e $save/${star}) then
		
		set sand = $star:r_lsd
		set file = $j:r.s

		echo $sand

		sed "s/FILE/$file/g" sum_input >! foo
		sed "s/LSDMASK/$ST/g" foo >! foo1
		sed "s/DATE/$date/g" foo1 >! foo2
		sed "s/SAVE/$sand/g" foo2 >! foo3	
		
#		echo "****** DATE =" $date

# Actually doing the LSD
		$WORKINGDIR/sum_subgiants < foo3 >! foo.out

		set detection = `grep "signal detection" foo.out | awk 'NR==2 {print $2}'`
		echo $detection 

		if ($detection == "No") then
			set detect = 1
			else set detect = 2			
		endif

		echo "DETECT =" $detect

                set meansn = `grep 'mean sn =' foo.out | awk 'NR==1 {print $10}'`
	
		echo $star $date $detection $detect $meansn $temp $logg $vsini $lumin $mass1 $mass2 $age >> $WORKINGDIR/subgiants/subgiant_detections.dat
		echo "  "
		echo "Results: " $star $date $detection $detect $vsini $mass1 $mass2 
		echo "  "				

	       echo $star $temp "50" $lumin $luminerr $detection $detect >> $WORKINGDIR/subgiants/Corrine_stellar_para_sub.dat 


#		echo $addcode v $date ${i:t:r} $ST $save/${star}
#                if !(-e $save/${star}) $addcode v $date $j:r $ST $save/${star}
            endif
	    endif
	cd -    

	end 

        cd $WORKINGDIR/subgiants

#        end
    if ($group == 1) end
#end

if ($group == 0) end

if (-e output.ps) \rm output.ps

