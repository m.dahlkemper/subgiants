#include <stdio.h>
#include <math.h>
#include "structures.h"
#include "file.h"
#include "mem_alloc.h"

main()
{
	float *lam, *data, *errdata, fluxha, fluxk, fluxv, fluxr, weightha, weightk, weightv, weightr, 
		  velstar, deltalam, deltalamha, deltalamcont, vel_light, lamha, lamk, lamv, lamr;
	int    i, j, k, npts, n;
	char   filename[80], comment[120];

	read_ascfile(&npts, &n, &lam, &data, 1, filename, comment); 

	lamha = 656.285;
	lamv = 655.885;
	lamr = 656.730;
	deltalamha = .36;
	deltalamcont = .22;
	vel_light = 299792.5;

	errdata = vector(0, npts -1);		
    if (n < 4) errdata = data + npts;
	else errdata = data + 4*npts;

	fluxha = fluxk = fluxv = fluxr = 0.;
	weightha = weightk = weightv = weightr = 0.;

	printf("stellar velocity?\n");
	scanf("%f", &velstar);

	deltalam = velstar/vel_light;
	lamha *= (1. + deltalam);
	lamv *= (1. + deltalam);
	lamr *= (1. + deltalam);
	
	for (j = 0; j < npts; j++) 
	{
	//errdata[j] = 1.;
		if ((lam[j] > lamv - deltalamcont/2.) && (lam[j] < lamv + deltalamcont/2.)) 
		{
			fluxv += data[j]/errdata[j];
			weightv += 1./errdata[j];
		} 

		if ((lam[j] > lamr - deltalamcont/2.) && (lam[j] < lamr + deltalamcont/2.)) 
		{
			fluxr += data[j]/errdata[j];
			weightr += 1./errdata[j];
		}

		if ((lam[j] > lamha - deltalamha/2.) && (lam[j] < lamha + deltalamha/2.)) 
		{
			fluxha += data[j]/errdata[j];
			weightha += 1./errdata[j];
		}

	}

	fluxv /= weightv;
	fluxr /= weightr;
	fluxha /= weightha;
	//printf ("V = %f\nR = %f\nH = %f\nK = %f\nindex = %f\n", fluxv, fluxr, fluxh, fluxk, (-6.39439179e+00*fluxh + 1.64816487e+02*fluxk)/(2.06646020e+01*fluxv + 9.49961109e+01*fluxr)-3.25173833e-02);
	printf ("Haindex = %f\n", fluxha/(fluxv + fluxr));
}
