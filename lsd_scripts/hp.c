#include <stdio.h>
#include <math.h>
#include "structures.h"
#include "file.h"
#include "mem_alloc.h"
#include "smear.h"
#include "lsfit.h"


int locate(float x, float array[], int n);
float ccf(float *spec1, float *spec2, int nspec, float maxshift, int fill); 
static int select_file(int npt, int nfile, float **x); 
void linear(float *x, float *y, int npt, float *lam, float *data, int nspec); 


int main(void)
{
    int    j,k,n,num,num2,open_ok,getname,npt,nspec,ndiv,nspec2,nfile,nfile2,nord;
    float  *lam,*lams,*lam2,moy,step,alf,*x,*xx,xxx,yyy;
    float  *Im,*Is,fac,*spec2,tmp, fac2,width, c1, c2, eps;
    char   filename[80],com[122],answer[2];
    double *specd,coeff[2];
    FILE   *fp; 

    getname = 1;
    do {
        open_ok = read_ascfile(&nspec,&nfile,&lam,&Im,getname,filename,com);
        printf("%s\n", filename); 
        if (open_ok == 1) printf("File not found.  Try again\n");
    } while (open_ok == 1);
/*
    if (nfile > 1) select_file(nspec,nfile,&Im);
*/
if (nfile > 1) num = select_file(nspec,nfile,&Im);
else           num = 0; 
Im += nspec*num; 

    printf("Zmin StatY Exp biN sHi difX rmsQ cRos inV Mul Flip Add Smoo Blur Cut Lc fDiv fmUl Wide Interp\n"); 
    printf("Prof Transl rOt: "); 
    scanf("%1s", answer); 
    printf("%1s\n", answer); 

    if ((answer[0] == 'E') || (answer[0] == 'e')) {
        printf("Expansion factor : "); 
        scanf("%f", &fac); 
        printf("%f\n", fac); 
        for (k = 0; k < nspec; k++) Im[k] = 1.0 - fac*(1.0-Im[k]); 
Im -= nspec*num; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
    } else if ((answer[0] == 'T') || (answer[0] == 't')) {
        printf("Shift (in km/s) : "); 
        scanf("%f", &fac); 
        printf("%f\n", fac); 
        if (lam[0] > 300.0) for (k = 0; k < nspec; k++) lam[k] += fac*lam[k]/299792.5; 
        else                for (k = 0; k < nspec; k++) lam[k] += fac; 
Im -= nspec*num; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
    } else if ((answer[0] == 'X') || (answer[0] == 'x')) {
        Is = vector(0, nspec-1); 
        for (k = 1; k < nspec-1; k++) Is[k] = (Im[k+1]-Im[k-1])/(lam[k+1]-lam[k-1]); 
        Is[0] = Is[1]; Is[nspec-1] = Is[nspec-2]; 
        for (k = 0; k < nspec; k++) Im[k] = Is[k]; 
        free_vector(Is, 0, nspec-1); 
Im -= nspec*num; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
    } else if ((answer[0] == 'N') || (answer[0] == 'n')) {
Im -= nspec*num; 
        printf("Binning : "); 
        scanf("%d", &npt); printf("%d\n", npt); 
        for (k = 0; k < nspec; k += npt)  { 
            for (n = k, fac = 0.0; n < k+npt; n++) fac += lam[n]/npt; 
            lam[k/npt] = fac; 
        } 
        for (j = 0; j < nfile; j++)  
            for (k = 0; k < nspec; k += npt)  { 
                for (n = k, tmp = 0.0; n < k+npt; n++) 
                    if (j == nfile-1) tmp += Im[n+j*nspec]/npt/sqrt((float) npt); 
                    else              tmp += Im[n+j*nspec]/npt; 
                Im[k/npt+nspec/npt*j] = tmp; 
            } 
        save_ascfile(nspec/npt,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
    } else if ((answer[0] == 'H') || (answer[0] == 'h')) {
        printf("Number of orders: "); 
        do { 
            scanf("%d", &nord); printf("%d\n", nord); 
            if (nspec%nord) printf("Number of pts not a multiple of %d\n", nord); 
        } while (nspec%nord); 
        npt = nspec/nord; 
        printf("Shift in pixels : "); 
        scanf("%f", &fac); printf("%f\n", fac); 
        Is = vector(0, nspec-1); 
        x = vector(0, npt-1); xx = vector(0, npt-1); 
        for (k = 0; k < nord; k++) {
            for (j = 0; j < npt; j++) { 
                x[j] = (float) j; xx[j] = (float) j+fac; 
            } 
            linear(xx, Is+k*npt, npt, x, Im+k*npt, npt);
        } 
        for (k = 0; k < nspec; k++) Im[k] = Is[k]; 
        free_vector(x, 0, npt-1); free_vector(xx, 0, npt-1); 
        free_vector(Is, 0, nspec-1); 
Im -= nspec*num; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
    } else if ((answer[0] == 'I') || (answer[0] == 'i')) {
        printf("Number of subdivision : "); 
        scanf("%d", &ndiv); 
        printf("%d\n", ndiv); 
        spec2 =  vector(0,nspec*ndiv-1); 
        specd = dvector(0,nspec*ndiv-1); 
        lam2  =  vector(0,nspec*ndiv-1); 
        step = (lam[nspec-1] - lam[0]) / (nspec-1); 
/*
        for (k = 0; k < nspec; k++) specd[k] = Im[k]; 
        oversample(specd,nspec,ndiv);
*/
        for (k = 0; k < nspec*ndiv; k++) {
            npt = k/ndiv;
            if (npt == nspec-1) tmp = 0.0; 
            else                tmp = Im[npt+1]-Im[npt];
            spec2[k] = Im[npt] + tmp*(k-npt*ndiv)/ndiv; 
            lam2[k]  = lam[0] + step*k/ndiv;
        }
Im -= nspec*num; 
        save_ascfile(nspec*ndiv,1,lam2,spec2,getname,filename,com);
        printf("%s\n", filename); 
        free_vector(spec2,  0, nspec*ndiv-1); 
        free_dvector(specd, 0, nspec*ndiv-1); 
        free_vector(lam2,   0, nspec*ndiv-1); 
    } else if ((answer[0] == 'P') || (answer[0] == 'p')) {
        step = (lam[nspec-1] - lam[0]) / (nspec-1); 
        ndiv = (float) -lam[0]/step + 0.5; 
        npt = nspec-ndiv; 
        if (npt > ndiv+1) npt = ndiv+1; 
        printf("Name of file to save profile to : "); 
        scanf("%s", filename); 
        printf("%s\n", filename); 
        fp = fopen(filename,"w"); 
        for (k = ndiv; k < ndiv+npt; k++) {
            fprintf(fp," %8.5f",Im[k]); 
            if ((k-ndiv)%8 == 7) fprintf(fp,"\n"); 
        }
        if ((k-ndiv)%8 != 0) fprintf(fp,"\n"); 
        for (k = ndiv; k > ndiv-npt; k--) {
            fprintf(fp," %8.5f",Im[k]); 
            if ((ndiv-k)%8 == 7) fprintf(fp,"\n"); 
        }
        if ((ndiv-k)%8 != 0) fprintf(fp,"\n"); 
        fclose(fp); 
    } else if ((answer[0] == 'l') || (answer[0] == 'L')) {
        do {
            open_ok = read_ascfile(&nspec2,&nfile2,&lam2,&spec2,getname,filename,com);
            printf("%s\n", filename); 
            if (open_ok == 1) printf("File not found.  Try again\n");
        } while (open_ok == 1);
        if (nfile2 > 1) num2 = select_file(nspec2,nfile2,&spec2);
        else num2 = 0.0; 
spec2 += num2*nspec2; 
        printf("Coefficients : "); 
        scanf("%f %f", &fac, &fac2); 
        printf("%f %f\n", fac, fac2); 

/*
        for (k = 0; k < nspec; k++) 
            Im[k] = fac*Im[k] + fac2*spec2[k]; 
*/
        for (k = 0; k < nspec; k++) { 
            j = locate(lam[k],lam2,nspec2); 
            alf = (lam[k]-lam2[j])/(lam2[j+1]-lam2[j]); 
            if (alf > 1.0)      alf = 1.0; 
            else if (alf < 0.0) alf = 0.0; 
            tmp = (1.0-alf)*spec2[j]+alf*spec2[j+1]; 
            Im[k] = fac*Im[k] + fac2*tmp; 
        } 
Im -= nspec*num; 
spec2 -= num2*nspec2; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
        free_vector(spec2, 0, nspec2*nfile2-1); 
        free_vector(lam2,  0, nspec2-1); 
    } else if ((answer[0] == 'd') || (answer[0] == 'D')) {
        do {
            open_ok = read_ascfile(&nspec2,&nfile2,&lam2,&spec2,getname,filename,com);
            printf("%s\n", filename); 
            if (open_ok == 1) printf("File not found.  Try again\n");
        } while (open_ok == 1);
        if (nfile2 > 1) num2 = select_file(nspec2,nfile2,&spec2);
        else num2 = 0.0; 
spec2 += num2*nspec2; 
        for (k = 0; k < nspec; k++) 
            if (spec2[k]) Im[k] /= spec2[k];  
            else          Im[k] = 1.0; 
/*
        for (k = 0; k < nspec; k++) { 
            j = locate(lam[k],lam2,nspec2); 
            alf = (lam[k]-lam2[j])/(lam2[j+1]-lam2[j]); 
            if (alf > 1.0)      alf = 1.0; 
            else if (alf < 0.0) alf = 0.0; 
            tmp = (1.0-alf)*spec2[j]+alf*spec2[j+1]; 
            if (tmp) Im[k] /= tmp; 
        } 
*/
Im -= nspec*num; 
spec2 -= num2*nspec2; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
        free_vector(spec2,  0, nspec2*nfile2-1); 
        free_vector(lam2,  0, nspec2-1); 
    } else if ((answer[0] == 'r') || (answer[0] == 'R')) {
        do {
            open_ok = read_ascfile(&nspec2,&nfile2,&lam2,&spec2,getname,filename,com);
            printf("%s\n", filename); 
            if (open_ok == 1) printf("File not found.  Try again\n");
        } while (open_ok == 1);
        if (nfile2 > 1) num2 = select_file(nspec2,nfile2,&spec2);
        else num2 = 0.0; 
spec2 += num2*nspec2; 

        printf("Continuum: "); 
        scanf("%f", &fac); printf("%f\n", fac); 

        for (j = 0; j < nspec; j++) { 
            Im[j] -= fac; 
            spec2[j] -= fac; 
        } 

        tmp = ccf(Im, spec2, nspec, 100.0, 0); 
        printf("Shift: %f pxl\n", tmp); 
        free_vector(spec2,  0, nspec2*nfile2-1); 
        free_vector(lam2,  0, nspec2-1); 
    } else if ((answer[0] == 'q') || (answer[0] == 'Q')) {
        do {
            open_ok = read_ascfile(&nspec2,&nfile2,&lam2,&spec2,getname,filename,com);
            printf("%s\n", filename); 
            if (open_ok == 1) printf("File not found.  Try again\n");
        } while (open_ok == 1);
        if (nfile2 > 1) num2 = select_file(nspec2,nfile2,&spec2);
        else num2 = 0.0; 
spec2 += num2*nspec2; 
        for (j = 0; j < nspec; j++) { 
            tmp = sqrt(Im[j]*Im[j]+spec2[j]*spec2[j]); 
            if (Im[j]+spec2[j]>0.0) Im[j] = tmp; 
            else                    Im[j] = -tmp; 
        } 
Im -= nspec*num; 
spec2 -= num2*nspec2; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
        free_vector(spec2,  0, nspec2*nfile2-1); 
        free_vector(lam2,  0, nspec2-1); 
    } else if ((answer[0] == 'u') || (answer[0] == 'U')) {
        do {
            open_ok = read_ascfile(&nspec2,&nfile2,&lam2,&spec2,getname,filename,com);
            printf("%s\n", filename); 
            if (open_ok == 1) printf("File not found.  Try again\n");
        } while (open_ok == 1);
        if (nfile2 > 1) num2 = select_file(nspec2,nfile2,&spec2);
        else num2 = 0.0; 
spec2 += num2*nspec2; 
        for (k = 0; k < nspec; k++) Im[k] *= spec2[k];  
/*
        for (k = 0; k < nspec; k++) { 
            j = locate(lam[k],lam2,nspec2); 
            alf = (lam[k]-lam2[j])/(lam2[j+1]-lam2[j]); 
            if (alf > 1.0)      alf = 1.0; 
            else if (alf < 0.0) alf = 0.0; 
            Im[k] *= (1.0-alf)*spec2[j]+alf*spec2[j+1]; 
        } 
*/
Im -= nspec*num; 
spec2 -= num2*nspec2; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
        free_vector(spec2,  0, nspec2*nfile2-1); 
        free_vector(lam2,  0, nspec2-1); 
    } else if ((answer[0] == 'z') || (answer[0] == 'Z')) {
        do {
            open_ok = read_ascfile(&nspec2,&nfile2,&lam2,&spec2,getname,filename,com);
            printf("%s\n", filename); 
            if (open_ok == 1) printf("File not found.  Try again\n");
        } while (open_ok == 1);
        if (nfile2 > 1) num2 = select_file(nspec2,nfile2,&spec2);
        else num2 = 0.0; 
spec2 += num2*nspec2; 
        for (k = 0; k < nspec; k++) if (spec2[k] < Im[k]) Im[k] = spec2[k];  
/*
        for (k = 0; k < nspec; k++) { 
            j = locate(lam[k],lam2,nspec2); 
            alf = (lam[k]-lam2[j])/(lam2[j+1]-lam2[j]); 
            if (alf > 1.0)      alf = 1.0; 
            else if (alf < 0.0) alf = 0.0; 
            Im[k] *= (1.0-alf)*spec2[j]+alf*spec2[j+1]; 
        } 
*/
Im -= nspec*num; 
spec2 -= num2*nspec2; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
        free_vector(spec2,  0, nspec2*nfile2-1); 
        free_vector(lam2,  0, nspec2-1); 
    } else if ((answer[0] == 'A') || (answer[0] == 'a')) {
        printf("Constant to be added : "); 
        scanf("%f", &fac); 
        printf("%f\n", fac); 
        for (k = 0; k < nspec; k++) Im[k] += fac; 
Im -= nspec*num; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
    } else if ((answer[0] == 'F') || (answer[0] == 'f')) {
        printf("Constant to multiply wavelength scale : "); 
        scanf("%f", &fac2); 
        printf("%f\n", fac2); 
        printf("Constant to be added to wavelength scale : "); 
        scanf("%f", &fac); 
        printf("%f\n", fac); 
        for (k = 0; k < nspec; k++) lam[k] = lam[k]*fac2+fac; 
Im -= nspec*num; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
    } else if ((answer[0] == 'Y') || (answer[0] == 'y')) {
        adjust(lam,Im,nspec,coeff,1,5.0); 
        printf("Fit: %e %e %e %e\n", coeff[0], coeff[1], coeff[0]+0.5*coeff[1]*(lam[0]+lam[nspec-1]), coeff[1]*(lam[nspec-1]-lam[0])); 
    } else if ((answer[0] == 'V') || (answer[0] == 'v')) {
        for (k = 0; k < nspec; k++) Im[k] = 1.0/Im[k]; 
Im -= nspec*num; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
    } else if ((answer[0] == 'M') || (answer[0] == 'm')) {
        printf("Constant to multiply by : "); 
        scanf("%f", &fac); 
        printf("%f\n", fac); 
        for (k = 0; k < nspec; k++) Im[k] *= fac; 
Im -= nspec*num; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
    } else if ((answer[0] == 'W') || (answer[0] == 'w')) {
        printf("Number of pixels to add on both sides : "); 
        scanf("%d", &npt); 
        printf("%d\n", npt); 
        npt *= 2; 
        Is   = vector(0, nspec+npt-1); 
        lams = vector(0, nspec+npt-1); 
        step = (lam[nspec-1] - lam[0]) / (nspec-1); 
        for (k = 0; k < npt/2; k++) {
            Is[k] = 0.00; 
            lams[k] = lam[0]-step*(npt/2-k); 
        } 
        for (k = 0; k < nspec; k++) {
            Is[k+npt/2] = Im[k]; 
            lams[k+npt/2] = lam[k]; 
        }
        for (k = nspec+npt/2; k < nspec+npt; k++) {
            Is[k] = 0.00; 
            lams[k] = lam[nspec-1]+step*(k-nspec-npt/2+1); 
        }
        save_ascfile(nspec+npt,1,lams,Is,getname,filename,com);
        printf("%s\n", filename); 
        free_vector(lams,0,npt+nspec-1); 
        free_vector(Is,0,npt+nspec-1); 
    } else if ((answer[0] == 'S') || (answer[0] == 's')) {
        printf("Threshold, Smoothing window width (in pix) : "); 
        scanf("%f %d", &fac, &npt); 
        printf("%f %d\n", fac, npt); 
        npt /= 2; if (npt < 1) npt = 1; 
        Is = vector(0, nspec-1); 
        for (k = 0; k < nspec; k++)  
            if (Im[k] >= fac) {
                moy = 0.0; j = 0; 
                for (n = k-npt; n < k+npt; n++) 
                    if ((n >= 0) && (n < nspec)) 
                        if (Im[n] >= fac) {
                            moy += Im[n]; j++; 
                        }
                Is[k] = (float) moy/j; 
            } else 
                Is[k] = Im[k]; 
        for (k = 0; k < nspec; k++) 
            Im[k] = Is[k]; 
        free_vector(Is, 0, nspec-1); 
Im -= nspec*num; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
    } else if ((answer[0] == 'B') || (answer[0] == 'b')) {
        printf("Resolution : "); 
        step = (lam[nspec-1] - lam[0]) / (nspec-1); 
        scanf("%f", &width); 
        printf("%f\n", width); 
        if (lam[nspec/2-1] > 100.0)  width = lam[nspec/2-1]/width; 
        smear(width, nspec, Im, step); 
Im -= nspec*num; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
    } else if ((answer[0] == 'O') || (answer[0] == 'o')) {
        printf("Rotation vel, limb darkening const : "); 
        step = lam[1] - lam[0];  
        scanf("%f %f", &width, &eps); 
        printf("%f %f\n", width, eps); 
        if (lam[1] > 100.0)  width *= lam[1]/299792.5; 
        Is = vector(0, nspec-1); 
        c1 = 2.0*(1.0-eps)/3.1415926/(1.0-eps/3.0)/width; 
        c2 = 0.5*eps/(1.0-eps/3.0)/width; 
printf("%f %f %f %f\n", step, width, c1, c2); 
        for (k = 0; k < nspec; k++) { 
            xxx = k*step/width; 
            yyy = (nspec-k)*step/width; 
            if (xxx <= 1.0) Is[k] = c1*sqrt(1.0-xxx*xxx) + c2*(1.0-xxx*xxx); 
            else if (yyy <= 1.0) Is[k] = c1*sqrt(1.0-yyy*yyy) + c2*(1.0-yyy*yyy);
            else           Is[k] = 0.0; 
        } 
        convolve(nspec,nspec,Im,Is,0.0,step); 
        free_vector(Is, 0, nspec-1); 
Im -= nspec*num; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
    } else if ((answer[0] == 'C') || (answer[0] == 'c')) {
    } else if ((answer[0] == 'C') || (answer[0] == 'c')) {
        printf("Cutting threshold : "); 
        scanf("%f", &fac); 
        printf("%f\n", fac); 
        for (k = 0; k < nspec; k++) if (Im[k] >= fac) Im[k] = 1.0; 
Im -= nspec*num; 
        save_ascfile(nspec,nfile,lam,Im,getname,filename,com);
        printf("%s\n", filename); 
    }
    free_vector(lam,0,nspec-1); 
    free_vector(Im,0,nspec*nfile-1); 
}


int locate(float x, float array[], int n)
{
    int klo, khi, k, dir;

    klo = 0;
    khi = n-1;
    if (array[0] < array[n-1])
        dir =  1;
    else
        dir = -1;
    while (khi-klo > 1) {
        k = (khi + klo)/2;
        if (((array[k] > x) && (dir == 1)) || ((array[k] < x) && (dir == -1)))
            khi = k;
        else
            klo = k;
    }
    return klo;
}


float ccf(float *spec1, float *spec2, int nspec, float maxshift, int fill)
{
    int    dim, j, k, pix, shift, npix, sub;
    float  centr, ew, fwhm, *ccf, max, *xx, min1, min2, max1, max2;
    double *vect1, *vect2, *ans;
    float  *xxx, *yyy;

    dim = 1; while (dim < nspec) dim *= 2;
    xx    =  vector(0, dim-1); ccf   =  vector(0, dim-1);
    vect1 = dvector(0, dim-1); vect2 = dvector(0, dim-1);
    ans   = dvector(0, 2*dim+1);

    min1 = min2 = 1.0e30;
    max1 = max2 = -1.0e30;
    for (j = 0; j < nspec; j++) {
        vect1[j] = spec1[j]; vect2[j] = spec2[j];
        if (spec1[j] < min1) min1 = spec1[j];
        if (spec2[j] < min2) min2 = spec2[j];
        if (spec1[j] > max1) max1 = spec1[j];
        if (spec2[j] > max2) max2 = spec2[j];
        xx[j]    = (float) (j-0.5*dim);
    }
    if (fill == 2)
        for (j = 0; j < nspec; j++) {
            vect1[j] -= min1; vect1[j] /= max1-min1+1.0;
            vect2[j] -= min2; vect2[j] /= max2-min2+1.0;
        }
    else if (fill == 3)
        for (j = 0; j < nspec; j++) {
            vect1[j] -= min1; vect1[j] /= max1-min1+1.0;
            vect2[j] -= min2; vect2[j] /= max2-min2+1.0;
        }
    else if (fill == 3)
        for (j = 0; j < nspec; j++) {
            vect1[j] -= max1; vect1[j] /= max1-min1+1.0;
            vect2[j] -= max2; vect2[j] /= max2-min2+1.0;
        }
    for (j = nspec; j < dim; j++) {
        if (fill != 1) vect1[j] = vect2[j] = 0.0;
        else {
            vect1[j] = spec1[nspec-1];
            vect2[j] = spec2[nspec-1];
        }
        xx[j]    = xx[j-1]+1.0;
    }
    correl(vect2, vect1, dim, ans);
    for (j = 0; j < dim/2; j++) {
        ccf[j]       = ans[dim/2+j];
        ccf[dim/2+j] = ans[j];
    }
    free_dvector(vect1, 0, dim-1); free_dvector(vect2, 0, dim-1);
    free_dvector(ans,   0, 2*dim+1);

    max = -1.0e30; pix = 0;
    npix = maxshift; if (dim/2 < npix) npix = dim/2;
    if (npix < 3) npix = 3;
    for (j = dim/2-npix; j < dim/2+npix; j++)
        if (ccf[j] > max) max = ccf[pix=j];

    npix = 8;
    if (npix > dim/2-1) npix = dim/2-1;

    for (j = 0; j < dim; j++) ccf[j] /= max;
    centr = xx[pix];
    fwhm  = npix;
    ew    = -fwhm;
    shift = pix-npix;
    if (shift < 0) shift = 0;
    if (pix+npix >= dim) shift = dim-2-2*npix;
/*
for (j = 0; j <= 2*npix; j++) printf("%d %f %f\n", j, xx[shift+j], ccf[shift+j]);
*/
    gaussian_lsfit(xx+shift,ccf+shift,2*npix+1,&centr,&fwhm,&ew,1.0e30,0);
    free_vector(xx, 0, dim-1); free_vector(ccf, 0, dim-1);

    return centr;
}


static int select_file(int npt, int nfile, float **x)
{
    int   j, k;
    float *tmp;

    do {
        printf("File to be selected (1-%d) : ", nfile);
        scanf("%d", &k);
    } while ((k > nfile) || (k < 1));
    k--;
return k; 
/*
    tmp = vector(0, npt-1);
    for (j = 0; j < npt; j++) tmp[j] = (*x)[j+k*npt];
    free_vector(*x, 0, npt*nfile-1);
    *x = tmp;
*/
}


void linear(float *x, float *y, int npt, float *lam, float *data, int nspec)
{
    int   k, xin;
    float slope;

    for (k = 0; k < npt; k++) {
        xin = locate(x[k], lam, nspec);
        slope = (x[k]-lam[xin]) / (lam[xin+1]-lam[xin]);
        y[k] = (data[xin+1]-data[xin])*slope + data[xin];
    }
}

