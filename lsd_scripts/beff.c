#include <stdio.h>
#include <math.h>
#include "structures.h"
#include "file.h"
#include "mem_alloc.h"

static void fit(float *lam, float *spec, int *good, int nspec, int sub); 

int main(int argc, char *argv[])
{
    int    getname, nspec, j, k, err, n, nfile, *good, side, jmin, jmax,kkk; 
    char   filename[80], comment[80], answer[2]; 
    float  *lam, *spec, ew, momV, momN, lande, wave, tmp, min, max, *I, *V, *N, 
           e1, e2, e3, *errI, *errV, *errN, cc, BV, BN, eB, eN, mean, tmp2, tmp3; 

    getname = 1; 
    do {
        printf("LSD profiles : \n"); 
        err = read_ascfile(&nspec, &nfile, &lam, &spec, getname, filename, comment);
        printf("%s\n", filename); 
        if (err) 
            printf("File not found.  Try again\n");
        else if (nfile != 6) {
            printf("Wrong file.  Try again\n");
            err = 1;
        } 
    } while (err);
    I = spec; 
    V = spec+2*nspec; 
    N = spec+4*nspec; 
    errI = spec+nspec; 
    errV = spec+3*nspec; 
    errN = spec+5*nspec; 

    printf("Lande factor and wavelength of mean line : "); 
    scanf("%f %f", &lande, &wave); 
    printf("%f %f\n", lande, wave); 

    good = ivector(0, nspec);
    for (j = 0; j < nspec; j++) good[j] = 0;
    tmp = 0.2;
    min = 100.0; max = -100.0; 
    for (j = 0; j < nspec; j++) { 
        if (I[j] < min) min = I[j]; 
        if (I[j] > max) max = I[j]; 
    } 
    //for (j = 0; j < nspec; j++) if (I[j] < max-tmp*(max-min)) good[j] = 1;
    for (j = 0; j < nspec; j++) if (I[j] < .975) good[j] = 1;

    jmin = nspec-1; jmax = 0; 
    for (j = k = 0; j < nspec; j++)
        if (good[j]) { 
            if (j < jmin) jmin = j; 
            if (j > jmax) jmax = j; 
        } 
    side = (float) 0.2*(jmax-jmin+1)+0.5; if (side == 0) side = 1; 
    if (side > 8) side = 8; 
/*
for (j = 0; j < nspec; j++) 
if ((lam[j] >= -90.0) && (lam[j] <= 0)) good[j] = 1; 
else good[j] = 0; side = 0; 
*/
/*
for (j = 0; j < nspec; j++) 
if ((lam[j] >= -200.0) && (lam[j] <= 300)) good[j] = 1; 
else good[j] = 0; 
side = 0; 
for (j = 0; j < nspec; j++) 
if ((lam[j] >= 3.0) && (lam[j] <= 81.0)) good[j] = 1; 
else good[j] = 0; 
for (j = 0; j < nspec; j++) 
if ((lam[j] >= -180.0) && (lam[j] <= 180.0)) good[j] = 1; 
else good[j] = 0; 
for (j = 0; j < nspec; j++) 
if ((lam[j] >= -54.0) && (lam[j] <= 36.0)) good[j] = 1; 
else good[j] = 0; 
side = 0; 
*/

for (kkk = -5; kkk <= 5; kkk++) { 
    for (j = 0; j < nspec; j++) if (good[j] == 2) good[j] = 0;
    for (j = n = 0; j < nspec; n += (good[j] > 0), j++)
        if (good[j] == 0)
            for (k = j-side-kkk; k <= j+side+kkk; k++)
                if ((k >= 0) && (k < nspec))
                    if (good[k] == 1) {
                        good[j] = 2;
                        break;
                    }

    mean = 0.0; 
    jmin = nspec-1; jmax = 0; 
 
    for (j = k = 0; j < nspec; j++)
        if (good[j]) { 
            if (j < jmin) jmin = j; 
            if (j > jmax) jmax = j; 
            mean += lam[j]; 
            k++; 
        } 
    mean /= k; 

    printf("Line span:  %d (out of %d) pxl, [%.0f %.0f] km/s, centre @ %.0f km/s \n", 
            n, nspec, lam[jmin],lam[jmax],mean);

    fit(lam,I,good,nspec,0); 
    fit(lam,V,good,nspec,1); 
    fit(lam,N,good,nspec,1); 

    tmp = 0.0; 

    ew = momV = momN = e1 = e2  = e3 = 0.0; 
    for (j = 0; j < nspec; j++)
        if (good[j]) { 
            tmp2 = lam[j]-mean; 
            tmp3 = sqrt(fabs(1.0-I[j])); 
tmp3 = 1.0; 
            if (fabs(1.0-I[j]) >= tmp*errI[j]) { 
                ew  += tmp3*(1.0-I[j]); 
                e1  += tmp3*tmp3*errI[j]*errI[j]; 
            } 
            if (fabs(V[j]) >= tmp*errV[j]) { 
                momV += tmp3*tmp2*V[j]; 
                e2  += tmp3*tmp3*tmp2*tmp2*errV[j]*errV[j]; 
//printf("%d %f %f %f\n", j, lam[j], V[j], I[j]);
            } 
            if (fabs(N[j]) >= tmp*errN[j]) { 
                momN += tmp3*tmp2*N[j]; 
                e3  += tmp3*tmp3*tmp2*tmp2*errN[j]*errN[j]; 
            } 
/*
printf("%d %f %f %f %f\n", j, lam[j], I[j], V[j], momV); 
*/
        } 
/*
e1 = 0.0; ew = 3.8/5.0; 
*/
    cc = 4.67e-12*2.9979e5; 
    BV = -momV/ew/lande/wave/cc; 
printf("%f\n", 1./lande/wave/cc);
    BN = -momN/ew/lande/wave/cc; 
    eB =  sqrt(e1*momV*momV+e2*ew*ew)/ew/ew/lande/wave/cc; 
    eN =  sqrt(e1*momN*momN+e3*ew*ew)/ew/ew/lande/wave/cc; 
/*
printf("%f %f %f\n", ew, momV, momN); 
*/
    printf("    Beff > using V : %.1f +- %.1f G,  using N : %.1f +- %.1f G\n", BV, eB, BN, eN); 
} 

    free_vector(lam,   0, nspec-1); 
    free_ivector(good,   0, nspec-1); 
    free_vector(spec,  0, nfile*nspec-1); 
}


static void fit(float *lam, float *spec, int *good, int nspec, int sub)
{
    float sx, sx2, sy, sxy, a, b, d, mean, ww, tmp; 
    int   j, n, k; 

    mean = 0.0; 
    for (j = k = 0; j < nspec; j++)
        if (good[j]) { 
            mean += lam[j]; 
            k++; 
        } 
    mean /= k; 

    tmp = sx = sy = sx2 = sxy = 0.0; 
    for (j = n = 0; j < nspec; j++) 
        if (!good[j]) { 
            ww  = lam[j]-mean; ww = 1.0/ww/ww; 
            sx  += ww*lam[j]; 
            sy  += ww*spec[j]; 
            sx2 += ww*lam[j]*lam[j]; 
            sxy += ww*lam[j]*spec[j]; 
            tmp += ww; 
        } 
    d = tmp*sx2-sx*sx; 
    a = (tmp*sxy-sx*sy)/d; b = (sx2*sy-sx*sxy)/d; 

    if (sub) for (j = 0; j < nspec; j++) spec[j] -= a*lam[j]+b; 
    else     for (j = 0; j < nspec; j++) spec[j] /= a*lam[j]+b; 
/*
printf("%f %f %f\n", a, b, d); 
*/
}
