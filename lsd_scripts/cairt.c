#include <stdio.h>
#include <math.h>
#include "structures.h"
#include "file.h"
#include "mem_alloc.h"

main()
{
	float *lam, *data, *errdata, fluxca1, fluxca2, fluxca3, fluxv, fluxr, weightca1, weightca2, weightca3, weightv, weightr, 
		  velstar, deltalam, deltalamca, deltalamcont, vel_light, lamca1, lamca2, lamca3, lamv, lamr;
	int    i, j, k, npts, n;
	char   filename[80], comment[120];

	read_ascfile(&npts, &n, &lam, &data, 1, filename, comment); 

	lamca1 = 849.8023;
	lamca2 = 854.2091;
	lamca3 = 866.2141;
	lamv = 847.58;
	lamr = 870.49;
	deltalamca = .2;
	deltalamcont = .5;
	vel_light = 299792.5;

	errdata = vector(0, npts -1);		
    if (n < 4) errdata = data + npts;
	else errdata = data + 4*npts;

	fluxca1 = fluxca2 = fluxca3 = fluxv = fluxr = 0.;
	weightca1 = weightca2 = weightca3 = weightv = weightr = 0.;

	printf("stellar velocity?\n");
	scanf("%f", &velstar);

	deltalam = velstar/vel_light;
	lamca1 *= (1. + deltalam);
	lamca2 *= (1. + deltalam);
	lamca3 *= (1. + deltalam);
	lamv *= (1. + deltalam);
	lamr *= (1. + deltalam);
	
	for (j = 0; j < npts; j++) 
	{
	//errdata[j] = 1.;
		if ((lam[j] > lamv - deltalamcont/2.) && (lam[j] < lamv + deltalamcont/2.)) 
		{
			fluxv += data[j]/errdata[j];
			weightv += 1./errdata[j];
		} 

		if ((lam[j] > lamr - deltalamcont/2.) && (lam[j] < lamr + deltalamcont/2.)) 
		{
			fluxr += data[j]/errdata[j];
			weightr += 1./errdata[j];
		}

		if ((lam[j] > lamca1 - deltalamca/2.) && (lam[j] < lamca1 + deltalamca/2.)) 
		{
			fluxca1 += data[j]/errdata[j];
			weightca1 += 1./errdata[j];
		}

		if ((lam[j] > lamca2 - deltalamca/2.) && (lam[j] < lamca2 + deltalamca/2.)) 
		{
			fluxca2 += data[j]/errdata[j];
			weightca2 += 1./errdata[j];
		}

		if ((lam[j] > lamca3 - deltalamca/2.) && (lam[j] < lamca3 + deltalamca/2.)) 
		{
			fluxca3 += data[j]/errdata[j];
			weightca3 += 1./errdata[j];
		}
	}

	fluxv /= weightv;
	fluxr /= weightr;
	fluxca1 /= weightca1;
	fluxca2 /= weightca2;
	fluxca3 /= weightca3;
	//printf ("V = %f\nR = %f\nH = %f\nK = %f\nindex = %f\n", fluxv, fluxr, fluxh, fluxk, (-6.39439179e+00*fluxh + 1.64816487e+02*fluxk)/(2.06646020e+01*fluxv + 9.49961109e+01*fluxr)-3.25173833e-02);
	printf ("caIRTindex = %f\n", (fluxca1 + fluxca2 + fluxca3)/(fluxv + fluxr));
}
