/*

This package of routines uses the dynamic memory allocations
of C to allocate and free memory for the data arrays.

*/

#define COMPILING_MEM_ALLOC
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include "mem_alloc.h"

float *vector(nl,nh)
int nl,nh;
{
    float *v;

    v = (float *) malloc((unsigned) (nh-nl+1)*sizeof(float));
    if (!v) {
        fprintf(stderr,"Unable to allocate sufficient space for vector\n");
        exit(1);
    }

    return v-nl;
}


float **pfvector(nl,nh)
int nl,nh;
{
    float **v;

    v = (float **) malloc((unsigned) (nh-nl+1)*sizeof(float *));
    if (!v) {
        fprintf(stderr,"Unable to allocate sufficient space for pfvector\n");
        exit(1);
    }

    return v-nl;
}


void *svector(length,size)
int length,size;
{
    float *v;

    v = (void *) malloc((unsigned) (length*size));
    if (!v) {
        fprintf(stderr,"Unable to allocate sufficient space for svector\n");
        exit(1);
    }

    return v;
}


double *dvector(nl,nh)
int nl,nh;
{
    double *v;

    v = (double *) malloc((unsigned) (nh-nl+1)*sizeof(double));
    if (!v) {
        fprintf(stderr,"Unable to allocate sufficient space for dvector\n");
        fprintf(stderr,"nl = %d, nh = %d\n",nl,nh);
        exit(1);
    }

    return v-nl;
}


int *ivector(nl,nh)
int nl,nh;
{
    int *v;

    v = (int *) malloc((unsigned) (nh-nl+1)*sizeof(int));
    if (!v) {
        fprintf(stderr,"Unable to allocate sufficient space for ivector\n");
        exit(1);
    }

    return v-nl;
}


float **matrix(nrl,nrh,ncl,nch)
int nrl,nrh,ncl,nch;
{
    int   i;
    float **m;

    m = (float **) malloc((unsigned) (nrh-nrl+1)*sizeof(float *));
    if (!m) {
        fprintf(stderr,"Unable to allocate sufficient space for matrix\n");
        exit(1);
    }
    m -= nrl;

    for (i = nrl; i <= nrh; i++) {
        m[i] = (float *) malloc((unsigned) (nch-ncl+1)*sizeof(float));
        if (!m[i]) {
            fprintf(stderr,"Unable to allocate sufficient space for matrix\n");
            exit(1);
        }
        m[i] -= ncl;
    }

    return m;
}


double **dmatrix(nrl,nrh,ncl,nch)
int nrl,nrh,ncl,nch;
{
    int    i;
    double **m;

    m = (double **) malloc((size_t) (nrh-nrl+1)*sizeof(double *));
    if (!m) {
        fprintf(stderr,"Unable to allocate sufficient space for dmatrix\n");
        exit(1);
    }
    m -= nrl;

    for (i = nrl; i <= nrh; i++) {
        m[i] = (double *) malloc((size_t) (nch-ncl+1)*sizeof(double));
        if (!m[i]) {
            fprintf(stderr,"Unable to allocate sufficient space for dmatrix\n");
            exit(1);
        }
        m[i] -= ncl;
    }

    return m;
}


int **imatrix(nrl,nrh,ncl,nch)
int nrl,nrh,ncl,nch;
{
    int  i;
    int  **m;

    m = (int **) malloc((unsigned) (nrh-nrl+1)*sizeof(int *));
    if (!m) {
        fprintf(stderr,"Unable to allocate sufficient space for imatrix\n");
        exit(1);
    }
    m -= nrl;

    for (i = nrl; i <= nrh; i++) {
        m[i] = (int *) malloc((unsigned) (nch-ncl+1)*sizeof(int));
        if (!m[i]) {
            fprintf(stderr,"Unable to allocate sufficient space for imatrix\n");
            exit(1);
        }
        m[i] -= ncl;
    }

    return m;
}


float ***matrix3D(n1l,n1h,n2l,n2h,n3l,n3h)
int n1l,n1h,n2l,n2h,n3l,n3h;
{
    int   i,j;
    float ***m;

    m = (float ***) malloc((unsigned) (n1h-n1l+1)*sizeof(float **));
    if (!m) {
        fprintf(stderr,"Unable to allocate sufficient space for array3D\n");
        exit(1);
    }
    m -= n1l;

    for (i = n1l; i <= n1h; i++) {
        m[i] = (float **) malloc((unsigned) (n2h-n2l+1)*sizeof(float *));
        if (!m[i]) {
            fprintf(stderr,"Unable to allocate sufficient space for array3D\n");
            exit(1);
        }
        m[i] -= n2l;

        for (j = n2l; j <= n2h; j++) {
            m[i][j] = (float *) malloc((unsigned) (n3h-n3l+1)*sizeof(float));
            if (!m[i][j]) {
                fprintf(stderr,"Unable to allocate sufficient space for array3D\n");
                exit(1);
            }
            m[i][j] -= n3l;
        }
    }

    return m;
}


float ****matrix4D(n1l,n1h,n2l,n2h,n3l,n3h,n4l,n4h)
int n1l,n1h,n2l,n2h,n3l,n3h,n4l,n4h;
{
    int   i,j,k;
    float ****m;

    m = (float ****) malloc((unsigned) (n1h-n1l+1)*sizeof(float ***));
    if (!m) {
        fprintf(stderr,"Unable to allocate sufficient space for array4D\n");
        exit(1);
    }
    m -= n1l;

    for (i = n1l; i <= n1h; i++) {
        m[i] = (float ***) malloc((unsigned) (n2h-n2l+1)*sizeof(float **));
        if (!m[i]) {
            fprintf(stderr,"Unable to allocate sufficient space for array4D\n");
            exit(1);
        }
        m[i] -= n2l;

        for (j = n2l; j <= n2h; j++) {
            m[i][j] = (float **) malloc((unsigned) (n3h-n3l+1)*sizeof(float *));
            if (!m[i][j]) {
                fprintf(stderr,"Unable to allocate sufficient space for array4D\n");
                exit(1);
            }
            m[i][j] -= n3l;

            for (k = n3l; k <= n3h; k++) {
                m[i][j][k] = (float *) malloc((unsigned) (n4h-n4l+1)*sizeof(float));
                if (!m[i][j][k]) {
                    fprintf(stderr,"Unable to allocate sufficient space for array4D\n");
                    exit(1);
                }
                m[i][j][k] -= n4l;
            }
        }
    }

    return m;
}


void free_vector(v,nl,nh)
float *v;
int nl,nh;
{
    free((void *) (v+nl));
}


void free_pfvector(v,nl,nh)
float **v;
int nl,nh;
{
    free((void *) (v+nl));
}


void free_svector(v)
void *v;
{
    free((void *) v);
}


void free_dvector(v,nl,nh)
double *v;
int nl,nh;
{
    free((void *) (v+nl));
}


void free_ivector(v,nl,nh)
int *v;
int nl,nh;
{
    free((void *) (v+nl));
}


void free_matrix(m,nrl,nrh,ncl,nch)
float **m;
int nrl,nrh,ncl,nch;
{
    int i;

    for (i = nrh; i >= nrl; i--)
        free((void *) (m[i]+ncl));
    free((void *) (m+nrl));
}


void free_dmatrix(m,nrl,nrh,ncl,nch)
double **m;
int nrl,nrh,ncl,nch;
{
    int i;

    for (i = nrh; i >= nrl; i--)
        free((void *) (m[i]+ncl));
    free((void *) (m+nrl));
}


void free_imatrix(m,nrl,nrh,ncl,nch)
int **m;
int nrl,nrh,ncl,nch;
{
    int i;

    for (i = nrh; i >= nrl; i--)
        free((void *) (m[i]+ncl));
    free((void *) (m+nrl));
}


void free_matrix3D(m,n1l,n1h,n2l,n2h,n3l,n3h)
float ***m;
int n1l,n1h,n2l,n2h,n3l,n3h;
{
    int i,j;

    for (i = n1h; i >= n1l; i--) {
        for (j = n2h; j >= n2l; j--)
            free((void *) (m[i][j]+n3l));
        free((void *) (m[i]+n2l));
    }
    free((void *) (m+n1l));
}


void free_matrix4D(m,n1l,n1h,n2l,n2h,n3l,n3h,n4l,n4h)
float ****m;
int n1l,n1h,n2l,n2h,n3l,n3h,n4l,n4h;
{
    int i,j,k;

    for (i = n1h; i >= n1l; i--) {
        for (j = n2h; j >= n2l; j--) {
            for (k = n3h; k >= n3l; k--)
                free((void *) (m[i][j][k]+n4l));
            free((void *) (m[i][j]+n3l));
        }
        free((void *) (m[i]+n2l));
    }
    free((void *) (m+n1l));
}
