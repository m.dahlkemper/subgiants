#include <stdio.h>
#include <math.h>
#include "mem_alloc.h"
#include "lsfit.h"
#include "util.h"
#include "smear.h"
#include "invert.h"
#include "plot.h"
#include "prob.h"

#define NELEM 99
#define NTEL 6
#define BIG 1.0e30

static int plot_dev, comments, test_dsp; 
static float wtl[NTEL] = {627, 686, 716, 759, 813, 895}; 
/*
static float wtl[NTEL] = {632, 697, 734, 770, 835, 986}; 
*/
static float wtu[NTEL] = {632, 697, 734, 770, 835, 986}; 
static char names[NELEM][3] = {"H ","He","Li","Be","B ","C ","N ","O ","F ","Ne",
                               "Na","Mg","Al","Si","P ","S ","Cl","Ar","K ","Ca",
                               "Sc","Ti","V ","Cr","Mn","Fe","Co","Ni","Cu","Zn",
                               "Ga","Ge","As","Se","Br","Kr","Rb","Sr","Y ","Zr",
                               "Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn",
                               "Sb","Te","I ","Xe","Cs","Ba","La","Ce","Pr","Nd",
                               "Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb",
                               "Lu","Hf","Ta","W ","Re","Os","Ir","Pt","Au","Hg",
                               "Tl","Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th",
                               "Pa","U ","Np","Pu","Am","Cm","Bk","Cf","Es"};

static float fit(float *x, float *y, int n, int deg, int ok_to_sub, float maxdev); 
static void get_psf(float **R, float **A, float **T, float *X, float *Y, float *S, int ntot, 
                    float *P, float *err, int nvel, float maxdev, int **ind, int *nn); 
static void fill(float **R, float **A, float **S, int ntot, int *first, int *last, float *vel, 
                 float *ll, float *ss, int nvel, int *fpix, int *lpix, double *wave, 
                 float *ww, int *index, int *used, int nsel, int **ind, int *nn); 

static float cc = 299792.5; 
static int   side = 5; 
static float resol; 



void get_resol(float *lam, int nspec, int **nppo, int *nord, float *stepvel)
{
    int j, k, n; 
    float tmp, mean, low=0.70, high=1.30; 

    if (comments) printf("Looking for mean pixel separation...\n"); 
    mean = resol = 0.0; 
    for (j = n = 0; j < nspec-1; j++) { 
        tmp = 2.0*(lam[j+1]-lam[j])/(lam[j+1]+lam[j]); 
        if ((j == 0) || ((tmp >= low*resol) && (tmp <= high*resol))) {  
            mean += tmp; 
            n++; 
            resol = mean/n; 
        } 
    } 
    *nord = j-n+1; 
 
    *nppo = ivector(0, *nord-1); 
    for (j = n = k = 0; j < nspec-1; j++) { 
        tmp = 2.0*(lam[j+1]-lam[j])/(lam[j+1]+lam[j]); 
        if ((j == nspec-2) || ((tmp < low*resol) || (tmp > high*resol))) {  
            if (j == nspec-2) j++; 
            (*nppo)[n] = j+1-k; 
            if (comments) { 
                printf("   Order #%2d spans %4d pxl (p0 = %5d, lam = [%.4f %.4f] nm",
                       n,j+1-k,k,lam[k],lam[j]); 
                if (j < nspec-2) printf(", dlam = %.4f nm)\n", lam[j+1]-lam[j]); 
                else             printf(")\n"); 
            } 
            k = j+1; n++; 
        } 
    } 
    if (comments) printf("   Mean 1 pxl resolution: %.0f (%.3f km/s)\n",1.0/resol,cc*resol); 
/*
    if (*stepvel < cc*resol) { 
        if (comments) printf("   WARNING: velocity step set to pxl size\n"); 
        *stepvel = cc*resol; 
    } 
*/
    if (comments) printf("\n"); 
    
}


void get_lines(float *lam, int nspec, int *nppo, int nord, double *wave, int *tobeused, 
               int nlines, int **index, int **pix, int **used, int *ntot) 
{
    int j, k, m, n; 

    if (comments) 
        printf("Listing available stellar lines... \n"); 

    for (k = *ntot = n = 0; k < nord; n += nppo[k], k++)   
        for (j = 0; j < nlines; j++) 
            if ((tobeused[j]) && (wave[j] >= lam[n+side]) && 
                (wave[j] <= lam[n+nppo[k]-1-side])) (*ntot)++; 
 
    *index = ivector(0, *ntot-1); 
    *pix   = ivector(0, *ntot-1); 
    *used  = ivector(0, *ntot-1); 
    for (k = m = n = 0; k < nord; n += nppo[k], k++)   
        for (j = 0; j < nlines; j++) 
            if ((tobeused[j]) &&  (wave[j] >= lam[n+side]) && 
                (wave[j] <= lam[n+nppo[k]-1-side]) && (m < *ntot)) { 
                (*index)[m] = j; 
                (*pix)[m]   = locate(wave[j],lam+n,nppo[k])+n; 
                (*used)[m]  = 1.0; 
                m++; 
            } 
}


void cont_pol(float *lam, float *specv, float *specn, int *nppo, int nord, int pol) 
{
    int j, ntot; 

    if (comments) 
        printf("Removing continuum polarisation... \n"); 

    for (j = ntot = 0; j < nord; ntot += nppo[j], j++) { 
        fit(lam+ntot, specv+ntot, nppo[j], 2, 1, 5);
        if (pol == 1) fit(lam+ntot, specn+ntot, nppo[j], 2, 1, 5);
    } 
}


void select_lines(float *lam, int nspec, double *wave, int *index, int *pixc, 
                  int *used, int ntot, int *nppo, int nord, float liml, float limu, 
                  int *fpix, int *lpix, float *vel, int nvel, int *first, int *last) 
{
    int    j, k, kk, n, pix, pixl, pixu; 
    float  xl, xu, xxl, xxu, tmp; 
    double www; 

    if (comments) 
        printf("Selecting stellar lines with [%.1f %.1f] km/s field... \n",liml,limu); 

    for (j = 0; j < ntot; j++) 
        if (used[j]) { 
            pix = pixc[j]; 
            www = wave[index[j]]; 
            xl = www*(liml/cc+1.0);
            xu = www*(limu/cc+1.0);
            for (k = n = 0; n+nppo[k] <= pix; n += nppo[k], k++);  
            if ((xl >= lam[n+side]) && (xu <= lam[n+nppo[k]-1-side])) {
                for (kk = 0; (wtu[kk] < xl) && (kk < NTEL) ; kk++);  
                if (kk == NTEL) kk--; 
                    xxu = www*(vel[nvel-1]/cc+1.0);
                if ((xl > wtu[kk]) || (xu < wtl[kk])) {
                    xxl = www*(vel[0]/cc+1.0);
                    xxu = www*(vel[nvel-1]/cc+1.0);
                    if (xxl < lam[n+side])           xxl = lam[n+side]; 
                    if (xxu > lam[n+nppo[k]-1-side]) xxu = lam[n+nppo[k]-1-side]; 
                    pixl = pixu = pix;
                    while (lam[pixl] > xxl) pixl--; pixl++; 
                    while (lam[pixu] < xxu) pixu++; pixu--; 
                    fpix[j] = pixl;
                    lpix[j] = pixu;
                    first[j] = locate(tmp=(lam[pixl]/www-1.0)*cc, vel, nvel);
                    last[j]  = locate(tmp=(lam[pixu]/www-1.0)*cc, vel, nvel)+1;
                } else 
                    used[j] = 0; 
            } else 
                used[j] = 0; 
        }  
}


void get_limits(float *vel, int nvel, float *lam, float *spec, int nspec, 
                double *wave, float *prof, int *index, int *fpix, int *lpix, 
                int *used, int *first, int *last, int ntot, float *liml, float *limu, 
                float threshold)
{
    int   j, k, pixl, pixu, n, *good, nout, min, max; 
    float xl, xu, *mean, *vv, *yy, ymin, ymax, stepvel, moy, dev, tmp, 
          maxdev, maxx, *ss, *den; 

    if (comments) printf("Localising stellar lines in velocity space... \n"); 

    min = nvel-1; max = 0; 
    for (j = 0; j < ntot; j++) 
        if (used[j]) { 
            if (min > first[j]) min = first[j]; 
            if (max < last[j])  max = last[j]; 
        } 
    mean = vector(0, max-min); 
    yy   = vector(0, max-min); 
    den  = vector(0, max-min); 

    maxdev = 10.0; 
    for (k = min; k <= max; k++) mean[k-min] = den[k-min] = 0.0; 
    for (j = 0; j < ntot; j++) 
        if (used[j]) {
            pixl = fpix[j]-1; 
            pixu = lpix[j]+1; 
            vv   =  vector(0, pixu-pixl); 
            ss   =  vector(0, pixu-pixl); 
            good = ivector(0, pixu-pixl); 
            for (k = 0; k <= pixu-pixl; k++) good[k] = 1; 
            for (k = 0; k <= pixu-pixl; k++) { 
                vv[k] = (lam[k+pixl]/wave[index[j]]-1.0)*cc;
                ss[k] = spec[k+pixl]; 
            } 
            reject(ss,good,pixu-pixl+1,&moy,&dev,maxdev,pixu-pixl+1);
            do { 
                moy = dev = maxx = 0.0;  
                for (k = n = 0; k <= pixu-pixl; k++) 
                    if (good[k]) { 
                        moy += ss[k]; 
                        dev += ss[k]*ss[k]; 
                        n++; 
                    } 
                moy /= n; dev = sqrt(dev/n-moy*moy); 
                nout = -1; 
                for (k = 0; k <= pixu-pixl; k++) 
                    if ((good[k]) && (ss[k] > moy+dev)) { 
                        tmp = 0.0;
                        for (n = -1; n+k >= 0; n--) if (good[n+k]) break;
                        if (n+k >= 0) tmp += ss[k]-ss[n+k];
                        for (n = 1; n+k <= pixu-pixl; n++) if (good[n+k]) break;
                        if (n+k <= pixu-pixl) tmp += ss[k]-ss[n+k];
                        if ((tmp > maxx) && (tmp > 3.0*dev)) {
                            maxx = tmp;
                            nout = k;
                        }
                    } 
                if (nout > 0) { 
                    good[nout] = 0;
                    ss[nout] = 1.0; 
                } 
            } while (nout > 0); 
            spl(vel+first[j],yy+first[j]-min,last[j]-first[j]+1,vv,ss,pixu-pixl+1);
            for (k = first[j]; k <= last[j]; k++) { 
                mean[k-min] += prof[index[j]]*yy[k-min]; 
                den[k-min]  += prof[index[j]]; 
            } 

            free_vector(vv,    0, pixu-pixl); 
            free_vector(ss,    0, pixu-pixl); 
            free_ivector(good, 0, pixu-pixl); 
        } 
    for (k = min; k <= max; k++) mean[k-min] /= den[k-min]; 

    minmax(mean, max-min+1, &ymin, &ymax);
    *liml = vel[max]; *limu = vel[min];
    for (k = min; k <= max; k++) 
        if (mean[k-min] < ymax-threshold*(ymax-ymin)) {
            if (*liml > vel[k]) *liml = vel[k];
            if (*limu < vel[k]) *limu = vel[k];
        }
    stepvel = (vel[nvel-1]-vel[0])/(nvel-1); 
    *liml -= 5.0*stepvel; if (*liml < vel[0])      *liml = vel[0];
    *limu += 5.0*stepvel; if (*limu > vel[nvel-1]) *limu = vel[nvel-1];

    if (comments) printf("   Stellar lines span %.1f km/s to %.1f km/s\n", *liml, *limu); 
    free_vector(mean, 0, max-min); 
    free_vector(yy,   0, max-min); 
    free_vector(den,  0, max-min); 
}


void get_weights(float *I, float *err, int nspec, double *wave, float *code, float *prof, 
                 float *lande, int *used, float *wwi, float *wwv, float *sn, 
                 int *index, int *fpix, int *lpix, int ntot, int choice, float equ, float eqi) 
{ 
    int    j, k, n, pixl, pixu; 
    float  ptot, dd, tmp; 
    double ww; 

    if (comments) printf("Listing line weights...\n"); 

    for (j = n = 0; j < ntot; j++) 
        if (used[j]) {
            ww = wave[index[j]]; 
            dd = prof[index[j]]; 
            ptot = -dd; 
            for (k = j; k >= 0; k--) 
                if (used[k]) { 
                    if (fabs(1.0-wave[index[k]]/ww)*cc > 5.0) break;  
                    else ptot += prof[index[k]]; 
                } 
            for (k = j; k < ntot; k++) 
                if (used[k]) { 
                    if (fabs(wave[index[k]]/ww-1.0)*cc > 5.0) break;  
                    else ptot += prof[index[k]]; 
                } 
            if (ptot > 1.0) { 
                dd *= 1.0/ptot; 
                if (comments) printf(" * "); 
            } else 
                if (comments) printf("   "); 

            wwi[j] = dd/eqi; 
            if (choice == 0)      wwv[j] = lande[index[j]];
            else if (choice == 1) wwv[j] = dd*lande[index[j]];
            else if (choice == 2) wwv[j] = 0.002*ww*dd*lande[index[j]];
            else if (choice == 3) wwv[j] = dd;
            else if (choice == 4) wwv[j] = 0.002*ww*dd;
            else if (choice == 5) wwv[j] = 1.0;
            else if (choice == 6) wwv[j] = 0.000004*ww*dd*lande[index[j]]*lande[index[j]]*ww;
            wwv[j] /= equ; 
            k = code[index[j]]; 
            if (comments)
                printf("%2s %.0f line #%3d-%4d (@ %.4f nm, d: %.3f, g: %.3f, w: %.3f): ", 
                       names[k-1],100.0*(code[index[j]]-k)+1.0,n+1,
                       index[j]+1,ww,dd,lande[index[j]],wwv[j]);

            pixl = fpix[j]; 
            pixu = lpix[j]; 
            sn[j] = 1.0; 
//            for (k = pixl; k <= pixu; k++) sn[j] += 1.0/err[k]/(pixu-pixl+1); 
            for (k = pixl; k <= pixu; k++) { 
                if ((I[k] > 0.0) && (err[k] > 0.0)) sn[j] += sqrt(I[k])/err[k]/(pixu-pixl+1); 
                else I[k] = err[k] = 1.0; 
//printf("%d   %d %f %f    %f\n", j, k, I[k], err[k], sn[j]); 
               
            } 
            if (comments) printf("SN = %4.0f\n", sn[j]);
            n++;
        } 

    if (comments) printf("\n"); 
} 


void extract_spec(float *lam, float *spec, float *specv, float *specn, float *err, int nspec,
                  int *used, int *fpix, int *lpix, int nsel, int pol, float **ll, float **ii, 
                  float **vv, float **nn, float **ss, int *ntot)
{
    int   j, k, n, last, first, npix; 

    if (comments) printf("Extracting core spectrum...\n"); 

    for (j = last = *ntot = 0; j < nsel; j++) 
        if (used[j]) { 
            if (fpix[j] > last) *ntot += lpix[j]-fpix[j]+1; 
            else                *ntot += lpix[j]-last; 
            last = lpix[j]; 
        } 

    *ll = vector(0, *ntot-1); 
    *ii = vector(0, *ntot-1); 
    *ss = vector(0, *ntot-1); 
    if (pol) { 
        *vv = vector(0, *ntot-1); 
        *nn = vector(0, *ntot-1); 
    } 

    for (j = last = n = 0; j < nsel; j++) 
        if (used[j]) { 
            if (fpix[j] > last) first = fpix[j];  
            else                first = last+1; 
            last = lpix[j]; 
            for (k = first; k <= last; k++, n++) { 
                (*ll)[n] = lam[k]; 
                (*ii)[n] = spec[k]; 
                (*ss)[n] = 1.0/err[k]; 
if (spec[k] > 0.0) (*ss)[n] *= sqrt(spec[k]); 
/*
(*ss)[n] = 200.0; 
*/
                if (pol) { 
                    (*vv)[n] = specv[k]; 
                    (*nn)[n] = specn[k]; 
                } 
            } 
            npix = lpix[j]-fpix[j]+1; 
            lpix[j] = n-1; 
            fpix[j] = lpix[j]-npix+1; 
        } 
}


void ls_dcv(float *vel, int nvel, float *ll, float *ii, float *vv, float *nn, 
            float *ss, int ntot, int *fpix, int *lpix, int *used, 
            float *wwi, float *wwv, double *wave, int *index, int *first, int *last, 
            int nsel, int pol, float *mean, float *meanv, float *meann, float *errI, float *errV, 
            float *errN) 
{
    int   j, min, max, **ind, *nnn; 
    float **R, **A, **S, maxdev; 

    if (comments)  printf("Least square deconvolution of spectrum...\n"); 

    min = nvel-1; max = 0; 
    for (j = 0; j < nsel; j++)
        if (used[j]) { 
            if (first[j] < min) min = first[j];
            if (last[j]  > max) max = last[j];
    } 

    maxdev = 50.0; 
    R = matrix(0, ntot-1, 0, max-min);
    S = matrix(0, ntot-1, 0, max-min);
    A = matrix(0, max-min, 0, max-min);
    ind = imatrix(0, max-min, 0, ntot-1); 
    nnn = ivector(0, max-min); 
    if (pol) { 
        fill(R,A,S,ntot,first,last,vel,ll,ss,nvel,fpix,lpix,wave,wwv,index,used,nsel,ind,nnn); 
        if (comments) printf("   Mean V spectrum : \n"); 
        get_psf(R,A,S,ll,vv,ss,ntot,meanv+min,errV+min,max-min+1,maxdev,ind,nnn);
        if (comments) printf("\n   Mean N spectrum : \n"); 
        get_psf(R,A,S,ll,nn,ss,ntot,meann+min,errN+min,max-min+1,maxdev,ind,nnn);
    } 

    maxdev = 1500.0; 
    for (j = 0; j < ntot; j++) ii[j] -= 1.0;  
    fill(R,A,S,ntot,first,last,vel,ll,ss,nvel,fpix,lpix,wave,wwi,index,used,nsel,ind,nnn); 
    if (comments) printf("\n   Mean I spectrum : \n"); 
    get_psf(R,A,S,ll,ii,ss,ntot,mean+min,errI+min,max-min+1,maxdev,ind,nnn);
    for (j = min; j <= max; j++) mean[j] += 1.0;  

    free_matrix(R, 0, ntot-1, 0, max-min);
    free_matrix(S, 0, ntot-1, 0, max-min);
    free_matrix(A, 0, max-min, 0, max-min);
    if (comments) printf("\n"); 

    free_vector(ii, 0, ntot-1);
    if (pol) { 
        free_vector(vv, 0, ntot-1);
        free_vector(nn, 0, ntot-1);
    } 
    free_vector(ll, 0, ntot-1);
    free_vector(ss, 0, ntot-1);
    free_imatrix(ind, 0, max-min, 0, ntot-1); 
    free_ivector(nnn, 0, max-min); 
}


void filter(float *meanv, float *meann, float *meanF, int *first, int *last, int *used, int nsel, 
            int nvel, float filt)
{
    int j, min, max; 

    if (comments) printf("Filtering frequencies higher than %.1f pxl...\n", filt); 

    min = 1e6; max = 0; 
    for (j = 0; j < nsel; j++)
        if (used[j]) { 
            if (first[j] < min) min = first[j];
            if (last[j]  > max) max = last[j];
    } 

    for (j = min; j <= max; j++) {
        meanF[j] = meanv[j];
        meanF[j+nvel] = meann[j];
    } 

    noise_filter(meanF+min, max-min+1, filt);
    noise_filter(meanF+nvel+min, max-min+1, filt);
}


void statistics(float *vel, float *mean, float *meanv, float *meann, float *errv, float *errn, 
                int nvel, int pol, float *wwv, float *wwi, float *sn, double *wave, float *lande, 
                float *prof, float *exc, int *used, int *index, int *first, int *last, 
                int nlines)
{
    int   j, k, n, ntot, min, max, *good; 
    float ave, den, wmax, wmin, sn2, accv, accvf, accn, accnf, lmean, lmin, lmax, 
          pmean, pmin, pmax, emean, emin, emax, gmean, gmin, gmax, ww, gg, chi2v,chi2n,
          ee, pp, smin, smax, smin2, smax2, nmin, nmax, nmin2, nmax2, tmp, avev, aven, 
          noisen, noisev, wimax, wimin, avei, denv, deni; 
    double probv, probn; 

    if (comments) printf("Statistics...\n"); 

    min = nvel-1; max = 0;
    ave = avei = den = denv = deni = lmean = pmean = emean = gmean = 0.0; 
    wimax = wmax = lmax = pmax = emax = gmax = 0.0; 
    wimin = wmin = lmin = emin = pmin = gmin = BIG; 
    for (j = ntot = 0; j < nlines; j++)  
        if (used[j]) { 
            ww = wave[index[j]];
            pp = prof[index[j]];  
            ee = exc[index[j]]; 
            gg = lande[index[j]]; 
            if (first[j] < min) min = first[j]; if (last[j]  > max) max = last[j];
            if (wmax < wwv[j]) wmax = wwv[j];   if (wmin > wwv[j]) wmin = wwv[j]; 
            if (wimax< wwi[j]) wimax = wwi[j];  if (wimin> wwi[j]) wimin = wwi[j]; 
            if (emax < ee)     emax = ee;       if (emin > ee)     emin = ee;  
            if (lmax < ww)     lmax = ww;       if (lmin > ww)     lmin = ww; 
            if (pmax < pp)     pmax = pp;       if (pmin > pp)     pmin = pp; 
            if (gmax < gg)     gmax = gg;       if (gmin > gg)     gmin = gg; 
            sn2 = sn[j]*sn[j]; 
            ave   += sn2*wwv[j]*wwv[j]; 
            avei  += sn2*wwi[j]*wwi[j]; 
            emean += sn2*ee; 
            lmean += sn2*ww; 
            pmean += sn2*pp; 
            gmean += sn2*gg; 
            den   += sn2;
            denv  += sn2*wwv[j];
            deni  += sn2*wwi[j];
            ntot++; 
        } 
    ave   = ave/denv;  
    avei  = avei/deni;  
    emean /= den; 
    lmean /= den; 
    pmean /= den; 
    gmean /= den; 

    if (comments) { 
        printf("   %d lines used\n", ntot); 
        printf("   Mean pol weight    = %.3f  (%.3f %.3f)\n", ave, wmin, wmax); 
        printf("   Mean int weight    = %.3f  (%.3f %.3f)\n", avei, wimin, wimax); 
        printf("      Mean line depth = %.3f  (%.3f  %.3f)\n", pmean, pmin, pmax); 
        printf("      Mean wavelength = %.4f (%.4f %.4f)\n", lmean, lmin, lmax); 
        printf("      Mean excitation = %.3f  (%.3f  %.3f)\n", emean, emin, emax); 
        printf("      Mean lande fact = %.3f  (%.3f  %.3f)\n", gmean, gmin, gmax); 

        if (pol) {
            good = ivector(0, max-min); 
            for (j = 0; j <= max-min; j++) good[j] = 0; 

            tmp = 0.4; 
            minmax(mean+min,max-min+1,&smin,&smax); 
            for (j = 0; j <= max-min; j++)  
                if (mean[j+min] < smax-tmp*(smax-smin)) good[j] = 1; 
            for (j = n = 0; j <= max-min; n += (good[j]>0), j++)  
                if (good[j] == 0) 
                    for (k = j-5; k <= j+5; k++)  
                        if ((k >= 0) && (k <= max-min))
                            if (good[k] == 1) { 
                                good[j] = 2; 
                                break; 
                            } 
            printf("\n   Stellar lines span %d pixels\n", n); 

            printf("   Outside stellar lines: \n"); 
            noisev = noisen = chi2v = chi2n = avev = aven = 0.0; 
            smin = nmin = BIG; smax = nmax = -BIG;  
            for (j = k = 0; j <= max-min ; k += (good[j]==0), j++)  
                if (!good[j]) { 
                    tmp = meanv[min+j]/errv[min+j]; avev += tmp; chi2v += tmp*tmp; 
                    if (smin > tmp) { smin = tmp; smin2 = meanv[min+j]; }
                    if (smax < tmp) { smax = tmp; smax2 = meanv[min+j]; }
                    noisev += errv[min+j]; 
                    tmp = meann[min+j]/errn[min+j]; aven += tmp; chi2n += tmp*tmp; 
                    if (nmin > tmp) { nmin = tmp; nmin2 = meann[min+j]; }
                    if (nmax < tmp) { nmax = tmp; nmax2 = meann[min+j]; }
                    noisen += errn[min+j]; 
                } 
            if (k) { 
                smax = fabs(smax-smin); smax2 = fabs(smax2-smin2); 
                nmax = fabs(nmax-nmin); nmax2 = fabs(nmax2-nmin2); 
                chi2v = chi2v-avev*avev/k; chi2n = chi2n-aven*aven/k; 
                probv = gammp((double) 0.5*k, 0.5*chi2v); 
                probn = gammp((double) 0.5*k, 0.5*chi2n); 
                printf("      Maximum signal :   in 0.01%% : %.3f (%.3f), in sigma : %.3f (%.3f)\n",
                       10000.0*smax2, 10000.0*nmax2, smax, nmax); 
                printf("      Mean noise :       in 0.01%% : %.3f (%.3f)\n", 
                       10000.0*noisev/k, 10000.0*noisen/k); 
                printf("      Reduced chi2 = %.3f (%.3f), detec prob = %.4f (%.4f)\n", 
                       sqrt(chi2v/k), sqrt(chi2n/k), probv, probn); 
                if (probv > 0.99999) 
                    printf("         >>> DEFINITE signal detection (fap = %.3e)\n",1.0-probv); 
                else if (probv > 0.999) 
                    printf("         >>> MARGINAL signal detection (fap = %.3e)\n",1.0-probv); 
                else
                    printf("         >>> No signal detection (fap = %.3e)\n",1.0-probv); 
            } 

            printf("   Inside stellar lines: \n"); 
            noisev = noisen = chi2v = chi2n = avev = aven = 0.0; 
            smin = nmin = BIG; smax = nmax = -BIG;  
            for (j = k = 0; j <= max-min ; k += (good[j]>0), j++)  
                if (good[j]) { 
                    tmp = meanv[min+j]/errv[min+j]; avev += tmp; chi2v += tmp*tmp; 
                    if (smin > tmp) { smin = tmp; smin2 = meanv[min+j]; }
                    if (smax < tmp) { smax = tmp; smax2 = meanv[min+j]; }
                    noisev += errv[min+j]; 
                    tmp = meann[min+j]/errn[min+j]; aven += tmp; chi2n += tmp*tmp; 
                    if (nmin > tmp) { nmin = tmp; nmin2 = meann[min+j]; }
                    if (nmax < tmp) { nmax = tmp; nmax2 = meann[min+j]; }
                    noisen += errn[min+j]; 
                } 
            if (k) { 
                smax = fabs(smax-smin); smax2 = fabs(smax2-smin2); 
                nmax = fabs(nmax-nmin); nmax2 = fabs(nmax2-nmin2); 
                chi2v = chi2v-avev*avev/k; chi2n = chi2n-aven*aven/k; 
                probv = gammp((double) 0.5*k, 0.5*chi2v); 
                probn = gammp((double) 0.5*k, 0.5*chi2n); 
                printf("      Maximum signal :   in 0.01%% : %.3f (%.3f), in sigma : %.3f (%.3f)\n",
                       10000.0*smax2, 10000.0*nmax2, smax, nmax); 
                printf("      Mean noise :       in 0.01%% : %.3f (%.3f)\n", 
                       10000.0*noisev/k, 10000.0*noisen/k); 
                printf("      Reduced chi2 = %.3f (%.3f), detec prob = %.4f (%.4f)\n", 
                       sqrt(chi2v/k), sqrt(chi2n/k), probv, probn); 
                if (probv > 0.9999) 
                    printf("         >>> DEFINITE signal detection (fap = %.3e)\n",1.0-probv); 
                else if (probv > 0.99) 
                    printf("         >>> MARGINAL signal detection (fap = %.3e)\n",1.0-probv); 
                else
                    printf("         >>> No signal detection (fap = %.3e)\n",1.0-probv); 
            } 

            free_ivector(good, 0, max-min); 
        } 
    } 
    if (comments) printf("\n"); 
} 


void show(float *vel, float *mean, float *meanv, float *meann, float *meanF, int nvel, int *first, 
          int *last, int *used, int nsel, int pol)
{
    int   j, k, min, max; 
    float shiftn, shiftv, mult, ymin, ymax, tmp, lineI[2], lineV[2], lineN[2], x[2];
    char  labx[80] = {"Velocity (km/s)"},
          title[80],
          //laby[80] = {"I/I\\Dc\\U           25*N/I\\Dc\\U    25*V/I\\Dc"},
          //labyn[80] = {"I/I\\Dc"};

	laby[80] = {"                 I/I\\Dc\\U             200*V/I\\Dc"},
          labyn[80] = {"I/I\\Dc"};



    shiftn = 1.05; shiftv = 1.10; mult = 20.0;

    //modified value!!
    //shiftv = 1.07;

    if (comments) printf("Displaying results...\n"); 

    min = nvel-1; max = 0; 
    for (j = 0; j < nsel; j++)
        if (used[j]) { 
            if (first[j] < min) min = first[j];
            if (last[j]  > max) max = last[j];
    } 

    set_device(plot_dev);
    for (k = min; k <= max; k++) {
        if (pol) {
            meanv[k] = meanv[k]*mult+shiftv;
            meanF[k] = meanF[k]*mult+shiftv;
            meann[k]      = meann[k]*mult+shiftn;
            meanF[k+nvel] = meanF[k+nvel]*mult+shiftn;
        }
    }
    minmax(mean+min, max-min+1, &ymin, &ymax);
    if (pol) { 
        if (shiftn > shiftv)
            minmax(meann+min, max-min+1, &tmp, &ymax);
        else
            minmax(meanv+min, max-min+1, &tmp, &ymax);
    } 
    tmp = ymax-ymin; ymax += 0.05*tmp; ymin -= 0.05*tmp;
    for (j = k = 0; j < nsel; k += used[j], j++);
    sprintf(title,"LS deconvolved Zeeman signature (%d lines used)",k);
    if (pol)
        init_plot(vel[0], vel[nvel-1], ymin, ymax, labx, laby, title, 0, 0);
    else
        init_plot(vel[0], vel[nvel-1], ymin, ymax, labx, labyn, title, 0, 0);
    plot(vel+min, mean+min, max-min+1, 1);
    if (pol) {
        plot_sym(vel+min, meanv+min, max-min+1, 1);
        plot(vel+min, meanF+min, max-min+1, 1);
        plot_sym(vel+min, meann+min, max-min+1, 1);
        plot(vel+min, meanF+nvel+min, max-min+1, 1);
    }
    for (k = 0; k < 2; k++) {
        if (k == 0) x[k] = vel[0]; 
        else        x[k] = vel[nvel-1]; 
        lineI[k] = 1.0;
        if (pol) { 
            lineV[k] = shiftv;
            lineN[k] = shiftn;
        } 
    }
    plot(x, lineI, 2, 2);
    if (pol) { 
        plot(x, lineV, 2, 2);
        plot(x, lineN, 2, 2);
    } 
    end_plot();
}


static void get_psf(float **R, float **A, float **T, float *X, float *Y, float *S, int ntot, 
                    float *PP, float *err, int nvel, float maxdev, int **ind, int *nn)
{
    int   j, k, n, p, nout, *good, nrej; 
    float *B, **C, *F, *P, max, res, tmp, tmp2, maxdev2, chi2, sn, *diag, d1, d2, ave; 
float *d; 

    maxdev2 = 10.0; 
    good = ivector(0, ntot-1);
    for (n = 0; n < ntot; n++) good[n] = 1;
    k = reject(Y,good,ntot,&tmp,&tmp2,maxdev2,ntot);
    if (comments && k) printf("   %d pixel(s) rejected PRIOR to LSD\n", k);

    B = vector(0, nvel-1);
    P = vector(0, nvel-1);
    C = matrix(0, nvel-1, 0, nvel-1); 

    for (j = 0; j < nvel; j++) B[j] = err[j] = 0.0; 
    for (j = 0; j < nvel; j++) for (k = j; k < nvel; k++) C[j][k] = A[j][k];

/*
d = vector(0, nvel-1); 
for (j = 0; j < nvel; j++) d[j] = 0.0; 
for (j = 0; j < nvel; j++) for (k = j; k < nvel; k++) d[k-j] += C[j][k]/(-k+j+nvel); 
for (j = 0; j < nvel; j++) printf("%d %f\n", j, d[j]/d[0]); 
d[0] = 7000.0*7000.0; printf("%f %f\n", d[0], sqrt(d[0])); 
for (j = 0; j < nvel; j++) for (k = j; k < nvel; k++) C[j][k] = d[k-j]; 
free_vector(d, 0, nvel-1); 
*/

    for (j = 0; j < nvel; j++)  
        for (n = 0; n < nn[j]; n++) { 
            p = ind[j][n]; 
            if (good[p])  { 
                B[j]   += S[p]*S[p]*R[p][j]*Y[p];
/*
if (good[p]) printf("%d %d %d %d   %f %f %f %f\n", j, n, nn[j], p, S[p], R[p][j], Y[p], B[j]);
*/
                err[j] += T[p][j]; 
            } else 
                for (k = j; k < nvel; k++) C[j][k] -= S[p]*S[p]*R[p][j]*R[p][k]; 
        } 
/*
for (j = 0; j < nvel; j++) for (k = j+1; k < nvel; k++) C[j][k] = 0.0;
*/

    F    = vector(0, ntot-1);
    diag = vector(0, nvel-1); 
    do { 
        choldc(C, nvel, diag);
        cholsl(C, nvel, diag, B, P);

        for (j = 0; j < ntot; j++) F[j] = 0.0; 
        for (k = 0; k < nvel; k++) 
            for (j = 0; j < nn[k]; j++) { 
                p = ind[k][j]; 
                F[p] += R[p][k]*P[k]; 
            } 
        nrej = 0; 

        do { 
            max = -BIG; nout = -1; chi2 = ave = 0.0; 
            for (j = k = 0; j < ntot; k += good[j], j++)
                if (good[j]) {
                    tmp = (Y[j]-F[j])*S[j]; res = fabs(tmp); 
                    ave  += tmp; chi2 += res*res; 
                    if ((res > maxdev) && (max < res)) {
                        max = res;
                        nout = j;
                    }
                }
            ave /= k; chi2 = sqrt(chi2/k-ave*ave); 
            if (nout >= 0) {
                good[nout] = 0;
                if (comments)
                    printf("   Rejecting pixel @ %8.4f nm (val : %6.3f; dev : %6.1f sig)\n", 
                           X[nout], Y[nout], max);
                for (j = 0; j < nvel; j++) { 
                    B[j]   -= S[nout]*S[nout]*R[nout][j]*Y[nout]; 
                    err[j] -= T[nout][j]; 
                    for (k = j; k < nvel; k++)  C[j][k] -= S[nout]*S[nout]*R[nout][j]*R[nout][k]; 
                } 
                nrej++; 
            }
        } while ((nout >= 0) && (nrej < 1+ntot/100)); 
    } while (nrej > 0); 

    sn = 0.0; tmp2 = 1.0; if (chi2 > 1.0) tmp2 = chi2; 
/*
tmp2 = 1.0; 
*/
    for (k = 0; k < nvel; k++) { 
        sn += (tmp = sqrt(err[k]))/nvel/tmp2; 
        err[k] = tmp2/tmp; 
    } 
    if (comments) printf("   Rms deviation of fit = %.3f, mean sn = %.0f\n", chi2, sn); 

    d1 = d2 = 0.0; 
    for (k = 0; k < nvel; k++) { 
        d1 += C[k][k]/nvel; 
        if (k) d2 += C[k-1][k]/(nvel-1); 
    } 
/*
d2 *= 0.0; 
*/
    for (k = 0; k < nvel; k++)  
        if ((k == 0) || (k == nvel-1)) PP[k] = P[k]; 
        else                           PP[k] = (d1*P[k]+0.5*d2*(P[k+1]+P[k-1]))/(d1+d2); 

    free_ivector(good, 0, ntot-1); 
    free_vector(F,     0, ntot-1); 
    free_vector(B, 0, nvel-1); 
    free_vector(P, 0, nvel-1); 
    free_vector(diag, 0, nvel-1); 
    free_matrix(C, 0, nvel-1, 0, nvel-1); 
}


static void fill(float **R, float **A, float **S, int ntot, int *first, int *last, float *vel, 
                 float *ll, float *ss, int nvel, int *fpix, int *lpix, double *wave, 
                 float *ww, int *index, int *used, int nsel, int **ind, int *nn) 
{
    int   j, k, jj, pix, pix2, min, max, **ind2, *nnn, p, q;
    float rad, alf, alf2, tmp1, tmp2, tmp3, tmp4, s2; 

    min = nvel-1; max = 0; 
    for (j = 0; j < nsel; j++)
        if (used[j]) { 
            if (first[j] < min) min = first[j];
            if (last[j]  > max) max = last[j];
        } 

    nnn = ivector(0, ntot-1); 
    for (k = 0; k < ntot; k++) nnn[k] = 0;
    for (k = 0; k <= max-min; k++) nn[k] = 0;
    for (j = 0; j <= max-min; j++) for (k = 0; k <= max-min; k++) A[j][k] = 0.0;
    for (j = 0; j < ntot; j++) for (k = 0; k <= max-min; k++) S[j][k] = R[j][k] = 0.0;

    ind2 = imatrix(0, ntot-1, 0, max-min); 
    for (j = 0; j < nsel; j++)
        if (used[j]) {
            for (k = fpix[j]; k <= lpix[j]; k++) {
                rad = (ll[k]/wave[index[j]]-1.0)*cc;
                pix = locate(rad, vel, nvel);
                if ((pix < first[j]) || (pix > last[j])) { 
                    printf("pix = %d, out of [%d %d]\n", pix, first[j],last[j]); 
                    //exit(1); 
                } 
                alf = (vel[pix+1]-rad)/(vel[pix+1]-vel[pix]);
/*
if (alf < 0.5) alf = 0.0; 
else           alf = 1.0; 
*/

                if ((alf < 0.0) || (alf > 1.0)) { 
                    printf("This should never happen %d %f %d %d %d %f %f\n", j, rad, pix, first[j],last[j],vel[pix],vel[pix+1]); 
                    //exit(1); 
                } 
                if ((R[k][pix-min] == 0.0) && (alf > 0.0)) { 
                    ind[pix-min][nn[pix-min]] = k; (nn[pix-min])++;  
                    ind2[k][nnn[k]] = pix-min; (nnn[k])++; 
                }  
                if ((R[k][pix-min+1] == 0.0) && (alf < 1.0)) { 
                    ind[pix-min+1][nn[pix-min+1]] = k; (nn[pix-min+1])++; 
                    ind2[k][nnn[k]] = pix-min+1; (nnn[k])++; 
                } 
                s2 = ss[k];  s2 *= s2; 
                tmp1 = ww[j]*alf; 
                tmp2 = ww[j]*(1.0-alf);
                if (pix == first[j])  tmp3 = ww[j]; else tmp3 = tmp1; 
                if (pix == last[j]-1) tmp4 = ww[j]; else tmp4 = tmp2; 
/*
R[k][pix-min]   += tmp3;
R[k][pix-min+1] += tmp4;
*/
                R[k][pix-min]   += tmp1;
                R[k][pix-min+1] += tmp2;
                S[k][pix-min]   += s2*tmp3*ww[j]; 
                S[k][pix-min+1] += s2*tmp4*ww[j]; 
            } 
        }

    for (j = 0; j <= max-min; j++) 
        for (k = 0; k < nn[j]; k++) 
            for (jj = 0, p = ind[j][k]; jj < nnn[p]; jj++)  
                if ((q = ind2[p][jj]) >= j) A[j][q] += ss[p]*ss[p]*R[p][j]*R[p][q]; 

    free_imatrix(ind2, 0, ntot-1, 0, max-min); 
    free_ivector(nnn, 0, ntot-1); 
}


static float fit(float *x, float *y, int n, int deg, int ok_to_sub, float maxdev)
{
    int    j;
    float  acc, *mod;
    double *coeff;

    coeff = dvector(0, deg); 
    mod = vector(0, n-1);
    acc = adjust(x, y, n, coeff, deg, maxdev);
    poly(x, mod, n, 0.0, coeff, deg);
    if (ok_to_sub) 
        for (j = 0; j < n; j++) y[j] -= mod[j];

    free_dvector(coeff, 0, deg);
    free_vector(mod, 0, n-1);
    return acc;
}


void set_trace(int ok_to_plot, int verbose, int test)
{
    if (ok_to_plot == 1)      plot_dev = 0;    /* Screen plot (default)*/
    else if (ok_to_plot == 2) plot_dev = 1;    /* Laser  plot */
    else                      plot_dev = -1;    /* No plot     */

    if (verbose)              comments = 1;    /* Comments printed (default)*/
    else                      comments = 0;    /* No comments      */

    if (test)                 test_dsp = 1;    /* Test display active */
    else                      test_dsp = 0;    /* Test display inactive (default) */
}


void conv(float *lam, int nspec, int *used, int *index, int *fpix, int *lpix, int nsel, 
          double *wave, int pol, float *Is, float *Vs, float *Ns, float *vel, 
          float *meanI, float *meanV, float *meanN, float *wi, float *wv, int nvel)
{
    int   j, k, last, first, npix; 
    double www; 
    float  *vv, *yy; 

    if (comments) printf("Convolving LSD signature with line mask...\n"); 

    for (k = 0; k < nspec; k++) Is[k] = 0.0; 
    if (pol) for (k = 0; k < nspec; k++) Vs[k] = Ns[k] = 0.0;
    for (j = last = 0; j < nsel; j++) 
        if (used[j]) { 
            first = fpix[j];  last = lpix[j]; 
            npix = last-first+1; 
            www = wave[index[j]]; 
            vv = vector(0, npix-1); 
            yy = vector(0, npix-1); 
            for (k = 0; k < npix; k++) vv[k] = cc*(lam[first+k]/www-1.0); 
            linear(vv, yy, npix, vel, meanI, nvel); 
            for (k = 0; k < npix; k++) Is[first+k] += wi[j]*(yy[k]-1.0); 
            if (pol) { 
                linear(vv, yy, npix, vel, meanV, nvel); 
                for (k = 0; k < npix; k++) Vs[first+k] += yy[k]*wv[j]; 
                linear(vv, yy, npix, vel, meanN, nvel); 
                for (k = 0; k < npix; k++) Ns[first+k] += yy[k]*wv[j]; 
            } 
            free_vector(vv, 0, npix-1); 
            free_vector(yy, 0, npix-1); 
        } 
}

