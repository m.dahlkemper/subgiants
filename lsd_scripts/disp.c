#include <stdlib.h>
#include <strings.h>
#include <math.h>
#include "structures.h"
#include "file.h"
#include "mem_alloc.h"
#include "plot.h"
#include "lsfit.h"
#include "smear.h"
//#include "/usr/local/pgplot/cpgplot.h"
#include <stdio.h>


#define MAXPLOT 30 

static int locat(float x, float array[], int n); 
static void normalise(float *xx, float *spec, int nspec, int deg, int pix1, int pix2, 
                      int ok_to_plot); 
static void fitt(float *xx, float *spec, int nspec, int deg, int pix1, int pix2, 
                int ok_to_plot); 
static void display(float xmin, float xmax, float ymin, float ymax, 
                    char lab_x[], char lab_y[], char title[], float xx[], 
                    float spec[], float err[], int nspec, int noerase, int mode);
static float vel(float wave, float lamref); 
static void select_file(int npt, int nfile, float **x); 

char com[120] = {"Extracted line, from file"};

int main(void)
{
    int    getname, j, open_ok, npix, pix, pix1, pix2, exit, npt, k, indx[MAXPLOT], 
           pixu, pixl, n_param, nspec[MAXPLOT], overplot, sign, full, deg, nfile, mode;
    char   filename[80], comment[120], answer[2], curse[100][2], comm[120],
           title[80] = {" "}, com1[120], fname[80], 
				//lab_x[80] = {"Velocity (km/s)"}, 
           lab_x[80] = {"Wavelength (nm)"}, 
           //lab_y[80] = {"\\Gx\\U2\\D"}, 
		   //lab_x[80] = {"Period (d)"};
           //lab_y[80] = {"V/I\\Dc"}; 
           lab_y[80] = {"I/I\\Dc"}; 
    float  xmin, xmax, ymin, ymax, tmp, xc[100], yc[100], ew, resol, alpha, 
           centr, fwhm, *x_new, *y_new, param[4], dummy[4], **xx, **spec, **err;
float centr2, fwhm2, ew2, centr3, fwhm3, ew3;  
    float  SN, moy, lamref, shift, csq;

/************  Reading data                      */

    /* mode = 0: line plot;  mode = 1: symbols with error bars */ 
    mode = 0; 
    getname = 1; lamref = 0.0; 
    full = 0; overplot = 1;
    for (j = 0; j < MAXPLOT; j++) indx[j] = j;  
    xx   = (float **) svector(MAXPLOT, (int) sizeof(float *)); 
    spec = (float **) svector(MAXPLOT, (int) sizeof(float *)); 
    err  = (float **) svector(MAXPLOT, (int) sizeof(float *)); 

    do {
        open_ok = read_ascfile(nspec, &nfile, xx, spec, getname, filename, comment);
        if (open_ok == 1) printf("File not found.  Try again\n");
    } while (open_ok == 1);
    if (nfile > 1) select_file(nspec[0],nfile,spec); 
if (mode == 1) { 
    read_ascfile(nspec, &nfile, xx, err, 0, filename, comment);
    select_file(nspec[0],nfile,err); 
}

/************  First plot of data            */

    xmin = xx[0][0]; xmax = xx[0][nspec[0]-1]; 
    minmax(spec[0], nspec[0], &ymin, &ymax); 
    tmp = ymax-ymin; ymin = ymin - 0.05*tmp; ymax = ymax + 0.05*tmp; 
    
    set_device(0);

/************  Choose option                 */

    exit = 0;
    do { 
        printf("Quit, Mouse zoom, Kbd zoom, Vel, Full spec, Curs, Gaus, Bisec, Adjust, Remove noise\n eq Wid, line Ext, Sn, cont Lev., Norm, Transl, +/-, Overpl, Ps, Zcroll, aXes, gravitY, Interp \n");
        printf("Choose option : ");
        scanf("%1s",answer);

/************  Right and left shift         */

        if ((answer[0] == '+') || (answer[0] == '-')) { 
            tmp = xmax-xmin; 
            if (answer[0] == '+')      sign = 1; 
            else if (answer[0] == '-') sign = -1; 
            else continue; 
            xmin += sign*tmp; xmax += sign*tmp; 
            end_plot();
            for (j = 0; j < overplot; j++) 
                display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);

/************  Zcroll the file stack         */

        } else if ((answer[0] == 'Z') || (answer[0] == 'z')) {
            k = indx[0]; 
            for (j = 0; j < overplot-1; j++) 
                indx[j] = indx[j+1]; 
            indx[overplot-1] = k; 
            end_plot();
            for (j = 0; j < overplot; j++) 
                display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);

/************  Zooming with keyboard         */

        } else if ((answer[0] == 'K') || (answer[0] == 'k')) {
            printf("Enter xmin, xmax, ymin, ymax : ");
            scanf("%f %f %f %f",&xmin,&xmax,&ymin,&ymax);
            end_plot();
            if (xmin > xmax) { 
                tmp = xmax; 
                xmax = xmin; 
                xmin = tmp; 
            } 
            for (j = 0; j < overplot; j++) 
                display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);

/************  Zooming with mouse         */

        } else if ((answer[0] == 'M') || (answer[0] == 'm')) {
            get_points(xc,yc,curse,2,&npt,1);
            if (npt == 2 ) {
                xmin = xc[0]; xmax = xc[1]; 
                ymin = yc[0]; ymax = yc[1]; 
                end_plot();
                if (xmin > xmax) { 
                    tmp = xmax; 
                    xmax = xmin; 
                    xmin = tmp; 
                } 
                for (j = 0; j < overplot; j++) 
                    display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);
            }

/************  changing aXes */

        } else if ((answer[0] == 'X') || (answer[0] == 'x')) {
            gets(lab_x); 
            printf("Xlabel : "); 
            gets(lab_x); 
            printf("Ylabel : "); 
            gets(lab_y); 
            printf("Title : "); 
            gets(title); 
            end_plot();
            for (j = 0; j < overplot; j++) 
                display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);

/************  Translate wavelength axis */

        } else if ((answer[0] == 'T') || (answer[0] == 't')) {
            printf("Enter shift : "); 
            scanf("%f", &shift); 
            for (j = 0; j < nspec[indx[overplot-1]]; j++) 
                xx[indx[overplot-1]][j] += shift; 
            if (overplot == 1) {
                xmax += shift; 
                xmin += shift; 
            }
            end_plot();
            for (j = 0; j < overplot; j++) 
                display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);
            if (overplot == 1) 
                save_ascfile(nspec[indx[0]], 1, xx[indx[0]], spec[indx[0]], getname, fname, comment);

/************  Full spectrum         */

        } else if ((answer[0] == 'F') || (answer[0] == 'f')) {
            xmin = xx[indx[0]][0]; xmax = xx[indx[0]][nspec[indx[0]]-1]; 
            minmax(spec[indx[0]], nspec[indx[0]], &ymin, &ymax); 
            tmp = ymax-ymin; ymin = ymin - 0.05*tmp; ymax = ymax + 0.05*tmp; 
            end_plot();
            for (j = 0; j < overplot; j++) 
                display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);

/************  Reading with cursor         */

        } else if ((answer[0] == 'C') || (answer[0] == 'c')) {
            for (;;) {
                get_points(xc,yc,curse,1,&npt,1);
                if (npt != 1) break; 
                pix = locat(xc[0], xx[indx[0]], nspec[indx[0]]); 
                printf(" Curs  >>  x = %10.4e, y = %5.3e, spec = %5.3e\n", 
                       vel(xc[0],lamref), yc[0], spec[indx[0]][pix]); 
            } 

/************ Changing continuum level     */

        } else if ((answer[0] == 'L') || (answer[0] == 'l')) {
            get_points(xc,yc,curse,1,&npt,1);
            if (npt != 1) continue; 
            for (j = 0; j < nspec[indx[0]]; j++) spec[indx[0]][j]  /= yc[0]; 
            ymin /= yc[0]; ymax /= yc[0]; 
            end_plot();
            for (j = 0; j < overplot; j++) 
                display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);
            save_ascfile(nspec[indx[0]], 1, xx[indx[0]], spec[indx[0]], getname, fname, comment);

/************ Removing noise */

        } else if ((answer[0] == 'r') || (answer[0] == 'R')) {
            printf("Resolution (in pix) : "); 
            scanf("%f",&resol); 
/*
            noise_filter(spec[indx[0]],nspec[indx[0]],resol);
*/
            bpro_filter(spec[indx[0]],nspec[indx[0]],resol);
            end_plot();
            for (j = 0; j < overplot; j++) 
                display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);
            save_ascfile(nspec[indx[0]], 1, xx[indx[0]], spec[indx[0]], getname, fname, comment);

/************ Normalising the continuum  */

        } else if ((answer[0] == 'N') || (answer[0] == 'n')) {
            printf("Degree of polynomial fit : "); 
            scanf("%d",&deg); 
            get_points(xc,yc,curse,2,&npt,0);
            if (npt != 2) continue; 
            pix1 = locat(xc[0], xx[indx[0]], nspec[indx[0]]); 
            pix2 = locat(xc[1], xx[indx[0]], nspec[indx[0]]); 
            if (pix2 < pix1) { 
                tmp = pix1; pix1 = pix2; pix2 = tmp;
            }
pix2++; if (pix2 > nspec[indx[0]]) pix2 = nspec[indx[0]]; 
            normalise(xx[indx[0]],spec[indx[0]],nspec[indx[0]],deg,pix1,pix2,1); 
            printf("Type return to go on \n"); 
            gets(com); gets(com); 
            end_plot();
            pix1 = locat(xmin, xx[indx[0]], nspec[indx[0]]); 
            pix2 = locat(xmax, xx[indx[0]], nspec[indx[0]]); 
            minmax(spec[indx[0]]+pix1, pix2-pix1+1, &ymin, &ymax); 
            tmp = ymax-ymin; ymin = ymin - 0.05*tmp; ymax = ymax + 0.05*tmp; 
            for (j = 0; j < overplot; j++) 
                display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);
            save_ascfile(nspec[indx[0]], 1, xx[indx[0]], spec[indx[0]], getname, fname, comment);

/************ Interpolate between two points  */

        } else if ((answer[0] == 'I') || (answer[0] == 'i')) {
            get_points(xc,yc,curse,2,&npt,0);
            if (npt != 2) continue; 
            pix1 = locat(xc[0], xx[indx[0]], nspec[indx[0]]); 
            pix2 = locat(xc[1], xx[indx[0]], nspec[indx[0]]); 
            if (pix2 < pix1) { 
                tmp = pix1; pix1 = pix2; pix2 = tmp;
            }
pix2++; if (pix2 > nspec[indx[0]]) pix2 = nspec[indx[0]]; 
          if (pix2 > pix1+1) {
            for (j = pix1+1; j < pix2; j++) { 
                alpha = (xx[indx[0]][j]-xx[indx[0]][pix1])/(xx[indx[0]][pix2]-xx[indx[0]][pix1]);
                spec[indx[0]][j] = (1.0-alpha)*spec[indx[0]][pix1] + alpha*spec[indx[0]][pix2]; 
            } 
            end_plot();
            pix1 = locat(xmin, xx[indx[0]], nspec[indx[0]]); 
            pix2 = locat(xmax, xx[indx[0]], nspec[indx[0]]); 
            minmax(spec[indx[0]]+pix1, pix2-pix1+1, &ymin, &ymax); 
            tmp = ymax-ymin; ymin = ymin - 0.05*tmp; ymax = ymax + 0.05*tmp; 
            for (j = 0; j < overplot; j++) 
                display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);
            save_ascfile(nspec[indx[0]], 1, xx[indx[0]], spec[indx[0]], getname, fname, comment);
          } 
/************ Polynomial fit */

        } else if ((answer[0] == 'A') || (answer[0] == 'a')) {
            printf("Degree of polynomial fit : "); 
            scanf("%d",&deg); 
            get_points(xc,yc,curse,2,&npt,0);
            if (npt != 2) continue; 
            pix1 = locat(xc[0], xx[indx[0]], nspec[indx[0]]); 
            pix2 = locat(xc[1], xx[indx[0]], nspec[indx[0]]); 
            if (pix2 < pix1) { 
                tmp = pix1; pix1 = pix2; pix2 = tmp;
            }
            fitt(xx[indx[0]],spec[indx[0]],nspec[indx[0]],deg,pix1,pix2,1); 
            printf("Type return to go on \n"); 
            gets(com); gets(com); 
            end_plot();
            pix1 = locat(xmin, xx[indx[0]], nspec[indx[0]]); 
            pix2 = locat(xmax, xx[indx[0]], nspec[indx[0]]); 
            minmax(spec[indx[0]]+pix1, pix2-pix1+1, &ymin, &ymax); 
            tmp = ymax-ymin; ymin = ymin - 0.05*tmp; ymax = ymax + 0.05*tmp; 
            for (j = 0; j < overplot; j++) 
                display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);
            save_ascfile(nspec[indx[0]], 1, xx[indx[0]], spec[indx[0]], getname, fname, comment);

/************ Laser copy      */

        } else if ((answer[0] == 'P') || (answer[0] == 'p')) {
            end_plot(); 
            set_device(1); 
            for (j = 0; j < overplot; j++) 
                display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);
            end_plot(); 
            set_device(0); 
            for (j = 0; j < overplot; j++) 
                display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);

/************  Gaussian fit         */

        } else if ((answer[0] == 'G') || (answer[0] == 'g')) {
            for (;;) {
                get_points(xc,yc,curse,2,&npt,0);
                if (npt != 2) break;
                pix1 = locat(xc[0], xx[indx[0]], nspec[indx[0]]); 
                pix2 = locat(xc[1], xx[indx[0]], nspec[indx[0]]); 
                if (pix2 < pix1) { 
                    tmp = pix1; pix1 = pix2; pix2 = tmp;
                }
                if (pix2 == pix1) pix2++; npix = pix2 - pix1 + 1;
                centr = 0.50*(xx[indx[0]][pix1]+xx[indx[0]][pix2]);
                fwhm  = 0.25*(xx[indx[0]][pix2]-xx[indx[0]][pix1]); 
                ew    = -0.5*fwhm; 
                csq = gaussian_lsfit(xx[indx[0]]+pix1,spec[indx[0]]+pix1,npix,&centr,&fwhm,&ew,1.0e36,1);
/*
                csq = gaussian_lsfit(xx[indx[0]]+pix1,spec[indx[0]]+pix1,npix,&centr,&fwhm,&ew,1.0e36,0);
centr2=centr-0.03; 
fwhm2 = 0.6*fwhm; ew2 = 0.15*ew; 
csq = dgaussian_lsfit(xx[indx[0]]+pix1,spec[indx[0]]+pix1,npix,&centr,&fwhm,&ew,&centr2,&fwhm2,&ew2,1.0e36,1);
*/
/*
ew3=0.0; fwhm3=fwhm; 
                csq = gaussian_lsfit(xx[indx[0]]+pix1,spec[indx[0]]+pix1,npix,&centr,&fwhm,&ew,1.0e36,1);
csq = tgaussian_lsfit(xx[indx[0]]+pix1,spec[indx[0]]+pix1,npix,&centr,&fwhm,&ew,&centr2,&fwhm2,&ew2,&centr3,&fwhm3,&ew3,1.0e36,1);
*/

                if (csq < 0.0) 
                    printf("Error in gaussian fit\n"); 
                else { 
printf(" Gauss  >>  centr  = %12.6f, fwhm  = %10.6f, ew  = %9.6f\n", vel(centr,lamref), fwhm, -ew); 
/*
printf(" Gauss  >>  centr2 = %10.4f, fwhm2 = %8.4f, ew2 = %7.4f\n", vel(centr2,lamref),fwhm2,-ew2); 
printf(" Gauss  >>  centr3 = %10.4f, fwhm3 = %8.4f, ew3 = %7.4f\n", vel(centr3,lamref),fwhm3,-ew3); 
                    printf(" Gauss  >>  centr = %10.4f, fwhm = %8.4f, ew = %7.4f, str = %7.4f\n", 
                           vel(centr,lamref), fwhm, -ew, -2.0*ew*sqrt(log((double) 2.0))/fwhm/sqrt(3.1415926)); 
*/
}
            }
/************  Bisector         */

        } else if ((answer[0] == 'B') || (answer[0] == 'b')) {
            for (;;) {
                get_points(xc,yc,curse,2,&npt,0);
                if (npt != 2) break;
                tmp = 0.5*(xc[0]+xc[1]); 
                printf(" Bisec >>  centr = %10.4f\n", vel(tmp,lamref)); 
                plot_sym(&tmp,yc,1,2); 
            }

/************  gravitY centre         */
        } else if ((answer[0] == 'Y') || (answer[0] == 'y')) {
            for (;;) {
                get_points(xc,yc,curse,2,&npt,0);
                if (npt != 2) break;
                pix1 = locat(xc[0], xx[indx[0]], nspec[indx[0]]); 
                pix2 = locat(xc[1], xx[indx[0]], nspec[indx[0]]); 
                if (pix2 < pix1) { 
                    tmp = pix1; pix1 = pix2; pix2 = tmp;
                }
                if (pix2 == pix1) pix2++; npix = pix2 - pix1 + 1;
                centr = tmp = 0.0; 
                yc[0] = 0.5*(yc[0]+yc[1]); 
                xc[0] = 0.5*(xc[0]+xc[1]); 
                for (j = pix1; j < pix2; j++) { 
                    centr += (yc[0]-spec[indx[0]][j])*(xx[indx[0]][j]-xc[0])*(xx[indx[0]][j+1]-xx[indx[0]][j]); 
                    tmp   += (yc[0]-spec[indx[0]][j])*(xx[indx[0]][j+1]-xx[indx[0]][j]); 
                }
                if (fabs(yc[0]) > 0.02) { 
                    centr /= tmp; 
                    centr += xc[0]; 
                    printf(" Gravity centre >>  centr = %10.4f\n", vel(centr,lamref)); 
                    plot_sym(&centr,yc,1,31); 
                } else 
                    printf(" Gravity centre >>  centr = %10.6f\n", vel(centr,lamref)); 
printf("%.6f %.6f %.6f\n", centr,tmp,yc[0]); 
            }


/************  Equivalent width     */

        } else if ((answer[0] == 'W') || (answer[0] == 'w')) {
            for (;;) {
                get_points(xc,yc,curse,2,&npt,0);
                if (npt != 2) break;
                pix1 = locat(xc[0], xx[indx[0]], nspec[indx[0]]); 
                pix2 = locat(xc[1], xx[indx[0]], nspec[indx[0]]); 
                if (pix2 < pix1) { 
                    tmp = pix1; pix1 = pix2; pix2 = tmp;
                }
                if (pix2 == pix1) pix2++;
                for (ew = 0.0, j = pix1; j < pix2; j++) 
                    ew += (1.0-spec[indx[0]][j])*(xx[indx[0]][j+1]-xx[indx[0]][j]); 
                printf(" Equ w  >>  ew = %7.4f\n", ew); 
            } 

/************  SN measurement        */

        } else if ((answer[0] == 'S') || (answer[0] == 's')) {
            for (;;) {
                get_points(xc,yc,curse,2,&npt,0);
                if (npt != 2) break;
                pix1 = locat(xc[0], xx[indx[0]], nspec[indx[0]]); 
                pix2 = locat(xc[1], xx[indx[0]], nspec[indx[0]]); 
                if (pix2 < pix1) { 
                    tmp = pix1; pix1 = pix2; pix2 = tmp;
                }
                if (pix2 == pix1) pix2++; npix = pix2 - pix1 + 1; 
                for (moy = 0.0, SN = 0.0, j = pix1; j <= pix2; j++) {
                    tmp  = spec[indx[0]][j]; 
                    moy += tmp/npix; 
                    SN  += tmp*tmp/npix; 
                }
                SN = sqrt(SN - moy*moy); 
                printf(" SN rat >>  mean = %.3e, noise = %.2e sn = %6.2e\n", 
                        moy, SN, fabs(moy)/SN); 
            } 

/************  line Extraction         */

        } else if ((answer[0] == 'E') || (answer[0] == 'e')) {
            get_points(xc,yc,curse,2,&npt,1);
            if (npt > 1) {
                pix1 = locat(xc[0], xx[indx[0]], nspec[indx[0]]); 
                pix2 = locat(xc[1], xx[indx[0]], nspec[indx[0]]); 
                if (pix2 < pix1) { 
                   tmp = pix1; pix1 = pix2; pix2 = tmp;
                }
                npix = pix2-pix1+1; 
                /*
                npix = 2; 
                while (npix < pix2-pix1+1) npix *= 2; 
                pix1 -= (npix-pix2+pix1-1) / 2; 
                pix2  = pix1 + npix - 1; 
                */ 
                x_new = vector(0, npix-1); 
                y_new = vector(0, npix-1); 
                for (j = 0; j < npix; j++) { 
                    x_new[j]   = xx[indx[0]][pix1+j]; 
                    y_new[j]   = spec[indx[0]][pix1+j]; 
                }
                pixl = npix-1; pixu = 0; 
/*
                for (j = 0; j < npt; j++) { 
                    pix = locat(xc[j], x_new, npix);
                    if (yc[j] > 1.0) yc[j] = 1.0; 
                    y_new[pix] = yc[j];
                    if (pixl > pix) pixl = pix;   
                    if (pixu < pix) pixu = pix;   
                }
                for (j = 0; j < npix; j++) 
                    if ((j < pixl) || (j > pixu)) y_new[j] = 1.0; 
*/
                plot(x_new,y_new,npix,1);
                getname = 1; 
                sprintf(com1, "%s %s", com, filename); 
                save_ascfile(npix, 1, x_new, y_new, getname, fname, com1);
                free_vector(x_new, 0, npix-1); 
                free_vector(y_new, 0, npix-1); 
            }

/************  Overplot         */

        } else if ((answer[0] == 'O') || (answer[0] == 'o')) {
            if (overplot == MAXPLOT) {
                end_plot(); 
                full = 1; 
                free_vector(xx[indx[MAXPLOT-1]],   0, nspec[indx[MAXPLOT-1]]-1);
                free_vector(spec[indx[MAXPLOT-1]], 0, nspec[indx[MAXPLOT-1]]-1); 
                if (MAXPLOT > 1) overplot--;
                end_plot(); 
                for (j = 0; j < overplot; j++) 
                    display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);
            }
            do {
                open_ok = read_ascfile(nspec+indx[overplot], &nfile, xx+indx[overplot], spec+indx[overplot], getname, filename, comment);
                if (filename[0] == '-') {
                    sprintf(title," "); 
                    open_ok = 0; 
                    if ((overplot > 1) && !full) { 
                        free_vector(xx[indx[overplot-1]],   0, nspec[indx[overplot-1]]-1);
                        free_vector(spec[indx[overplot-1]], 0, nspec[indx[overplot-1]]-1);
                        if (mode == 1) free_vector(err[indx[overplot-1]], 0, nspec[indx[overplot-1]]-1);
                        overplot--; 
                        end_plot(); 
                        for (j = 0; j < overplot; j++) 
                            display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[j]],spec[indx[j]],err[indx[j]],nspec[indx[j]],j,mode);
                    }
                    full = 0; 
                } else if (open_ok) { 
                    printf("File not found.  Try again\n");
                } else { 
                    overplot++; 
                    if (nfile > 1) 
                        select_file(nspec[indx[overplot-1]],nfile,spec+indx[overplot-1]); 
if (mode == 1) { 
read_ascfile(nspec+indx[overplot-1], &nfile, xx+indx[overplot-1], err+indx[overplot-1], 0, filename, comment);
select_file(nspec[indx[overplot-1]],nfile,err+indx[overplot-1]); 
}
                    display(xmin,xmax,ymin,ymax,lab_x,lab_y,title,xx[indx[overplot-1]],spec[indx[overplot-1]],err[indx[overplot-1]],nspec[indx[overplot-1]],overplot-1,mode);
                }
            } while (open_ok == 1);
            printf("\n"); 

/************  change Velocity scale */

        } else if ((answer[0] == 'V') || (answer[0] == 'v')) {
            printf("Rest wavelength (in A) : "); 
            scanf("%f", &lamref); 

/************  Quit         */

        } else if ((answer[0] == 'Q') || (answer[0] == 'q')) {
            end_plot(); 
            exit = 1;
        }

    } while (exit == 0);

    for (j = 0; j < overplot; j++) { 
        free_vector(xx[indx[j]],   0, nspec[indx[j]]-1);
        free_vector(spec[indx[j]], 0, nspec[indx[j]]-1); 
        if (mode == 1) free_vector(err[indx[j]],  0, nspec[indx[j]]-1); 
    }
    free_svector((void *) xx);
    free_svector((void *) spec);
    if (mode == 1) free_svector((void *) err);
}


static float vel(float wave, float lamref) 
{
    if (lamref < 1.0) return wave; 
    else              return (wave-lamref)*299792.5/lamref; 
}


static void display(float xmin, float xmax, float ymin, float ymax, 
                    char lab_x[], char lab_y[], char title[], float xx[], 
                    float spec[], float err[], int nspec, int noerase, int mode)
{
    float x[2], y[2], min, max, tmp; 
float *jj1, *jj2; 
    int   j, npix, pix1, pix2, init, step; 

    if (xmax < xmin) { 
        tmp = xmin; xmin = xmax; xmax = tmp; 
    }
    if (ymax < ymin) { 
        tmp = ymin; ymin = ymax; ymax = tmp; 
    }

min = xmin; max = xmax; 
pix1 = init = 0; 
    if (min <= xx[0]) min = xx[0]; 
    if (max >= xx[nspec-1]) max = xx[nspec-1];
do { 
    step = 25; 
    for (j = pix1; j < nspec; j += step) { 
        if (j+step > nspec) step = nspec-j; 
        pix1 = j+locat(min, xx+j, step); 
        if (xx[pix1] < xx[j+step-1]) break; 
    } 
    step = 25; 
    for (j = pix1; j < nspec; j += step) { 
        if (j+step > nspec) step = nspec-j; 
        pix2 = j+locat(max, xx+j, step); 
        if (xx[pix2] < xx[j+step-1]) break; 
    } 
/*
    pix2 = pix1+locat(max, xx+pix1, nspec-pix1)+1; 
*/
    if (pix2 > nspec-1) pix2 = nspec-1; 
    npix = pix2 - pix1 + 1; 
/*
xmin = 1.0; xmax = 0.0; 
pix1 = 0; pix2 = nspec; 
*/
if (mode == 1) { 
    jj1 = vector(0,npix-1); 
    jj2 = vector(0,npix-1); 
    for (j = 0; j < npix; j++) jj1[j] = spec[pix1+j]-err[pix1+j]; 
    for (j = 0; j < npix; j++) jj2[j] = spec[pix1+j]+err[pix1+j]; 
}

    if (noerase == 0) { 
        if (init == 0) init_plot(xmin,xmax,ymin,ymax,lab_x,lab_y,title,0,0);
/*
cpgsci(4); 
*/
        if (mode == 0) { 

	  //plot_sym(xx+pix1,spec+pix1,npix,4+noerase%4);
	  //plot_sym(xx+pix1,spec+pix1,npix,4);

            plot(xx+pix1,spec+pix1,npix,1);
        } else { 
            plot_erry(xx+pix1,jj1,jj2,npix);
            plot(xx+pix1,spec+pix1,npix,17);
        } 
    } else { 
/* cpgsci(2); 
if (noerase == 3) cpgsci(1); 
else if (noerase == 8) cpgsci(3); 
else if (noerase == 4) cpgsci(2); 
*/
        if (mode == 0) { 
            plot(xx+pix1,spec+pix1,npix,noerase%4+1);
/*
if (noerase == 2) plot_sym(xx+pix1,spec+pix1,npix,4);
else 
if (noerase >= 2) plot(xx+pix1,spec+pix1,npix,noerase%4+99);
else 
if (noerase == 3) 
            plot(xx+pix1,spec+pix1,npix,noerase%4+1);
else 
plot(xx+pix1,spec+pix1,npix,101);
*/
        } else { 
            plot_erry(xx+pix1,jj1,jj2,npix);
            plot_sym(xx+pix1,spec+pix1,npix,16+noerase);
        } 
if (mode == 1) { 
free_vector(jj1, 0, npix-1); 
free_vector(jj2, 0, npix-1); 
}
    } 

for (j = pix2+1; j < nspec; j++) 
    if (xx[j] <= xx[pix2]) { 
        pix1 = j; 
        break; 
    } 
init++; 
} while (j < nspec); 
}


static int locat(float x, float array[], int n)
{
    int klo, khi, k, dir;

    klo = 0; 
    khi = n-1; 
    if (array[0] < array[n-1]) 
        dir =  1; 
    else 
        dir = -1; 
    while (khi-klo > 1) {
    k = (khi + klo)/2;
    if (((array[k] > x) && (dir == 1)) || ((array[k] < x) && (dir == -1))) 
        khi = k;
    else 
        klo = k;
    }
    if (fabs(x-array[klo]) > fabs(array[khi]-x)) 
        return khi;
    else 
        return klo;
}


static void normalise(float *xx, float *spec, int nspec, int deg, int pix1, int pix2, 
                      int ok_to_plot) 
{
    int   npt, j, k, m, n, p, step, side; 
    float *xpt, *ypt, max, *cont, xx0; 
    double *coeff, tmp; 

    side = 0; 
    step = (pix2-pix1)/20; if (step < 1) step = 1; 
/*
if (step < 50) step = 50; 
*/
    npt = (float) (pix2-pix1+1)/step + 0.999; 
    xpt = vector(0, npt-1); ypt = vector(0, npt-1); 
    if (deg < 0) deg = 0; 
    coeff = dvector(0, deg); 
    cont = vector(0, nspec-1); 

    for (j = pix1, n = 0; j < pix2; j += step, n++) {
        max = -1.0e30; 
        for (k = 0; k < step && k+j < pix2; k++) {
            tmp = 0.0; p = 0; 
            for (m = -side; m <= side; m++) 
                if ((j+k+m >= 0) && (j+k+m < nspec)) {
                    tmp += spec[j+k+m]; 
                    p++; 
                }
            if (tmp/p > max) { 
                max = tmp/p; 
                xx0 = xx[k+j]; 
            }
        }
        xpt[n] = xx0; 
        ypt[n] = max; 
    }
    adjust(xpt,ypt,n,coeff,deg,2.0); 
    poly(xx+pix1,cont+pix1,pix2-pix1,0.0,coeff,deg); 

    for (j = pix1; j < pix2; j++) spec[j] /= cont[j]; 

    free_vector(cont, 0, nspec-1); 
    free_vector(xpt, 0, npt-1); free_vector(ypt, 0, npt-1); 
    free_dvector(coeff, 0, deg); 
}


static void fitt(float *xx, float *spec, int nspec, int deg, int pix1, int pix2, 
                int ok_to_plot) 
{
    int   j, k, n, k0, nnn; 
    float *cont, *xxx, *yyy; 
    double *coeff, tmp, tmp2; 

    if (deg < 0) deg = 0; 
    coeff = dvector(0, deg); 
    cont = vector(0, nspec-1); 
    adjust(xx+pix1,spec+pix1,pix2-pix1+1,coeff,deg,2.5); 
nnn=1000; 
xxx=vector(0,nnn-1); 
yyy=vector(0,nnn-1); 
for (j = 0; j < nnn; j++) xxx[j] = xx[pix1]+(xx[pix2]-xx[pix1])*j/nnn; 
    poly(xxx,yyy,nnn,0.0,coeff,deg); 
    poly(xx+pix1,cont+pix1,pix2-pix1,0.0,coeff,deg); 
/*
for (j = 0; j < nnn; j++) {
xxx[j] = xx[pix1]+(xx[pix2]-xx[pix1])*j/nnn; 
tmp = 0.0; tmp2 = xxx[j]; 
for (n = deg; n >= 0; n--) tmp = tmp*tmp2 + coeff[n];
yyy[j]  = tmp;
}
    
    for (j = pix1; j <= pix2; j++) {
        tmp = 0.0; tmp2 = xx[j]; 
        for (n = deg; n >= 0; n--) tmp = tmp*tmp2 + coeff[n];
        cont[j]  = tmp;
    } 
*/
/*
    if (ok_to_plot) plot(xx+pix1,cont+pix1,pix2-pix1+1,2);
*/
    if (ok_to_plot) plot(xxx,yyy,nnn,2);
free_vector(xxx,0,nnn-1); 
free_vector(yyy,0,nnn-1); 

    printf("Divide or subtract (0/1) : "); 
    scanf("%d", &k); 
    for (j = pix1; j <= pix2; j++) 
        if (!k) spec[j] /= cont[j]; 
        else    spec[j] -= cont[j]; 

    free_vector(cont, 0, nspec-1); 
    free_dvector(coeff, 0, deg); 
}


static void select_file(int npt, int nfile, float **x)
{
    int   j, k; 
    float *tmp; 

    do { 
        printf("File to be selected (1-%d) : ", nfile); 
        scanf("%d", &k); 
    } while ((k > nfile) || (k < 1)); 
    k--; 

    tmp = vector(0, npt-1); 
    for (j = 0; j < npt; j++) tmp[j] = (*x)[j+k*npt];  
    free_vector(*x, 0, npt*nfile-1); 
    *x = tmp; 
}


#undef MAXPLOT
