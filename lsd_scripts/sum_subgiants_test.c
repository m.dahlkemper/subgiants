#include <stdio.h> 
#include <stdlib.h> 
#include <strings.h> 
#include <math.h> 
#include "structures.h" 
#include "file.h" 
#include "util.h" 
#include "mem_alloc.h" 
#include "plot.h" 
#include "line_esp.h" 
 
int comments;  
int read_lines(int *nlines, double **wave, float **code, float **prof, float **exc,  
               float **lande, int **tobeused, int getname, char filename[]); 
static void save_lines(int nlines, int nsel, double *wave, float *code, float *prof, float *ep, 
                       float *lande, int *index, int *used, int getname, char filename[]);  
  
 
int main(int argc, char *argv[]) 
{ 
    int    getname, nspec, j, k, err, nlines, nvel, choice, ok_to_plot, batch,  
           test, bswap, pol, plot_dev, *tobeused, nord, *nppo, nsel, *index,  
           *pix, *fpix, *lpix, *used, *first, *last, ntot, min, max, invpol, contpol, nfile,  
           *ffpix, *llpix;  
    char   filename[80], comment[80], answer[2];  
    float  *lam, *lamv, *lamn, *spec, *I, *V, *N, *errI, *code, *prof, *lande, *meanF,  
           vell, velu, *vel, stepvel, tmp, *wwv, *wwi, *sn, *ll, *ii, *vv, *nn, *ss,  
           geq, leq, eeq, weq, liml, limu, *mean, *meanI, *meanV, *meanN, *meanS, filt,  
           *exc, *mean_errI, *mean_errV, *mean_errN, *spec_s, *Is, *Vs, *Ns;  
    double *wave;  
 
// geq = 1.208; eeq = .543; leq = 560.000; // epsilon Eridani mask_5000_g4.5md0.1 NARVAL data
geq = 1.208; eeq = yyyy; leq = xxxx; // epsilon Eridani mask_5000_g4.5md0.1 HARPSpol data
// geq = 1.208; eeq = .426; leq = 554.002; // epsilon Eridani mask_5000_g4.5md0.1

 
    parse_args(argc,argv,&batch,&ok_to_plot,&comments,&bswap,&test,&invpol,&contpol); 
    if (ok_to_plot == 1)      plot_dev = 0;     
    else if (ok_to_plot == 2) plot_dev = 1;     
    else                      plot_dev = -1;     
    set_trace(ok_to_plot, comments, test);  
 
/************  Reads in one spectrum */ 
    getname = 1;  
    do { 
        printf("Intensity spectrum : \n");  
        err = read_ascfile(&nspec, &nfile, &lam, &spec, getname, filename, comment); 
        if (batch) printf("%s\n",filename); 
        if (err) 
            if (batch) { 
                printf("Error : file not found\n"); 
                exit(1); 
            } else 
                printf("File not found.  Try again\n"); 
    } while (err); 
 
    pol = 0;  
    I = spec;  
    if (nfile == 4) {  
        pol = 1;  
        V = spec+nspec;  
        N = spec+2*nspec;  
        errI = spec+3*nspec;  
    } else if (nfile == 5) {  
        pol = 1;  
        V = spec+nspec;  
//        N = spec+3*nspec; //N2  
        N = spec+2*nspec;  
        errI = spec+4*nspec;  
    } else if (nfile == 6) {  
        pol = 0;  
        errI = spec+3*nspec;  
    } else if (nfile == 2)  
        errI = spec+nspec;  
    else {  
        printf("Wrong file\n");  
        free_vector(spec, 0, nfile*nspec-1);  
        free_vector(lam, 0, nspec-1);  
        exit(1);  
    }  
 
    do { 
        err = read_lines(&nlines, &wave, &code, &prof, &exc, &lande, &tobeused, getname,  
                         filename);  
        if (batch) printf("%s\n",filename); 
        if (err) 
            if (batch) { 
                printf("Error : file not found\n"); 
                exit(1); 
            } else 
                printf("File not found.  Try again\n"); 
    } while (err); 
 
/* Chooses method of averaging */  
    printf("Weighting (0: g, 1: prof*g, 2: lam*prof*g, 3: prof, 4: lam*prof, 5: 1, 6:prof*(lam*g)^2) : "); 
    scanf("%d", &choice);  
    printf("%d\n", choice);  
 
    if (choice == 0)      weq = geq;  
    else if (choice == 1) weq = geq*eeq;  
    else if (choice == 2) weq = 0.002*geq*eeq*leq;  
    else if (choice == 3) weq = eeq;  
    else if (choice == 4) weq = 0.002*eeq*leq;  
    else if (choice == 5) weq = 1.0;  
    else if (choice == 6) weq = 0.000004*geq*geq*eeq*leq*leq;  
 
/* Sets up domain */  
    printf("Upper and lower velocities (km/s) : ");  
    scanf("%f %f", &vell, &velu);  
    printf("%f %f\n", vell, velu);  
    if (velu < vell) {  
        tmp  = velu;  
        velu = vell;  
        vell = tmp;  
    } 
 
/* Sets velocity step in LSD spectra */  
    printf("Velocity step in LSD profile (km/s) : ");  
    scanf("%f", &stepvel);  
    printf("%f\n", stepvel);  
 
/* Fixes width of Fourier filtering */  
    printf("Width of Fourier filtering (pix) : ");  
    scanf("%f", &filt);  
    printf("%f\n", filt);  
    printf("\n");  
 
/* Measures mean pixel separation */  
    get_resol(lam,nspec,&nppo,&nord,&stepvel);  
 
/* Removes continuum polarisation if present */  
    if (pol) cont_pol(lam,V,N,nppo,nord,pol);  
 
/* Allocates memory */  
    nvel = (velu - vell)/stepvel+1.999;  
    vel = vector(0, nvel-1);  
    for (j = 0; j < nvel; j++) vel[j] = vell + stepvel*j;  
 
/* Selects available lines and computes corresponding weights */  
    get_lines(lam,nspec,nppo,nord,wave,tobeused,nlines,&index,&pix,&used,&nsel);  
 
    fpix  = ivector(0, nsel-1); ffpix = ivector(0, nsel-1);  
    lpix  = ivector(0, nsel-1); llpix = ivector(0, nsel-1); 
    first = ivector(0, nsel-1);  
    last  = ivector(0, nsel-1);  
    liml = vel[0]; limu = vel[nvel-1];  
    select_lines(lam,nspec,wave,index,pix,used,nsel,nppo,nord,liml,limu, 
                 fpix,lpix,vel,nvel,first,last);  
    get_limits(vel,nvel,lam,spec,nspec,wave,prof,index,fpix,lpix,used,first, 
               last,nsel,&liml,&limu,0.5);  
    for (j = 0; j < nsel; j++) used[j] = 1; 
    select_lines(lam,nspec,wave,index,pix,used,nsel,nppo,nord,liml,limu, 
                 fpix,lpix,vel,nvel,first,last);  
    for (j = 0; j < nsel; j++) {  
        ffpix[j] = fpix[j];  
        llpix[j] = lpix[j];  
    }  
 
    wwi = vector(0, nsel-1);  
    wwv = vector(0, nsel-1);  
    sn  = vector(0, nsel-1);  
    get_weights(I,errI,nspec,wave,code,prof,lande,used,wwi,wwv,sn,index,fpix, 
                lpix,nsel,choice,weq,eeq);  
 
/* Extracts signal by least square deconvolution from core spectrum */  
    extract_spec(lam,I,V,N,errI,nspec,used,fpix,lpix,nsel,pol,&ll,&ii, 
                 &vv,&nn,&ss,&ntot);  
 
    mean = vector(0, (4*pol+2)*nvel-1);  
    if (pol) meanF = vector(0, 2*nvel-1);  
    meanI = mean;  
    mean_errI = mean+nvel;  
    if (pol) {  
        meanV = mean+2*nvel;  
        mean_errV = mean+3*nvel;  
        meanN = mean+4*nvel;  
        mean_errN = mean+5*nvel;  
    }  
 
    ls_dcv(vel,nvel,ll,ii,vv,nn,ss,ntot,fpix,lpix,used,wwi,wwv,wave,index,first, 
           last,nsel,pol,meanI,meanV,meanN,mean_errI,mean_errV,mean_errN);  
    if (pol) filter(meanV,meanN,meanF,first,last,used,nsel,nvel,filt);  
 
/* Saves sublist of used lines */  
/* 
    save_lines(nlines,nsel,wave,code,prof,exc,lande,index,used,0,"atlas/sub");  
*/ 
 
/* Line statistics */  
    statistics(vel,meanI,meanV,meanN,mean_errV,mean_errN,nvel,pol,wwv,wwi, 
               sn,wave,lande,prof,exc,used,index,first,last,nsel);  
 
/* Plots results */  
    printf("Do you want to save results (y/n) : ");  
    scanf("%1s", answer);  
    if ((answer[0] == 'y') || (answer[0] == 'Y')) {  
        if (batch) printf("yes\n");  
        getname = 1;  
        save_ascfile(nvel,4*pol+2,vel,mean,getname,filename,comment); 
        if (batch) printf("%s\n", filename);  
    } else  
        if (batch) printf("no\n");  
/* 
    spec_s = vector(0, nspec*nfile-1);  
    Is = spec_s;  
    if (pol) {  
        Vs = spec_s+nspec;  
        Ns = spec_s+2*nspec;  
        for (j = 0; j < nspec; j++) spec_s[j+3*nspec] = errI[j];  
    } else  
        for (j = 0; j < nspec; j++) spec_s[j+nspec] = errI[j];  
    conv(lam,nspec,used,index,ffpix,llpix,nsel,wave,pol,Is,Vs,Ns,vel, 
         meanI,meanV,meanN,wwi,wwv,nvel);  
    for (j = 0; j < nspec; j++) {   
        Is[j] = I[j]-Is[j];  
        if (pol) {  
            Vs[j] = V[j]-Vs[j];  
            Ns[j] = N[j]-Ns[j];  
        }  
    }  
    save_ascfile(nspec,nfile,lam,spec_s,getname,filename,comment); 
    free_vector(spec_s, 0, nspec*nfile-1); 
*/ 
    show(vel,meanI,meanV,meanN,meanF,nvel,first,last,used,nsel,pol);  
 
/* Releases memory */  
    free_vector(vel,   0, nvel-1);  
    free_vector(mean,  0, (4*pol+2)*nvel-1);  
    if (nfile ==4) free_vector(meanF,  0, 2*nvel-1);  
    free_ivector(fpix,  0, nsel-1); free_ivector(lpix, 0, nsel-1);  
    free_ivector(ffpix,  0, nsel-1); free_ivector(llpix, 0, nsel-1);  
    free_ivector(first, 0, nsel-1); free_ivector(last, 0, nsel-1);  
    free_dvector(wave, 0, nlines-1);  
    free_vector(code,  0, nlines-1);  
    free_vector(prof,  0, nlines-1);  
    free_vector(exc ,  0, nlines-1);  
    free_vector(lande, 0, nlines-1);  
    free_vector(wwi,   0, nsel-1);  
    free_vector(wwv,   0, nsel-1);  
    free_vector(sn,    0, nsel-1);  
    free_ivector(used, 0, nsel-1);  
    free_ivector(index, 0, nsel-1);  
    free_vector(lam,   0, nspec-1);  
    free_vector(spec,  0, nfile*nspec-1);  
    free_ivector(nppo, 0, nord-1);  
    free_ivector(pix, 0, nsel-1);  
} 
 
 
/* Reads list of lines */  
 
int read_lines(int *nlines, double **wave, float **code, float **prof, float **exc,  
               float **lande, int **tobeused, int getname, char filename[])  
{ 
    FILE  *fp; 
    int   j, k, n, ntot; 
 
    if (getname) { 
        printf("Name of file to read lines from : "); 
        scanf("%s", filename); 
    } 
 
    fp = fopen(filename,"r"); 
    if (fp == NULL) return 1; 
 
    fscanf(fp, "%d", nlines);  
    *wave  = dvector(0, *nlines-1);  
    *code  =  vector(0, *nlines-1);  
    *prof  =  vector(0, *nlines-1);  
    *exc   =  vector(0, *nlines-1);  
    *lande =  vector(0, *nlines-1);  
    *tobeused  = ivector(0, *nlines-1);  
    for (j = 0; j < *nlines; j++)  
        fscanf(fp, "%lf %f %f %f %f %d", *wave+j, *code+j, *prof+j, *exc+j, *lande+j,  
                                         *tobeused+j);  
    fclose(fp);  
    return 0;  
} 
 
 
static void save_lines(int nlines, int nsel, double *wave, float *code, float *prof, float *ep, 
                       float *lande, int *index, int *used, int getname, char filename[]) 
{ 
    int   j,nused,*uu; 
    FILE  *fp; 
 
    if (getname) { 
        printf("Name of file to save lines from : "); 
        scanf("%s", filename); 
    } 
 
    uu = ivector(0,nlines-1);  
    for (j = 0; j < nlines; j++) uu[j] = 0;  
 
    for (j = nused = 0; j < nsel; j++)  
        if ((!uu[index[j]]) && (used[j])) {  
            uu[index[j]] = 1;  
            nused++;  
        }  
 
    fp = fopen(filename,"w"); 
    fprintf(fp, "%d\n", nused); 
 
    for (j = 0; j < nlines; j++) 
        if (uu[j])  
            fprintf(fp, "%8.4f  %5.2f  %5.3f  %5.3f  %5.3f  1\n",wave[j],code[j],prof[j],ep[j],lande[j]); 
 
    fclose(fp); 
} 
