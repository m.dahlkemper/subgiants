#include <stdio.h>
#include <math.h>
#include "structures.h"
#include "file.h"
#include "mem_alloc.h"

main()
{
	float *lam, *data, *errdata, fluxh, fluxk, fluxv, fluxr, weighth, weightk, weightv, weightr, 
		  velstar, deltalam, deltalamca, deltalamcont, vel_light, lamh, lamk, lamv, lamr;
	int    i, j, k, npts, n;
	char   filename[80], comment[120];

	read_ascfile(&npts, &n, &lam, &data, 1, filename, comment); 

	lamk = 393.3663;
	lamh = 396.8469;
	lamv = 390.107;
	lamr = 400.107;
	deltalamca = .218;
	deltalamcont = 2.;
	vel_light = 299792.5;

	errdata = vector(0, npts -1);
    if (n < 4) errdata = data + npts;
	else errdata = data + 4*npts;

	fluxh = fluxk = fluxv = fluxr = 0.;
	weighth = weightk = weightv = weightr = 0.;

	printf("stellar velocity?\n");
	scanf("%f", &velstar);

	deltalam = velstar/vel_light;
	lamh *= (1. + deltalam);
	lamk *= (1. + deltalam);
	lamv *= (1. + deltalam);
	lamr *= (1. + deltalam);
	
	for (j = 0; j < npts; j++) 
	{
	errdata[j] = 1.;
		if ((lam[j] > lamv - deltalamcont/2.) && (lam[j] < lamv + deltalamcont/2.)) 
		{
			fluxv += data[j]/errdata[j];
			weightv += 1./errdata[j];
		} 

		if ((lam[j] > lamr - deltalamcont/2.) && (lam[j] < lamr + deltalamcont/2.)) 
		{
			fluxr += data[j]/errdata[j];
			weightr += 1./errdata[j];
		}

		if ((lam[j] > lamh - deltalamca/2.) && (lam[j] < lamh + deltalamca/2.)) 
		{
			fluxh += data[j]/errdata[j]*(deltalamca/2. - fabs(lam[j] - lamh))/deltalamca*2.;
			weighth += 1./errdata[j]*(deltalamca/2. - fabs(lam[j] - lamh))/deltalamca*2.;
		}

		if ((lam[j] > lamk - deltalamca/2.) && (lam[j] < lamk + deltalamca/2.)) 
		{
			fluxk += data[j]/errdata[j]*(deltalamca/2. - fabs(lam[j] - lamk))/deltalamca*2.;
			weightk += 1./errdata[j]*(deltalamca/2. - fabs(lam[j] - lamk))/deltalamca*2.;
		}
	}

	fluxv /= weightv;
	fluxr /= weightr;
	fluxh /= weighth;
	fluxk /= weightk;
	//printf ("V = %f\nR = %f\nH = %f\nK = %f\n", fluxv, fluxr, fluxh, fluxk);
	// same notation as Wright et al 2004 -- S = (H+K)/(V+R) with scaling for Mnt Wilson

//from Stephen:
//The parameters for NARVAL are:
//(-1.45563335e+03*fluxh - 7.48877076e+03*fluxk)/(-5.16404110e+03*fluxv - 2.48461978e+03*fluxr) + 1.18285595e-03
//The parameters for ESPADONS are:
//(-0.90321508*fluxh + 1.85056153*fluxk)/(0.26606579*fluxv + 0.29814537*fluxr) - 0.06867997

	printf ("Sindex = %f\n",(-1.45563335e+03*fluxh - 7.48877076e+03*fluxk)/(-5.16404110e+03*fluxv - 2.48461978e+03*fluxr) + 1.18285595e-03);
	printf ("Sindex_HARPS = %f\n",1.111*(2.3*(fluxh+fluxk)/(fluxv+fluxr)) + 0.0153);
//	printf ("Sindex = %f\n", (-6.39439179e+00*fluxh + 1.64816487e+02*fluxk)/(2.06646020e+01*fluxv + 9.49961109e+01*fluxr)-3.25173833e-02);
	//printf ("Sindex = %f\n", (fluxh + fluxk)/(fluxv + fluxr));
}
