void set_device(int dev_type);
void init_plot(float xmin, float xmax, float ymin, float ymax, char lab_x[], 
               char lab_y[], char title[], int axis, int log_plot);
void init_multiplot(int j, int k); 
void start_plot(float xmin, float xmax, float ymin, float ymax, char lab_x[],
               char lab_y[], char title[], int axis, int log_plot); 
void plot(float x[], float spec[], int length, int ls);
void plot_circle(float xc, float yc, float rad, int ls); 
void plot_sym(float x[], float spec[], int length, int symbol);
void plot_errx(float xl[], float xu[], float y[], int npt);
void plot_erry(float x[], float yl[], float yu[], int npt);
void write_text(char text[], float x, float y);
void write_smalltext(char text[], float x, float y);
void write_bigtext(char text[], float x, float y);
void write_lab(char text[], float x, float y);
void get_curs(float *x, float *y, char curse[]);
void contours(float **data, float *x, float *y, int nx, int ny, float min, float max, 
              int nc); 
void image(float **data, float *x, float *y, int nx, int ny, float min, float max, int nc, 
           int rect, int lut, int print_lut); 
void fill_poly(float *x, float *y, int n, int col); 
void end_plot(void);
void get_points(float xc[], float yc[], char curse[][2], int nptmax, int *npt, 
                int plot);
void minmax(float spec[], int length, float *min, float *max); 
