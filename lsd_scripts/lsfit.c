#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <math.h>
#include "mem_alloc.h"
#include "plot.h"
#include "invert.h"
#include "mrqmin.h"
#include "lsfit.h"

#define PI 3.141592654

static int comb(int n, int p); 
static void gauss_param(float *fwhm, float *ew, float *sigma, float *amp, 
                        int dir);
static void tgauss(float x[], float a[], float y[], float **dyda, int na, 
                  int nfit, int ndata); 
static void dgauss(float x[], float a[], float y[], float **dyda, int na, 
                  int nfit, int ndata); 
static void gauss(float x[], float a[], float y[], float **dyda, int na, 
                  int nfit, int ndata); 

/* gaussian_lsfit returns -1.0 if singular matrix in gaussj, 
                          -2.0 if improper permutation in lista, 
                          Csq otherwise */ 

float gaussian_lsfit(float lam[], float spec[], int nspec, float *centr, 
                    float *fwhm, float *ew, float cut, int ok_to_plot)
{
    int   mfit, n_param, *lista, iter, maxit, fault, j, res; 
    float *sig, *param, *covar, *alpha, Csq, test, old_test, **dyda, *yy; 

    n_param = mfit = 4; 
    sig = vector(0, nspec-1);         
    yy = vector(0, nspec-1);         
    param = vector(0, n_param-1);         
    dyda  = matrix(0, n_param-1, 0, nspec-1);
    lista = ivector(0, n_param-1); 
    alpha = vector(0, n_param*n_param-1); 
    covar = vector(0, n_param*n_param-1); 
    param[0] = *centr; param[3] = 0.5*(spec[0]+spec[nspec-1]); 
    gauss_param(fwhm, ew, &param[1], &param[2], 1); 
    for (j = 0; j < n_param; j++) lista[j] = j; 
    for (j = 0; j < nspec; j++) 
        if (spec[j] < cut) sig[j] = 0.1;  
        else               sig[j] = 10.0; 
    
    maxit = 30; fault = 0; test = -1.0; old_test = 0.001; 
    for (iter = 0; (iter < maxit) && (fault < 10); iter++) {

      /*  printf("i = %d centr = %f %f %f %f >> %f\n",iter,param[0],param[1],param[2],param[3],Csq); */

        res = mrqmin(lam,spec,sig,nspec,param,n_param,lista,mfit,covar,alpha,
               n_param,&Csq,gauss,&test);
        if (res) return -res; 

        if (test > old_test) 
            fault++; 
        else 
            fault = 0;
        old_test = test; 
    }
    test = 0; 
    res = mrqmin(lam,spec,sig,nspec,param,n_param,lista,mfit,covar,alpha,
           n_param,&Csq,gauss,&test);
    if (res) return -res; 

    *centr = param[0]; 
    gauss_param(fwhm, ew, &param[1], &param[2], -1); 

    if (ok_to_plot == 1) {    
        gauss(lam,param,yy,dyda,n_param,mfit,nspec);
        plot(lam, yy, nspec, 1); 
    }

    free_vector(sig, 0, nspec-1);
    free_vector(yy, 0, nspec-1);
    free_vector(param, 0, n_param-1);
    free_matrix(dyda,  0, n_param-1, 0, nspec-1);
    free_ivector(lista, 0, n_param-1);
    free_vector(covar, 0, n_param*n_param-1);
    free_vector(alpha, 0, n_param*n_param-1);
    return Csq; 
}


float tgaussian_lsfit(float lam[], float spec[], int nspec, 
                      float *centr1, float *fwhm1, float *ew1, 
                      float *centr2, float *fwhm2, float *ew2, 
                      float *centr3, float *fwhm3, float *ew3, 
                      float cut, int ok_to_plot)
{
    int   mfit, n_param, *lista, iter, maxit, fault, j, res; 
    float *sig, *param, *covar, *alpha, Csq, test, old_test, **dyda, *yy; 

    n_param = mfit = 10; 
    sig = vector(0, nspec-1);         
    yy = vector(0, nspec-1);         
    param = vector(0, n_param-1);         
    dyda  = matrix(0, n_param-1, 0, nspec-1);
    lista = ivector(0, n_param-1); 
    alpha = vector(0, n_param*n_param-1); 
    covar = vector(0, n_param*n_param-1); 
    param[0] = *centr1; param[3] = *centr2; param[6] = *centr3; 
    gauss_param(fwhm1, ew1, &param[1], &param[2], 1); 
    gauss_param(fwhm2, ew2, &param[4], &param[5], 1); 
    gauss_param(fwhm3, ew3, &param[7], &param[8], 1); 
    param[9] = 0.5*(spec[0]+spec[nspec-1]); 
    for (j = 0; j < n_param; j++) lista[j] = j; 
    for (j = 0; j < nspec; j++) 
        if (spec[j] < cut) sig[j] = 0.1;  
        else               sig[j] = 10.0; 

    maxit = 30; fault = 0; test = -1.0; old_test = 0.001; 
    for (iter = 0; (iter < maxit) && (fault < 10); iter++) {
/*
printf("%d %f %f %f %f >> %f\n",iter,param[0],param[1],param[2],param[3],Csq); 
*/
        res = mrqmin(lam,spec,sig,nspec,param,n_param,lista,mfit,covar,alpha,
               n_param,&Csq,tgauss,&test);
        if (res) return -res; 

        if (test > old_test) 
            fault++; 
        else 
            fault = 0;
        old_test = test; 
    }
    test = 0; 
    res = mrqmin(lam,spec,sig,nspec,param,n_param,lista,mfit,covar,alpha,
           n_param,&Csq,tgauss,&test);
    if (res) return -res; 

    *centr1 = param[0]; 
    *centr2 = param[3]; 
    *centr3 = param[6]; 
    gauss_param(fwhm1, ew1, &param[1], &param[2], -1); 
    gauss_param(fwhm2, ew2, &param[4], &param[5], -1); 
    gauss_param(fwhm3, ew3, &param[7], &param[8], -1); 

    if (ok_to_plot == 1) {    
        tgauss(lam,param,yy,dyda,n_param,mfit,nspec);
        plot(lam, yy, nspec, 1); 
    }

    free_vector(sig, 0, nspec-1);
    free_vector(yy, 0, nspec-1);
    free_vector(param, 0, n_param-1);
    free_matrix(dyda,  0, n_param-1, 0, nspec-1);
    free_ivector(lista, 0, n_param-1);
    free_vector(covar, 0, n_param*n_param-1);
    free_vector(alpha, 0, n_param*n_param-1);
    return Csq; 
}


float dgaussian_lsfit(float lam[], float spec[], int nspec, 
                      float *centr1, float *fwhm1, float *ew1, 
                      float *centr2, float *fwhm2, float *ew2, 
                      float cut, int ok_to_plot)
{
    int   mfit, n_param, *lista, iter, maxit, fault, j, res; 
    float *sig, *param, *covar, *alpha, Csq, test, old_test, **dyda, *yy; 

    n_param = mfit = 7; 
    sig = vector(0, nspec-1);         
    yy = vector(0, nspec-1);         
    param = vector(0, n_param-1);         
    dyda  = matrix(0, n_param-1, 0, nspec-1);
    lista = ivector(0, n_param-1); 
    alpha = vector(0, n_param*n_param-1); 
    covar = vector(0, n_param*n_param-1); 
    param[0] = *centr1; param[3] = *centr2;  
    gauss_param(fwhm1, ew1, &param[1], &param[2], 1); 
    gauss_param(fwhm2, ew2, &param[4], &param[5], 1); 
    param[6] = 0.5*(spec[0]+spec[nspec-1]); 
    for (j = 0; j < n_param; j++) lista[j] = j; 
    for (j = 0; j < nspec; j++) 
        if (spec[j] < cut) sig[j] = 0.1;  
        else               sig[j] = 10.0; 

    maxit = 30; fault = 0; test = -1.0; old_test = 0.001; 
    for (iter = 0; (iter < maxit) && (fault < 10); iter++) {
/*
printf("%d %f %f %f %f >> %f\n",iter,param[0],param[1],param[2],param[3],Csq); 
*/
        res = mrqmin(lam,spec,sig,nspec,param,n_param,lista,mfit,covar,alpha,
               n_param,&Csq,dgauss,&test);
        if (res) return -res; 

        if (test > old_test) 
            fault++; 
        else 
            fault = 0;
        old_test = test; 
    }
    test = 0; 
    res = mrqmin(lam,spec,sig,nspec,param,n_param,lista,mfit,covar,alpha,
           n_param,&Csq,tgauss,&test);
    if (res) return -res; 

    *centr1 = param[0]; 
    *centr2 = param[3]; 
    gauss_param(fwhm1, ew1, &param[1], &param[2], -1); 
    gauss_param(fwhm2, ew2, &param[4], &param[5], -1); 

    if (ok_to_plot == 1) {    
        dgauss(lam,param,yy,dyda,n_param,mfit,nspec);
        plot(lam, yy, nspec, 1); 
    }

    free_vector(sig, 0, nspec-1);
    free_vector(yy, 0, nspec-1);
    free_vector(param, 0, n_param-1);
    free_matrix(dyda,  0, n_param-1, 0, nspec-1);
    free_ivector(lista, 0, n_param-1);
    free_vector(covar, 0, n_param*n_param-1);
    free_vector(alpha, 0, n_param*n_param-1);
    return Csq; 
}


static void gauss(float x[], float a[], float y[], float **dyda, int na, 
                  int nfit, int ndata)
{
    float arg, ex, fac; 
    int   j;

    a[1] = fabs((double) a[1]); 
    for (j = 0; j < ndata; j++) { 
        arg = (x[j] - a[0]) / a[1]; 
        ex = exp(-arg*arg);
        fac = a[2]*ex*2.0*arg; 
        y[j] = a[2]*ex + a[3]; 
        *(*(dyda  )+j) = fac/a[1]; 
        *(*(dyda+1)+j) = fac*arg/a[1]; 
        *(*(dyda+2)+j) = ex; 
        *(*(dyda+3)+j) = 1.0; 
        if (na > 4) {
            y[j] += a[4] * (x[j] - a[0]);
            *(*(dyda  )+j) += -a[4];
            *(*(dyda+4)+j)  =  x[j] - a[0];
        }
    }
}


static void tgauss(float x[], float a[], float y[], float **dyda, int na, 
                  int nfit, int ndata)
{
    float arg, ex, fac; 
    int   j;

    a[1] = fabs((double) a[1]); 
    a[4] = fabs((double) a[4]); 
    a[7] = fabs((double) a[7]); 
    for (j = 0; j < ndata; j++) { 

        arg = (x[j] - a[0]) / a[1]; 
        ex  = exp(-arg*arg);
        fac = a[2]*ex*2.0*arg; 
        *(*(dyda  )+j) = fac/a[1]; 
        *(*(dyda+1)+j) = fac*arg/a[1]; 
        *(*(dyda+2)+j) = ex; 
        y[j] = a[2]*ex; 


        arg = (x[j] - a[3]) / a[4]; 
        ex  = exp(-arg*arg);
        fac = a[5]*ex*2.0*arg; 
        *(*(dyda+3)+j) = fac/a[4]; 
        *(*(dyda+4)+j) = fac*arg/a[4]; 
        *(*(dyda+5)+j) = ex; 
        y[j] += a[5]*ex; 

        arg = (x[j] - a[6]) / a[7]; 
        ex  = exp(-arg*arg);
        fac = a[8]*ex*2.0*arg; 
        *(*(dyda+6)+j) = fac/a[7]; 
        *(*(dyda+7)+j) = fac*arg/a[7]; 
        *(*(dyda+8)+j) = ex; 
        y[j] += a[8]*ex; 

        *(*(dyda+9)+j) = 1.0; 
        y[j] += a[9]; 
    }
}


static void dgauss(float x[], float a[], float y[], float **dyda, int na, 
                  int nfit, int ndata)
{
    float arg, ex, fac; 
    int   j;

    a[1] = fabs((double) a[1]); 
    a[4] = fabs((double) a[4]); 
    for (j = 0; j < ndata; j++) { 

        arg = (x[j] - a[0]) / a[1]; 
        ex  = exp(-arg*arg);
        fac = a[2]*ex*2.0*arg; 
        *(*(dyda  )+j) = fac/a[1]; 
        *(*(dyda+1)+j) = fac*arg/a[1]; 
        *(*(dyda+2)+j) = ex; 
        y[j] = a[2]*ex; 


        arg = (x[j] - a[3]) / a[4]; 
        ex  = exp(-arg*arg);
        fac = a[5]*ex*2.0*arg; 
        *(*(dyda+3)+j) = fac/a[4]; 
        *(*(dyda+4)+j) = fac*arg/a[4]; 
        *(*(dyda+5)+j) = ex; 
        y[j] += a[5]*ex; 

        *(*(dyda+6)+j) = 1.0; 
        y[j] += a[6]; 
    }
}


static void gauss_param(float *fwhm, float *ew, float *sigma, float *amp, 
                        int dir)
{
    float kk,cc; 

    kk = sqrt((double) PI); 
    cc = sqrt(log((double) 2.0));
    if (dir == 1) {
        *sigma =  0.5 * *fwhm / cc; 
        *amp   =  *ew / (*sigma * kk); 
    } else {
        *fwhm  =  2.0 * *sigma * cc; 
        *ew    =  *amp * *sigma * kk; 
    }
}


void interpol(float *x, float *spec, float *sig, int length, double *b, int n)
{
    int    p, q, k;
    double *mat, tmp, *sx, *sy, xx, yy, meanx, meany, s2, sigx, sigy;  

    sx  = dvector(0, 2*n);
    sy  = dvector(0, n);
    mat = dvector(0, n*n+2*n);

    meanx = meany = sigx = sigy = tmp = 0.0; 
    for (k = 0; k < length; k++) {
        s2 = sig[k]; s2 *= s2; 
        tmp   += (double)     1.0/s2; 
        meanx += (double)    x[k]/s2;
        meany += (double) spec[k]/s2;
        sigx  += (double) x[k]*x[k]/s2; 
        sigy  += (double) spec[k]*spec[k]/s2; 
    }
    meanx /= tmp; meany /= tmp; 
    sigx = sqrt(sigx/tmp - meanx*meanx); 
    sigy = sqrt(sigy/tmp - meany*meany); 

    for (k = 0; k <= 2*n; k++) sx[k] = 0.0;
    for (k = 0; k <= n; k++)   sy[k] = 0.0;
    for (p = 0; p < length; p++) {
        xx = (double) (x[p]-meanx)/sigx;
        yy = (double) (spec[p]-meany)/sigy;
        s2 = sig[p]; s2 *= s2; 
        tmp = 1.0;
        for (k = 0; k <= 2*n; k++, tmp *= xx) {
            sx[k] += tmp/s2;
            if (k <= n) sy[k] += tmp*yy/s2;
        }
    }
    for (p = 0; p <= n; p++) {
        for (q = 0; q <= n; q++) mat[q+p*(n+1)] = sx[p+q];
        b[p] = sy[p];
    }
    dgaussj(mat,n+1,n+1,b,1,1);

    for (k = 0, tmp = 1.0; k <= n; k++, tmp *= sigx) b[k] /= tmp/sigy; 
    if (n > 0)
        for (k = 0; k < n; k++)
            for (p = k+1, tmp = -meanx; p <= n; p++, tmp *= -meanx)
                b[k] += b[p]*comb(p,k)*tmp;
    b[0] += meany;

    free_dvector(sx,  0, 2*n);
    free_dvector(sy,  0, n);
    free_dvector(mat, 0, n*n+2*n);
}


void interpol_1d2(float *x, float *y, float *image, float *sig, int ntot,  
                  double *b, int nx, int ny)
{
    int     p, q, j, k, l, nxy, nz, np1, np2, nq1, nq2, npq;
    double  *mat, *sxy, *sz, **xn, **yn, xx, yy, zz, s2, meanx, meany, meanz, tmp, 
            sigx, sigy, sigz, tmp2;

    nxy = (2*nx+1)*(2*ny+1); 
    nz  = (nx+1)*(ny+1); 
    sxy = dvector(0, nxy-1);
    sz  = dvector(0, nz-1);
    mat = dvector(0, nz*nz-1);
    xn = dmatrix(0, ntot-1, 0, 2*nx); 
    yn = dmatrix(0, ntot-1, 0, 2*ny); 

    meanx = meany = meanz = sigx = sigy = sigz = tmp = 0.0; 
    for (p = 0; p < ntot; p++) {  
        s2 = sig[p]; s2 *= s2; 
        tmp   += (double)  1.0/s2; 
        meanx += (double) x[p]/s2;
        meany += (double) y[p]/s2;
        meanz += (double) image[p]/s2;
        sigx  += (double) x[p]*x[p]/s2; 
        sigy  += (double) y[p]*y[p]/s2; 
        sigz  += (double) image[p]*image[p]/s2; 
    }
    meanx /= tmp; meany /= tmp; meanz /= tmp; 
    sigx = sqrt(sigx/tmp - meanx*meanx); 
    sigy = sqrt(sigy/tmp - meany*meany); 
    sigz = sqrt(sigz/tmp - meanz*meanz); 

    for (p = 0; p < ntot; p++) {
        xx = (double) (x[p]-meanx)/sigx;  
        yy = (double) (y[p]-meany)/sigy;  
        for (j = 0, tmp = 1.0; j <= 2*nx; j++, tmp *= xx) xn[p][j] = tmp;  
        for (j = 0, tmp = 1.0; j <= 2*ny; j++, tmp *= yy) yn[p][j] = tmp;  
    }

    for (k = 0; k < nxy; k++) sxy[k] = 0.0;
    for (k = 0; k < nz;  k++) sz[k]  = 0.0;
    for (p = 0; p < ntot; p++) { 
        zz = (double) (image[p]-meanz)/sigz;
        s2 = sig[p]; s2 *= s2; 
        for (j = l = 0; j <= 2*ny; j++) 
            for (k = 0; k <= 2*nx; k++, l++) 
                sxy[l] += xn[p][k]*yn[p][j]/s2; 
        for (j = l = 0; j <= ny; j++) 
            for (k = 0; k <= nx; k++, l++) 
                sz[l] += zz*xn[p][k]*yn[p][j]/s2;
    }

    l = nx+1; 
    for (p = 0; p < nz; p++) {
        for (q = 0; q < nz; q++)  
            mat[q+p*nz] = sxy[p%l+q%l + (p/l+q/l)*(2*nx+1)];
        b[p] = sz[p];
    }
    dgaussj(mat,nz,nz,b,1,1);

    for (q = 0, tmp = 1.0; q <= ny; q++, tmp *= sigy)  
        for (k = 0, tmp2 = 1.0; k <= nx; k++, tmp2 *= sigx) 
            b[k+q*(nx+1)] /= tmp*tmp2/sigz; 
    if (nx > 0)  
        for (q = 0; q <= ny; q++)  
            for (k = 0; k < nx; k++)
                for (p = k+1, tmp = -meanx; p <= nx; p++, tmp *= -meanx)
                    b[k+q*(nx+1)] += b[p+q*(nx+1)]*comb(p,k)*tmp;
    if (ny > 0)
        for (q = 0; q <= nx; q++)
            for (k = 0; k < ny; k++)
                for (p = k+1, tmp = -meany; p <= ny; p++, tmp *= -meany)
                    b[k*(nx+1)+q] += b[p*(nx+1)+q]*comb(p,k)*tmp;
    b[0] += meanz;

    free_dvector(sxy, 0, nxy-1);
    free_dvector(sz, 0, nz-1);
    free_dvector(mat, 0, nz*nz-1);
    free_dmatrix(xn, 0, ntot-1, 0, 2*nx); 
    free_dmatrix(yn, 0, ntot-1, 0, 2*ny); 
}


void interpol_2d(float *x, float *y, float *image, float *sig, int ntot,  
                 double *b, int n)
{
    int     p, q, j, k, l, nxy, nz, np1, np2, nq1, nq2, npq;
    double  *mat, *sxy, *sz, **xn, **yn, xx, yy, zz, s2, meanx, meany, meanz, tmp, 
            sigx, sigy, sigz, tmp2;

    nxy = (2*n+1)*(n+1); 
    nz  = (n+1)*(n+2)/2; 
    sxy = dvector(0, nxy-1);
    sz  = dvector(0, nz-1);
    mat = dvector(0, nz*nz-1);
    xn = dmatrix(0, ntot-1, 0, 2*n); 
    yn = dmatrix(0, ntot-1, 0, 2*n); 

    meanx = meany = meanz = sigx = sigy = sigz = tmp = 0.0; 
    for (p = 0; p < ntot; p++) {  
        s2 = sig[p]; s2 *= s2; 
        tmp   += (double)  1.0/s2; 
        meanx += (double) x[p]/s2;
        meany += (double) y[p]/s2;
        meanz += (double) image[p]/s2;
        sigx  += (double) x[p]*x[p]/s2; 
        sigy  += (double) y[p]*y[p]/s2; 
        sigz  += (double) image[p]*image[p]/s2; 
    }
    meanx /= tmp; meany /= tmp; meanz /= tmp; 
    sigx = sqrt(sigx/tmp - meanx*meanx); 
    sigy = sqrt(sigy/tmp - meany*meany); 
    sigz = sqrt(sigz/tmp - meanz*meanz); 

    for (p = 0; p < ntot; p++) {
        xx = (double) (x[p]-meanx)/sigx;  
        yy = (double) (y[p]-meany)/sigy;  
        for (j = 0, tmp = 1.0; j <= 2*n; j++, tmp *= xx) xn[p][j] = tmp;  
        for (j = 0, tmp = 1.0; j <= 2*n; j++, tmp *= yy) yn[p][j] = tmp;  
    }

    for (k = 0; k < nxy; k++) sxy[k] = 0.0;
    for (k = 0; k < nz;  k++) sz[k]  = 0.0;
    for (p = 0; p < ntot; p++) { 
        zz = (double) (image[p]-meanz)/sigz;
        s2 = sig[p]; s2 *= s2; 
        for (j = l = 0; j <= 2*n; j++) 
            for (k = 0; k <= j; k++, l++) 
                sxy[l] += xn[p][j-k]*yn[p][k]/s2; 
        for (j = l = 0; j <= n; j++) 
            for (k = 0; k <= j; k++, l++) 
                sz[l] += zz*xn[p][j-k]*yn[p][k]/s2;
    } 

    for (p = 0; p < nz; p++) {
        for (j = np1 = 0; j+np1+1 <= p; j += np1) np1++;   
        np2 = p - j; np1 -= np2; 
        for (q = 0; q < nz; q++) { 
            for (j = nq1 = 0; j+nq1+1 <= q; j += nq1) nq1++;   
            nq2 = q - j; nq1 -= nq2;
            npq = np1+np2+nq1+nq2;  
            mat[q+p*nz] = sxy[npq*(npq+1)/2+np2+nq2];
        }
        b[p] = sz[p];
    }
    dgaussj(mat,nz,nz,b,1,1);

    for (q = 0, tmp = 1.0; q <= n; q++, tmp *= sigy)  
        for (k = 0, tmp2 = 1.0; k <= n-q; k++, tmp2 *= sigx) 
            b[(k+q)*(k+q+1)/2+q] /= tmp*tmp2/sigz; 
    if (n > 0) {
        for (q = 0; q < n; q++)
            for (k = 0; k < n-q; k++)
                for (p = k+1, tmp = -meanx; p <= n-q; p++, tmp *= -meanx)
                    b[(k+q)*(k+q+1)/2+q] += b[(p+q)*(p+q+1)/2+q]*comb(p,k)*tmp;
        for (q = 0; q < n; q++)
            for (k = 0; k < n-q; k++)
                for (p = k+1, tmp = -meany; p <= n-q; p++, tmp *= -meany)
                    b[(k+q)*(k+q+1)/2+k] += b[(p+q)*(p+q+1)/2+p]*comb(p,k)*tmp;
    } 
    b[0] += meanz;

    free_dvector(sxy, 0, nxy-1);
    free_dvector(sz, 0, nz-1);
    free_dvector(mat, 0, nz*nz-1);
    free_dmatrix(xn, 0, ntot-1, 0, 2*n); 
    free_dmatrix(yn, 0, ntot-1, 0, 2*n); 
}


float adjust(float *x, float *y, int npt, double *b, int deg, float nsig)
{
    int    j, k, *good, nout, maxpt;
    float  *dif, *sig, moy, dev;

    good = ivector(0, npt-1); 
    dif  =  vector(0, npt-1);
    sig  =  vector(0, npt-1); 

    for (j = 0; j < npt; j++) {
        good[j] = 1;
        sig[j]  = 1.0;
    }
    do {
        for (j = 0; j < npt; j++)
            if (!good[j]) sig[j] = 1000.0; 
        interpol(x, y, sig, npt, b, deg);
        poly(x, dif, npt, 0.0, b, deg); 
        for (j = 0; j < npt; j++) dif[j] = y[j]-dif[j];
        maxpt = npt/100; if (maxpt == 0) maxpt = 1; 
        nout = reject(dif, good, npt, &moy, &dev, nsig, maxpt);
    } while (nout);

    free_ivector(good, 0, npt-1); 
    free_vector(dif,   0, npt-1);
    free_vector(sig,   0, npt-1);
    return dev;
}


void poly(float *x, float *y, int npt, float shift, double *b, int deg)
{
    int    j, k; 
    double xx, tmp; 

    for (j = 0; j < npt; j++) {
        tmp = 0.0; xx = (double) x[j]+shift; 
        for (k = deg; k >= 0; k--) tmp = tmp*xx + b[k];
        y[j] = tmp;
    }
}


float adjust_1d2(float *x, float *y, float *image, int ntot, double *b, 
                int degx, int degy, float nsig)
{
    int    j, l, m, n, *good, nout, maxpt;
    float  *dif, *sig, moy, dev; 

    good = ivector(0, ntot-1); 
    dif  = vector(0, ntot-1);
    sig  = vector(0, ntot-1);

    for (j = 0; j < ntot; j++) { 
        good[j] = 1;
        sig[j]  = 1.0;
    }
    do {
        for (j = 0; j < ntot; j++) 
            if (!good[j]) sig[j] = 1000.0; 
        interpol_1d2(x, y, image, sig, ntot, b, degx, degy);
        poly_1d2(x, y, dif, ntot, b, degx, degy); 
        for (j = 0; j < ntot; j++) dif[j] = image[j]-dif[j];
        maxpt = ntot/100; if (maxpt == 0) maxpt = 1; 
        nout = reject(dif, good, ntot, &moy, &dev, nsig, maxpt);
    } while (nout);

    free_ivector(good, 0, ntot-1); 
    free_vector(dif,   0, ntot-1);
    free_vector(sig,   0, ntot-1);
    return dev;
}


void poly_1d2(float *x, float *y, float *image, int ntot, double *b, int degx, 
              int degy)
{
    int    j, l, m, n; 
    double xx, yy, **xn, **yn, tmp; 

    xn = dmatrix(0, ntot-1, 0, degx); 
    yn = dmatrix(0, ntot-1, 0, degy); 

    for (j = 0; j < ntot; j++) {
        xx = (double) x[j];  
        yy = (double) y[j];  
        for (n = 0, tmp = 1.0; n <= degx; n++, tmp *= xx) xn[j][n] = tmp;  
        for (n = 0, tmp = 1.0; n <= degy; n++, tmp *= yy) yn[j][n] = tmp;  
    }
    for (j = 0; j < ntot; j++) { 
        tmp = 0.0;
        for (n = l = 0; n <= degy; n++)
            for (m = 0; m <= degx; m++, l++)  
                tmp += b[l]*xn[j][m]*yn[j][n]; 
        image[j] = tmp;
    }

    free_dmatrix(xn, 0, ntot-1, 0, degx); 
    free_dmatrix(yn, 0, ntot-1, 0, degy);
}


float adjust_2d(float *x, float *y, float *image, int ntot, double *b, 
                int deg, float nsig)
{
    int    j, l, m, n, *good, nout, maxpt;
    float  *dif, *sig, moy, dev; 

    good = ivector(0, ntot-1); 
    dif  = vector(0, ntot-1);
    sig  = vector(0, ntot-1);

    for (j = 0; j < ntot; j++) { 
        good[j] = 1;
        sig[j]  = 1.0;
    }
    do {
        for (j = 0; j < ntot; j++) 
            if (!good[j]) sig[j] = 1000.0; 
        interpol_2d(x, y, image, sig, ntot, b, deg);
        poly_2d(x, y, dif, ntot, b, deg); 
        for (j = 0; j < ntot; j++) dif[j] = image[j]-dif[j];
        maxpt = ntot/100; if (maxpt == 0) maxpt = 1; 
        nout = reject(dif, good, ntot, &moy, &dev, nsig, maxpt);
    } while (nout);

    free_ivector(good, 0, ntot-1); 
    free_vector(dif,   0, ntot-1);
    free_vector(sig,   0, ntot-1);
    return dev;
}


void poly_2d(float *x, float *y, float *image, int ntot, double *b, int deg)
{
    int    j, l, m, n; 
    double xx, yy, **xn, **yn, tmp; 

    xn = dmatrix(0, ntot-1, 0, deg); 
    yn = dmatrix(0, ntot-1, 0, deg); 

    for (j = 0; j < ntot; j++) {
        xx = (double) x[j];  
        yy = (double) y[j];  
        for (n = 0, tmp = 1.0; n <= deg; n++, tmp *= xx) xn[j][n] = tmp;  
        for (n = 0, tmp = 1.0; n <= deg; n++, tmp *= yy) yn[j][n] = tmp;  
    }
    for (j = 0; j < ntot; j++) { 
        tmp = 0.0;
        for (n = l = 0; n <= deg; n++)
            for (m = 0; m <= n; m++, l++)  
                tmp += b[l]*xn[j][n-m]*yn[j][m]; 
        image[j] = tmp;
    }

    free_dmatrix(xn, 0, ntot-1, 0, deg); 
    free_dmatrix(yn, 0, ntot-1, 0, deg);
}


void smooth(float *x, float *y, int *good, int npt, int wid)
{
    int   j, n, p; 

    for (j = 0; j < npt; j++) {  
        y[j] = 0.0;  p = 0; 
        for (n = -wid; n <= wid; n++) 
            if ((j+n >= 0) && (j+n < npt) && good[j+n]) { 
                y[j] += x[j+n]; 
                p++;
            }
        if (p) 
            y[j] /= p; 
        else {
            printf("Increase width of smoothing profile\n"); 
            exit(1); 
        } 
    } 
}


void adjust_smooth(float *x, float *y, int npt, int wid, float nsig)
{
    int    j, k, *good, nout, maxpt;
    float  *dif, moy, dev;

    good = ivector(0, npt-1); 
    dif  =  vector(0, npt-1);

    for (j = 0; j < npt; j++) good[j] = 1;
    do {
        smooth(x, y, good, npt, wid);
        for (j = 0; j < npt; j++) dif[j] = y[j]-x[j];
        maxpt = npt/100; if (maxpt == 0) maxpt = 1; 
        nout = reject(dif, good, npt, &moy, &dev, nsig, maxpt);
/*
printf("   %d %f %f\n", nout, moy, dev); 
*/
    } while (nout);
/*
printf("Out of adjust_smoo\n"); 
*/

    free_ivector(good, 0, npt-1); 
    free_vector(dif,   0, npt-1);
}


int reject(float *spec, int *good, int nspec, float *moy, float *dev, 
           float nsig, int maxpt)
{
    int   k, nout, n, npix, npt;
    float tmp, max;
    double mea, sig; 

    npix = 0;
    do {
        mea = sig = 0.0;
        npt = nout = 0;
        for (k = 0; k < nspec; k++)
            if (good[k]) {
                mea += spec[k];
                sig += spec[k]*spec[k];
                npt++;
            }
        mea /= npt; sig /= npt; sig = sqrt(sig-mea*mea); 
        *moy = mea; *dev = sig; 
        max = -1.0;
        for (k = 0; k < nspec; k++) {
            if (good[k]) { 
                tmp = fabs((double) spec[k] - *moy);
                if ((max < tmp) && (tmp > nsig * *dev)){
                    max = tmp;
                    n = k;
                }
            }
        }
        if (max > 0.0) {
            good[n] = 0;
/*
printf(">> %d %f %d %f %f\n", n, spec[n], good[n], *dev, max / *dev); 
*/
            npix++; nout++;
        }
    } while (nout && (npix < maxpt));
    return npix;
}


static int comb(int n, int p)
{
    int i, j, k;
    if ((p < 0) || (n < 0)) { 
        printf("Illegal parameters for comb\n"); 
        exit(1); 
    } 
    if (n < p) return 0;
    if ((n <= 1) || (p == 0)) return 1; 
    j = k = 1;
    for (i = 1; i <= n; i++) {
        if (i <= p)  j *= i;
        if (i > n-p) k *= i;
    }
    return k/j;
}


void polint(float xa[], float ya[], int n, float x, float *y, float *dy)
{
    int   i,m,ns=0;
    float den,dif,dift,ho,hp,w,*c,*d;

    dif=fabs((double) x-xa[0]);
    c=vector(0,n-1);
    d=vector(0,n-1);
    for (i = 0; i < n; i++) {
        if ((dift=fabs((double) x-xa[i])) < dif) {
            ns=i;
            dif=dift;
        }
        c[i]=ya[i];
        d[i]=ya[i];
    }
    *y=ya[ns--];
    for (m = 0; m < n-1; m++) {
        for (i = 0; i < n-m; i++) {
            ho=xa[i]-x;
            hp=xa[i+m+1]-x;
            w=c[i+1]-d[i];
            if ((den=ho-hp) == 0.0) { 
                printf("Error in routine polint\n"); 
                exit(1); 
            } 
            den=w/den;
            d[i]=hp*den;
            c[i]=ho*den;
        }
        *y += (*dy=(2*ns+1 < (n-m) ? c[ns+1] : d[ns--]));
    }
    free_vector(d,0,n-1);
    free_vector(c,0,n-1);
}

