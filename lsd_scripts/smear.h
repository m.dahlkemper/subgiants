void oversample(double data[], int n, int ndiv);

void smear(float width, int nspec, float data[], float dlambda);

void convolve(int nspec, int nspec2, float data1[], float data2[], float shift, 
              float dlambda); 

void cross(int nspec, float data1[], float data2[], float dlam, float *shift);

void noise_filter(float data[], int nspec, float resol);

void wiener_filter(float data[], int nspec, float resol);

void bpro_filter(float data[], int nspec, float resol); 

void fft_mod(float data[], int nspec, float fft[], int *dim, float step);

void fft_vect(float data[], int nspec, float fft[], int *dim, float step, 
              int dir);

void correl(double data1[], double data2[], int n, double ans[]);

