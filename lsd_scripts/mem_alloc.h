#ifdef PROTOTYPES

float *vector(int nl, int nh);
float **pfvector(int nl, int nh);
void *svector(int length, int size);
double *dvector(int nl, int nh);
int *ivector(int nl, int nh);
float **matrix(int nrl, int nrh, int ncl, int nch);
double **dmatrix(int nrl, int nrh, int ncl, int nch);
int **imatrix(int nrl, int nrh, int ncl, int nch);
void free_vector(float *v, int nl, int nh);
void free_pfvector(float **v, int nl, int nh);
void free_svector(void *v);
void free_dvector(double *v, int nl, int nh);
void free_ivector(int *v, int nl, int nh);
void free_matrix(float **m, int nrl, int nrh, int ncl, int nch);
void free_dmatrix(double **m, int nrl, int nrh, int ncl, int nch);
void free_imatrix(int **m, int nrl, int nrh, int ncl, int nch);
float ***matrix3D(int n1l, int n1h, int n2l, int n2h, int n3l, int n3h);
void free_matrix3D(float ***m, int n1l, int n1h, int n2l, int n2h, int n3l, int n3h);
float ****matrix4D(int n1l, int n1h, int n2l, int n2h, int n3l, int n3h, int n4l, int n4h);
void free_matrix4D(float ****m, int n1l, int n1h, int n2l, int n2h, int n3l, int n3h, int n4l, int n4h);

#else  /* Don't use prototypes */

float *vector();
float **matrix();
float ***matrix3D();
float ****matrix4D();
float **pfvector();
void *svector();
double *dvector();
double **dmatrix();
int *ivector();
int **imatrix();
void free_vector();
void free_pfvector();
void free_svector();
void free_dvector();
void free_ivector();
void free_matrix();
void free_dmatrix();
void free_imatrix();
void free_matrix3D();
void free_matrix4D();

#endif  /* PROTOTYPES */

