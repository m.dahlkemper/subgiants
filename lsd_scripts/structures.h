/*

This file contains typedef's for the composite data structures used
in the ZDI programs.

In each structure the components are declared in order from the largest,
of type double, to the smallest, type char, to help optimise the
compiler memory access to each element. On some machines it may be necessary
to add some dummy elements to the end of each structure to pad the structure
to the machines natural addressing boundary.

The first structure we define contains all the information about an
observed spectrum, taken at a given phase in a given spectral line.
The elements nsample, selected and line_id are derived from the
other elements and the controlling program. The other elements are all
intrinsic to the observation.

*/

#define MAX_NAME 64  /* Maximum number of characters allowed in a line name */

typedef struct {
    double line_centre; /* Nominal wavelength of line centre */
    double laml;   /* Wavelength of the first data point, lambda_l */
    double lamu;   /* Wavelength of the last data point, lambda_u */
    float  FWHM;   /* The width of the instrumental profile */
    float  SN;     /* Signal to noise ratio of the spectrum */
    float  continuum; /* Local continuum level in Stokes I */
    float  polariser; /* (relative) Polariser angle at this phase */
    float  phasec; /* The rotational phase when the observation commenced */
    float  phasef; /* The rotational phase when the observation finished */
    int    nsample; /* Number of phase samples to take when modelling this line */
    int    nspec;     /* Number of data points at this phase */
    int    datsw[4]; /* Which polarisations are measured */
    int    datsel[4]; /* Which polarisations have been selected to be fitted */
    int    photometric; /* Is this observation photometric */
    int    broadened; /* Does the line model already include instrumental broadening */
    int    selected;  /* Has this observation been selected to be fitted */
    int    line_id;   /* ID number of the observed line */
    int    linear;    /* Is the associated line model linear */
    char   line_name[MAX_NAME]; /* Name of the line */
} spec_info_t;


/*
We use a structure to communicate the details of how the spectrum
should be sampled.
*/

typedef struct {
    int oversample;  /* Oversampling factor */
    int zoom;        /* Zoom factor */
} spec_sample_t;


/*
The structure cell_info_t contains all the information about a grid
cell at a given phase.  The structure cell_deriv_t holds the derivatives
of the Stokes angles gamma and chi with respect to the three
field quantities B_r, B_t and B_p.
*/

typedef struct {
    float mu;     /* Cosine of the limb angle mu */
    float parea;  /* Projected area */
    float vel;    /* Line of sight velocity v_parallel */
} cell_info_t;

typedef struct {
    float dcgdBr;     /* $B^3 \partial (\cos \gamma) / \partial Br$ */
    float dcgdBt;     /* $B^3 \partial (\cos \gamma) / \partial Bt$ */
    float dcgdBp;     /* $B^3 \partial (\cos \gamma) / \partial Bp$ */
    float dchdBr;     /* $B^3 \partial \chi / \partial Br$ */
    float dchdBt;     /* $B^3 \partial \chi / \partial Bt$ */
    float dchdBp;     /* $B^3 \partial \chi / \partial Bp$ */
} cell_deriv_t;


/* 
Structure for FITS files 
*/

typedef struct{
    int    bitpix;      /* Bits per pixel in fits record */ 
    char   object[80];  /* Id of exposure */ 
    double date;        /* UT date of exposure in format yyyy.mmdd */ 
    double ut;          /* Universal time at mean exposure in format hh.mmss */ 
    float  exptime;     /* Universal time at mean exposure in format hh.mmss */ 
    float  ha;          /* Hour angle of mean exposure in fractional degrees */ 
    float  ra;          /* Right ascention of object in fractional degrees */ 
    float  dec;         /* Declination of object in fractional degrees */ 
    float  lat;         /* Latitude of observation in fractional degrees */ 
    int    moon;        /* Is the exposure on the moon? */ 
    int    nline;       /* Number of lines in header */ 
    int    nlused;      /* Number of used lines in header */ 
} fits_info_t; 

