float gaussian_lsfit(float lam[], float spec[], int nspec, float *centr, 
                     float *fwhm, float *ew, float cut, int ok_to_plot);
float dgaussian_lsfit(float lam[], float spec[], int nspec,
                      float *centr1, float *fwhm1, float *ew1,
                      float *centr2, float *fwhm2, float *ew2,
                      float cut, int ok_to_plot); 
float tgaussian_lsfit(float lam[], float spec[], int nspec,
                      float *centr1, float *fwhm1, float *ew1,
                      float *centr2, float *fwhm2, float *ew2,
                      float *centr3, float *fwhm3, float *ew3,
                      float cut, int ok_to_plot); 
void profile_lsfit(float lam[], float spec[], int nspec, float SN, float *vsin, 
                   float *dzeta, float *err, float A0, float eta0, float a, 
                   float vd, float instr[], float shift, float cut, 
                   int ok_to_fit, int ok_to_plot);
void fft_lsfit(float lam[], float spec[], int nspec, float SN, float *vsin, 
               float *dzeta, float *err, float A0, float eta0, float a, 
               float vd, float instr[], float shift, int ok_to_fit, 
               int ok_to_plot);
void meanfft_lsfit(float fft_x[], float fft_y[], float fft_s[], int nspec, 
                   float *vsin, float *dzeta, float err[], int ok_to_fit, 
                   int ok_to_plot);
void interpol(float *x, float *spec, float *sig, int length, double *b, int n); 
void interpol_1d2(float *x, float *y, float *image, float *sig, int ntot,
                  double *b, int nx, int ny); 
void interpol_2d(float *x, float *y, float *image, float *sig, int ntot,
                 double *b, int n); 
float adjust(float *x, float *y, int npt, double *b, int deg, float nsig); 
void poly(float *x, float *y, int npt, float shift, double *b, int deg); 
float adjust_1d2(float *x, float *y, float *image, int ntot, double *b,
                 int degx, int degy, float nsig); 
void poly_1d2(float *x, float *y, float *image, int ntot, double *b, int degx,
               int degy); 
float adjust_2d(float *x, float *y, float *image, int ntot, double *b,
                int deg, float nsig); 
void poly_2d(float *x, float *y, float *image, int ntot, double *b, int deg); 
void smooth(float *x, float *y, int *good, int npt, int wid); 
void adjust_smooth(float *x, float *y, int npt, int wid, float nsig); 
int reject(float *spec, int *good, int nspec, float *moy, float *dev, float nsig, 
           int maxpt); 
void polint(float xa[], float ya[], int n, float x, float *y, float *dy); 
