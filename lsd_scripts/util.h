float ccf(float *spec1, float *spec2, int nspec, float maxshift, int fill); 
int locate(float x, float array[], int n); 
void continuum(float *x, float *lam, float *spec, int nspec, int step, float *pix, float *cont, 
               int *npix); 
void linear(float *x, float *y, int npt, float *lam, float *data, int nspec); 
void spl(float *x, float *y, int npt, float *lam, float *data, int nspec); 
void get_loc(float loc, float *lam, float *spec, int nspec, int npixx, 
             float *centre, float *pix, float *width); 
void parse_args(int argc, char *argv[], int *batch, int *plot, int *verbose, 
                int *swap, int *test, int *invpol, int *contpol); 
void indexx(int n, float *arrin, int *indx);
