
/*  -------------------------------------------------------------------  */
/*  device dependant graphics routines for C (using pgplot on a convex)  */
/*  -------------------------------------------------------------------  */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <strings.h>
#include "/usr/include/cpgplot.h"

#define twopi 6.283185307

static void free_vector(float *v, int nl, int nh);
static float *vector(int nl, int nh); 

static int  dev, nx=1, ny=1, nv = 0;
char device[20]; 
float xx0 = 0.0, yy0 = 0.0; 

void set_device(int dev_type)
/* sets device type : 0 for screen plot, 1 for laser plot */
{
    dev = dev_type;
    if (dev == 0) 
        sprintf(device,"/xserve"); 
    else if (dev == 1)
        sprintf(device,"output.ps/cps");
    else if (dev == 2)
        sprintf(device,"output.ps/vcps");
    else if (dev == 4)
        sprintf(device,"output.gif/gif");
    else if (dev == 5)
        sprintf(device,"output.gif/vgif");
    else if (dev == 3)
        sprintf(device,"/xdisp");
    else
        sprintf(device,"/null");
}


void init_plot(float xmin, float xmax, float ymin, float ymax, char lab_x[], 
               char lab_y[], char title[], int axis, int log_plot)
/* initiate graphics */
{
    int    ier, n; 

    if (dev == 0) ier = cpgbeg(15, device, 1, 1); 
    else          ier = cpgbeg(15, device, 1, 1);
    if (ier != 1) {
        exit(1); 
        printf(" cpgbeg error\n");
    }

    cpgslw(1); if (dev != 0) cpgslw(2); cpgsls(1); cpgsch(1.5); 

    if (log_plot == 1)       n = 30;  /* Both axis plotted logarithmically */ 
    else if (log_plot == 2)  n = 10;  /* Y-axis plotted logarithmically */ 
    else if (log_plot == 3)  n = 20;  /* X-axis plotted logarithmically */ 
    else if (log_plot == -1) n = -2;  /* No axes plotted */ 
    else                     n = 0;     /* Standard axes */ 
    cpgenv(xmin,xmax,ymin,ymax,axis,n);
/*
cpgvsp(0.15,0.60,0.10,0.98); 
cpgswin(xmin,xmax,ymin,ymax); 
cpgbox("BCNST",0.0,0,"BCST",0.0,0);
*/
    if (dev != 0) cpgslw(4); 
    cpglab(lab_x,lab_y,title); 
}


void init_multiplot(int j, int k) 
/* initiate graphics */
{
    int    ier; 

    if (dev == 0) ier = cpgbeg(15,device,j,k); 
    else          ier = cpgbeg(16,device,j,k); 
nx = j; ny = k; 
    if (ier != 1) {
        exit(1); 
        printf(" cpgbeg error\n");
    }
}


void start_plot(float xmin, float xmax, float ymin, float ymax, char lab_x[], 
               char lab_y[], char title[], int axis, int log_plot)
/* initiate graphics */
{
    int    n, sep = 1; 
    float  side; 
    char text[80], text2[80]; 

    cpgslw(1); if (dev != 0) cpgslw(2); cpgsls(1); cpgsch(2.5); 
    cpgsch(1.5); 

    if (log_plot == 1)       n = 30;  /* Both axis plotted logarithmically */ 
    else if (log_plot == 2)  n = 10;  /* Y-axis plotted logarithmically */ 
    else if (log_plot == 3)  n = 20;  /* X-axis plotted logarithmically */ 
    else if (log_plot == -1) n = -2;  /* No axes plotted */ 
    else if (log_plot == -2) sep = n = 0;  /* Graph touch each other */ 
    else                     n = 0;     /* Standard axes */ 
    if (sep) { 
        cpgenv(xmin,xmax,ymin,ymax,axis,n);
        if (dev != 0) cpgslw(4); 
        cpglab(lab_x,lab_y,title); 
    } else { 
side = 0.8; 
        side = 0.9; 
/*
        cpgsvp(1.0-side*(nx-nv)-0.01,1.0-side*(nx-nv-1)-0.01,1.0-ymax+ymin-0.01,1.0-0.01); 
*/
        cpgsvp(1.0-side*(nx-nv)-0.01,1.0-side*(nx-nv-1)-0.01,1.0-side,side); 
/*
        cpgsvp(1.0-side*(nx-nv)-0.01,1.0-side*(nx-nv-1)-0.01,1.0-side,0.99); 
        cpgsvp(1.0-side*(nx-nv)+0.1,1.0-side*(nx-nv-1)-0.01,1.0-side,0.99); 
*/

        cpgswin(xmin,xmax,ymin,ymax); 
        if (nv == 0) cpgbox("BCNST",0.0,0,"BCNST",0.0,0);
        else         cpgbox("BCNST",0.0,0,"BCST",0.0,0);
if (dev != 0) cpgslw(4); 
        if (nv == 0) cpglab(lab_x,lab_y,title);
        else         cpglab(lab_x," ",title);
        nv++; 
    } 
}


void plot(float x[], float spec[], int length, int ls)
/* plot an array */
{
    int lw;

    if (ls < 1) ls = 1;
    lw = 1; if (dev != 0) lw = 2; 
    if (ls > 100) { 
        lw += 2; 
        ls -= 100; 
    }
/*
if (ls == 2) ls = 4; 
*/
    cpgslw(lw); 
    cpgsls(ls); 
    cpgline(length,x,spec);
}


void plot_circle(float xc, float yc, float rad, int ls)
/* plots a circle */
{
    int lw, j, npt = 100; 
    float *x, *y; 

    if (ls < 1) ls = 1;
    lw = 1; if (dev != 0) lw = 2; 
    if (ls > 100) { 
        lw += 2; 
        ls -= 100; 
    }
    cpgslw(lw); 
    cpgsls(ls); 
    x = vector(0, npt-1); y = vector(0, npt-1); 
    for (j = 0; j < npt; j++) { 
        x[j] = xc + rad*cos(twopi*j/(npt-1)); 
        y[j] = yc + rad*sin(twopi*j/(npt-1)); 
    } 
    cpgline(npt,x,y);
    free_vector(x, 0, npt-1); free_vector(y, 0, npt-1); 
}


void plot_sym(float x[], float spec[], int length, int symbol)
/* plot an array with symbols */
{
    cpgslw(1); if (dev != 0) cpgslw(2); cpgsls(1); cpgsch(2.0); 
    cpgpt(length,x,spec,symbol);
}


void plot_errx(float xl[], float xu[], float y[], int npt)
/* plot an array of horizontal error bars */
{
    cpgslw(1); if (dev != 0) cpgslw(2); cpgsls(1); 
    cpgerrx(npt,xl,xu,y,0.0);
}


void plot_erry(float x[], float yl[], float yu[], int npt)
/* plot an array of vertical error bars */
{
    cpgslw(1); if (dev != 0) cpgslw(2); cpgsls(1); 
    cpgerry(npt,x,yl,yu,0.0);
}


void write_text(char text[], float x, float y)
/* write text at position (x,y) */
{
    cpgslw(1); if (dev != 0) cpgslw(3); cpgsls(1); cpgsch(1.3); 
    if (nv == 0) cpgsch(1.3);
    else         cpgsch(2.0);
cpgslw(5); 
    cpgptxt(x,y,0.0,0.5,text); 
}


void write_smalltext(char text[], float x, float y)
/* write text at position (x,y) */
{
    cpgslw(1); if (dev != 0) cpgslw(3); cpgsls(1); cpgsch(1.3); 
    if (nv == 0) cpgsch(1.0);
    else         cpgsch(1.5);
    cpgptxt(x,y,0.0,0.5,text); 
}


void write_bigtext(char text[], float x, float y)
/* write text at position (x,y) */
{
    cpgslw(1); if (dev != 0) cpgslw(4); cpgsls(1); cpgsch(1.3); 
    if (nv == 0) cpgsch(1.8);
    else         cpgsch(2.7);
    cpgptxt(x,y,0.0,0.5,text); 
}


void write_lab(char text[], float x, float y)
/* write text at position (x,y) */
{
    cpgslw(1); if (dev != 0) cpgslw(4); cpgsls(1); cpgsch(1.5); 
    cpgptxt(x,y,0.0,0.5,text); 
}


void get_curs(float *x, float *y, char curse[])
/* get cursor coordinates */
{
    *x = xx0; *y = yy0; 
    cpgcurs(x,y,curse); 
    xx0 = *x; yy0 = *y; 
    curse[1] = '\0'; 
}


void contours(float **data, float *x, float *y, int nx, int ny, float min, float max, 
              int nc) 
{
    int   j, k; 
    float *array, tr[6], st_y, st_x, *c; 

    array = vector(0, nx*ny-1); 
    for (j = 0; j < ny; j++) for (k = 0; k < nx; k++) array[k+nx*j] = data[k][j]; 
    nc--; c = vector(0, nc-1); 
    for (j = 0; j < nc; j++) c[j] = min + (j+0.5)*(max-min)/nc;  
    st_x = (x[nx-1]-x[0])/(nx-1); st_y = (y[ny-1]-y[0])/(ny-1); 
    tr[0] = x[0]-st_x; tr[1] = st_x; tr[2] = 0.0; 
    tr[3] = y[0]-st_y; tr[4] = 10.0;  tr[5] = st_y; 
    cpgslw(1); if (dev != 0) cpgslw(2); 
    cpgcons(array,nx,ny,1,nx,1,ny,c,nc,tr); 
    free_vector(array, 0, nx*ny-1); 
    free_vector(c, 0, nc-1); 
}


void image(float **data, float *x, float *y, int nx, int ny, float min, float max, int nc, 
           int rect, int lut, int print_lut) 
{
    int   j, k, nlev, ncont; 
    float *array, tr[6], st_y, st_x, *l, *r, *g, *b, tmp, *c; 
/*
float x2[4], y2[4], yl[4], yu[4], xl[4], xu[4]; 
*/

    cpgqcir(&j,&k); 
    if (nc > k-j+1) nc = 0; 
    if ((k-j+1  < 8) || (nc > k-j+1)) { 
        printf("Not enough colors available on this device\n"); 
        return; 
    } 
    array = vector(0, nx*ny-1); 
    for (j = 0; j < ny; j++) for (k = 0; k < nx; k++) array[k+nx*j] = data[k][j]; 
    st_x = (x[nx-1]-x[0])/(nx-1); st_y = (y[ny-1]-y[0])/(ny-1); 
    tr[0] = x[0]-st_x; tr[1] = st_x; tr[2] = 0.0; 
    tr[3] = y[0]-st_y; tr[4] = 0.0;  tr[5] = st_y; 
    j = 16; k = nc+j-1; cpgscir(j,k);
    nlev = 4; l = vector(0, nlev-1); 
    r = vector(0, nlev-1); g = vector(0, nlev-1); b = vector(0, nlev-1); 
    if (lut == 0) {          /* black and white */ 
        for (j = 0; j < nlev; j++) l[j] = r[j] = g[j] = b[j] = j/(nlev-1); 
    } else if (lut == 1) {   /* brown */ 
        l[0] = 0.0; l[1] = 0.5; l[2] = 0.9; l[3] = 1.0; 
        r[0] = 0.5; r[1] = 1.0; r[2] = 1.0; r[3] = 1.0; 
        g[0] = 0.0; g[1] = 0.5; g[2] = 1.0; g[3] = 1.0;
        b[0] = 0.0; b[1] = 0.0; b[2] = 0.0; b[3] = 1.0; 
    } else if (lut == 2) {   /* blue-red */ 
        l[0] = 0.0; l[1] = 0.5; l[2] = 1.0; l[3] = 1.0; 
        r[0] = 0.0; r[1] = 1.0; r[2] = 1.0; r[3] = 1.0; 
        g[0] = 0.0; g[1] = 1.0; g[2] = 0.0; g[3] = 0.0;
        b[0] = 1.0; b[1] = 1.0; b[2] = 0.0; b[3] = 0.0; 
    } else if (lut == 3) {   /* green-red */ 
        l[0] = 0.0; l[1] = 0.5; l[2] = 1.0; l[3] = 1.0; 
        r[0] = 0.0; r[1] = 1.0; r[2] = 1.0; r[3] = 1.0; 
        g[0] = 1.0; g[1] = 1.0; g[2] = 0.0; g[3] = 0.0;
        b[0] = 0.0; b[1] = 1.0; b[2] = 0.0; b[3] = 0.0; 
    } 
    cpgctab(l,r,g,b,nlev,1.0,0.5); 
    cpgimag(array,nx,ny,1,nx,1,ny,min,max,tr); 

    cpgslw(1); if (dev != 0) cpgslw(2); 
    if (rect < 0) rect = 0; 
    tmp = 4.0-2.0*rect;  
    if (print_lut) cpgwedg("RI",4.0-2.0*rect,2.5,min,max," "); 
/*
    ncont = 1; c = vector(0, ncont-1); c[0] = 0.0; 
    cpgslw(1); if (dev != 0) cpgslw(2); 
    cpgcons(array,nx,ny,1,nx,1,ny,c,ncont,tr); 
    free_vector(c, 0, ncont-1); 

x2[0]=0.9235;x2[1]=0.939;x2[2]=0.8685;x2[3]=0.5085; 
xl[0]=0.923;xl[1]=0.937;xl[2]=0.867;xl[3]=0.505; 
xu[0]=0.924;xu[1]=0.941;xu[2]=0.870;xu[3]=0.512; 
y2[0]=53.0; y2[1]=43.0; y2[2]=36.0; y2[3]=90.0; 
yl[0]=43.0; yl[1]=33.0; yl[2]=26.0; yl[3]=70.0; 
yu[0]=63.0; yu[1]=53.0; yu[2]=46.0; yu[3]=90.0; 
cpgslw(1); if (dev != 0) cpgslw(4); cpgsls(1); cpgsch(3.0); 
cpgpt(4,x2,y2,17);
cpgerry(4,x2,yl,yu,0.0);
cpgerrx(4,xl,xu,y2,0.0);

x2[0]=0.688;x2[1]=0.832;x2[2]=0.212; 
y2[0]=75.0; y2[1]=71.0; y2[2]=61.0;  
yl[0]=70.0; yl[1]=66.0; yl[2]=56.0;  
yu[0]=80.0; yu[1]=76.0; yu[2]=66.0;  
cpgslw(1); if (dev != 0) cpgslw(4); cpgsls(1); cpgsch(3.0); 
cpgpt(3,x2,y2,17);
cpgerry(3,x2,yl,yu,0.0);
*/

    free_vector(array, 0, nx*ny-1); 
    free_vector(l, 0, nlev-1); 
    free_vector(r, 0, nlev-1); free_vector(g, 0, nlev-1); free_vector(b, 0, nlev-1); 
}


void fill_poly(float *x, float *y, int n, int col)
{
    if (col < 0) col = 0; if (col > 255) col = 255; 
    cpgsci(col); cpgsfs(1); 
    cpgpoly(n,x,y); 
}


void end_plot(void)
/* ends graphics */
{
    cpgend();
}


void get_points(float xc[], float yc[], char curse[][2], int nptmax, int *npt, 
                int plot)
{
    int i; 
    void get_curs(float *x, float *y, char curse[]); 
    void plot_sym(float x[], float spec[], int length, int symbol); 

    *npt = 0; 
    if (nptmax < 1) return;
    for (i = 0; i < nptmax; i++) {
        get_curs(xc+i,yc+i,curse[i]);
        if (curse[i][0] == ' ') break; 
        if (plot == 1) plot_sym(xc+i,yc+i,1,2);
    } 
    *npt = i; 
}


#define BIG 1.0e30
void minmax(float spec[], int length, float *min, float *max)
{
    int j;

    *min = BIG; *max = -BIG; 
    for (j = 0; j < length; j++) {
        if (*min > spec[j]) *min = spec[j]; 
        if (*max < spec[j]) *max = spec[j]; 
    }
}
#undef BIG


/*

This package of routines uses the dynamic memory allocations
of C to allocate and free memory for the data arrays.

*/

static float *vector(int nl, int nh)
{
    float *v;

    v = (float *) malloc((unsigned) (nh-nl+1)*sizeof(float));
    if (!v) {
        fprintf(stderr,"Unable to allocate sufficient space for vector\n");
        exit(1);
    }

    return v-nl;
}


static void free_vector(float *v, int nl, int nh)
{
    free((void *) (v+nl));
}
