int read_fits(float **image, int *naxis, int **npt, float **first, float **step, 
              fits_info_t *info, char **header, int getname, char filename[], int bswap); 
void save_fits(float *image, int naxis, int *npt, float *first, float *step, 
               fits_info_t info, char *header, int getname, char filename[], int bswap); 
int read_ascfile(int *nspec, int *nfile, float **lam, float **spec, int getname,
                 char filename[], char comment[]); 
void save_ascfile(int nspec, int nfile, float lam[], float spec[], int getname,
                  char filename[], char comment[]); 
int read_dfgdata(int *nspec, float **I, int getname, char filename[], 
                 char comment[]);
int read_dfgspec(int *nspec, float *lam0, float **lam, float **I, int getname, 
                 char filename[], char comment[]);
void save_dfgspec(int nspec, float lam0, float lam[], float I[], int getname, 
                  char filename[], char comment[]);
int read_dfg2spec(int *nspec, float *lam0, float **lam, float **I1, float **I2, int getname, 
                  char filename[], char comment[]);
void save_dfg2spec(int nspec, float lam0, float lam[], float I1[], float I2[], int getname, 
                   char filename[], char comment[]);
void save_bright_old(int ngrid, float Cq[], float Cm[], float Br[], float Bp[], 
                     float Bt[], char filename[], int getname, char comment[],
                     float inc, float ve, int magnetic);
void save_spec_old(int nphase, float phases[], int datsw[], int photometric,
                   float I[], float Q[], float U[], float V[], int nspec[], 
                   float laml[], float lamu[], float SN[], float FWHM[],
                   float lamref, char filename[], int getname, char comment[]);
int read_bright_old(int *ngrid, float **Cq, float **Cm, float **Br,
                    float **Bp, float **Bt, char filename[], int getname,
                    char comment[], float *inc, float *ve, int *magnetic);
int read_spec_old(int *nphase, float **phases, int datsw[], int *photometric,
                  float **I, float **Q, float **U, float **V, int **nspec, 
                  float **laml, float **lamu, float **SN, float **FWHM,
                  float *lamref, char filename[], int getname, char comment[]);
int read_photometry(float **phases, double **jd, float **uu, float **bb, float **vv, 
                    float **rr, float **ii, int photsw[], float errs[], 
                    int *nphot, int getname, char filename[], char comment[]); 
void save_photometry(float *phases, double *jd, float *uu, float *bb, float *vv, 
                     float *rr, float *ii, int photsw[], float errs[], 
                     int nphot, int getname, char filename[], char comment[]); 
void save_bright(int ngrid, float Cq[], float Cm[], float Br[], float Bt[], float Bp[],
                 char filename[], int getname, char comment[],
                 double inc, double ve, int magnetic);
int read_bright(int *ngrid, float **Cq, float **Cm, float **Br,
                float **Bt, float **Bp, char filename[], int getname,
                char comment[], float *inc, float *ve, int *magnetic);
void save_spec(float I[], float Q[], float U[], float V[], int nobs, 
               spec_info_t spec_info[], char filename[], int getname, char comment[], 
               int datsel);
int read_spec(float **I, float **Q, float **U, float **V, int *nobs, 
              spec_info_t **spec_info, char filename[], int getname, char comment[]);
void read_header(char *header, fits_info_t *info); 
int read_map(int *nx, int *ny, float **x, float **y, float ***map, int getname, 
             char filename[], char labx[], char laby[], char title[]); 
void save_map(int nx, int ny, float *x, float *y, float **map, int getname,
              char filename[], char labx[], char laby[], char title[]); 
