#include <stdio.h>
#include <math.h>
#include "mem_alloc.h"

static void real_fft(double data[], int n, int direction);
static void cmpx_fft(double data[], int nn, int direction);
       void oversample(double data[], int n, int ndiv);
static void two_fft(double data1[], double data2[], 
                    double fft1[], double fft2[], int n);
static void spctrm(double power[], int m, int k, double data[]);
void correl(double data1[], double data2[], int n, double ans[]);


#define  TWO_PI    6.28318530717959
#define  TWO_LOG2  1.386294361


void init_smear(int nspec)
{
}


void smear(float width, int nspec, float data[], float dlambda)
{
    double *fft_vec,sigma2,arg,tmp;
    int    fft_size,j;

    if (width < 0.01*dlambda)
        return;   /* No need to do convolution */
    fft_size = 1;
    while (fft_size < nspec)
        fft_size *= 2;
    fft_vec = dvector(0,fft_size-1);
    for (j = 0; j < nspec; j++)
        fft_vec[j] = data[j];
    for (j = nspec; j < fft_size; j++)
        fft_vec[j] = data[nspec-1];
    real_fft(fft_vec,fft_size/2,1);
    sigma2 = width*width/(4.0*TWO_LOG2);
    arg = -0.5*sigma2*TWO_PI*TWO_PI/(dlambda*dlambda*fft_size*fft_size);
    for (j = 2; j < fft_size; j += 2) {
        tmp = exp(0.25*arg*j*j);
        fft_vec[j] *= tmp;
        fft_vec[j+1] *= tmp;
    }
    tmp = exp(0.25*arg*fft_size*fft_size);
    fft_vec[1] *= tmp;
    real_fft(fft_vec,fft_size/2,-1);
    arg = 2.0 / fft_size;
    for (j = 0; j < nspec; j++)
        data[j] = fft_vec[j] * arg;
    free_dvector(fft_vec,0,fft_size-1);
}


void convolve(int nspec, int nspec2, float data1[], float data2[], float shift, 
              float dlambda)
{
    double *fft_vec1, *fft_vec2, arg, tmp1, tmp2, tmp, phase;
    int    fft_size, j;

    fft_size = 1;
    while ((fft_size < nspec) || (fft_size < nspec2)) 
        fft_size *= 2;
    fft_vec1 = dvector(0,fft_size-1);
    fft_vec2 = dvector(0,fft_size-1);
    for (j = 0; j < nspec; j++)  fft_vec1[j] = data1[j];
    for (j = nspec;  j < fft_size; j++) fft_vec1[j] = data1[nspec-1];
    for (j = 0; j < nspec2/2; j++) fft_vec2[j] = data2[j];
    for (j = nspec2/2+1; j < nspec2; j++) fft_vec2[fft_size-nspec2+j] = data2[j];
    for (j = nspec2/2+1; j < fft_size-nspec2+nspec2/2+1; j++) fft_vec2[j] = 0.0;
    real_fft(fft_vec1,fft_size/2,1);
    real_fft(fft_vec2,fft_size/2,1);
    fft_vec1[0] *= fft_vec2[0]; 
    fft_vec1[1] *= fft_vec2[1]; 
    for (j = 2; j < fft_size; j += 2) {
        tmp = TWO_PI*shift*j/2; 
        tmp1 =  fft_vec2[j]*cos(tmp) + fft_vec2[j+1]*sin(tmp); 
        tmp2 = -fft_vec2[j]*sin(tmp) + fft_vec2[j+1]*cos(tmp); 
        tmp =           fft_vec1[j]*tmp1 - fft_vec1[j+1]*tmp2; 
        fft_vec1[j+1] = fft_vec1[j]*tmp2 + fft_vec1[j+1]*tmp1; 
        fft_vec1[j]   = tmp; 
    }
    real_fft(fft_vec1,fft_size/2,-1);
    arg = 2.0 * dlambda / fft_size;
    for (j = 0; j < nspec; j++)
        data1[j] = fft_vec1[j] * arg;
    free_dvector(fft_vec1,0,fft_size-1);
    free_dvector(fft_vec2,0,fft_size-1);
}


void fft_mod(float data[], int nspec, float fft[], int *dim, float step)
{
    double *fft_vec;
    int    j,fft_size;

    fft_size = 1;
    while (fft_size < nspec) fft_size *= 2;
    if (*dim < fft_size/2 + 1) {
        printf("Not enough size to store Fourier transform modulus\n"); 
        return;
    }
    fft_vec = dvector(0,fft_size-1);
    for (j = 0; j < nspec; j++)         fft_vec[j] = data[j];
    for (j = nspec; j < fft_size; j++) fft_vec[j] = data[nspec-1];
    real_fft(fft_vec,fft_size/2,1);
    fft[0] = sqrt(fft_vec[0]*fft_vec[0])*step; 
    for (j = 1; j < fft_size/2; j++) 
        fft[j] = step*sqrt(fft_vec[2*j]*fft_vec[2*j] + fft_vec[2*j+1]*fft_vec[2*j+1]); 
    fft[fft_size/2] = sqrt(fft_vec[1]*fft_vec[1])*step; 
    free_dvector(fft_vec,0,fft_size-1);
    *dim = fft_size / 2 + 1; 
}


void fft_vect(float data[], int nspec, float fft[], int *dim, float step, 
              int dir)
{
    double *fft_vec;
    int    j,fft_size;

    fft_size = 1;
    while (fft_size < nspec) fft_size *= 2;
    if (*dim < fft_size) {
        printf("Not enough size to store Fourier transform vector\n"); 
        return;
    }
    fft_vec = dvector(0,fft_size-1);
    if (dir == 1) { 
        for (j = 0; j < nspec; j++)        fft_vec[j] = data[j];
        for (j = nspec; j < fft_size; j++) fft_vec[j] = data[nspec-1];    
        real_fft(fft_vec,fft_size/2,1);
        for (j = 0; j < fft_size; j++) 
            fft[j] = step*fft_vec[j]; 
    } if (dir == -1) { 
        for (j = 0; j < nspec; j++)        fft_vec[j] = data[j];
        for (j = nspec; j < fft_size; j++) fft_vec[j] = 0.0; 
        real_fft(fft_vec,fft_size/2,-1);
        for (j = 0; j < fft_size; j++) 
            fft[j] = 2.0*fft_vec[j]/(step*fft_size); 
    }
    free_dvector(fft_vec,0,fft_size-1);
    *dim = fft_size; 
}


static void spctrm(double power[], int m, int k, double data[])
/* not really working yet */
{
    int    mm, m4, kk, j, j2, joff, doff; 
    double den, sumw, *window, w, *w1; 

    mm = 2*m; m4 = 2*mm;  
    den = sumw = 0.0; 
    window = dvector(0, mm-1); 
    w1     = dvector(0, m4-1); 
    for (j = 0; j < mm; j++) {
        w = 1.0 - fabs((float) (j-(m-0.5))/(m+0.5));
        window[j] = 1.0;
        sumw += w*w;
    }
    for (j = 0; j < m; j++) power[j] = 0.0; 
    for (kk = 0; kk < k; kk++) {
        for (joff = 0; joff <= 1; j++) {
            doff = (joff+kk)*m;
            for (j = 0; j < m; j++) w1[joff+2*j] = data[doff+j];
            joff += mm; 
            for (j = 0; j < m; j++) w1[joff+2*j] = data[doff+m+j];
        }
        for (j = 0; j < mm; j++) {
            j2 = 2*j; 
            w = window[j];
            w1[j2] *= w; 
            w1[j2+1] *= w; 
        }
        cmpx_fft(w1,mm,1);
        power[0] += w1[0]*w1[0]+w1[1]*w1[1]; 
        for (j = 1; j < m; j++) {
            j2 = 2*j; 
            power[j] += w1[j2]*w1[j2] + w1[j2+1]*w1[j2+1] + 
                        w1[mm-j2]*w1[mm-j2] + w1[mm-j2+1]*w1[mm-j2+1];
        }
        den += sumw; 
    }
    den *= m4; 
    for (j = 0; j < m; j++) power[j] /= den;
    free_dvector(window, 0, mm-1); 
    free_dvector(w1,     0, m4-1); 
}


void cross(int nspec, float data1[], float data2[], float dlam, float *shift)
{
    double *vect1,*vect2,*ans,max,tmp1,*tmp;
    int    dim,sub_int,j,nloc,jmax,jl,j0;

    sub_int = 128;
    dim = 1;
    while (dim < nspec) dim *= 2;
    nloc = dim/2;
    vect1 = dvector(0, dim-1);
    vect2 = dvector(0, dim-1); 
    ans   = dvector(0, 2*dim+1); 
    tmp   = dvector(0, 2*sub_int*nloc-1); 
    for (j = 0; j < nspec; j++) {
        vect1[j] = data1[j];
        vect2[j] = data2[j];
    }
    for (j = nspec; j < dim; j++) {
        vect1[j] = data1[nspec-1];
        vect2[j] = data2[nspec-1];
    }
    correl(vect1, vect2, dim, ans);
    for (j = 0; j < dim/2; j++) {
        tmp1 = ans[j];
        ans[j] = ans[dim/2+j];
        ans[dim/2+j] = tmp1;
    }
    max = -1.0e+20; 
    for (j = 0; j < dim; j++) 
        if (ans[j] > max) {
            max = ans[j]; 
            jmax = j;
            *shift = dlam*(j-dim/2);
        }
    if (jmax < nloc) jl = 0;
    else if (jmax > dim-nloc) jl = dim-2*nloc;
    else jl = jmax-nloc;
    for (j = 0; j < 2*nloc; j++) tmp[j] = ans[jl+j];
    oversample(tmp,2*nloc,sub_int);
    max = -1.0e20;
    j0 = (jmax-jl)*sub_int;
    for (j = 0; j < 2*nloc*sub_int; j++) 
        if (tmp[j] > max) { 
            max = tmp[j];
            tmp1 = dlam*(j-j0)/sub_int;
        }
    *shift += tmp1;
    free_dvector(vect1, 0, dim-1);
    free_dvector(vect2, 0, dim-1);
    free_dvector(ans, 0, 2*dim+1);
    free_dvector(tmp, 0, 2*nloc*sub_int-1);
}


void noise_filter(float data[], int nspec, float resol)
{
    int    j, dim, lim, npt;
    double arg, freqc, *vect, tmp, sig2, n2;

    if ((nspec <= 1) || (resol <= 2.0)) return; 
    dim = 1;
    while (dim < nspec) dim *= 2;
    freqc = (float) dim / resol;
    lim = freqc + 0.5; 
    vect = dvector(0, dim-1);
    for (j = 0; j < nspec; j++) vect[j] = data[j];
    tmp = data[nspec-1];
    if (fabs(tmp) < 0.1) tmp = 0.0;
    for (j = nspec; j < dim; j++) vect[j] = tmp;
    real_fft(vect, dim/2, 1);
    for (j = 2*lim; j < dim; j += 2) vect[j] = vect[j+1] = 0.0; vect[1] = 0.0; 
    real_fft(vect,dim/2,-1);
    arg = 2.0 / dim;
    for (j = 0; j < nspec; j++) data[j] = vect[j]*arg;
    free_dvector(vect, 0, dim-1);
}


void wiener_filter(float data[], int nspec, float resol)
{
    int    j, dim, lim, npt;
    double arg, freqc, *vect, tmp, sig2, n2;

    if ((nspec <= 1) || (resol <= 2.0)) return; 
    dim = 1;
    while (dim < nspec) dim *= 2;
    freqc = (float) dim / resol;
    lim = freqc + 0.5; 
    vect = dvector(0, dim-1);
    for (j = 0; j < nspec; j++) vect[j] = data[j];
    tmp = data[nspec-1];
    if (fabs(tmp) < 0.1) tmp = 0.0;
    for (j = nspec; j < dim; j++) vect[j] = tmp;
    real_fft(vect, dim/2, 1);
    tmp = sqrt(vect[0]*vect[0]); 
    npt = dim/2-lim+1; n2 = sqrt(vect[1]*vect[1])/(tmp*npt); 
    for (j = 2*lim; j < dim; j += 2) 
        n2 += sqrt(vect[j]*vect[j] + vect[j+1]*vect[j+1])/(tmp*npt); 
    n2 *= n2; 
    sig2 = sqrt(log(7.0/n2)*0.5) / freqc; sig2 *= sig2;
    vect[0] /= 1.0 + n2; 
    vect[1] /= 1.0 + n2*exp(2.0*sig2*dim/2*dim/2);
    for (j = 2; j < dim; j += 2) {
        arg = 1.0 + n2*exp(2.0*sig2*j/2*j/2); 
        vect[j]   /= arg;
        vect[j+1] /= arg;
    }
    real_fft(vect,dim/2,-1);
    arg = 2.0 / dim;
    for (j = 0; j < nspec; j++) data[j] = vect[j]*arg;
    free_dvector(vect, 0, dim-1);
}


void bpro_filter(float data[], int nspec, float resol)
{
    int    j, dim, lim, npt;
    double arg, freqc, *vect, tmp, sig2, n2;

    if ((nspec <= 1) || (resol <= 2.0)) return; 
    dim = 1;
    while (dim < nspec) dim *= 2;
    freqc = (float) dim / resol;
    vect = dvector(0, dim-1);
    for (j = 0; j < nspec; j++) vect[j] = data[j];
    tmp = data[nspec-1];
    if (fabs(tmp) < 0.1) tmp = 0.0;
    for (j = nspec; j < dim; j++) vect[j] = tmp;
    real_fft(vect, dim/2, 1);
    vect[1] *= exp(-0.125*resol*resol*TWO_PI*TWO_PI/(4.0*TWO_LOG2)); 
    for (j = 2; j < dim; j += 2) {
        arg = exp(-0.125*j*j*resol*resol*TWO_PI*TWO_PI/(4.0*TWO_LOG2*dim*dim));
        vect[j]   *= arg;
        vect[j+1] *= arg;
    }
    real_fft(vect,dim/2,-1);
    arg = 2.0 / dim;
    for (j = 0; j < nspec; j++) data[j] /= vect[j]*arg;
    free_dvector(vect, 0, dim-1);
}


void oversample(double data[], int n, int ndiv)
{
    int    i;
    double arg;

    if (n <= 1) return; 
    real_fft(data, n/2, 1);
    data[n] = data[1]; 
    data[1] = 0.0; 
    for (i = n+1; i < n*ndiv; i++) data[i] = 0.0;
    real_fft(data,n*ndiv/2,-1);
    arg = 2.0 / n;
    for (i = 0; i < n*ndiv; i++) data[i] *= arg;
}


static void real_fft(double data[], int n, int direction)
{
    double wr,wi,wpr,wpi,wtemp,theta,c1,c2,h1r,h1i,h2r,h2i;
    int    i,i1,i2,i3,i4,n2p3;

    theta = TWO_PI / (2.0*n);
    c1 = 0.5;
    if (direction == 1) {
        c2 = -0.5;
        cmpx_fft(data,n,1);
    } else {
        c2 = 0.5;
        theta = -theta;
    }
    wpr = sin(0.5*theta);
    wpr = -2.0*wpr*wpr;
    wpi = sin(theta);
    wr = 1.0 + wpr;
    wi = wpi;
    n2p3 = 2*n+3;
    for (i = 2; i <= n/2+1; i++) {
        i1 = 2*i - 2;
        i2 = i1 + 1;
        i3 = n2p3 - i2 - 2;
        i4 = i3 + 1;
        h1r = c1*(data[i1] + data[i3]);
        h1i = c1*(data[i2] - data[i4]);
        h2r = -c2*(data[i2] + data[i4]);
        h2i = c2*(data[i1] - data[i3]);
        data[i1] = h1r + wr*h2r - wi*h2i;
        data[i2] = h1i + wr*h2i + wi*h2r;
        data[i3] = h1r - wr*h2r + wi*h2i;
        data[i4] = -h1i + wr*h2i + wi*h2r;
        wtemp = wr;
        wr += wr*wpr - wi*wpi;
        wi += wi*wpr + wtemp*wpi;
    }
    if (direction == 1) {
        h1r = data[0];
        data[0] = h1r + data[1];
        data[1] = h1r - data[1];
    } else {
        h1r = data[0];
        data[0] = c1*(h1r + data[1]);
        data[1] = c1*(h1r - data[1]);
        cmpx_fft(data,n,-1);
    }
}


void correl(double data1[], double data2[], int n, double ans[])
{
    double *fft,fr,fi,ar,ai;
    int    i;

    fft = dvector(0, 2*n+1);
    two_fft(data1, data2, fft, ans, n);
    for (i = 0; i <= n; i += 2) { 
        fr = fft[i]; fi = fft[i+1]; 
        ar = ans[i]; ai = ans[i+1]; 
        ans[i]   = 2.0*(fr*ar+fi*ai)/n;
        ans[i+1] = 2.0*(fi*ar-fr*ai)/n;
    }
    ans[1] = ans[n];
    real_fft(ans, n/2, -1);
    free_dvector(fft, 0, 2*n+1);
}


static void two_fft(double data1[], double data2[], 
                    double fft1[], double fft2[], int n)
{
    double h1r,h1i,h2r,h2i,c1r,c1i,c2r,c2i;
    int    j;

    for (j = 0; j < n; j ++) {
        fft1[2*j] = data1[j]; fft1[2*j+1] = data2[j];
    }
    cmpx_fft(fft1, n, 1);
    fft1[2*n] = fft1[0]; fft1[2*n+1] = fft1[1]; 
    for (j = 0; j <= n; j += 2) {
        c1r = fft1[j];       c1i = fft1[j+1]; 
        c2r = fft1[2*n-j];   c2i = fft1[2*n-j+1];
        h1r = 0.5*(c1r+c2r); h1i = 0.5*(c1i-c2i);
        h2r = 0.5*(c1i+c2i); h2i = 0.5*(c2r-c1r);
        fft1[j]     = h1r;   fft1[j+1]     =  h1i;
        fft1[2*n-j] = h1r;   fft1[2*n-j+1] = -h1i;
        fft2[j]     = h2r;   fft2[j+1]     =  h2i;
        fft2[2*n-j] = h2r;   fft2[2*n-j+1] = -h2i;
    }
}


static void cmpx_fft(double data[], int nn, int direction)
{
    double wr,wi,wpr,wpi,wtemp,theta,tempr,tempi;
    int    i,istep,j,m,mmax,n;

    n = 2*nn;
    j = 0;
    for (i = 0; i < n; i += 2) {
        if (j > i) {
            tempr = data[j];
            tempi = data[j+1];
            data[j] = data[i];
            data[j+1] = data[i+1];
            data[i] = tempr;
            data[i+1] = tempi;
        }
        m = nn;
        while ((m >= 2) && ((j+1) > m)) {
            j -= m;
            m /= 2;
        }
        j += m;
    }
    mmax = 2;
    while (n > mmax) {
        istep = 2*mmax;
        theta = TWO_PI/(direction*mmax);
        wpr = sin(0.5*theta);
        wpr = -2.0*wpr*wpr;
        wpi = sin(theta);
        wr = 1.0;
        wi = 0.0;
        for (m = 1; m <= mmax; m += 2) {
            for (i = (m-1); i < n; i += istep) {
                j = i+mmax;
                tempr = wr*data[j] - wi*data[j+1];
                tempi = wr*data[j+1] + wi*data[j];
                data[j] = data[i] - tempr;
                data[j+1] = data[i+1] - tempi;
                data[i] = data[i] + tempr;
                data[i+1] = data[i+1] + tempi;
            }
            wtemp = wr;
            wr += wr*wpr - wi*wpi;
            wi += wi*wpr + wtemp*wpi;
        }
        mmax = istep;
    }
}
