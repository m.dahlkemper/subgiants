/* These routines handles operations on 2D fits files */ 

#include <stdio.h>
#include <math.h>
#include "structures.h"
#include "mem_alloc.h"
#include "file.h"

void extract(float **data, float **data2, int *naxis, int **npt, int **npt2, 
             int dir, int pix1, int pix2, int sum); 

int main(void)
{
    int   j, k, m, n, getname, naxis, naxis2, *npt, *npt2, ntot, ntot2, dir, 
          pix1, pix2, open_ok, sum, bswap, pix, off; 
    float *data, *data2, min, max, *xx, kk, *first, *step, *first2, 
          *step2, mean; 
    char  *header, *header2, filename[80], answer[2], ans[2]; 
    fits_info_t info, info2; 

    bswap = 1; 
    getname = 1; 
    do { 
        open_ok = read_fits(&data, &naxis, &npt, &first, &step, &info, 
                            &header, getname, filename, bswap); 
        if (open_ok == 1) printf("File not found, try again\n"); 
    } while (open_ok == 1); 
    for (j = 0, ntot = 1; j < naxis; j++) ntot *= npt[j]; 

    do {
        printf("Choose option\n"); 
        printf("Header, Minmax, Extract, eXpand, Save, Constant or File arithmetics, Quit : "); 
        scanf("%1s", answer); 
        printf("%1s\n", answer); 

    /* Show file header on screen */ 
        if ((answer[0] == 'H') || (answer[0] == 'h')) {
            for (j = 0; j < info.nlused; j++) {
                for (k = 0; k < 80; k++) printf("%c", header[80*j+k]); 
                printf("\n"); 
            }

    /* Computes Min and Max in file */ 
        } else if ((answer[0] == 'M') || (answer[0] == 'm')) {
            min = 1.0e30; 
            max = -1.0e30; 
            mean = 0.0; 
            pix = 0; 
            for (j = 0; j < ntot; j++) {
                mean += data[j]/ntot; 
                if (data[j] < min) min = data[j]; 
                if (data[j] > max) { 
                    max = data[j]; 
                    pix = 1; 
                } else if (data[j] == max) {
                    pix++; 
                }
            }
            printf("Min = %f, max = %f (%d pts), mean = %f\n", min, max, pix, mean); 

    /* Extract a region of the file and collapse it */ 
        } else if ((answer[0] == 'E') || (answer[0] == 'e')) {
            if (naxis > 1) {
                do {
                    printf("Extracting direction ["); 
                    for (j = 0; j < naxis-1; j++) printf("%d (%d) : ",j+1, npt[j]); 
                    printf("%d (%d)] : ",naxis, npt[naxis-1]); 
                    scanf("%d", &dir); dir--; 
                    printf("%d\n", dir+1); 
                } while ((dir < 0) || (dir >= naxis)); 
                printf("Lines to extract [1:%d] : ", npt[dir]); 
                scanf("%d %d", &pix1, &pix2); 
                if (pix1 > pix2) { 
                    j    = pix1; 
                    pix1 = pix2; 
                    pix2 = j; 
                } 
                if (pix1 < 1) pix1 = 1; 
                if (pix2 > npt[dir]) pix2 = npt[dir]; 
                printf("%d %d\n", pix1, pix2); 
                pix1--; pix2--; 
                do {
                    printf("Extract, Sum or Average : "); 
                    scanf("%1s", ans); 
                } while ((ans[0] != 'E') && (ans[0] != 'e') && 
                         (ans[0] != 'S') && (ans[0] != 's') && 
                         (ans[0] != 'A') && (ans[0] != 'a')); 
                printf("%1s\n", ans); 

                if      ((ans[0] == 'E') || (ans[0] == 'e')) sum = 0; 
                else if ((ans[0] == 'S') || (ans[0] == 's')) sum = 1; 
                else if ((ans[0] == 'A') || (ans[0] == 'a')) sum = 2; 

                if (pix1 == pix2) sum = 2; 
                extract(&data, &data2, &naxis, &npt, &npt2, dir, pix1, pix2, sum); 

                free_vector(data, 0, ntot-1); 
                if (sum) { 
                    free_ivector(npt, 0, naxis-1); 
                    naxis--; 
                    npt = ivector(0, naxis-1); 
                }
                for (j = 0, ntot = 1; j < naxis; j++) ntot *= (npt[j]=npt2[j]); 
                data = vector(0, ntot-1); 
                for (j = 0; j < ntot; j++) data[j] = data2[j]; 
                free_vector(data2, 0, ntot-1); 
                free_ivector(npt2, 0, naxis-1); 
            } else 
                printf("This option is only for multiD files\n"); 

    /* Expand a file to a bigger format (padding with 0s) */ 
        } else if ((answer[0] == 'X') || (answer[0] == 'x')) {
            printf("New dimensions ["); 
            for (j = 0; j < naxis-1; j++) printf("%d (%d) : ",j+1, npt[j]); 
            printf("%d (%d)] : ",naxis, npt[naxis-1]); 
            npt2 = ivector(0, naxis-1); 
            for (j = 0; j < naxis; j++) { 
                scanf("%d", npt2+j);  
                if (npt2[j] < npt[j]) npt2[j] = npt[j]; 
                printf("%d\n", npt2[j]); 
            } 
            for (j = 0, ntot2 = 1; j < naxis; j++) ntot2 *= npt2[j]; 
            data2 = vector(0, ntot2-1); 
            for (j = 0; j < ntot2; j++) data2[j] = 0.0; 
            for (j = 0; j < ntot; j++) { 
                for (k = pix = 0, n = j, m = 1; k < naxis; n /= npt[k], m *= npt2[k], k++) { 
                    off = (npt2[k]-npt[k])/2; 
                    pix += (n%npt[k]+off) * m; 
                } 
                data2[pix] = data[j]; 
            } 

            free_vector(data, 0, ntot-1);  
            ntot = ntot2;  for (j = 0; j < naxis; j++) npt[j] = npt2[j]; 
            data = vector(0, ntot-1); 
            for (j = 0; j < ntot; j++) data[j] = data2[j]; 
            free_vector(data2, 0, ntot-1); 
            free_ivector(npt2, 0, naxis-1); 

    /* save files to disc */ 
        } else if ((answer[0] == 'S') || (answer[0] == 's')) {
            if (naxis > 1) {
                printf("bitpix : "); 
                scanf("%d", &(info.bitpix)); 
                printf("%d\n", info.bitpix); 
                save_fits(data, naxis, npt, first, step, info, header, getname, 
                          filename, bswap); 
            } else {
                printf("save to Fits or Ascii : "); 
                scanf("%1s", ans); 
                printf("%1s\n", ans); 
                if ((ans[0] == 'F') || (ans[0] == 'f')) {
                    printf("bitpix : "); 
                    scanf("%d", &(info.bitpix)); 
                    printf("%d\n", info.bitpix); 
                    save_fits(data, naxis, npt, first, step, info, header, getname, 
                              filename, bswap); 
                } else if ((ans[0] == 'A') || (ans[0] == 'a')) {
                    xx = vector(0, npt[0]-1); 

/*
                    for (j = 0; j < npt[0]; j++) xx[j] = first[0] + step[0]*j; 
*/
                    for (j = 0; j < npt[0]; j++) xx[j] = j; 
                    save_ascfile(npt[0], 1, xx, data, getname, filename, info.object); 
                    free_vector(xx, 0, npt[0]-1); 
                }
            }

    /* Constant-file arithmetics */ 
        } else if ((answer[0] == 'C') || (answer[0] == 'c')) {
            printf("Operator (+ - * /) : "); 
            scanf("%1s", ans); 
            printf("%1s\n", ans); 
            printf("Constant : "); 
            scanf("%f", &kk); 
            printf("%f\n", kk); 
            if      (ans[0] == '+') for (j = 0; j < ntot; j++) data[j] += kk; 
            else if (ans[0] == '-') for (j = 0; j < ntot; j++) data[j] -= kk; 
            else if (ans[0] == '*') for (j = 0; j < ntot; j++) data[j] *= kk; 
            else if (ans[0] == '/') 
                if (kk == 0.0) printf("Can't divide by zero\n"); 
                else           for (j = 0; j < ntot; j++) data[j] /= kk; 

    /* File-file arithmetics */ 
        } else if ((answer[0] == 'F') || (answer[0] == 'f')) {
            printf("Operator (+ - * /) : "); 
            scanf("%1s", ans); 
            printf("%1s\n", ans); 

            do { 
                open_ok = read_fits(&data2, &naxis2, &npt2, &first2, &step2, &info2, 
                                    &header2, getname, filename, bswap); 
                if (open_ok == 1) 
                    printf("File not found, try again\n"); 
                else {
                    k = (naxis==naxis2); 
                    if (k) for (j = 0; j < naxis; j++) k *= (npt2[j]==npt[j]); 
                    if (!k) printf("Incompatible file, try again\n"); 
                }
            } while (open_ok || !k); 

            if      (ans[0] == '+') for (j = 0; j < ntot; j++) data[j] += data2[j]; 
            else if (ans[0] == '-') for (j = 0; j < ntot; j++) data[j] -= data2[j]; 
            else if (ans[0] == '*') for (j = 0; j < ntot; j++) data[j] *= data2[j]; 
            else if (ans[0] == '/') {
                for (j = k = 0; j < ntot; j++) 
                    if (data2[j] == 0.0) k++; 
                    else                 data[j] /= data2[j]; 
                if (k) printf("WARNING: %d zeros found in file %s\n", k, filename); 
            }
             
            free_vector(data2,  0, ntot-1); 
            free_ivector(npt2,  0, naxis-1); 
            free_vector(first2, 0, naxis-1); 
            free_vector(step2,  0, naxis-1); 
            free_svector((void *) header2); 

        }
    } while ((answer[0] != 'Q') && (answer[0] != 'q')); 

    free_vector(data,  0, ntot-1); 
    free_ivector(npt,  0, naxis-1); 
    free_vector(first, 0, naxis-1); 
    free_vector(step,  0, naxis-1); 
    free_svector((void *) header); 
}


void extract(float **data, float **data2, int *naxis, int **npt, int **npt2, int dir, int pix1, int pix2, int sum)
{
    int   *pix, pix0, shift, n, j, k, ntot, npix; 

    for (j = 0, ntot = 1; j < *naxis; j++) ntot *= (*npt)[j]; 

    if (sum) {
        ntot  /= (*npt)[dir]; 
        *data2 =  vector(0, ntot-1); 
        *npt2  = ivector(0, *naxis-2); 
        pix    = ivector(0, *naxis-2); 

        shift = 1; 
        for (n = 0; n < *naxis-1; n++) { 
            pix[n] = 0; 
            (*npt2)[n] = (*npt)[n+(n>=dir)]; 
            if (n < dir) shift *= (*npt)[n]; 
        }
        if (sum == 2) npix = pix2-pix1+1; 
        else          npix = 1; 

        for (j = 0; j < ntot; j++) {
            (*data2)[j] = 0.0; pix0 = 0; 
            for (n = *naxis-2; n >= 0; n--) {
                pix0 = pix[n] + pix0*(*npt2)[n]; 
                if (n == dir) pix0 *= (*npt)[dir]; 
            }
            for (k = pix1; k <= pix2; k++) 
                (*data2)[j] += (*data)[pix0+k*shift]/npix; 
            for (n = 0; n < *naxis-1; n++) 
                if (pix[n] < (*npt2)[n]-1) { 
                    pix[n]++; 
                    break; 
                } else 
                    pix[n] = 0; 
        }
        free_ivector(pix, 0, *naxis-2); 

    } else {
        npix   = pix2-pix1+1; 
        ntot  /= (*npt)[dir]; ntot  *= npix; 
        *data2 =  vector(0, ntot-1); 
        *npt2  = ivector(0, *naxis-1); 
        pix    = ivector(0, *naxis-1); 

        for (n = 0; n < *naxis; n++) {
            pix[n] = 0; 
            if (n == dir) (*npt2)[n] = npix; 
            else          (*npt2)[n] = (*npt)[n]; 
        }

        for (j = 0; j < ntot; j++) {
            pix0 = 0; 
            for (n = *naxis-1; n >= 0; n--) 
                if (n == dir) pix0 = pix[n]+pix1 + pix0*(*npt)[n]; 
                else          pix0 = pix[n]      + pix0*(*npt)[n]; 
            (*data2)[j] = (*data)[pix0]; 
            for (n = 0; n < *naxis; n++) 
                if (pix[n] < (*npt2)[n]-1) { 
                    pix[n]++; 
                    break; 
                } else 
                    pix[n] = 0; 
        }
        free_ivector(pix, 0, *naxis-1); 
    }
}
