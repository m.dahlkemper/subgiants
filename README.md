# Documentation for Subgiants project

*Introductory remark:* 
The main problem is that the file structures of the data are all in a totally different format:
- Sometimes the spectra are inside a folder which is named by to the date of the observation
- Sometimes all spectra of a specific star are in one folder which is named by the name of the star
- There are also several folders of the same star where the spelling is different (e.g. kappaboo and Kappa Boo etc.)
 
The procedure is as following:

- First, the data of the  star have to be enriched with Gaia data. This is done by feeding a list of the stars into an online tool on the [website of the ARI Heidelberg](http://gaia.ari.uni-heidelberg.de/singlesource.html). Careful: The tool uses Simbad, but it is not as resilient to spelling as the Simbad webpage, therefore the star list has to be "normalized" at first. All these steps are done in the code *prepare_database.py*. From Gaia the temperatures and luminosities of the stars are obtained.
- This list of stellar parameters is used in order to check the list for subgiants. This is done using Isochrones, where every star is plotted on an HRD and it is assigned a particular isochrone point. This gives a rough estimate of its log g value and its SGB membership.
- In the further process only those stars are used which are assigned to the SGB. with its log g and Teff value and its spectrum a least-squared-deconvolution is done in order to check the star for a magnetic field detection.
- After that the radial velocity is measured using the result from the lsd
- In the end the S-Index, Halpha and Ca IRT are measured using its spectrum.


## Initializing
As a first step all the filepaths have to be adapted in the file *filepaths.py*.

In the file *prepare_database.py* there are several routines in order to 
obtain stellar parameters from Gaia. This code involves some fine tuning 
depending on how exactly your folder structure looks like.
Please have a look at the code in this file before you run it as it will create 
some new files which you should use to download files from Gaia.

## Classify as subgiants
The classification as subgiants happens through comparison with MIST tracks.

First the EEP tracks have to be downloaded from [the MIST 
website](http://waps.cfa.harvard.edu/MIST/model_grids.html) (I used v/vcrit =  
0.4 and [Fe/H] = 0.0) and stored in the folder specified in *filepaths.py*. 
You also need the python routine *read_mist_models.py* from 
http://waps.cfa.harvard.edu/MIST/resources.html (which is also included in the 
scripts).

The routines to go on are in the file *classify_stars.py*:
- *load_eep_tracks* loads the tracks in a specified mass range into a single 
data frame (default is masses between 0.1 and 3 M_sol)
- *extract_sgb* extracts from the track. The EEP does not denote the sgb 
directly, but main sequence and giants. So the routine detects if the track 
above the MS is basically "horizontal" in the HRD. The routine can also be 
given a certain "extension value" above and below the SGB which means that also 
parts of the track above and below the sgb are taken into further analysis
- *create_sgb_grid* creates a grid in a certain mass range (default 0.7 M_Sol 
to 0.9 M_sol in steps of 0.05 M_sol and 0.9 M_sol to 2.0 M_sl in steps of 0.02 
M_sol
- *find_value* interpolates values for the stars from the sgb grid created 
before

An example usage of the code is then at the end of the file. The resulting 
parameers are stored in a csv file with the sufix "_enhanced_params.csv".

## Calculate LSD profile

This step has to be carried out separately using the file *subgiants_lsd.py*.

With the parameters which are gained from the step before the lsd profile can 
be calculated as well as a estimation of magnetic field detection. This is done 
using a C code which is wrapped by the *subgiants_lsd.py* routine. In this code 
a lot of magic happpens which I don't really understand, but input is the 
_enhanced_params file from before and output are lsd files which are stored 
inside the lsd directory specified in *filepaths.py* and a table with 
Detections ("DEFINITE", "MARGINAL" or "No"), which is stored in the result_dir.

This step is vey time consuming and can be skipped if there is no need to 
create new lsd_profiles or calculate detections.

## Calculate radvel and indices

Last step is to calculate the radial velocity from the lsd profile and the S 
index, Ca IRT index and H alpha index from the spectrum.

This is done in the *calculate_indices.py* file which uses the *cal_radvel.py*, 
the *halpha.py*, the *cairt.py*, the *cahk.py* and last but not least the 
*li.py* file. The latter one is used in order to detect lithium in the spectrum.

Input is the enhanced_params file as well as the lsd_dir, output is a file with 
the suffix "_indices.csv" which is also stored in the result_dir.

If radial velocities are already calculated, these are stored inside the 
lsd_dir and therefore aren't calculated again.

## Writing results and plotting

In the end, the results can be written into one file and plots can be created 
by making use of the plotting routines from the file *plot_hrd.py*.
The routines are basically self-explanatory.

## Conclusion

So, all in all the scripts are used as follows:

1. initialize the filepaths in *filepaths.py*
2. prepare source lists using the first half of *prepare_database.py*
3. Download data from Gaia
4. Create params list using Gaia data and second half of *prepare_database.py*
5. run first part of *subgiants.py* to classify stars
6. run run *subgiants_lsd.py* (this is the most time consuming step. Only do 
this if you really need to!)
7. run the rest of subgiants.py to calculate indices (this can also be very 
time consuming as there might be many spectra per star), write the output and 
create plots.


