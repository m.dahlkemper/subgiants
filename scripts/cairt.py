import numpy as np
import pandas as pd

# star = input('Star name?')
# id = input('Star ID?')
# folder = '../polar_database/sample_spectra/'
# spec = np.loadtxt(folder+star+'_espadons_2005_pol_RM_V_'+id+'p.s',skiprows=2)
# lam = spec[:,0]
# data = spec[:,1]
# errdata =spec[:,5]

def cairt(lam, data, errdata, velstar ):
    lamca1 = 849.8023
    lamca2 = 854.2091
    lamca3 = 866.2141
    lamv = 847.58
    lamr = 870.49
    deltalamca = .2
    deltalamcont = .5
    vel_light = 299792.5


    # errdata = vector(0, npts - 1)
    # if (n < 4)
    # errdata = data + npts;
    # else errdata = data + 4 * npts;

    fluxca1, fluxca2, fluxca3, fluxv, fluxr, = 0,0,0,0,0
    weightca1, weightca2, weightca3, weightv, weightr = 0,0,0,0,0

    # printf("stellar velocity?\n")
    # velstar = float(input ("stellar velocity?"))

    deltalam = velstar / vel_light
    lamca1 *= (1. + deltalam)
    lamca2 *= (1. + deltalam)
    lamca3 *= (1. + deltalam)
    lamv *= (1. + deltalam)
    lamr *= (1. + deltalam)
    #
    for j in range(len(lam)):
        errdata[j] = 1.
        if ((lam[j] > lamv - deltalamcont / 2.) & (lam[j] < lamv + deltalamcont / 2.)):
            fluxv += data[j] / errdata[j]
            weightv += 1. / errdata[j]
        if ((lam[j] > lamr - deltalamcont / 2.) & (lam[j] < lamr + deltalamcont / 2.)):
            fluxr += data[j] / errdata[j]
            weightr += 1. / errdata[j]

        if ((lam[j] > lamca1 - deltalamca / 2.) & (lam[j] < lamca1 + deltalamca / 2.)):
            fluxca1 += data[j] / errdata[j]# * (deltalamca / 2. - np.abs(lam[j] - lamca1)) / deltalamca * 2.
            weightca1 += 1. / errdata[j] #* (deltalamca / 2. - np.abs(lam[j] - lamca1)) / deltalamca * 2.

        if ((lam[j] > lamca2 - deltalamca / 2.) & (lam[j] < lamca2 + deltalamca / 2.)):
            fluxca2 += data[j] / errdata[j]# * (deltalamca / 2. - np.abs(lam[j] - lamca1)) / deltalamca * 2.
            weightca2 += 1. / errdata[j] #* (deltalamca / 2. - np.abs(lam[j] - lamca1)) / deltalamca * 2.

        if ((lam[j] > lamca3 - deltalamca / 2.) & (lam[j] < lamca3 + deltalamca / 2.)):
            fluxca3 += data[j] / errdata[j]# * (deltalamca / 2. - np.abs(lam[j] - lamca3)) / deltalamca * 2.
            weightca3 += 1. / errdata[j] #* (deltalamca / 2. - np.abs(lam[j] - lamca3)) / deltalamca * 2.


    #
    fluxv /= weightv
    fluxr /= weightr
    fluxca1 /= weightca1
    fluxca2 /= weightca2
    fluxca3 /= weightca3
    return (fluxca1 + fluxca2 + fluxca3)/(fluxv + fluxr)
    #print("caIRTindex = {}\n".format((fluxca1 + fluxca2 + fluxca3)/(fluxv + fluxr)))
