# base paths where the project is stored.
# Here your initial objectlists are stored and your output from Gaia is stored
base_path = '/home/mertennikolay/Uni/Tutorenjobs/Sandra/subgiants/'

# the frame (csv file) where a list of your objects is stored (without suffix)
object_frame = 'test'

# path where the EEP-Tracks from MIST are stored
eep_tracks = base_path+'EEP-Tracks/'

# path where th C-script for calculating the lsd is stored
workingdir = base_path+'lsd_scripts/'

# path where all the folders with stellar spectra are stored
writingdir = base_path+'spectra/'

# path where the VALD output is stored
vald_dir = base_path+'VALD/VALD_output/'

# path where the lsd files are stored. In this folder there is also the output from radvel calculations velocitoes.csv
lsd_dir = base_path+'subgiants_lsd/'

# path where the results are stored
result_dir = base_path+'results/'
