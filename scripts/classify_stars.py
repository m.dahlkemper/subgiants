import read_mist_models as rmm
import filepaths
import numpy as np
from glob import glob
import matplotlib.pyplot as plt
import pandas as pd
from scipy.interpolate import interp2d
import matplotlib.path as mplPath

def in_between(value,lower,upper):
    if lower > upper:
        print('lower is larger than upper')
        return False
    if value>=lower and value<=upper:
        return True
    else:
        return False

def find_nearest_neighbor(star,grid):
    def distance(star1,star2):
        return (star1[0]-star2[0])**2 + (star1[1]-star2[1])**2
    dist = distance(star,grid[-1])
    nearest_idx = 0
    for i,star2 in enumerate(grid[:-1]):
        temp = distance(star,star2)
        if temp < dist:
            nearest_idx = i
    return nearest_idx



def load_eep_tracks(min_mass=0.1,max_mass=3):
    """
    Function to load MIST eep tracks, it also writes the frame on the disk
    :param min_mass: float: minimum mass for tracks to load
    :param max_mass: float: maximum mass for tracks to load
    :return: pandas DataFrame with all the tracks concatenated and mass as index
    """
    eep_tracks = []
    eep_files = glob(filepaths.eep_tracks+'*.eep')
    fig,ax = plt.subplots()
    tracks = []
    for file in eep_files:
        eep = rmm.EEP(file,verbose=False)
        print(eep.minit)
        if eep.minit < max_mass and eep.minit > min_mass:
            masses = [eep.minit]*len(eep.eeps)
            track = pd.DataFrame(eep.eeps)
            track['mass'] = masses
            tracks.append(track)
            print('EEP track succesfully written to frame.')
    track_frame = pd.concat(tracks)
    track_frame.to_csv(filepaths.eep_tracks+'eep_frame.csv',index=False)
    return track_frame

def extract_sgb(track,threshold=.015,extension_below=0,extension_above=0,lum_max=None,teff_min=None):
    """
    Extracts the SGB from a given EEP
    :param track: DataFrame with the track
    :param threshold: float what the minimum difference between two values in log L has to be to be already on the RGB
    :return: Dataframe with EEPs only on the SGB
    """
    sgb_track = track[track['phase']==2]
    logL_diffs =np.diff(sgb_track['log_L'])
    turnoff_teff = sgb_track.iloc[np.argmax(logL_diffs>threshold)]['log_Teff']
    above_sgb = pd.DataFrame()
    if lum_max is not None:
        above_sgb = sgb_track[(sgb_track['log_Teff']<=turnoff_teff) & (sgb_track['log_L']<lum_max)]
    if teff_min is not None:
        if not above_sgb.empty:
            above_sgb = above_sgb[above_sgb['log_Teff']>teff_min]
    sgb_track = sgb_track[sgb_track['log_Teff'] > turnoff_teff]
    sgb_track = sgb_track.assign(phase = 'sgb')
    sgb_indices = sgb_track.index
    extension_num_below = int(np.ceil(len(sgb_indices) * extension_below))
    extension_num_above = int(np.ceil(len(sgb_indices) * extension_above))
    below_sgb = track.loc[sgb_indices[0] - extension_num_below:sgb_indices[0]]
    below_sgb = below_sgb.assign(phase = 'sgb extension')
    if above_sgb.empty:
        above_sgb = track.loc[sgb_indices[-1]:sgb_indices[-1] + extension_num_above]
    above_sgb = above_sgb.assign(phase='sgb extension')
    track.update(sgb_track['phase'])
    track.update(above_sgb['phase'])
    track.update(below_sgb['phase'])
    return track

def create_sgb_grid(track_frame, masses=np.concatenate((np.arange(0.7,0.9,0.05),np.arange(0.9,2.0,0.02))),extend_below = 0,extend_above=0,with_extension=False,lum_max=None,teff_min=None):
    """
    creates SGB-grid from several tracks and gives the border of this grid as matplotlib.path object
    :param track_frame: DataFrame with all the tracks
    :param masses: iterable: masses which are used to create the grid
    :return: DataFrame with SGB-only tracks, list with SGB only tracks, matplotlib.path object with border of the grid
    """
    tracks = []
    left_border = []
    right_border = []
    num_masses = len(masses)
    for i,mass in enumerate(masses):
        mass = np.round(mass,2)
        track = extract_sgb(track_frame[track_frame['mass']==mass],extension_below=extend_below,extension_above=extend_above,lum_max=lum_max,teff_min=teff_min)
        if with_extension:
            sgb = track[(track['phase'] == 'sgb') | (track['phase']=='sgb extension')]
        else:
            sgb = track[track['phase']=='sgb']
        tracks.append(track)
        left_border.append((sgb.iloc[0]['log_Teff'],sgb.iloc[0]['log_L']))
        right_border.append((sgb.iloc[-1]['log_Teff'],sgb.iloc[-1]['log_L']))
        if i==0:
            lower_border = list(zip(sgb['log_Teff'], sgb['log_L']))
        elif i==num_masses-1:
            upper_border = list(zip(sgb['log_Teff'],sgb['log_L']))
    border = left_border[1:-1]+upper_border+right_border[-1:1:-1]+lower_border[-1:1:-1]
    borderpath = mplPath.Path(border)
    return pd.concat(tracks),tracks,borderpath

def find_value(grid,grid_border,properties,stars,with_extension=False):
    """
    finds values of certain property which is given by the grid for all stars lying within the grid
    :param grid: DataFrame from SGB-only tracks
    :param grid_border: matplotlib.path object with the grid borders
    :param properties: properties which have to be found
    :param stars: dataframe with stars
    :return: dataframe containig only the subgiants, where columns with the interpolated properties are added
    """
    interpolated_grids={}
    prop_values = {}
    if with_extension:
        grid = grid[(grid['phase'] == 'sgb')|(grid['phase']=='sgb extension')]
    else:
        grid = grid[grid['phase']=='sgb']
    log_Teff = grid['log_Teff']
    log_L = grid['log_L']
    for prop in properties:
        interpolated_grids[prop] = interp2d(log_Teff,log_L,grid[prop])
        prop_values[prop] = []
    subgiants=pd.DataFrame()
    for i,star in stars.iterrows():
        log_teff_star = np.log10(star['teff_val'])
        log_L_star = np.log10(star['lum_val'])
        if with_extension or grid_border.contains_point((log_teff_star, log_L_star)):
            for prop in properties:
                prop_value = interpolated_grids[prop](log_teff_star,log_L_star)
                prop_values[prop].append(prop_value[0])
            subgiants = subgiants.append(star)
    for prop in properties:
        subgiants[prop]=prop_values[prop]
    subgiants['subgiant']=True
    return subgiants




#
# track_frame = pd.read_csv(filepaths.eep_tracks+'eep_frame.csv')
# sgb_grid,sgbs,grid_borders = create_sgb_grid(track_frame)
# log_Teff = sgb_grid['log_Teff']
# log_L = sgb_grid['log_L']
# #interpolated_grid = interp2d(log_Teff,log_L,sgb_grid['log_g'])
# grid_size = 20
# teff_grid = np.linspace(np.log10(5200),np.log10(5500),grid_size)
# logL_grid = np.linspace(np.log10(10),np.log10(14),grid_size)
# grid = np.meshgrid(teff_grid,logL_grid)
# xy = (grid[0].flatten(),grid[1].flatten())
#
# cmap = plt.get_cmap('viridis')
# #logg = interpolated_grid(xy[0],xy[1])
# # print(logg)
# fig,ax = plt.subplots()
# sgb = ax.scatter(xy[0],xy[1])
#                  # c=logg,cmap=cmap, vmin=min(logg), vmax=max(logg), marker=',', s=3)
# # fig.colorbar(sgb, ax=ax)
#
# plt.show()