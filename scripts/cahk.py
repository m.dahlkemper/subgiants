import numpy as np
import pandas as pd

# star = input('Star name?')
# id = input('Star ID?')
# folder = '../polar_database/sample_spectra/'
# id = '798231'
# spec = np.loadtxt(folder+star+'_espadons_2005_pol_RM_V_'+id+'p.s',skiprows=2)
# lam = spec[:,0]
# data = spec[:,1]
# errdata =spec[:,5]
parameters = [7.999,-3.904,1.150,1.289,-0.069] # parameters according to Marsden et.al. (2014) Table 6 for the ESPaDOnS instrument
# parameters = [12.873,2.502,8.877,4.271,1.183e-3] # parameters according to Marsden et.al. (2014) Table 6 for the Narval instrument

def cahk(lam, data, errdata, velstar, instrument = 'espadons'):
    '''
    :param lam: array of wavelengths
    :param data: array of fluxes
    :param errdata: array of errors to the fuxes
    :param parameters: numbers for a,b,c,d,e according to Marsden et.al. (2014) eq. 7
    :return:
    '''
    if instrument == 'espadons':
        parameters = [7.999, -3.904, 1.150, 1.289,
                      -0.069]  # parameters according to Marsden et.al. (2014) Table 6 for the ESPaDOnS instrument
    elif instrument == 'narval':
        parameters = [12.873, 2.502, 8.877, 4.271,1.183e-3]  # parameters according to Marsden et.al. (2014) Table 6 for the Narval instrument
        #parameters = [-1.45563335e+03, - 7.48877076e+03, - 2.48461978e+03,-5.16404110e+03, 1.18285595e-03] # Parameters which were given in the original script

    a,b,c,d,e = parameters
    lamk = 393.3663
    lamh = 396.8469
    lamv = 390.107
    lamr = 400.107
    deltalamca = .218
    deltalamcont = 2.
    vel_light = 299792.5

    # errdata = vector(0, npts - 1)
    # if (n < 4)
    # errdata = data + npts;
    # else errdata = data + 4 * npts;

    fluxh, fluxk, fluxv, fluxr, = 0,0,0,0
    weighth, weightk, weightv, weightr = 0,0,0,0

    # printf("stellar velocity?\n")
    # velstar = float(input ("stellar velocity?"))

    deltalam = velstar / vel_light
    lamh *= (1. + deltalam)
    lamk *= (1. + deltalam)
    lamv *= (1. + deltalam)
    lamr *= (1. + deltalam)
    #
    for j in range(len(lam)):
        errdata[j] = 1.
        if ((lam[j] > lamv - deltalamcont / 2.) & (lam[j] < lamv + deltalamcont / 2.)):
            fluxv += data[j] / errdata[j]
            weightv += 1. / errdata[j]
        if ((lam[j] > lamr - deltalamcont / 2.) & (lam[j] < lamr + deltalamcont / 2.)):
            fluxr += data[j] / errdata[j]
            weightr += 1. / errdata[j]

        if ((lam[j] > lamh - deltalamca / 2.) & (lam[j] < lamh + deltalamca / 2.)):
            fluxh += data[j] / errdata[j] * (deltalamca / 2. - np.abs(lam[j] - lamh)) / deltalamca * 2.
            weighth += 1. / errdata[j] * (deltalamca / 2. - np.abs(lam[j] - lamh)) / deltalamca * 2.

        if ((lam[j] > lamk - deltalamca / 2.) & (lam[j] < lamk + deltalamca / 2.)):
            fluxk += data[j] / errdata[j] * (deltalamca / 2. - np.abs(lam[j] - lamk)) / deltalamca * 2.
            weightk += 1. / errdata[j] * (deltalamca / 2. - np.abs(lam[j] - lamk)) / deltalamca * 2.


    #
    fluxv /= weightv
    fluxr /= weightr
    fluxh /= weighth
    fluxk /= weightk
    #print("V = {}\nR = {}\nH = {}\nK = {}\n".format(fluxv, fluxr, fluxh, fluxk))
    #same notation as Wright et al 2004 -- S = (H+K) / (V+R) with scaling for Mnt Wilson
    #
    # // from Stephen:
    #     //
    #     The
    # parameters
    # for NARVAL are:
    #     //
    #     (-1.45563335e+03 * fluxh - 7.48877076e+03 * fluxk) / (
    #                 -5.16404110e+03 * fluxv - 2.48461978e+03 * fluxr) + 1.18285595e-03
    #     // The
    # parameters
    # for ESPADONS are:
    #     //
    #(-0.90321508 * fluxh + 1.85056153 * fluxk) / (0.26606579 * fluxv + 0.29814537 * fluxr) - 0.06867997
    #
    # print("Sindex = {}\n".format((-1.45563335e+03 * fluxh - 7.48877076e+03 * fluxk) / (
    #              -5.16404110e+03 * fluxv - 2.48461978e+03 * fluxr) + 1.18285595e-03))
    # print("Sindex_HARPS = {}\n".format(1.111 * (2.3 * (fluxh + fluxk) / (fluxv + fluxr)) + 0.0153))
    # // printf("Sindex = %f\n", (-6.39439179e+00 * fluxh + 1.64816487e+02 * fluxk) / (
    #             2.06646020e+01 * fluxv + 9.49961109e+01 * fluxr) - 3.25173833e-02);
    # // printf("Sindex = %f\n", (fluxh + fluxk) / (fluxv + fluxr));
    # }
    return (a*fluxh+b*fluxk)/(c*fluxv + d*fluxr)+e
