import pandas as pd
import numpy as np
import os
from glob import glob
import subprocess
from folder_structure import extract_spectrum_files
import filepaths


datedict = {'1':'jan','2':'feb','3':'mar','4':'apr','5':'may','6':'jun',
            '7':'jul','8':'aug','9':'sep','10':'oct','11':'nov','12':'dec'}

workingdir = filepaths.workingdir
writingdir = filepaths.writingdir
basename = filepaths.object_frame
result_dir = filepaths.result_dir
lsd_dir = filepaths.lsd_dir
vald_dir = filepaths.vald_dir
if not os.path.exists(lsd_dir):
    os.mkdirs(lsd_dir)

stellar_para_sub = open(writingdir+'stellar_para_sub.dat','w')
subgiants_detections = open(result_dir+basename+'_detections.dat','w')
corrine_stellar_para_sub = open(writingdir+'Corrine_stellar_para_sub.dat','w')
error_stars = open(writingdir+'Errors.dat','w')

dummy = open(workingdir+'sum_subgiants_test.c','r')
c_code = dummy.read()
dummy.close()
input_file = open(workingdir+'sum_input.default','r')
input_default = input_file.read()
input_file.close()

lsd_weights = pd.read_csv(workingdir+'lsd_weights.csv',names=None, header=None)
lsd_weights.index = pd.MultiIndex.from_arrays([np.asarray(lsd_weights[0]),
                                               np.asarray(lsd_weights[1])],
                                              names=['temp','logg'])

save = workingdir+'subgiants_lsd/'
val_fischer = pd.read_fwf(workingdir+'table8_valenti_fischer.dat',names=None, header=None)
val_fischer.index=val_fischer[1]
val_fischer_t9 = pd.read_fwf(workingdir+'table9_valenti_fischer.dat',names=None, header=None)
val_fischer_t9.index=val_fischer_t9[1]




def process_folder(star=None,date=None,c_code=c_code,subgiants_detections=subgiants_detections,
                   corrine_stellar_para_sub=corrine_stellar_para_sub,stellar_para_sub=stellar_para_sub,input_default=input_default,
                   error_stars=error_stars,lsd_weights=lsd_weights,val_fischer=val_fischer,val_fischer_t9=val_fischer_t9,
                   workingdir=workingdir,writingdir=writingdir,vald_dir=vald_dir):
    if star is not None:
        starname = star['object']
        filepath, outfiles = extract_spectrum_files(starname,error_stars)
        if filepath is None:
            return

        temp = star['teff_val']
        logg = star['log_g']
        lumin = star['lum_val']
        luminerr = (star['lum_err_lower'] + star['lum_err_upper']) / 2
        mass = star['mass']
        age = star['star_age']
        mask_temp = np.round(temp / 250) * 250
        mask_logg = np.round(logg / 0.5) * 0.5
        if mask_logg < 3.0 or mask_logg > 4.0:
            error_stars.write(f'for star {starname} log g = {logg} not in range\n')
            return
        if mask_temp < 5000 or mask_temp > 8000:
            error_stars.write(f'for star {starname} teff = {temp} not in range\n')
            return

    elif date is not None:
        os.chdir(writingdir + date)
        filepath = os.getcwd()+'/'
        outfiles = glob('*.out')
        stars = [out.split('_')[0].replace('hd', 'HD ') for out in outfiles]
        for fname, star in zip(outfiles, stars):
            temp = val_fischer.at[star, 2]
            logg = val_fischer.at[star, 3]
            vsini = val_fischer.at[star, 10]
            lumin = val_fischer_t9.at[star, 11]
            luminerr = val_fischer_t9.at[star, 12]
            mass1 = val_fischer_t9.at[star, 15]
            mass1err = val_fischer_t9.at[star, 16]
            mass2 = val_fischer_t9.at[star, 17]
            age = val_fischer_t9.at[star, 21]
            mask_temp = np.round(temp / 250) * 250
            mask_logg = np.round(logg / 0.5) * 0.5

    metal = 0.0
    mask_name = 't{:d}_g{:.1f}_m{:.2f}'.format(int(mask_temp), mask_logg, metal)
    mask = vald_dir + mask_name
    eeq = lsd_weights.xs((mask_temp, mask_logg))[2]
    leq = lsd_weights.xs((mask_temp, mask_logg))[3]

    os.chdir(workingdir)
    c_code = c_code.replace('xxxx', str(leq)).replace('yyyy', str(eeq))
    c_prog = open('sum_subgiants.c', 'w')
    c_prog.write(c_code)
    c_prog.close()
    subprocess.run('make sum_subgiants', shell=True)

    for fname in outfiles:
        spec_filename = filepath + '/' + fname.split('.')[0] + '.s'
        if len(pd.read_csv(spec_filename, delim_whitespace=True, skiprows=2).columns) < 6:
            error_stars.write(f'Spectrum {fname} for star {starname} unpolarised\n')
            continue
        if 'narval' in fname:
            date = fname.split('_')[2]
        elif '/' in fname:
            date = fname.split('/')[0]
        else:
            if not os.getcwd() == writingdir + starname + '/':
                os.chdir(writingdir + starname + '/')
            with open(fname, 'r') as file:
                for i, line in enumerate(file):
                    if i == 6:
                        (year, month, day) = line.split(':')[1].split('@')[0].strip().split(' ')
                        date = f'{int(day):02d}{datedict[month]}{year}'

        stellar_para_sub.write('{} {} {} {} {} {} {}\n'.
                               format(date, starname, temp, logg, mask_temp, mask_logg, leq))

        os.chdir(workingdir)

        input = input_default.replace('WRITING_DIRDATE/FILE', spec_filename). \
            replace('MASK_DIRLSDMASK', vald_dir + mask_name). \
            replace('LSD_DIRSAVE', lsd_dir + starname.replace('HD ', 'hd') + '_lsd')
        input_file = open('foo3', 'w')

        input_file.write(input)
        input_file.close()
        input_file = open('foo3', 'r')
        output_fh = open('foo.out', 'w')
        subprocess.call('{}sum_subgiants'.format(workingdir), stdin=input_file,
                        stdout=output_fh, shell=True)
        input_file.close()
        output_fh.close()
        output_fh = open('foo.out', 'r')
        output = list(output_fh)
        print(starname)
        output_fh.close()
        signal_detection_lines = [line for line in output if 'signal detection' in line]
        if len(signal_detection_lines) > 0:
            detection = signal_detection_lines[1].strip(' ').split(' ')[1]
            meansnlines = [line for line in output if 'mean sn' in line]
            firstmeansnline = meansnlines[0]
            meansn = firstmeansnline.strip(' ').strip('\n').split(' ')[-1]
            if detection == 'No':
                detect = 1
            else:
                detect = 2
            print(detection)
            subgiants_detections.write('{} {} {} {} {} {} {:.2f}  {:.3f} {:.2f} {:.1f}\n'
                                       .format(starname, date, detection, detect, meansn,
                                               temp, logg, lumin, mass, age))
            corrine_stellar_para_sub.write('{} {} 50 {} {} {} {}\n'
                                           .format(starname, temp, lumin, luminerr, detection, detect))
        else:
            error_stars.write(f'No output for spectrum {fname} for star {starname}\n')
            return



mean = 1
add = 1

datelist = glob(writingdir+'*/')
datelist = [date.split('/')[-2] for date in datelist]
datelist = [i for i in datelist if i[:2].isnumeric() and i[2:5].islower() and i[5:7].isnumeric()]




parameter_frame =pd.read_csv(filepaths.base_path+filepaths.object_frame+'_enhanced_params.csv')


# for date in datelist:
#     process_folder(date=date)

for i,star in parameter_frame.iterrows():
    process_folder(star=star)


stellar_para_sub.close()
corrine_stellar_para_sub.close()
subgiants_detections.close()
error_stars.close()
if os.path.isfile('output.ps'):
    os.remove('output.ps')
