import pandas as pd
import os
import numpy as np
from glob import glob
import filepaths

def polar_to_dataframe(ascii_file):
    '''
    Parse data from polarbase into pandas DataFrame
    :param ascii_file: the ascii file downloaded from http://polarbase.irap.omp.eu/
    :return: polar_frame: pandas DataFrame with the data from the ascii file
    :return: objectlist: a list of single sources in the polarbase
    '''
    items = []
    with open(ascii_file) as f:
        items = f.read().split('Result : ')
        items = items[1:]
    for i,item in enumerate(items):
        item = item.replace('\n','').split('|mode_instru = pol||')[1].split('||')
        prop_tuples = {}
        for prop in item:
            prop=prop.strip('|')
            prop_tuple = prop.split(' = ')
            prop_tuples[prop_tuple[0]] = prop_tuple[1]
        items[i] = prop_tuples
    polar_frame = pd.DataFrame(items)
    objectlist = pd.Series(polar_frame['objet'].unique())
    return polar_frame, objectlist

def clean_objectlists(objectlist,path_to_outfile):
    '''

    :param objectlist: list with object names
    :param path_to_outfile: pathname where list should be stored
    :return: list with object names according to SIMBAD
    '''
    objectlist_clean = []
    for source in objectlist:
        if source[:2] == 'Cl' or source[:2] == 'EM':
            obj_class = source[:2]
            source_clean = obj_class+'* '+source[2:]
        elif source[:2] == 'EQ' :
            source_clean = ''
        elif source == 'FHya':
            source_clean = 'F Hya'
        elif source == 'f02cyg':
            source_clean = '*f02 Cyg'
        elif source == 'fPer':
            source_clean = 'f Per'
        elif source == 'HD195564A':
            source_clean = 'HD 195564'
        elif source == 'KOCH':
            source_clean = ''
        elif source[:3]=='NGC':
            source_clean = source[:7]+' '+source[7:]
        elif source[:4] == 'NOVA':
           source_clean = source[:4] + ' ' + source[4:]
        elif source[0]=='V' and source != 'VB10' and source != 'VAnd' and source != 'VepsEri' and source[1] != '*':
            source_clean = 'V*'+source[1:]
        elif source =='VAnd':
            source_clean = 'V And'
        elif source == 'VepsEri':
            source_clean = 'V*eps Eri'
        elif source[0].islower() and source[:4] != 'kelt':
            source_clean= '*'+source
        elif source[:3]=='ADS':
            source_clean = ''
        # Noch nicht bekannt: Form "Cl* Melotte 25 VA AGe m"
        else:
            source_clean = source
        objectlist_clean.append(source_clean.strip())
    object_frame = pd.DataFrame({'object':objectlist,'object_simbad':objectlist_clean})
    object_frame.to_csv(path_to_outfile,index=False)
    return objectlist_clean

def generate_objectlists_for_gaia(objectlist,folder,extra_name=''):
    '''
    Generates objectlists as chunks of 200 objects in order to downlad gaia data from http://gaia.ari.uni-heidelberg.de/singlesource.html
    (since the maximum number of single sources is 200)
    :param objectlist: list with single sources
    :param folder: folder where the lists should be generated
    :return: Nothing, lists are directly generated as files in the folder
    '''
    num_of_lists = int(np.ceil(len(objectlist)/200))
    for i in range(num_of_lists-1):
        with open(folder+f'sources_{extra_name}_{i}.csv','w+') as f:
            f.writelines('\n'.join(objectlist[i*200:i*200+200]))
    with open(folder+f'sources_{extra_name}_{num_of_lists-1}.csv', 'w+') as f:
        f.writelines('\n'.join(objectlist[(num_of_lists-1) * 200:]))
    return

def generate_gaia_frame(folder,columns ='../column_names_subset.txt',sourcefile_pattern = None):
    '''
    Generates a frame with gaia data from all sources lists, sources lists should be in the the form 'SingleSource*'
    :param folder: folder where the single gaia tables are
    :param sourcefile_pattern: pattern which is looked for in the singlesource files. If None, every file with pattern 'SingleSource' is taken
    :param columns: either str or list, gives the columns subset. If str, the parameter is interpreted as filename,
    which is looked for in folder
    :return: pandas  DataFrame with the gaia data for all downloaded single sources
    '''
    origin = os.getcwd()
    os.chdir(folder)
    if sourcefile_pattern is not None:
        file_list= glob('SingleSource*'+sourcefile_pattern+'*')
    else:
        file_list = glob('SingleSource*')
    frames = []
    for file in file_list:
        frames.append(pd.read_csv(file))
    gaia_frame = pd.concat(frames)
    column_names = []
    if type(columns)==str:
        with open(columns) as f:
            for line in f:
                column_names.append(line.rstrip('\n'))
    else:
        column_names = columns
    os.chdir(origin)
    return gaia_frame[column_names]


def find_gaia_errors(gaia_frame):
    """
    calculates errorbars from gaia percentiles
    :param gaia_frame: data frame with gaia params
    :return: the dataframe, enriched by the errorbars
    """
    df = gaia_frame
    df['teff_err_lower'] = df['teff_val'] - df['teff_percentile_lower']
    df['teff_err_upper'] = df['teff_percentile_upper'] - df['teff_val']
    df['lum_err_lower'] = df['lum_val'] - df['lum_percentile_lower']
    df['lum_err_upper'] = df['lum_percentile_upper'] - df['lum_val']
    return df



def create_database(polar_frame, gaia_frame):
    '''
    joins the polarbase data and the gaia data
    :param polar_frame: the data from polarbase
    :param gaia_frame: the respective gaia data for single sources
    :return: pandas DataFrame with polarbase data where gaia data are available
    '''
    together_frame = pd.merge(polar_frame, gaia_frame, left_on='objet', right_on='input_position', how='outer',
                              suffixes=('_polarbase', '_gaia'))
    return together_frame


"""
In the following is an example for the  usage of this code.
You have to adapt the filename of your objectlist file in filpaths.py
Careful: You probably have to adapt the column name of the identifier below

"""

# You first have to create the objectlists for Gaia

object_frames = glob(filepaths.base_path+filepaths.object_frame+'.*')
if len (object_frames)==1:
    objectframe_filename = object_frames[0]
else:
    objectframe_filename = filepaths.base_path + filepaths.object_frame + '.csv'

df = pd.read_csv(objectframe_filename)

# Adapt the column name of the Identifier
objectlist = df['Identifier']

dictionary_filename = filepaths.base_path+filepaths.object_frame+'_to_gaia.csv'

ol = clean_objectlists(objectlist,dictionary_filename)
# generate_objectlists_for_gaia(ol,filepaths.base_path,extra_name=filepaths.object_frame)

# Now you have to download the gaia params from the website http://gaia.ari.uni-heidelberg.de/singlesource.html
# Afer that you can process the downloaded files which you shuld put into the base_path folder

gf = generate_gaia_frame(filepaths.base_path, columns='column_names_subset.txt')
gf = find_gaia_errors(gf)
dictionary= pd.read_csv(dictionary_filename)
dictionary = dictionary.rename(columns={'object_simbad':'input_position'})

temp = pd.merge(gf,dictionary,on='input_position',how='inner')

data = pd.merge(temp,df,left_on='object',right_on='Identifier')
data.to_csv(filepaths.base_path+filepaths.object_frame+'_params.csv',index=False)

