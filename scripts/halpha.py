import numpy as np
import pandas as pd

# star = input('Star name?')
# id = input('Spectrum ID?')
# folder = '../polar_database/sample_spectra/'
# #id = '798231'
# spec = np.loadtxt(folder+star+'_espadons_2005_pol_RM_V_'+id+'p.s',skiprows=2)
# lam = spec[:,0]
# data = spec[:,1]
# errdata =spec[:,5]

def halpha(lam, data, errdata, velstar):
    lamha = 656.285
    lamv = 655.885
    lamr = 656.730
    deltalamha = .36
    deltalamcont = .22
    vel_light = 299792.5

    # errdata = vector(0, npts - 1)
    # if (n < 4)
    # errdata = data + npts;
    # else errdata = data + 4 * npts;

    fluxha, fluxv, fluxr, = 0,0,0
    weightha, weightv, weightr = 0,0,0

    # printf("stellar velocity?\n")
    # velstar = float(input ("stellar velocity?"))

    deltalam = velstar / vel_light
    lamha *= (1. + deltalam)
    lamv *= (1. + deltalam)
    lamr *= (1. + deltalam)
    #
    for j in range(len(lam)):
        errdata[j] = 1.
        if ((lam[j] > lamv - deltalamcont / 2.) & (lam[j] < lamv + deltalamcont / 2.)):
            fluxv += data[j] / errdata[j]
            weightv += 1. / errdata[j]
        if ((lam[j] > lamr - deltalamcont / 2.) & (lam[j] < lamr + deltalamcont / 2.)):
            fluxr += data[j] / errdata[j]
            weightr += 1. / errdata[j]

        if ((lam[j] > lamha - deltalamha / 2.) & (lam[j] < lamha + deltalamha / 2.)):
            fluxha += data[j] / errdata[j]
            weightha += 1. / errdata[j]
    #
    fluxv /= weightv
    fluxr /= weightr
    fluxha /= weightha
    return fluxha / (fluxr + fluxv)
#print("Haindex = {}\n".format(fluxha / (fluxv + fluxr)))