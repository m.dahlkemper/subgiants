import os
from glob import glob
import filepaths

workingdir = filepaths.workingdir
writingdir = filepaths.writingdir
datedict = {'1':'jan','2':'feb','3':'mar','4':'apr','5':'may','6':'jun',
            '7':'jul','8':'aug','9':'sep','10':'oct','11':'nov','12':'dec'}

def extract_spectrum_files(starname,error_file=None):
    if os.path.exists(writingdir + starname + '/'):
        os.chdir(writingdir + starname)
    elif os.path.exists(writingdir + starname.lower() + '/'):
        starname = starname.lower()
        os.chdir(writingdir + starname)
    else:
        if error_file is not None:
            error_file.write(f'star {starname} not in directory\n')
        return None,None

    filepath = os.getcwd() + '/'
    outfiles = glob('*.out')
    if len(outfiles) == 0:
        datelist = glob('*/')
        if len(datelist) != 0:
            datelist = [date.split('/')[-2] for date in datelist]
            for date in datelist:
                outfiles += glob(date + '/' + '*.out')
    #spec_files = [filepath + '/' + fname.split('.')[0] + '.s' for fname in outfiles]
    return filepath, outfiles