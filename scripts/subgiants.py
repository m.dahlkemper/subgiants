import numpy as np
import filepaths
import pandas as pd
import matplotlib.pyplot as plt
from classify_stars import create_sgb_grid,find_value
from calculate_indices import calculate_indices
from astropy.constants import G,R_sun,M_sun,L_sun
from plot_hrd import plot_hrd_of_stars, plot_stars_with_grid
G=G.value
R_sun = R_sun.value
M_sun=M_sun.value


def concat_results():
    '''
    concats the results from enhanced parameters, indices and detections.
    :return:
    '''
    parameters = pd.read_csv(filepaths.base_path+filepaths.object_frame+'_enhanced_params.csv')
    indices = pd.read_csv(filepaths.result_dir+filepaths.object_frame+'_indices.csv')
    detections =pd.read_csv(filepaths.result_dir+filepaths.object_frame+'_detections.dat',delim_whitespace=True,header=None)[[0,1,2]].rename(columns={0:'object',1:'date',2:'detection'})
    together = pd.merge(parameters, indices, how='outer', on='object')
    together = pd.merge(together,detections, how='outer', on=['object','date'])
    together['detection'].fillna('unknown', inplace=True)
    return together


def one_detection_per_star(df):
    """
    Condenses the detection data to one one star per detection. If for one star at least one observation gives a definite detection, the star is efined as having a definite detection,
     if there is no observation with a definite but at least one with a marginal detection the star is defined as having a marginal detection.
    :param df: frame with the columns "detection"
    :return: data frame with no duplicates
    """
    star_data = df
    star_data_unique = star_data.drop_duplicates('object')
    definite = star_data[star_data['detection'] == 'DEFINITE'].drop_duplicates('object')[['object','detection']]
    marginal = star_data[star_data['detection'] == 'MARGINAL'].drop_duplicates('object')[['object','detection']]
    no_det = star_data[star_data['detection'] == 'No'].drop_duplicates('object')[['object','detection']]
    unknown = star_data[star_data['detection'] == 'unknown'].drop_duplicates('object')[['object','detection']].rename(columns={'detection':'final_detection'})

    detected = pd.merge(definite,marginal,on='object',how='outer',indicator=True)
    detected.rename(columns={'_merge':'det'},inplace=True)
    detected['det'] = detected['det'].replace('both','DEFINITE').replace('left_only','DEFINITE').replace('right_only','MARGINAL')
    all_stars = pd.merge(detected,no_det,on='object',how='outer',indicator=True)
    all_stars.rename(columns={'_merge':'final_detection'},inplace=True)
    all_stars['final_detection'] = all_stars['final_detection'].replace('right_only','No')
    for i,det in all_stars.iterrows():
        if (det['final_detection'] == 'left_only') or (det['final_detection'] == 'both'):
            all_stars.loc[i,'final_detection'] = det['det']
    stars = pd.concat([all_stars,unknown])
    stars = pd.merge(stars,star_data_unique,on='object',how='left').drop(['detection_x','detection_y'],axis=1)

    return stars


"""
Usage of the above code where an eep frame is already created.
The code finds values for log_g, age and mass and stores an "enhanced params" file
"""

track_frame = pd.read_csv(filepaths.eep_tracks+'eep_frame.csv')
sgb_grid,sgbs,grid_borders = create_sgb_grid(track_frame,masses= np.arange(0.7,2.0,0.2),
                                             extend_below=10,extend_above=4,with_extension=True,lum_max=np.log10(1000),teff_min=np.log10(4000))
star_frame = pd.read_csv(filepaths.base_path+filepaths.object_frame+'_params.csv')

subgiants = find_value(sgb_grid,grid_borders,['log_g','star_age','mass'],star_frame,with_extension=True)

subgiants.to_csv(filepaths.base_path+filepaths.object_frame+'_enhanced_params.csv',index=False)



""" 
the following routine calculates the Ca IRT, S-Index and H alpha index for every star in the object frame.
It therefore calculates a radial velocity if there isa lsd profile
The results are written in a file with the suffix _indices.csv which is stored in the results_dir specified in filepaths.py
"""
calculate_indices()


"""
In the following lines the results are concatenated and condensed. The resulting data frames are also stored in the result_dir
"""
total = concat_results()

total_unique = one_detection_per_star(total)

total.to_csv(filepaths.result_dir+filepaths.object_frame+'_results_total.csv',index=False)
total_unique.to_csv(filepaths.result_dir+filepaths.object_frame+'_unique_results_total.csv',index=False)


"""
The following lines are some examples to make use of the plotting routines from the file plt_hrd.py
"""


# subgiants = find_value(sgb_grid,grid_borders,['log_g','star_age','mass'],df,with_extension=True)
# # subgiants['mass_calc']=(subgiants['radius_val']*R_sun)**2*10**subgiants['log_g']/(G*M_sun)
# subgiants['duplicate'] = subgiants.duplicated(['ra','dec'],keep=False)

# df = df.merge(subgiants[['input_position','subgiant']],on='input_position',how='outer')
# df = df.replace({'subgiant':{np.NaN:False}})
# subgiants.to_csv(folder+'subgiants.csv',index=False)

# fig,ax = plot_stars_with_grid(stars,sgb_grid=sgb_grid, properties=['log_g'],how='points',color_dict=None,color_category=None,with_extension=True,extension_color='black')
# ax.set(title='test')
# #
# fig,ax = plot_stars_with_grid(df[df['data_source']=='SCAM_directory'],sgbs=sgbs,how='tracks',color_dict={True:'r',False:'b'},color_category='subgiant',with_extension=True)
# ax.set(title='SCAM')
#
# fig,ax = plot_stars_with_grid(df[df['data_source']=='Polarbase'],sgbs=sgbs,how='tracks',color_dict={True:'r',False:'b'},color_category='subgiant')
# ax.set(title='Polarbase')
#
# fig,ax = plot_stars_with_grid(df,sgbs=sgbs,how='tracks',color_dict={True:'r',False:'b'},color_category='subgiant',with_extension=True)
#fig,ax = plot_stars_with_grid(df,sgb_grid=sgb_grid,properties=['log_g'],how='points',color_dict={True:'r',False:'b'},color_category='subgiant',with_extension=True)
# ax.set(title='Both datasets')


### plot all stars +Auriere ############################

# stars = one_detection_per_star(filepaths.writingdir+'all_detections.csv')
# stars.to_csv(filepaths.base_path+'all_detections.csv')
# auriere = pd.read_csv(filepaths.base_path+'Auriere_params.csv')[['lum_val','teff_val','det']]
#
#
# fig,ax= plot_stars_with_grid(sgbs=sgbs,how='tracks',with_extension=True)
#
# for track,mass in zip(sgbs,np.arange(0.7,2.0,0.1)):
#     start_index = max(track[track['phase']=='sgb extension'].index)
#     print(start_index)
#     ax.text(10** track.at[start_index,'log_Teff'],10**track.at[start_index,'log_L'],f'{np.round(mass,1)} M_sol')
#
# definite = stars[stars['final_detection']=='DEFINITE']
# marginal = stars[stars['final_detection']=='MARGINAL']
# no = stars[stars['final_detection']=='No']
#
# definite_auriere = auriere[auriere['det']=='DEFINITE']
# marginal_auriere = auriere[auriere['det']=='MARGINAL']
# no_auriere = auriere[auriere['det']=='No']
#
# l1 = ax.scatter(definite['teff'],definite['lum'],c='b',marker='D',label='Definite Detection',zorder=10)
# l2 = ax.scatter(marginal['teff'],marginal['lum'],c='b',marker='v',label='Marginal Detection',zorder=10)
# l3 = ax.scatter(no['teff'],no['lum'],c='b',marker='x',label='No Detection',zorder=10)
#
# l4= ax.scatter(definite_auriere['teff_val'],definite_auriere['lum_val'],marker='D',c= 'r',label='Definite Detection Auriere et al',zorder=10)
# l5 = ax.scatter(marginal_auriere['teff_val'],marginal_auriere['lum_val'],marker='v',c= 'r',label='Marginal Detection Auriere et al',zorder=10)
# l6 = ax.scatter(no_auriere['teff_val'],no_auriere['lum_val'],marker='x',c= 'r',label='No Detection Auriere et al',zorder=10)
# ax.legend((l1,l2,l3,l4,l5,l6),(l1.get_label(),l2.get_label(),l3.get_label(),l4.get_label(),l5.get_label(),l6.get_label()))
# ax.semilogy()
# ax.set_xlim(ax.get_xlim()[::-1])
#
# ######################################
# fig.set_size_inches(10,8)
# plt.savefig(filepaths.base_path+'plots/all_detections.pdf')
# plt.show()

# subgiants[['input_position','duplicate','teff_val','log_g']].to_csv(folder+'Polarbase_extra_subgiants.csv',index=False)
# subgiants.to_csv(folder+'extra_params.csv')
# subgiants[subgiants['data_source']=='SCAM_directory'][['input_position','duplicate','teff_val','log_g']].to_csv(folder+'SCAM_subgiants.csv',index=False)