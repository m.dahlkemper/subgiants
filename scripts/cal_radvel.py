import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from numpy import sqrt, pi, exp, linspace
from scipy.optimize import curve_fit
from lmfit import Model
from glob import glob
import filepaths

############################
# year="2016" ##the directory where the stellar spectra is located. In my case it is called 2018, where all 2018 spectra is located
# id = 798231
# ##the inital amplitude, line centre and width of the gaussian line to be fitted later. centre is key here. If line centre is too off then the script throws as error. So, one needs to provide a good inital estimate.
# guess_amp=.02
# guess_cen=20
# guess_wid=20000
#star = input('Star name?') #name of the star but it is required based on the directory strucutre in dir_path
c = 299792.458 #speed of light
spectra_dir = filepaths.writingdir
lsd_dir = filepaths.lsd_dir


##############################################################################################
###################################Do Not Change anything from here on #######################
##############################################################################################
def files(star):
    """
    returns the filenames in the directory
    """
    dirpath = "/home/mertennikolay/Uni/Tutorenjobs/Sandra/polar_database/sample_spectra/" + star + "/"
    filenames=[]
    for path,dirs,files in os.walk(dirpath):
        for fname in files:
             if fname.endswith(".lsd"):
                 f= fname
                 filenames.append(f)
    if not os.path.exists(dirpath+'velocities.csv'):
        with open(dirpath+'velocities.csv','w') as vel_file:
            vel_file.write('id,radvel\n')
    return dirpath,filenames

def filename_paths(star,id=None):
    """
    returns the full path for the files in the 
    directory
    """
    filepath=[]
    dirpath, filenames = files(star)
    if id is None:
        for i in filenames:
            #filepath[idx] = (line)
            paths= dirpath+i
            filepath.append(paths)
    elif type(id) is int or type(id) is str:
        paths = dirpath + str(id) + '_p.lsd'
        filepath.append(paths)
    else:
        for i in id:
            paths = dirpath+str(i)+'_p.lsd'
            filepath.append(paths)
    return filepath

def lsd_files(starname):
    lsd_files = glob(spectra_dir+starname+'/*lsd')
    if len(lsd_files) >0:
        lsd_file = lsd_files[0]
    else:
        lsd_file = lsd_dir+starname+'_lsd'
        lsd_files = glob(lsd_dir+'*_lsd')
        if lsd_file not in lsd_files:
            lsd_file = lsd_dir+starname.lower()+'_lsd'
        if lsd_file not in lsd_files:
            print(f'No lsd file for star {starname}')
            return ''
    if not os.path.exists(lsd_dir+'velocities.csv'):
        with open(lsd_dir+'velocities.csv','w') as vel_file:
            vel_file.write('star,radvel\n')
    return lsd_file

def load_file(name):
    """
    loads the files
    Returns
    ------
    wavelength 
    StokesI
    """
    if not os.path.exists(name):
        return None
    data = pd.read_csv(name, skiprows=2,header=None,delim_whitespace=True,names=['vel','I'],usecols=[0,1])
    data = data[(data['vel']>=-100) & (data['vel']<=100)]
    return data

def first_guesses(data):
    stokes_min = min(data['I'])
    vel_min = data.at[data['I'].idxmin(), 'vel']
    hm = 1 - (1 - min(data['I'])) / 2
    id_nearest = (np.abs(data['I'] - hm)).idxmin()
    nearest = data.at[id_nearest, 'vel']
    fwhm = np.abs(vel_min-nearest)*2
    return 1-stokes_min,vel_min, fwhm

def best_values():
    """
    loads the best guess amp,cen,wid
    """
    gmod=Model(gaussian)
    return gmod.param_names

def gaussian(x, amp, cen, wid):
    """
    Creates a Gaussian function
    
    """
    return 1.0 - amp * exp(-(x-cen)**2 /wid)


def plot_spectra(vel,flux):
    """
    plots the spectra and the fit to the data
    """
    plt.plot(vel,flux,c="r",linestyle="--")
    plt.xlim(-100,100)
    #plt.show()

def cal_radvel(star,id=None,plot=False):
    """
    calculates radial velocity from lsd file
    :param star: star name
    :param id: spectrum id if  available
    :param plot: boolean, if true, a plot with the best fit is shown
    :return: mean of the velocities of all the available spectra, array of all velocities
    """
    radial_velocity = []

    def calculate(path):
        data = load_file(path)
        if data is None:
            return
        guess_amp, guess_cen, guess_width = first_guesses(data)
        data = data[(data['vel'] >= guess_cen - guess_width) & (data['vel'] <= guess_cen + guess_width)]
        vel = data['vel']
        StokesI = data['I']
        velocity = np.array(vel)
        gmod = Model(gaussian)
        result = gmod.fit(StokesI, x=velocity, amp=guess_amp, cen=guess_cen, wid=guess_width)
        rad_vel = result.best_values['cen']
        radial_velocity.append(rad_vel)
        if plot:
            plt.plot(velocity, StokesI, 'bo-')
            # plt.plot(vel, result.init_fit, 'k--')
            plt.plot(velocity, result.best_fit, 'r-')
            plt.plot(velocity, result.init_fit, 'g-')
            plt.savefig(path + '.png')
        # print(result.fit_report())
        return rad_vel

    if id is not None:
        if type(id) is int or type(id) is str:
            ids = [id]
            for i in ids:
                path = filename_paths(star,i)[0]
                plt.clf()
                #print(i)
                velos = pd.read_csv(os.path.dirname(path)+'/velocities.csv')
                if i in velos['id']:
                    rad_vel = velos[velos['id']==i]['radvel'][0]
                    radial_velocity.append(rad_vel)
                    continue
                rad_vel = calculate(path)
                with open(os.path.dirname(path) + '/velocities.csv', 'a+') as vel_file:
                    vel_file.write(str(id) + ',' + str(rad_vel) + '\n')
    else:
        filename = lsd_files(star)
        if filename == '':
            return False,False
        try:
            rad_vel = calculate(filename)
        except ValueError:
            print(f'No radvel for star {star}')
            return False,False
        with open(os.path.dirname(lsd_dir) + '/velocities.csv', 'a+') as vel_file:
            vel_file.write(str(filename.split('/')[-1].split('_')[0]) + ',' + str(rad_vel) + '\n')

    #mean RV for the given epoch
    # plt.pause(1)

    mean_RV = np.mean(np.array(radial_velocity))
    #individual RVs
    all_RV = np.array(radial_velocity)
    return mean_RV,all_RV

#if __name__=="__main__":
    #print(cal_radvel())
