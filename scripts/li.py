import numpy as np
import matplotlib.pyplot as plt
from specutils import Spectrum1D
import astropy.units as u
from astropy.visualization import quantity_support
from specutils.analysis import equivalent_width
from specutils.analysis import snr
from specutils.spectra import SpectralRegion
from specutils.manipulation import extract_region
quantity_support()

lamHa = 6562.8*u.AA


stars = ['9sgr' '10lac' 'kappadra' 'LambdaAnd' 'HD205073' 'pi2cyg' 'HD162725'
 '15Sgr' 'lambdaAnd' 'HD180868' '31peg' '9sge' 'HD205331' 'lambaand']

id = '794875'
star = 'kappadra'
# star = input('Star?')
# id = input('ID?')
folder = '../polar_database/sample_spectra/'


lamli = 6707.776 * u.AA
deltalamli = 0.2 * u.nm
c = 299792.458 * u.km / u.s
def comp_ew(lam,flux):
    avg_dx = np.diff(lam)
    line_flux = np.sum(flux[1:] * avg_dx)
    dx = lam[-1] - lam[0]
    # continuum = ((flux[0]+flux[-1])/2)
    continuum = np.mean(flux[np.argsort(flux)[-3:]])
    ew = dx - (line_flux / continuum)

    return ew

def comp_region(lam,flux,central,delta):
    left_border = central -delta/2
    right_border = central + delta/2
    left_idx = ((np.abs(lam - left_border)).value).argmin()
    right_idx = ((np.abs(lam - right_border)).value).argmin()
    return lam.value[left_idx:right_idx+1]*lam.unit, flux.value[left_idx:right_idx+1]*flux.unit

def compute_li_ew(lam,flux,vel):
    # spec = np.loadtxt(folder+star+'_espadons_2005_pol_RM_V_'+id+'p.s',skiprows=2)
    lamli = 6707.926 * u.AA
    lamli *= (1+vel/c)
    region_lam, region_flux = comp_region(lam,flux,lamli,deltalamli)
    return comp_ew(region_lam,region_flux)


# spectrum_uncorr = Spectrum1D(spectral_axis=spec[:,0]*u.nm,flux=spec[:,1]*u.Jy)
# spec[:,0]=spec[:,0]*(1-vel/c)
# spectrum = Spectrum1D(spectral_axis=spec[:,0]*u.nm,flux=spec[:,1]*u.Jy)
# spec_reg = SpectralRegion(lamli-1*u.AA,lamli+1*u.AA)
# print(equivalent_width(spectrum))
def plot_spectrum(lam,flux,vel,central=lamli,delta=deltalamli,ew=0):
    lines = plt.plot(lam, flux, label='RV uncorrected')
    # central *= (1 - vel / c)
    # lamli = 6707.926 * u.AA
    # deltalamli = 0.2 * u.nm

    lam /= (1 + vel / c)
    region_lam, region_flux = comp_region(lam, flux, central, delta)
    lines = plt.plot(region_lam,region_flux,label='RV {} {} corrected'.format(np.round(vel.value,2),vel.unit.to_string()))
    plt.legend()
    if ew:
        plt.title('Li EW = {}'.format(np.round(ew,2)))
    plt.show()
# sub_spec = extract_region(spectrum,spec_reg)
# lines = plt.step(sub_spec.spectral_axis,sub_spec.flux)
# lines_uncorr = plt.step(spectrum_uncorr.spectral_axis,spectrum_uncorr.flux,label = 'RV uncorrected')
# plt.ylim(0,1)
# plt.axvline(lamHa)
# plt.xlim(656.*u.nm,656.6*u.nm)
# plt.xlim(lamli-2*u.AA,lamli+2*u.AA)
# plt.legend()
# plt.savefig(folder+id+'_halpha.png')
# plt.show()
# region = SpectralRegion(8*u.nm, 22*u.nm)
# spectrum1 = Spectrum1D(spectral_axis=np.arange(1, 50) * u.nm, flux=np.random.sample(49)*u.Jy)
# sub_spectrum = extract_region(spectrum1, region)
# sub_spectrum = extract_region(spectrum,spec_reg)


