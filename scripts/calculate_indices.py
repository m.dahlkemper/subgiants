from cahk import cahk
from halpha import halpha
import matplotlib.pyplot as plt
from cairt import cairt
from cal_radvel import cal_radvel
from li import compute_li_ew,plot_spectrum
import numpy as np
import astropy.units as u
import pandas as pd
from glob import glob
from folder_structure import extract_spectrum_files, datedict
import os
import filepaths




def load_spectrum(spec_file):
    #id = '798231'
    spec = pd.read_csv(spec_file, delim_whitespace=True, skiprows=2,header=None, names=None)
    if len(spec.columns)<6:
        return spec,False
    spec = spec.rename(columns={0:'lam',1:'data',5:'error'})
    return spec,True




def calculate_indices():
    stars_wo_li = []
    para_dict_list =[]
    dir = filepaths.writingdir

    # specify which for which stars the indices should be calculated
    parameter_frame = pd.read_csv(filepaths.base_path + filepaths.object_frame + '_enhanced_params.csv')

    # specify in which column the identifier of the star is !
    starlist = parameter_frame['object']
    for star in starlist:
        li_ew_arr = np.empty(0)
        radvel,_ = cal_radvel(star,id=None)
        if not radvel:
            continue
        vel = radvel
        filepath,outfiles = extract_spectrum_files(star)
        for file in outfiles:
            if 'narval' in file:
                date = file.split('_')[2]
            elif '/' in file:
                date = file.split('/')[0]
            else:
                if not os.getcwd() == dir + star + '/':
                    os.chdir(dir + star + '/')
                with open(file, 'r') as fh:
                    for i, line in enumerate(fh):
                        if i == 6:
                            (year, month, day) = line.split(':')[1].split('@')[0].strip().split(' ')
                            date = f'{int(day):02d}{datedict[month]}{year}'
            if 'narval' in file:
                instr = 'narval'
            else:
                instr = 'espadons'
            filename = filepath + '/' + file.split('.')[0] + '.s'
            spec,polarised = load_spectrum(filename)
            if not polarised:
                continue
            lam = spec['lam'].values
            data = spec['data'].values
            errdata = spec['error'].values

            # test if non-weighted data change anything?
            # errdata = np.ones(len(lam))

            li_ew = compute_li_ew(lam*u.nm,data*u.Jy,vel*u.km/u.s)
            li_ew_arr = np.append(li_ew_arr,li_ew.value)
            Sindex = cahk(lam,data,errdata, vel,instrument = instr)
            irt = cairt(lam,data,errdata, vel)
            haindex = halpha(lam,data,errdata, vel)
            para_dict={}
            para_dict['object']=star
            para_dict['date']=date
            para_dict['Sindex']=Sindex
            para_dict['haindex']=haindex
            para_dict['irt']=irt
            para_dict['rad_vel']=vel
            para_dict_list.append(para_dict)
            print('Star: {}, Date: {}\nRadial velocity = {}\nLithium EW = {}\nSindex = {}\nCaIRTindex = {}\nH alpha index = {}\n'.format(star,date,vel,li_ew,Sindex,irt,haindex))
        #plot_spectrum(lam * u.nm, data * u.Jy, vel * u.km / u.s, central=6707.926 * u.AA, ew=li_ew)
        li_ew_arr = li_ew_arr * u.nm
        if np.mean(li_ew_arr) < 0.005 * u.nm:
            stars_wo_li.append(star)
    prop_frame = pd.DataFrame(para_dict_list)
    prop_frame['Li']=True
    for i,row in prop_frame.iterrows():
        if row['star'] in stars_wo_li:
            prop_frame.loc[i,'Li']=False
    prop_frame.to_csv(filepaths.result_dir+filepaths.object_frame+'_indices.csv',index=False)
    return prop_frame

