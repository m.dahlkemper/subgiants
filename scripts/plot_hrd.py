import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import filepaths






def plot_hrd_of_stars(df,color_dict=None,color_category=None,fig=None,ax=None):
    """
    routine to plot the stars
    :param df: dataframe with all the stars to be plotted
    :param color_dict: dictionary which value should translate to which color
    :param color_category: what should be color coded (e.g. subgiant or not)
    :param fig: plt.fig object, should be None if new figure is created
    :param ax: axes object, should be None if new figure is created
    :return: figure and axes object
    """
    if fig is None or ax is None:
        fig,ax = plt.subplots()
    if color_dict is not None:
        for name in color_dict:
            indices = df[color_category] == name
            data= df.loc[indices]
            color = color_dict[name]
            ax.errorbar(data['teff_val'],data['lum_val'],xerr= np.asarray(np.transpose(data[['teff_err_lower','teff_err_upper']])),
                        yerr=np.asarray(np.transpose(data[['lum_err_lower','lum_err_upper']])),c = color,label=f'{color_category}={name}',fmt='.')
    else:
        df.plot.scatter('teff_val', 'lum_val',
                          xerr=np.asarray(np.transpose(df[['teff_err_lower', 'teff_err_upper']])),
                          yerr=np.asarray(np.transpose(df[['lum_err_lower', 'lum_err_upper']])), ax=ax)

    ax.set_xlim(ax.get_xlim()[::-1])
    ax.semilogy()
    return fig,ax

def plot_stars_with_grid(star_df=None,properties=None,sgb_grid=None,sgbs=None,how='points',color_dict=None,color_category=None,with_extension = False, extension_color='grey'):
    """
    plots an HRD together with either MIST tracks or a grid of stellar models
    :param star_df: stars which should be plotted in the HRD
    :param properties: if given, an SGB grid where this property is color coded is plotted
    :param sgb_grid: sgb grid to be plotted
    :param sgbs: single model stars
    :param how: either 'points' for single model stars or 'tracks' for several MIST tracks
    :param color_dict: as in plot_hrd_of_stars
    :param color_category: as in plot_hrd_of_stars
    :param with_extension: indicates whether an extension above and below the SGB shoould be plotted
    :param extension_color: indicates which color the extension of the MIST tracks should have. default is grey as the SGB
    :return: fig and ax object
    """
    if how == 'points':
        if with_extension:
            sgb_grid = sgb_grid[(sgb_grid['phase'] == 'sgb')|(sgb_grid['phase'] == 'sgb extension')]
        else:
            sgb_grid = sgb_grid[sgb_grid['phase']=='sgb']
        teff_values = 10 ** (sgb_grid['log_Teff'])
        l_values = 10 ** sgb_grid['log_L']
        for prop in properties:
            fig, ax = plt.subplots()
            ax.set(xlabel='T_eff / K', ylabel='Luminosity / L_sol')
            prop_values = sgb_grid[prop]
            if star_df is not None:
                plot_hrd_of_stars(star_df, color_dict=color_dict,color_category=color_category, fig=fig, ax=ax)
            sgb = ax.scatter(teff_values, l_values, c=prop_values,
                             cmap=cmap, vmin=min(prop_values), vmax=max(prop_values), marker=',', s=3,
                             label='MIST tracks')
            ax.legend()
            fig.colorbar(sgb, ax=ax, label=prop)
        return fig, ax
    elif how=='tracks':
        fig, ax = plt.subplots()
        ax.set(xlabel='T_eff / K',ylabel='Luminosity / L_sol')
        if star_df is not None:
            plot_hrd_of_stars(star_df, color_dict=color_dict,color_category=color_category, fig=fig, ax=ax)
        for track in sgbs:
            sgb = track[track['phase']=='sgb']
            ax.plot(10 ** (sgb['log_Teff']), 10 ** sgb['log_L'], label=None, c='grey')
            if with_extension:
                sgb_extension_indices = track[track['phase']=='sgb extension'].index
                sgb_indices = sgb.index
                below_sgb = track.loc[sgb_extension_indices[0]:sgb_indices[0]]
                above_sgb = track.loc[sgb_indices[-1]:sgb_extension_indices[-1]]
                ax.plot(10 ** (below_sgb['log_Teff']), 10**below_sgb['log_L'],label=None,c=extension_color)
                ax.plot(10 ** (above_sgb['log_Teff']), 10 ** above_sgb['log_L'], label=None, c=extension_color)
        return fig, ax

